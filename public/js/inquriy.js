var currentType = 0; 
var inqueryType = 0;
var current = 0; 
var cityId = 0;
// partys from blade
$( document ).ready(function() {

    $('#oldParty').hide();

    $('#divTender').hide();
    $('#divSLD').hide();
    $('#divGADrawing').hide();
    $('#divBOQ').hide();


    currentType = 0;
    $('input[type=radio][name=partytype]').change(function() {
        if (this.value == 'new') {
            currentType = 0;
            $('#oldPartyId').val('').trigger('change');
            $('#oldParty').hide();
            $('#partyname').prop('readonly', false);
            clearData();
        }else if (this.value == 'old') {
            
            $('#oldPartyId').val('').trigger('change');
            currentType = 1;          
            $('#partyname').prop('readonly', true);
            $('#oldParty').show();
       
        }
    });

    setImageTender();

});

//----------------------old party change-------------------------------------------------
function onOldPartyChange() {


    var oldPartyId = $('#oldPartyId').val();
    clearData();
    if(oldPartyId == ""){
        return;
    }else{
        for (let i = 0; i < partys.length; i++) {
            if(partys[i].inquirypartymasterid == oldPartyId){
                
                setData(partys[i]);
                break;
            }
        }
    
    }
}

//----------------------Clear Data From Filed-------------------------------------------------
function clearData() {
    $('#partyname').val('');
    $('#contactperson').val('');
    $('#mobile1').val('');
    $('#email1').val('');

    $('#address1').val('');
    $('#address2').val('');
    $('#address3').val('');

    $('#deliveryAddressLine1').val('');
    $('#deliveryAddressLine2').val('');
    $('#deliveryAddressLine3').val('');
    
    cityId = 0;
    $('#stateid').val('').trigger('change');
    
}

//----------------------Set Data From Filed-------------------------------------------------
function setData(data) {
    $('#partyname').val(data.partyname);
    
    $('#contactperson').val(data.contactperson);
    $('#mobile1').val(data.mobileno);
    $('#email1').val(data.email);

    $('#address1').val(data.mainaddress1);
    $('#address2').val(data.mainaddress2);
    $('#address3').val(data.mainaddress3);

    $('#deliveryAddressLine1').val(data.shipaddress1);
    $('#deliveryAddressLine2').val(data.shipaddress2);
    $('#deliveryAddressLine3').val(data.shipaddress3);

    
    $('#stateid').val(data.stateid).trigger('change');
    cityId =  data.cityid;        
}

//----------------------state Change-------------------------------------------------------
function statesChange(){
    var id = $('#stateid').val();

    states.forEach(element => {
        if(id == element.stateid){
            $('#gstno').val(element.gstcode);
        }
    });
    setCity(id);
}


//----------------------city ajax call for state Change-------------------------------------------------------
function setCity(id){
    var _token = $('input[name="_token"]').val();
    var url = $('#urlGetCity').val();

    $.ajax({
        url:url,
        method:"POST",
        data : {_token:_token,stateid:id},
        success:function(result)
        {
            $('#cityid').empty();
            var html = "";
            if(result){
                
                var html = '<option value="">--Select City--</option>';		
                result.forEach(element => {
                    html = html + '<option value="'+element.cityid+'">'+element.cityname+'</option>';
                });
            }else{
                html = '<option value="">--No city Available--</option>';
            }
            $('#cityid').append(html);

            if(cityId != 0){
                $('#cityid').val(cityId).trigger('change');
            }
            
        },
        dataType:"json"
    });
}

// -------------------------------------open add new source model ------------------------
function  openModel() {
    $('#addMasterModel').modal('show');
}

//------------------------------------ ajax call for add new ajax call-------------------
function addSourceAjax() {
    var name = $('#sourcename').val();
    var url = $('#urlSourceAdd').val();
    var _token = $('input[name="_token"]').val();

    if(name){    
        $.ajax({
            type : 'POST',
            url : url,
            data : {name:name,_token : _token},
            success : function(data){

               if(data.state){
                    updateMasterData(data);
                    $('#addMasterModel').modal('hide');
                    $('#sourcename').val('');
               }else{
                   alert(data.message);
               }
            }
        });
    }else{
        alert("Please Enter "+title);
    }
}

//------------------------------------ update source select2 ----------------------------
function updateMasterData(data) {

        var html = '<option value="">--Select Narration Terms--</option>';
                    
        data.data.forEach(element => {
            html = html+'<option value="'+element.inquerysourceid+'">'+element.inquerysourcename+'</option>';
        });

        $('#sourceid').empty();
        $('#sourceid').append(html);
        $('#sourceid').val(data.id).trigger('change');
}


//------------------------------------ Form Validation -----------------------------------
function onValid() {
    
    if(currentType == 1){
        var partyId =$('#oldPartyId').val();
        if(!partyId){ 
            $('#oldPartyId').focus();
            alert('Please Select Party');
            return false;
        }
    } 
    var partyname = $('#partyname').val();
    if(!partyname){
        $('#partyname').focus();
        alert('Please Enter Party Name');
        return false;
    }

    var contactperson = $('#contactperson').val();
    if(!contactperson){
        $('#contactperson').focus();
        alert('Please Enter Contact Person Name');
        return false;
    }

    // var mobile1 = $('#mobile1').val();
    // if(!mobile1){
    //     $('#mobile1').focus();
    //     alert('Please Enter mobile');
    //     return false;
    // }
    // var email1 = $('#email1').val();
    // if(!email1){
    //     $('#email1').focus();
    //     alert('Please Enter Email');
    //     return false;
    // }
    // var bompanelmasterid = $('#bompanelmasterid').val();
    // if(!bompanelmasterid){
    //     $('#bompanelmasterid').focus();
    //     alert('Please Select Panel');
    //     return false;
    // }
    
    var stateid = $('#stateid').val();
    if(!stateid){
        $('#stateid').focus();
        alert('Please Select State');
        return false;
    }

    var cityid = $('#cityid').val();
    if(!cityid){
        $('#cityid').focus();
        alert('Please Select City');
        return false;
    }
    var data = $('#date').val();
    if(!data){
        $('#date').focus();
        alert('Please Select date');
        return false;
    }

    var sourceid = $('#sourceid').val();
    if(!sourceid){
        $('#sourceid').focus();
        alert('Please Select Source');
        return false;
    }

    // var bompanelmasterid = $('#bompanelmasterid').val();

    // if(!bompanelmasterid || bompanelmasterid.length == 0){
    //     $('#bompanelmasterid').focus();    
    //     alert('Please Select Panel Type');
    //     return false;
    // }

    var inquirytype = $('#inquirytype').val();
    if(!inquirytype){
       /* $('#inquirytype').focus();
        alert('Please Select Type Inquiry');
        return false;*/
    }


    if(inqueryType == 1){
        var tenderDate = $('#tenderDate').val();
        if(!tenderDate){
            /*$('#tenderDate').focus();
            alert('Please Select Tender Date');
            return false;*/
        }
        var tenderNo = $('#tenderNo').val();
        if(!tenderNo){
            /*$('#tenderNo').focus();
            alert('Please Enter Tender No.');
            return false;*/
        }
        var tenderName = $('#tenderName').val();
        if(!tenderName){
            /*$('#tenderName').focus();
            alert('Please Enter Tender Name');
            return false;*/
        }
        var tenderdocument = $('#tenderdocument').val();
        if(!tenderdocument){
            /*$('#tenderdocument').focus();
            alert('Please Select Tender File');
            return false;*/
        }
    }


    if(inqueryType == 2){
        var sldDate = $('#sldDate').val();
        if(!sldDate){
           /* $('#sldDate').focus();
            alert('Please Select SLD Date');
            return false;*/
        }
        var sldNo = $('#sldNo').val();
        if(!sldNo){
            /*$('#sldNo').focus();
            alert('Please Enter SLD No.');
            return false;*/
        }
        var slddocument = $('#slddocument').val();
        if(!slddocument){
           /* $('#slddocument').focus();
            alert('Please Select SLD File');
            return false;*/
        }
    }

    if(inqueryType == 3){
        var gaDate = $('#gaDate').val();
        if(!gaDate){
            /*$('#gaDate').focus();
            alert('Please Select Date');
            return false;*/
        }
        var gaNo = $('#gaNo').val();
        if(!gaNo){
            /*$('#gaNo').focus();
            alert('Please Enter GA Drawing No.');
            return false;*/
        }
        var gadocument = $('#gadocument').val();
        if(!gadocument){
            /*$('#gadocument').focus();
            alert('Please Select GA Drawing File');
            return false;*/
        }
    }
    
    if(inqueryType == 4){
        var BOQNo = $('#BOQNo').val();
        if(!BOQNo){
           /* $('#BOQNo').focus();
            alert('Please Enter BOQ');
            return false;*/
        }
       
        var gadocument = $('#boqdocument').val();
        if(!gadocument){
           /* $('#boqdocument').focus();
            alert('Please Select BOQ Drawing File');
            return false;*/
        }
    }
}

//-----------------------------------onInquiryChange--------------------------------------
function onInquiryChange() {
    var type = $('#inquirytype').val();
    clearAllType(); 
    inqueryType = 0;
    if(type == "Tender"){
        setTender();
        inqueryType = 1;
    }else if(type == "SLD"){
        setSLD();
        inqueryType = 2;
    }else if(type == "GA Drawing"){
        setGADrawing();
        inqueryType = 3;
    }else if(type == "BOQ"){
        setBOQ();
        inqueryType = 4;
    }

}

function setTender() {
    $('#divTender').show();
}

function setSLD() {
    $('#divSLD').show();
}

function setGADrawing() {
    $('#divGADrawing').show();
}

function setBOQ() {
    $('#divBOQ').show();
}

//--------------------------------Clear All Type inquery---------------------
function clearAllType() {
    $('#divTender').hide();
    $('#divSLD').hide();
    $('#divGADrawing').hide();
    $('#divBOQ').hide();

    // clear view document
    $('#divTanderPreview').empty();
    $('#divSLDPreview').empty();
    $('#divGAPreview').empty();
    $('#divBOQPreview').empty();

    //Tender
    $('#tenderDate').val('');
    $('#tenderNo').val('');
    $('#tenderName').val('');
    $('#tenderdocument').val('');

    //SLD
    $('#sldDate').val('');
    $('#sldNo').val('');
    $('#slddocument').val('');

    //GA
    $('#gaDate').val('');
    $('#gaNo').val('');
    $('#gadocument').val('');

    //BOQ
    $('#BOQNo').val('');
    $('#boqdocument').val('');

}

//-----------------------------Tender---------------------------------------
function onUploadFileInTender() {
   $('#divTanderPreview').empty();
    var out =  document.getElementById('tenderdocument');
    for (let i = 0; i < out.files.length; i++) {    
        if(IsImage('tenderdocument',i)){
             readURLTender(i,"#divTanderPreview","tenderdocument");
        }else if(IsPDF('tenderdocument',i)){
            readPDF(i,"#divTanderPreview","tenderdocument");
        }else{
            console.log("is not image/pdf");
        }
    }
}

// ----------------------------- SLD ---------------------------------------
function onUploadFileInSLD() {
    $('#divSLDPreview').empty();
     var out =  document.getElementById('slddocument');
     for (let i = 0; i < out.files.length; i++) {    
         if(IsImage('slddocument',i)){
              readURLTender(i,"#divSLDPreview","slddocument");
         }else if(IsPDF('slddocument',i)){
             readPDF(i,"#divSLDPreview","slddocument");
         }else{
             console.log("is not image/pdf");
         }
     }
 
}

// ----------------------------- GA ---------------------------------------
function onUploadFileInGA() {
    $('#divGAPreview').empty();
     var out =  document.getElementById('gadocument');
;
     for (let i = 0; i < out.files.length; i++) {    
         if(IsImage('gadocument',i)){
              readURLTender(i,"#divGAPreview","gadocument");
         }else if(IsPDF('gadocument',i)){
             readPDF(i,"#divGAPreview","gadocument");
         }else{
             console.log("is not image/pdf");
         }
     }
}

// ----------------------------- BOQ ---------------------------------------
function onUploadFileInBAQ() {
    $('#divBOQPreview').empty();
     var out =  document.getElementById('boqdocument');
 
     for (let i = 0; i < out.files.length; i++) {    
         if(IsImage('boqdocument',i)){
              readURLTender(i,"#divBOQPreview","boqdocument");
         }else if(IsPDF('boqdocument',i)){
             readPDF(i,"#divBOQPreview","boqdocument");
         }else{
             console.log("is not image/pdf");
         }
     }
 
}

// ======================================Render================================

//----------------------------------render image-----------------------------
function readURLTender(count,viewId,fileId) {

    var html = '<span class="col-md-6 ImageBorder"><center>'+
               '<img id="theImg'+count+'" src="" height="607" width="440"/>'+
               '</center></span>';

    $(viewId).prepend(html);           
    var out =  document.getElementById(fileId);

    document.getElementById('theImg'+count).src = window.URL.createObjectURL(out.files[count]);
}


//----------------------------------render pdf-----------------------------

var pdfjsLib = window['pdfjs-dist/build/pdf'];

function readPDF(count,viewId,fileId) {
   
    var out =  document.getElementById(fileId);
    var file = out.files[count]
	if(file.type == "application/pdf"){
		var fileReader = new FileReader();  
		fileReader.onload = function() {
            var pdfData = new Uint8Array(this.result);
          
			// Using DocumentInitParameters object to load binary data.
			var loadingTask = pdfjsLib.getDocument({data: pdfData});
			loadingTask.promise.then(function(pdf) {
		
			  // Fetch the first page
              var totalPage = loadingTask._transport._numPages;
              
              for (let k = totalPage; k > 0; k--) {
                var html = '<canvas id="pdfViewer'+count+''+k+'" class="col-md-6 ImageBorder"></canvas>';
                $(viewId).prepend(html);
                var pageNumber = k;
                pdf.getPage(pageNumber).then(function(page) {
 
				
                    var scale = 1.5;
                    var viewport = page.getViewport({scale: scale});
    
                    // Prepare canvas using PDF page dimensions
                    var canvas = $("#pdfViewer"+count+''+k)[0];
                    var context = canvas.getContext('2d');
                    canvas.height = viewport.height;
                    canvas.width = viewport.width;
    
                    // Render PDF page into canvas context
                    var renderContext = {
                      canvasContext: context,
                      viewport: viewport
                    };
                    var renderTask = page.render(renderContext);
                    renderTask.promise.then(function () {
        
                    });
                  });
              }

			  
			}, function (reason) {
			  // PDF loading error
			  console.error(reason);
			});
		};
		fileReader.readAsArrayBuffer(file);
	}

}

//------------------------------------check type------------------------------
function IsImage(id,count) 
{
    
    var allowedExtension = ['jpeg', 'jpg','png'];
    var fileExtension = document.getElementById(id);
    var type = fileExtension.files[count].type;
    type =  type.split('/');
  
    var isValidFile = false;

        for(var index in allowedExtension) {

            if(type[1] === allowedExtension[index]) {
                isValidFile = true; 
                break;
            }
        }

        return isValidFile;
}

//------------------------------------check pdf------------------------------
function IsPDF(id,count) 
{
    var allowedExtension = ['pdf'];
    var fileExtension = document.getElementById(id);
    var type = fileExtension.files[count].type;
    type =  type.split('/');
  
    var isValidFile = false;

        for(var index in allowedExtension) {

            if(type[1] === allowedExtension[index]) {
                isValidFile = true; 
                break;
            }
        }

        return isValidFile;
}

