function onUploadFileInTender() {
    $('#divTanderPreview').empty();
     var out =  document.getElementById('tenderdocument');
     for (let i = 0; i < out.files.length; i++) {    
         if(IsImage('tenderdocument',i)){
              readURLTender(i,"#divTanderPreview","tenderdocument");
         }else if(IsPDF('tenderdocument',i)){
             readPDF(i,"#divTanderPreview","tenderdocument");
         }else{
             console.log("is not image/pdf");
         }
     }
 }
 function  openModel() {
    $('#addMasterModel').modal('show');
}
function addSourceAjax() {
    var name = $('#sourcename').val();
    var url = $('#urlSourceAdd').val();
    var _token = $('input[name="_token"]').val();

    if(name){    
        $.ajax({
            type : 'POST',
            url : url,
            data : {name:name,_token : _token},
            success : function(data){

               if(data.state){
                    updateMasterData(data);
                    $('#addMasterModel').modal('hide');
                    $('#sourcename').val('');
               }else{
                   alert(data.message);
               }
            }
        });
    }else{
        alert("Please Enter "+title);
    }
}

//------------------------------------ update source select2 ----------------------------
function updateMasterData(data) {

        var html = '<option value="">--Select Narration Terms--</option>';
                    
        data.data.forEach(element => {
            html = html+'<option value="'+element.inquerysourceid+'">'+element.inquerysourcename+'</option>';
        });

        $('#sourceid').empty();
        $('#sourceid').append(html);
        $('#sourceid').val(data.id).trigger('change');
}
