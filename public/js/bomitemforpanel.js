//feeders from blade
var currentType;
var currentClickTableId;
var currentCompartmentId;
var productList = [];
var gstArray = [];  
var count = 0;
var accessCount = 0;
var cumulativeAmount;
var OtherArray = ['OT']; //Other Compartment
var OtherArrayName = ['OTHER'];
var gstDefault = 0;
var gstDefaultId = 0;

var laborPer =Number($('#laborpername').val());
 $('#laborper').val(laborPer);

var profitPer = Number($('#profitpername').val());
$('#profitPer').val(profitPer);


/**********for feeder total*************** */
var compfinalarray=[];
var globaldiscountarray=[];

/**************End for feeder total******************** */

// $('#profitpername').on('change',function(){
   
//     updateBillTotal();
// });
$( document ).ready(function() {
    $('#divLoading').show();
    $('#selectByGroup').hide();    
    currentType = 0;
    $(window).keydown(function(event){
        if(event.keyCode == 13) {
          event.preventDefault();
          return false;
        }
      });
    
    $('input[type=radio][name=addproductType]').change(function() {
        if (this.value == 'Specific') {
            currentType = 0;
            setSpecific();
        }else if (this.value == 'Genral') {
            currentType = 1;
            setGenral();
        }else if (this.value == 'ProductGroup') {
            currentType = 2;
            setProductGroup();
        }
    });


   for (let i = 0; i < feeders.length; i++) {
       if(feeders[i].feederid != "-1" ){
        for (let j = 0; j < feeders[i].compatments.length; j++) {
            var temp = {
                 feederid : feeders[i].compatments[j].feederid,
                 bompanelcominfoid : feeders[i].compatments[j].bompanelcominfoid,
                 compartmentname : feeders[i].compatments[j].compartmentname,
                 compartmentseqence : feeders[i].compatments[j].compartmentseqence,
                 products : []
            }
            productList.push(temp);
         }
       }
       
   }
 
//    for (let k = 0; k < OtherArray.length; k++) {
//             var temp = {
//                 feederid : 0,
//                 bompanelcominfoid : 'OTCOMPARTMENT',
//                 compartmentname : OtherArrayName[k],
//                 compartmentseqence : OtherArray[k],
//                 products : []
//         }
//         productList.push(temp);
//    }


   
   gstDefault = bom.bomgstname;
   gstDefaultId = bom.gstmasterid;


   
   $('#defultGst').val(gstDefault);
   $('#defultGstId').val(gstDefaultId);

   
   // on first focus (bubbles up to document), open the menu
    $(document).on('focus', '.select2-selection.select2-selection--single', function (e) {
        $(this).closest(".select2-container").siblings('select:enabled').select2('open');
    });
  
    // steal focus during close - only capture once and stop propogation
    $('select.select2').on('select2:closing', function (e) {
        $(e.target).data("select2").$selection.one('focus focusin', function (e) {
            e.stopPropagation();
        });
    });

    setDefulatData();
    feedertotal();
    changefeedertotal();


});

//================================== set Defulate data=========================================

var tempProduct = [];
// setup defulate data
function setDefulatData() {

    tempProduct = [];
    
    
    for (let i = 0; i < feeders.length; i++) {
       
        for (let j = 0; j < feeders[i].compatments.length; j++) {
  
            var products = feeders[i].compatments[j].products;
            for (let k = 0; k < products.length; k++) {
                tempProduct.push(products[k]);       
             }
            
        }
        
    }

    if(tempProduct.length != 0){
        
         setProduct();
    }


    $('#divLoading').hide();
}


function setProduct() {
    for (let i = 0; i < tempProduct.length; i++) {
   
        var gstPer = gstDefault; 
        
        var result =  cal(tempProduct[i].bompanelproductqty,tempProduct[i].purchaserate,tempProduct[i].productdiscount,gstPer);
        var accessories = tempProduct[i].accessories;
        var compartmentSeq = tempProduct[i].bompanelcomartmentseq;
        var compartmentSId = tempProduct[i].bompanelcominfoid;

        if(compartmentSId == "-1"){
            compartmentSId = "OTCOMPARTMENT";
            compartmentSeq  = "OT";
        }

        addRowProductForSetDefulat(result,tempProduct[i],compartmentSeq,compartmentSId,gstPer,accessories);
          
        
    }
}

function addRowProductForSetDefulat(result,data,compartmentSeq,compartmentSId,gstPer,accessories) {
    cumulativeAmount = 0;
    var type = data.productcategory;
    
    if(type == "general"){
        var mainName = data.productname;
    }else if(type == "specific"){
        var mainName = data.subnameofproduct;
    }

    var panelproductdesc= data.bompanelproductdesc;
  
    var makename = data.makename;
    if(!makename){
        makename = "-";
    }
    var mainId = data.id;
    var mainQty = result.qty;
    var mainHSN = "-";
    if(data.hsnno){
        mainHSN = data.hsnno;
    }

    var mainCatNo = "-";
    if(data.productcode){
        mainCatNo = data.productcode;
    }
    var mainUnitId = data.unitid;
    var mainUnitText = "";
    var mainsalerate = parseFloat(result.rate).toFixed(2);
    var mainDis = result.discountper;
    var mainDisAmount = result.discountamount;
    var mainGstid = gstDefaultId;
    var mainGst = gstPer+"%"; 
    var mainGSTAmount =result.gstamount;
    var mainAmount =result.amount;
      
    var mainTotalAmount = result.total;     

    units.forEach(element => {
        if(element.productunitid == mainUnitId){
            mainUnitText = element.productunitname;
        }
    });

    // selctedResult.push(temp);
    count++;

     //push array
     var temp = {
        count : count,
        productid : data.id,
        productname : mainName,
        makename : makename,
        hsn : mainHSN,
        catno :mainCatNo,
        unitid : mainUnitId,
        salerate : mainsalerate,
        discount : mainDis,
        gstid : mainGstid,
        gstname : mainGst,
        qty : mainQty,
        accessories : []
    }
    for (let i = 0; i < productList.length; i++) {  
        if(productList[i].compartmentseqence == compartmentSeq){
            productList[i].products.push(temp);
        }
    }

    var showProductQty = "'"+"#showProductQty"+"'";
    var showProductDisPer = "'"+"#showProductDisPer"+"'";
    var showProductRate =   "'"+"#showProductRate"+"'";
    //PRODUCT
    var record =    '<tr id="tr'+count+'">'+          
                    '<input type="hidden" id="pcont'+count+'" name="pcont'+count+'" value="'+count+'">'+                      
                    '<input type="hidden" id="pid'+count+'" name="pid'+count+'" value="'+mainId+'">'+
                    '<input type="hidden" id="pname'+count+'" name="pname'+count+'" value="'+mainName+'">'+
                    '<input type="hidden" id="pmake'+count+'" name="pmake'+count+'" value="'+makename+'">'+
                    '<input type="hidden" id="ppanelcompartmentid'+count+'" name="ppanelcompartmentid'+count+'" value="'+compartmentSId+'">'+
                    '<input type="hidden" id="pcompartmentseq'+count+'" name="pcompartmentseq'+count+'" value="'+compartmentSeq+'">'+
                    '<input type="hidden" id="phsn'+count+'" name="phsn'+count+'" value="'+mainHSN+'">'+
                    '<input type="hidden" id="pcatno'+count+'" name="pcatno'+count+'" value="'+mainCatNo+'">'+
                    '<input type="hidden" id="pqty'+count+'" name="pqty'+count+'" value="'+result.qty+'">'+
                    '<input type="hidden" id="uid'+count+'" name="uid'+count+'" value="'+mainUnitId+'">'+
                    '<input type="hidden" id="unitText'+count+'" name="unitText'+count+'" value="'+mainUnitText+'">'+
                    '<input type="hidden" id="salerate'+count+'" name="salerate'+count+'" value="'+mainsalerate+'">'+
                    '<input type="hidden" id="discount'+count+'" name="discount'+count+'" value="'+mainDis+'">'+
                    '<input type="hidden" id="discountAmount'+count+'" name="discountAmount'+count+'" value="'+mainDisAmount+'">'+
                    '<input type="hidden" id="gstid'+count+'" name="gstid'+count+'" value="'+mainGstid+'">'+
                    '<input type="hidden" id="gstname'+count+'" name="gstname'+count+'" value="'+mainGst+'">'+
                    '<input type="hidden" id="gstamount'+count+'" name="gstamount'+count+'" value="'+mainGSTAmount+'">'+
                    '<input type="hidden" id="amount'+count+'" name="amount'+count+'" value="'+mainTotalAmount+'">'+
                    '<input type="hidden" id="cumulativeAmountId'+count+'" name="cumulativeAmountId'+count+'" value="">'+
                    '<input type="hidden" id="numberofaccessories'+count+'" name="numberofaccessories'+count+'" value="">'+
                    '<td style="width:40px; border: 1px solid black; text-align: center;"> <button type="button" id="addproductnaration'+count+'" onclick="addproductnaration('+count+','+compartmentSeq+')"><i class="fa fa-plus"></i></button> &#9670; </td>'+
                    '<td style="width:220px; border: 1px solid black;"  onclick="replaceproduct('+count+','+mainId+')">'+mainName+'</td>'+
                    '<td style="width:80px; border: 1px solid black; text-align: center;" >'+mainCatNo+'</td>'+
                    '<td style="width:40px; border: 1px solid black; text-align: center;">'+
                    '<input class="input-fix" type="number" min="0" step="any"  id="showProductQty'+count+'" value="'+mainQty+'" onchange="rowProductCal('+count+','+showProductQty+')" onfocus="getProductRowOldValueForCal('+count+','+showProductQty+')">'+
                    '</td>'+
                    '<td style="width:40px; border: 1px solid black; text-align: center;" >'+makename+'</td>'+
                    '<td style="width:40px; border: 1px solid black; text-align: center;">'+
                    '<input class="input-fix" type="number" min="0" step="any"  id="showProductRate'+count+'" value="'+mainsalerate+'" onchange="rowProductCal('+count+','+showProductRate+')" onfocus="getProductRowOldValueForCal('+count+','+showProductRate+')">'+
                    '</td>'+
                    '<td style="width:40px; border: 1px solid black; text-align: center;">'+
                    '<input class="input-fix" type="number" min="0" step="any"  max="100" id="showProductDisPer'+count+'" value="'+mainDis+'" onchange="rowProductCal('+count+','+showProductDisPer+')" onfocus="getProductRowOldValueForCal('+count+','+showProductDisPer+')">'+
                    '</td>'+
                    '<td style="width:40px; border: 1px solid black; text-align: center;" class="fedprod" id="showProductTotalAmount'+count+'">'+mainTotalAmount+'</td>'+
                    '<td style="width:5px; border: 1px solid black; text-align: center;" onclick="removeProduct('+count+','+mainId+')"><i class="fas fa-times" style="color: red;"></i></td>'+  
                    '</tr>';
                    if(panelproductdesc){
                        record+= '<tr id="ntr'+count+'"><td colspan="8" style="border:1px solid black;"><textarea cols="80" rows="4"name="bompanelproductdesc'+count+'">'+panelproductdesc+'</textarea></td><td style="border: 1px solid black; text-align: center;" onclick="removeProductNarration('+compartmentSeq+','+count+')"><i class="fas fa-times" style="color: red;"></i></td></tr>'
                    }
    
    $('#compartmenttable'+compartmentSeq).append(record);

    $('#numberOfProductInBill').val(count);
    var totalLength =accessories.length;

    if(totalLength == 0){
        totalLength = 0;
        updateBillTotal();
        allClear();
    }
    
    cumulativeAmount =Number(parseFloat(mainTotalAmount).toFixed(2)) + Number(parseFloat(cumulativeAmount).toFixed(2));  
    $('#cumulativeAmountId'+count).val(parseFloat(cumulativeAmount).toFixed(2));
  
    var numberofaccessories = 0;
    for (var i = 0; i < totalLength; i++) {
        numberofaccessories++;

        var gstPerAcc = gstDefault; 
    

        var result =  cal(accessories[i].bompanelaccproductqty,accessories[i].purchaserate,accessories[i].productdiscount,gstPerAcc);   
        

        if(compartmentSId == "-1"){
            compartmentSId = "OTCOMPARTMENT";
            compartmentSeq  = "OT";
        }


        setAeccorisForDefult(result,accessories[i],compartmentSeq,compartmentSId,gstPerAcc);
    }

    $('#numberofaccessories'+count).val(numberofaccessories);
   

}


function setAeccorisForDefult(result,data,compartmentSeq,compartmentSId,gstPerAcc) {
   
    var totalQty = result.qty;
    var type = data.productcategory;
    var mainName = "";

    if(type == "general"){
        var mainName = data.productname;
    }else if(type == "specific"){
        var mainName = data.subnameofproduct;
    }

    var mainId = data.id;  
    var makename = data.makename;
    if(!makename){
        makename = "-";
    }
    var mainQty = parseFloat(totalQty);
    var mainHSN = "-";
    if(data.hsnno){
        mainHSN = data.hsnno;
    }
    var mainCatNo = "-";
    if(data.productcode){
        mainCatNo = data.productcode;
    }
    var mainUnitId = data.unitid;
    var mainUnitText = "";
    var mainsalerate = result.rate;
    var mainDis = result.discountper;
    var mainDisAmount = result.discountamount;
    var mainGstid = gstDefaultId;
    var mainGst = gstPerAcc+"%";
    var mainGSTAmount = result.gstamount;
    var mainAmount = result.amount;
    var mainTotalAmount =result.total;

    units.forEach(element => {
        if(element.productunitid == mainUnitId){
            mainUnitText = element.productunitname;
        }
    });

    accessCount++;

    var accessQty = "'"+"#accessQty"+"'";
    var accessDisPer = "'"+"#accessDisPer"+"'";
    var accessRate =   "'"+"#accessRate"+"'";
    //ACC
    var record =    '<tr id="atr'+accessCount+'">'+
                    '<input type="hidden" id="aprodctcountno'+accessCount+'" name="aprodctcountno'+accessCount+'" value="'+count+'">'+
                    '<input type="hidden" id="aiproductid'+accessCount+'" name="aiproductd'+accessCount+'" value="'+data.mainproductid+'">'+
                    '<input type="hidden" id="aid'+accessCount+'" name="aid'+accessCount+'" value="'+mainId+'">'+
                    '<input type="hidden" id="apanelcompartmentid'+accessCount+'" name="apanelcompartmentid'+accessCount+'" value="'+compartmentSId+'">'+
                    '<input type="hidden" id="acompartmentseq'+accessCount+'" name="acompartmentseq'+accessCount+'" value="'+compartmentSeq+'">'+
                    '<input type="hidden" id="aname'+accessCount+'" name="apname'+accessCount+'" value="'+mainName+'">'+
                    '<input type="hidden" id="amake'+accessCount+'" name="amake'+accessCount+'" value="'+makename+'">'+
                    '<input type="hidden" id="ahsn'+accessCount+'" name="ahsn'+accessCount+'" value="'+mainHSN+'">'+
                    '<input type="hidden" id="acatno'+accessCount+'" name="acatno'+accessCount+'" value="'+mainCatNo+'">'+
                    '<input type="hidden" id="aqty'+accessCount+'" name="aqty'+accessCount+'" value="'+mainQty+'">'+
                    '<input type="hidden" id="aunitText'+accessCount+'" name="aunitText'+accessCount+'" value="'+mainUnitText+'">'+
                    '<input type="hidden" id="auid'+accessCount+'" name="auid'+accessCount+'" value="'+mainUnitId+'">'+
                    '<input type="hidden" id="asalerate'+accessCount+'" name="asalerate'+accessCount+'" value="'+mainsalerate+'">'+
                    '<input type="hidden" id="adiscount'+accessCount+'" name="adiscount'+accessCount+'" value="'+mainDis+'">'+
                    '<input type="hidden" id="adiscountAmount'+accessCount+'" name="adiscountAmount'+accessCount+'" value="'+mainDisAmount+'">'+
                    '<input type="hidden" id="agstid'+accessCount+'" name="agstid'+accessCount+'" value="'+mainGstid+'">'+
                    '<input type="hidden" id="agstname'+accessCount+'" name="agstname'+accessCount+'" value="'+mainGst+'">'+
                    '<input type="hidden" id="agstamount'+accessCount+'" name="agstamount'+accessCount+'" value="'+mainGSTAmount+'">'+
                    '<input type="hidden" id="aamount'+accessCount+'" name="aamount'+accessCount+'" value="'+mainTotalAmount+'">'+ 
                    '<td style="width:40px; border: 1px solid black; text-align: center;"></td>'+
                    '<td style="border: 1px solid black;"><i style="font-size:12px !important;">'+mainName+'</i></td>'+
                    '<td style="border: 1px solid black; text-align: center;"><i style="font-size:12px !important;">'+mainCatNo+'</i></td>'+
                    '<td style="border: 1px solid black; text-align: center;">'+
                    '<input class="input-fix" style="font-style:italic; font-size:12px !important;" type="number" min="0" step="any"  id="accessQty'+accessCount+'" value="'+mainQty+'" onchange="rowAccCal('+count+','+accessCount+','+accessQty+')" onfocus="getAccRowOldValueForCal('+count+','+accessCount+','+accessQty+')">'+
                    '</td>'+
                    '<td style="border: 1px solid black; text-align: center;"><i style="font-size:12px !important;">'+makename+'</i></td>'+
                    '<td style="border: 1px solid black; text-align: center;">'+
                    '<input class="input-fix" style="font-style:italic; font-size:12px !important;" type="number" min="0" step="any"  id="accessRate'+accessCount+'" value="'+mainsalerate+'" onchange="rowAccCal('+count+','+accessCount+','+accessRate+')" onfocus="getAccRowOldValueForCal('+count+','+accessCount+','+accessRate+')">'+
                    '</td>'+
                    '<td style="border: 1px solid black; text-align: center;">'+
                    '<input class="input-fix" style="font-style:italic; font-size:12px !important;" type="number" min="0" step="any"  id="accessDisPer'+accessCount+'" value="'+mainDis+'" onchange="rowAccCal('+count+','+accessCount+','+accessDisPer+')" onfocus="getAccRowOldValueForCal('+count+','+accessCount+','+accessDisPer+')">'+
                    '</td>'+
                    '<td style="border: 1px solid black; text-align: center;" class="fedacc"><i style="font-size:12px !important;" id="accessTotalAmount'+accessCount+'">'+mainTotalAmount+'</i></td>'+
                    '<td style="border: 1px solid black; text-align: center;" onclick="removeProductAcc('+accessCount+','+mainId+','+data.mainproductid+')"><i class="fas fa-times" style="color: red;"></i></td>'+ 
                    '</tr>';
    $('#compartmenttable'+compartmentSeq).append(record);
    $('#numberOfAccessoriesInBill').val(accessCount);
    cumulativeAmount =Number(parseFloat(mainTotalAmount).toFixed(2)) + Number( parseFloat(cumulativeAmount).toFixed(2));

    $('#cumulativeAmountId'+count).val(parseFloat(cumulativeAmount).toFixed(2));
    updateBillTotal();
    feedertotal();
    changefeedertotal();
  
}



//=============================================================================================




// ------------------------------------- click add product ----------------------------
function addProductIntoCompartment(comSeqence,comfeederid,bompanelcominfoid,type) {
   var groupsType = [];

    if(type == 1){
        if(comSeqence != currentClickTableId){
            allClear();
            for (let i = 0; i < feeders.length; i++) {
           
                if(feeders[i].feederid == comfeederid){
                    var compartments = feeders[i].compatments;
                    for (let k = 0; k < compartments.length; k++) {
                        if(compartments[k].compartmentseqence == comSeqence){
                            groupsType = compartments[k].groupstypes;
                        }
                    }
                }
                
            }
           
            var grpTypeHtml = "";
            if(groupsType.length == 0){
                grpTypeHtml = '<option value="">--No Group Type Available--</option>';
            }else{
                grpTypeHtml = '<option value="">Select Group Type</option>';
            }
            groupsType.forEach(element => {
                grpTypeHtml = grpTypeHtml + '<option value="'+element.grouptypeid+'">'+element.grouptypename+'</option>';
            });
            $('#grouptypeid').empty();
           
             $('#grouptypeid').append(grpTypeHtml);

        }
        currentClickTableId = comSeqence;   
        currentCompartmentId  =  bompanelcominfoid;
        $("#selectProductModal").modal();
    }else{
        var status = $('#checkOther'+comSeqence).prop('checked');

        if(comSeqence != currentClickTableId){
            allClear();
            groupsType = allGroups;

            var grpTypeHtml = "";
            if(groupsType.length == 0){
                grpTypeHtml = '<option value="">--No Group Type Available--</option>';
            }else{
                grpTypeHtml = '<option value="">Select Group Type</option>';
            }
            groupsType.forEach(element => {
                grpTypeHtml = grpTypeHtml + '<option value="'+element.id+'">'+element.grouptypename+'</option>';
            });
            $('#grouptypeid').empty();
            $('#grouptypeid').append(grpTypeHtml);
        }

        currentClickTableId =comSeqence;   
        currentCompartmentId  =  bompanelcominfoid; 
        $("#selectProductModal").modal();
       
      
    }
}

//---------------------------------set specific value in model--------
function setSpecific(){

    $('#selectProduct').show();
    $('#specificProduct').show();
    $('#selectQty').show();
    $('#selectByGroup').hide();
    
    $('#tableListgroupProductAccessories tbody').empty();
    
    $('#grouptypeid').val('').trigger('change');
    $('#productid').empty();
    $('#groupid').empty();
   
    $('#productaccessoriesdiv').hide();
    $('#tableListAccessories tbody').html('');

    $('#simlierproduct').hide();
    $('#tablesimlierproduct tbody').html('');
    
    $('#productgroupaccessoriesdiv').hide();
}

//---------------------------------set genral value in model--------
function setGenral(){

    $('#selectProduct').show();
    $('#selectQty').show();
    $('#selectByGroup').hide();
    $('#specificProduct').hide();
    
    $('#tableListgroupProductAccessories tbody').empty();

    $('#grouptypeid').val('').trigger('change');
    $('#productid').empty();
    $('#groupid').empty();
    genralProductAjax();
   
    $('#productaccessoriesdiv').hide();
    $('#tableListAccessories tbody').html('');

    $('#simlierproduct').hide();
    $('#tablesimlierproduct tbody').html('');
  $('#productgroupaccessoriesdiv').hide();
}

//---------------------------------set Product group value in model--------
function setProductGroup(){

    $('#selectProduct').hide();
    $('#specificProduct').hide();
    $('#selectQty').show();
    $('#selectByGroup').show();

    $('#grouptypeid').val('').trigger('change');
    $('#productid').empty();
    $('#groupid').empty();

    $('#productaccessoriesdiv').hide();
    $('#tableListAccessories tbody').html('');

    $('#simlierproduct').hide();
    $('#tablesimlierproduct tbody').html('');
}

// ---------------------------------genral product--------------------
function genralProductAjax(){
    var url = $('#urlGetGenralProduct').val();
    $('#productaccessoriesdiv').hide(); 
    $('#tableListAccessories tbody').empty();
    $('#productid').empty();
    $.ajax({
        type : 'GET',
        url : url,
        success : function(data){
            $('#productid').append(data);
        }
    });

}

//---------------------company change in specific model---------------- 
function changeCompany(){
    let company = $('#companyid').val();
    $('#grouptypeid').val('').trigger('change');
    $('#productid').empty();
    $('#groupid').empty();
    $('#simlierproduct').hide();
    $('#tablesimlierproduct tbody').empty();

    
    $('#simlierproduct').hide();
    $('#tablesimlierproduct tbody').empty();

    
    $('#productaccessoriesdiv').hide(); 
    $('#tableListAccessories tbody').empty();
   
}

//---------------------group type change in specific model----------------
function groupTypeChange(){
    let grouptypeid = $('#grouptypeid').val();
    var url = $('#urlGetProductGroup').val();
    var _token = $('input[name="_token"]').val();
    let company = $('#companyid').val();

    $('#groupid').empty();
    $('#productid').empty();
    
    $('#simlierproduct').hide();
    $('#tablesimlierproduct tbody').empty();

    
    $('#productaccessoriesdiv').hide(); 
    $('#tableListAccessories tbody').empty();
    
    if(grouptypeid){
        $.ajax({
            type : 'POST',
            url : url,
            data : {productgrouptypeid:grouptypeid, _token : _token},
            success : function(data){
                $('#groupid').append(data);
            }
        });
    }
}

//---------------------group change in specific model----------------
function groupChange(){
    let productsubgroup = $("#groupid").val();
    let productgroup = $('#grouptypeid').val();
    let company = $('#companyid').val();
    var _token = $('input[name="_token"]').val();
    var url = $('#urlGetProduct').val();

    $('#simlierproduct').hide();
    $('#tablesimlierproduct tbody').empty();

    if(company == ""){
        alert("Please Select Make");
        return;
    }

    $('#productaccessoriesdiv').hide(); 
    $('#tableListAccessories tbody').empty();
    $('#productid').empty();
    

    if(productsubgroup){
        $.ajax({
            type : 'GET',
            url : url,
            data : {groupid:productsubgroup, grouptypeid:productgroup, companyid:company, _token : _token},
            success : function(data){
                $('#productid').append(data);
            }
        });
    }
}

var newSelectedProductId;
var mainProductName = "";

//---------------------product change in model----------------
function productChange(){
    newSelectedProductId = "";
    var productid = $('#productid').val();
    newSelectedProductId = productid;
    if(currentType == 0){
        mainProductName = "";
        getMainProduct(productid);
      
    }else if(currentType == 1){
        getAccProduct(productid);
    }

}

//--------------------get main name of product------------------
function getMainProduct(pid) {
    var url = '/getproductajax/'+pid; 
    $.ajax({
        type : 'get',
        url : url,
        success : function(data){
            if(data.length != 0){
            
                mainProductName = data[0].productname;
                getSimlierProduct(pid);
                getAccProduct(pid);
            }
           
        }
    });
    
}

//---------------------Simlier Product Ajax Call ----------------
function getSimlierProduct(productid){
    var selGroup = $("#groupid :selected").text();
    var selCompany =   "-"+$('#companyid :selected').text();
    var selProductName =  mainProductName;
    var _token = $('input[name="_token"]').val();
    var queryName =  selProductName.replace(selGroup,'');
    queryName = queryName.replace(selCompany,'');
    var groupid = $("#groupid").val();
 
    $('#simlierproduct').hide();
    $('#tablesimlierproduct tbody').html('');
 
    var url = $('#urlgetSimilerProduct').val();
    $.ajax({
         type : 'POST',
         url : url,
         data : {productName:queryName,groupid:groupid, _token : _token},
         success : function(data){
           
             arraySort(data,productid,queryName);
             
         }
     });
    
}

function arraySort(data,productid,queryName) {
    
    var sortData = [];
    data.forEach(element => {
        // refiltering a url
        
        var temp = {
            id : element.id,
            name : element.subnameofproduct,
            sellrate : element.purchaserate,
            makename : element.makename,
            stock : element.stockqty,
            discount : element.productdiscount,
            amount : discountCal(element.purchaserate,element.productdiscount)
        }
        //console.log(element.purchaserate,element.productdiscount,temp.amount);
        sortData.push(temp);    
    });

    var newArray = sortData.sort(function(a,b) {
        return a.amount -b.amount;
    });

    setSimlierProduct(newArray,productid);
}

//---------------------Set Simlier Product in model table ----------------
function setSimlierProduct(data,productid){

    $('#simlierproduct').show();
    $('#tablesimlierproduct tbody').html('');
    var selectProductrate = 0;
    data.forEach(element => {
        if(element.id == productid){
            selectProductrate  = element.amount;
        }
    }); 

    var html ="";
    data.forEach(element => {

        if(element.id == productid){
        html = html +
                    '<tr>'+
                    '<td>'+element.name+'</td>'+
                    '<td>'+element.makename+'</td>'+
                    '<td>'+element.sellrate+'</td>'+
                    '<td>'+element.stock+'</td>'+                            
                    '<td>'+element.discount+'</td>'+
                    '<td>'+element.amount+'</td>'+
                    '<td><input type="radio" onchange="changeSelctedProduct('+element.id+')" name="selctedProduct" value="'+element.id+'" checked>'+
                    '</td>'+
                '</tr>';
        }else{
            if(Number(element.amount) > Number(selectProductrate)){
                html = html +
                    '<tr style="color:red !important; ">'+
                        '<td>'+element.name+'</td>'+
                        '<td>'+element.makename+'</td>'+
                        '<td>'+element.sellrate+'</td>'+
                        '<td>'+element.stock+'</td>'+
                        '<td>'+element.discount+'</td>'+
                        '<td>'+element.amount+'</td>'+
                        '<td><input type="radio" onchange="changeSelctedProduct('+element.id+')" name="selctedProduct" value="'+element.id+'" >'+
                        '</td>'+
                    '</tr>';
            }else if(Number(element.amount) < Number(selectProductrate)){
             
                html = html +
                    '<tr  style="color:green !important;">'+
                        '<td>'+element.name+'</td>'+
                        '<td>'+element.makename+'</td>'+
                        '<td>'+element.sellrate+'</td>'+
                        '<td>'+element.stock+'</td>'+
                        '<td>'+element.discount+'</td>'+
                        '<td>'+element.amount+'</td>'+
                        '<td><input type="radio" onchange="changeSelctedProduct('+element.id+')" name="selctedProduct" value="'+element.id+'" >'+
                        '</td>'+
                    '</tr>';
            }else if(Number(element.amount) == Number(selectProductrate)){
                html = html +
                    '<tr>'+
                        '<td>'+element.name+'</td>'+
                        '<td>'+element.makename+'</td>'+
                        '<td>'+element.sellrate+'</td>'+
                        '<td>'+element.stock+'</td>'+                            
                        '<td>'+element.discount+'</td>'+
                        '<td>'+element.amount+'</td>'+
                        '<td><input type="radio" onchange="changeSelctedProduct('+element.id+')" name="selctedProduct" value="'+element.id+'" >'+
                        '</td>'+
                    '</tr>';
            }
        }


        

    });

    $('#tablesimlierproduct').append(html);
}

function  discountCal(rate,discount) {
    var temp = (parseFloat(rate)*parseFloat(discount))/100;    
    var result=  Number(parseFloat(rate)) - Number(parseFloat(temp));
     return parseFloat(result).toFixed(2);
 }

 //----------------------------change similer product --------------------
function changeSelctedProduct(id){
    newSelectedProductId = id;
    getAccProduct(id);
}

//----------------------------Product Acc--------------------
function getAccProduct(productid){
    var urlaccessories = $('#urlaccessories').val();
    var _token = $('input[name="_token"]').val();

    $('#productaccessoriesdiv').hide();
    $('#tableListAccessories tbody').html('');

    //clear replace product
    $('#tableListReplaceAccessories tbody').html('');
   
    if(productid){
        
        $.ajax({
            type : 'POST',
            url : urlaccessories,
            data : {productid:productid, _token : _token},
            success : function(data){
                $('#productaccessoriesdiv').show();
                $('#tableListAccessories tbody').append(data);
            }
        });
    }
}

function showAllChangeInAccs() {

    if ($('#selectAllAcc').prop("checked") == true){

        var totalLength=$('#tableListAccessories').find('input[name="accessoriescheckbox[]"]').length;

        if(totalLength == 0){
            totalLength = $('#tableListReplaceAccessories').find('input[name="accessoriescheckbox[]"]').length;
        }
        for (var i = 1; i <= totalLength; i++) {
            var tempId = "#accessoriesadd"+i;
            $(tempId).prop("checked", true);
        }
    } else{
        var totalLength=$('#tableListAccessories').find('input[name="accessoriescheckbox[]"]').length;
        if(totalLength == 0){
            totalLength = $('#tableListReplaceAccessories').find('input[name="accessoriescheckbox[]"]').length;
        }
        for (var i = 1; i <= totalLength; i++) {
            var tempId = "#accessoriesadd"+i;
            $(tempId).prop("checked", false);
        }

    }
}

// ------------------------------- change acc table qty--------------------
function changeqty(pcount){
    var id='#tdqty'+pcount;
    let $this = $(id);
    let qty = $('#tdqty'+pcount).text();
    let $input = $('<input>', {
        type: 'number',
        blur : function(){
            $this.text($(this).val());
            if(($(this).val()).trim() == "" || $(this).val() == null){
               $this.text(qty);
            }
        },
        keyup : function(e){
            if (e.which === 13) $input.blur();
        }
    }).appendTo( $this.empty() ).focus()
}

//----------------------------------Product Change------------------------
function productGroupChange(){

    var productgrpid = $('#productgroupid').val();
    var producturl = '/getproductgrp/'+productgrpid;

    $('#tableListgroupProductAccessories tbody').empty();

    if(productgrpid){

        $.ajax({

            type : 'get',
            url : producturl, 
            success : function(productgrp){

                $('#productgroupaccessoriesdiv').show();

                $('#tableListgroupProductAccessories tbody').append(productgrp);
            }
        });
    }
}


//---------------------------------------- Clear Model----------------
function allClear(){  
    if(currentType == 0){
        $("#radioSpecific").prop("checked", true);
        setSpecific();
    }else if(currentType == 1){
        $("#radioGenral").prop("checked", true);
        setGenral();
    }else if(currentType == 2){
        $("#radioProductGroup").prop("checked", true);
        setProductGroup();
    }
    $('#companyid').val('').trigger('change');
    $('#tableListgroupProductAccessories tbody').empty();
    $('#productqty').val(1);

    
    $('#simlierproduct').hide();
    $('#tablesimlierproduct tbody').empty();
    feedertotal();
    changefeedertotal();
}

//---------------------------add product info verification-------------
function addProductInTable(){

    if(currentType == 0 || currentType == 1){

        var qty = ($("#productqty").val()).trim();
        var id = newSelectedProductId;
        
        if(id == "" || !id){
            alert("Please Select Product");
            return false;   
        }
        
        if(qty == "" || !qty){
         
            $('#productqty_error').show();
            return false;   
        }else{
           
            $('#productqty_error').hide();
        }

        //check a product already list in this particluar compartment table
        for (let i = 0; i < productList.length; i++) {
           
            if(productList[i].compartmentseqence == currentClickTableId){

                for (let k = 0; k < productList[i].products.length; k++) {
                    if(productList[i].products[k].productid == id){
                        alert("This Product Already Lisi in "+ productList[i].compartmentname +" Compartment.");
                        return false;
                    }
                }
            }
        }

        $('#selectProductModal').modal('hide');
        ajaxCall(id,qty);
       
    }else if(currentType == 2){
       var id = $('#productgroupid').val();

       if(id == "" || !id){
        alert("Please Select Product Group");
        return false;   
        }

        appProductGroupIntoList();
        $('#selectProductModal').modal('hide');
    }
}

var selectData;
var selectPId;
var selectQty;
var selectSaleRate;
var selctgstPer;
var selctgstAmount;
var selctdecPer;
var selctdecAmount; 
var selctedTotal;

//Product Ajax Call
function ajaxCall(id,qty){ 
    var url = '/getproductajax/'+id; 
    selectData = "";
    selectPId = id;
    $.ajax({
        type : 'get',
        url : url,
        success : function(data){
            addRowProduct(data[0],qty);
        }
    });
}

// ---------------------------- Add Product info List -----------------------------------
function addRowProduct(data, qty){ 

    var gstName = gstDefault;
   
 
    var result = cal(qty,data.purchaserate,data.productdiscount,gstName);

  
    cumulativeAmount = 0;
    var type = data.productcategory;
    
    if(type == "general"){
        var mainName = data.productname;
    }else if(type == "specific"){
        var mainName = data.subnameofproduct;
    }

    var makename = data.makename;
    if(!makename){
        makename = "-";
    }
    var mainId = data.pid;
    var mainQty = qty;
    var mainHSN = "-";
    if(data.hsnno){
        mainHSN = data.hsnno;
    }

    var mainCatNo = "-";
    if(data.productcode){
        mainCatNo = data.productcode;
    }
    var mainUnitId = data.unitid;
    var mainUnitText = "";
    var mainsalerate = result.rate;
    var mainDis = result.discountper;
    var mainDisAmount = result.discountamount;
    var mainGstid = gstDefaultId;
    var mainGst = gstName+"%"; 
    var mainGSTAmount = result.gstamount;
    var mainAmount =result.amount;
    var mainTotalAmount =result.total;       
 
   

    // selctedResult.push(temp);
    count++;

    //push array
    var temp = {
        count : count,
        productid : data.pid,
        productname : mainName,
        makename : makename,
        hsn : mainHSN,
        catno :mainCatNo,
        unitid : mainUnitId,
        salerate : mainsalerate,
        discount : mainDis,
        gstid : mainGstid,
        gstname : mainGst,
        qty : mainQty,
        accessories : []
    }
   
    for (let i = 0; i < productList.length; i++) {
           
        if(productList[i].compartmentseqence == currentClickTableId){
            productList[i].products.push(temp);
        }
    }

   

   
    
    units.forEach(element => {
        if(element.productunitid == mainUnitId){
            mainUnitText = element.productunitname;
        }
    });
    
    

   
    var showProductQty = "'"+"#showProductQty"+"'";
    var showProductDisPer = "'"+"#showProductDisPer"+"'";
    var showProductRate =   "'"+"#showProductRate"+"'";
    //PRODUCT
    var record =    '<tr id="tr'+count+'">'+          
                    '<input type="hidden" id="pcont'+count+'" name="pcont'+count+'" value="'+count+'">'+                      
                    '<input type="hidden" id="pid'+count+'" name="pid'+count+'" value="'+mainId+'">'+
                    '<input type="hidden" id="pname'+count+'" name="pname'+count+'" value="'+mainName+'">'+
                    '<input type="hidden" id="pmake'+count+'" name="pmake'+count+'" value="'+makename+'">'+
                    '<input type="hidden" id="ppanelcompartmentid'+count+'" name="ppanelcompartmentid'+count+'" value="'+currentCompartmentId+'">'+
                    '<input type="hidden" id="pcompartmentseq'+count+'" name="pcompartmentseq'+count+'" value="'+currentClickTableId+'">'+
                    '<input type="hidden" id="phsn'+count+'" name="phsn'+count+'" value="'+mainHSN+'">'+
                    '<input type="hidden" id="pcatno'+count+'" name="pcatno'+count+'" value="'+mainCatNo+'">'+
                    '<input type="hidden" id="pqty'+count+'" name="pqty'+count+'" value="'+qty+'">'+
                    '<input type="hidden" id="uid'+count+'" name="uid'+count+'" value="'+mainUnitId+'">'+
                    '<input type="hidden" id="unitText'+count+'" name="unitText'+count+'" value="'+mainUnitText+'">'+
                    '<input type="hidden" id="salerate'+count+'" name="salerate'+count+'" value="'+mainsalerate+'">'+
                    '<input type="hidden" id="discount'+count+'" name="discount'+count+'" value="'+mainDis+'">'+
                    '<input type="hidden" id="discountAmount'+count+'" name="discountAmount'+count+'" value="'+mainDisAmount+'">'+
                    '<input type="hidden" id="gstid'+count+'" name="gstid'+count+'" value="'+mainGstid+'">'+
                    '<input type="hidden" id="gstname'+count+'" name="gstname'+count+'" value="'+mainGst+'">'+
                    '<input type="hidden" id="gstamount'+count+'" name="gstamount'+count+'" value="'+mainGSTAmount+'">'+
                    '<input type="hidden" id="amount'+count+'" name="amount'+count+'" value="'+mainTotalAmount+'">'+
                    '<input type="hidden" id="cumulativeAmountId'+count+'" name="cumulativeAmountId'+count+'" value="">'+
                    '<input type="hidden" id="numberofaccessories'+count+'" name="numberofaccessories'+count+'" value="">'+
                    '<td style="width:40px; border: 1px solid black; text-align: center;"> <button type="button" id="addproductnaration'+count+'" onclick="addproductnaration('+count+','+currentClickTableId+')"><i class="fa fa-plus"></i></button>&#9670;</td>'+
                    '<td style="width:220px; border: 1px solid black;" onclick="replaceproduct('+count+','+mainId+')" >'+mainName+'</td>'+
                    '<td style="width:80px; border: 1px solid black; text-align: center;" >'+mainCatNo+'</td>'+
                    '<td style="width:40px; border: 1px solid black; text-align: center;">'+
                    '<input class="input-fix" type="number" min="0" step="any"  id="showProductQty'+count+'" value="'+mainQty+'" onchange="rowProductCal('+count+','+showProductQty+')" onfocus="getProductRowOldValueForCal('+count+','+showProductQty+')">'+
                    '</td>'+
                    '<td style="width:40px; border: 1px solid black; text-align: center;" >'+makename+'</td>'+
                    '<td style="width:40px; border: 1px solid black; text-align: center;">'+
                    '<input class="input-fix" type="number" min="0" step="any"  id="showProductRate'+count+'" value="'+mainsalerate+'" onchange="rowProductCal('+count+','+showProductRate+')" onfocus="getProductRowOldValueForCal('+count+','+showProductRate+')">'+
                    '</td>'+
                    '<td style="width:40px; border: 1px solid black; text-align: center;">'+
                    '<input class="input-fix" type="number" min="0" step="any"  max="100" id="showProductDisPer'+count+'" value="'+mainDis+'" onchange="rowProductCal('+count+','+showProductDisPer+')" onfocus="getProductRowOldValueForCal('+count+','+showProductDisPer+')">'+
                    '</td>'+
                    '<td style="width:40px; border: 1px solid black; text-align: center;" class="fedprod" id="showProductTotalAmount'+count+'">'+mainTotalAmount+'</td>'+
                    '<td style="width:5px; border: 1px solid black; text-align: center;" onclick="removeProduct('+count+','+mainId+')"><i class="fas fa-times" style="color: red;"></i></td>'+  
                    '</tr>';
        
                

    $('#compartmenttable'+currentClickTableId).append(record);

    $('#numberOfProductInBill').val(count);
 
   
    var totalLength=$('#tableListAccessories').find('input[name="accessoriescheckbox[]"]').length;
 
    if(!totalLength || totalLength == undefined){
        totalLength = 0;
        updateBillTotal();
        allClear();
    }else{
        // updateBillTotal();   
        // allClear();
    }

    cumulativeAmount =Number(parseFloat(mainTotalAmount).toFixed(2)) + Number(parseFloat(cumulativeAmount).toFixed(2));  
    $('#cumulativeAmountId'+count).val(parseFloat(cumulativeAmount).toFixed(2));

   
    var numberofaccessories = 0;
    for (var i = 1; i <= totalLength; i++) {

        var tempId = "#accessoriesadd"+i;
        if ($(tempId).prop("checked") == true) {
            numberofaccessories++;
            var tempQty = "#tdqty"+i;
            var tempQtyValue = ($(tempQty).text()).trim();
            var temp = "#accessoriesid"+i;
            var accessId = $(temp).val();
            setAeccoris(tempQtyValue,accessId,mainId,mainQty);
        }
    }
    
   

    $('#numberofaccessories'+count).val(numberofaccessories);
    // resertEnterRow();
    updateBillTotal();
    allClear();
}

// ----------------------------Set Aeccoris Ajax Call------------------------------------
function setAeccoris(qty,accessId,productID,productQty){

    var url = '/getproductajax/'+accessId; 
    $.ajax({
        type : 'get',
        url : url,
        success : function(data){
            setTableForAeccoris(data[0],qty,productID,productQty)     
        }
    });

}

// ----------------------------Set Aeccoris In model table--------------------------------
function setTableForAeccoris(data,qty,productID,productQty){
    
    var totalQty = qty*productQty;

    var gstName = gstDefault;
   

    var result = cal(totalQty,data.purchaserate,data.productdiscount,gstName);


    var mainName = data.productname;
    var mainId = data.pid;
    var makename = data.makename;
    if(!makename){
        makename = "-";
    }
    var mainQty = parseFloat(totalQty);
    var mainHSN = "-";
    if(data.hsnno){
        mainHSN = data.hsnno;
    }
    var mainCatNo = "-";
    if(data.productcode){
        mainCatNo = data.productcode;
    }
    var mainUnitId = data.unitid;
    var mainUnitText = "";
    var mainsalerate = result.rate;
    var mainDis = result.discountper;
    var mainDisAmount = result.discountamount;
    var mainGstid = gstDefaultId;
    var mainGst = gstName+"%"; 
    var mainGSTAmount = result.gstamount;
    var mainAmount = result.amount;
    var mainTotalAmount = result.total;
    

    units.forEach(element => {
        if(element.productunitid == mainUnitId){
            mainUnitText = element.productunitname;
        }
    });
  

    accessCount++;

    var accessQty = "'"+"#accessQty"+"'";
    var accessDisPer = "'"+"#accessDisPer"+"'";
    var accessRate =   "'"+"#accessRate"+"'";
    //ACC
    var record =    '<tr id="atr'+accessCount+'">'+
                    '<input type="hidden" id="aprodctcountno'+accessCount+'" name="aprodctcountno'+accessCount+'" value="'+count+'">'+
                    '<input type="hidden" id="aiproductid'+accessCount+'" name="aiproductd'+accessCount+'" value="'+productID+'">'+
                    '<input type="hidden" id="aid'+accessCount+'" name="aid'+accessCount+'" value="'+mainId+'">'+
                    '<input type="hidden" id="apanelcompartmentid'+accessCount+'" name="apanelcompartmentid'+accessCount+'" value="'+currentCompartmentId+'">'+
                    '<input type="hidden" id="acompartmentseq'+accessCount+'" name="acompartmentseq'+accessCount+'" value="'+currentClickTableId+'">'+
                    '<input type="hidden" id="aname'+accessCount+'" name="apname'+accessCount+'" value="'+mainName+'">'+
                    '<input type="hidden" id="amake'+accessCount+'" name="amake'+accessCount+'" value="'+makename+'">'+
                    '<input type="hidden" id="ahsn'+accessCount+'" name="ahsn'+accessCount+'" value="'+mainHSN+'">'+
                    '<input type="hidden" id="acatno'+accessCount+'" name="acatno'+accessCount+'" value="'+mainCatNo+'">'+
                    '<input type="hidden" id="aqty'+accessCount+'" name="aqty'+accessCount+'" value="'+mainQty+'">'+
                    '<input type="hidden" id="aunitText'+accessCount+'" name="aunitText'+accessCount+'" value="'+mainUnitText+'">'+
                    '<input type="hidden" id="auid'+accessCount+'" name="auid'+accessCount+'" value="'+mainUnitId+'">'+
                    '<input type="hidden" id="asalerate'+accessCount+'" name="asalerate'+accessCount+'" value="'+mainsalerate+'">'+
                    '<input type="hidden" id="adiscount'+accessCount+'" name="adiscount'+accessCount+'" value="'+mainDis+'">'+
                    '<input type="hidden" id="adiscountAmount'+accessCount+'" name="adiscountAmount'+accessCount+'" value="'+mainDisAmount+'">'+
                    '<input type="hidden" id="agstid'+accessCount+'" name="agstid'+accessCount+'" value="'+mainGstid+'">'+
                    '<input type="hidden" id="agstname'+accessCount+'" name="agstname'+accessCount+'" value="'+mainGst+'">'+
                    '<input type="hidden" id="agstamount'+accessCount+'" name="agstamount'+accessCount+'" value="'+mainGSTAmount+'">'+
                    '<input type="hidden" id="aamount'+accessCount+'" name="aamount'+accessCount+'" value="'+mainTotalAmount+'">'+ 
                    '<td style="width:40px; border: 1px solid black; text-align: center;"></td>'+
                    '<td style="border: 1px solid black;"><i style="font-size:12px !important;">'+mainName+'</i></td>'+
                    '<td style="border: 1px solid black; text-align: center;"><i style="font-size:12px !important;">'+mainCatNo+'</i></td>'+
                    '<td style="border: 1px solid black; text-align: center;">'+
                    '<input class="input-fix" style="font-style:italic; font-size:12px !important;" type="number" min="0" step="any"  id="accessQty'+accessCount+'" value="'+mainQty+'" onchange="rowAccCal('+count+','+accessCount+','+accessQty+')" onfocus="getAccRowOldValueForCal('+count+','+accessCount+','+accessQty+')">'+
                    '</td>'+
                    '<td style="border: 1px solid black; text-align: center;"><i style="font-size:12px !important;">'+makename+'</i></td>'+
                    '<td style="border: 1px solid black; text-align: center;">'+
                    '<input class="input-fix" style="font-style:italic; font-size:12px !important;" type="number" min="0" step="any"  id="accessRate'+accessCount+'" value="'+mainsalerate+'" onchange="rowAccCal('+count+','+accessCount+','+accessRate+')" onfocus="getAccRowOldValueForCal('+count+','+accessCount+','+accessRate+')">'+
                    '</td>'+
                    '<td style="border: 1px solid black; text-align: center;">'+
                    '<input class="input-fix" style="font-style:italic; font-size:12px !important;" type="number" min="0" step="any"  id="accessDisPer'+accessCount+'" value="'+mainDis+'" onchange="rowAccCal('+count+','+accessCount+','+accessDisPer+')" onfocus="getAccRowOldValueForCal('+count+','+accessCount+','+accessDisPer+')">'+
                    '</td>'+
                    '<td style="border: 1px solid black; text-align: center;" class="fedacc"><i style="font-size:12px !important;" id="accessTotalAmount'+accessCount+'">'+mainTotalAmount+'</i></td>'+
                    '<td style="border: 1px solid black; text-align: center;" onclick="removeProductAcc('+accessCount+','+mainId+','+productID+')"><i class="fas fa-times" style="color: red;"></i></td>'+ 
                    '</tr>';
    $('#compartmenttable'+currentClickTableId).append(record);
    $('#numberOfAccessoriesInBill').val(accessCount);
    cumulativeAmount =Number(parseFloat(mainTotalAmount).toFixed(2)) + Number( parseFloat(cumulativeAmount).toFixed(2));

    $('#cumulativeAmountId'+count).val(parseFloat(cumulativeAmount).toFixed(2));
    updateBillTotal();
    feedertotal();
    changefeedertotal();

}

//-----------------------------add product group into list--------------------------------
function appProductGroupIntoList(){
    var length =  $('#productgrouplength').val();
    if(length == undefined || length == "" || !length){
        return;
    }
    var userqty = ($("#productqty").val()).trim();
    
    
    
    if(userqty == "" || !userqty){
        
        $('#productqty_error').show();
        return false;   
    }else{
        
        $('#productqty_error').hide();
    }
    var pArray = [];
    for (let i = 1; i <= length; i++) {
        var ptype = parseInt($('#accessoriesid'+i).val());
        var pname = $('#productgrpname'+i).val();
        var pid = parseInt($('#productgrp'+i).val());
        var qty = parseFloat($('#productgroupqty'+i).val());

         //check a product already list in this particluar compartment table
         for (let i = 0; i < productList.length; i++) {
           
            if(productList[i].compartmentseqence == currentClickTableId){

                for (let k = 0; k < productList[i].products.length; k++) {
                    if(productList[i].products[k].productid == pid){
                        alert("This Product Already List in "+ productList[i].compartmentname +" Compartment.");
                        return;
                    }
                }
            }
        }

        if(qty < 0 ){
            alert('Please Enter Minmum 1 Qty '+pname);
            return;
        }

        if(ptype == 0){
            var temp = {
                mainPid : pid,
                mainPname : pname,
                qty : qty,
                accessoriess : []
            }
            pArray.push(temp);
        }
    }



    for (let k = 0; k < pArray.length; k++) {
      
        for (let i = 1; i <= length; i++) {
            var ptype = parseInt($('#accessoriesid'+i).val());
            var pname = $('#productgrpname'+i).val();
            var pid = parseInt($('#productgrp'+i).val());
            var qty = parseFloat($('#productgroupqty'+i).val());
            var mainpid = pArray[k].mainPid;
            if(ptype != 0){
                if(mainpid == ptype){
                    var temp = {
                        masterPid : mainpid,
                        aPid : pid,
                        aPname : pname,
                        qty : qty
                    }   
                    pArray[k].accessoriess.push(temp);
                }
            }
        }
        
    }

    var arrayLength = pArray.length;


    addRowFromProductGroup(0,arrayLength,pArray,userqty);
}

//----------------------------- add product row fROM product group-----------------------
function addRowFromProductGroup(currentC,arrayLength,pdata,userqty){ 

    if(currentC < arrayLength){
        var url = '/getproductajax/'+pdata[currentC].mainPid; 
        selectData = "";
        selectPId = pdata[currentC].mainPid;
        $.ajax({
            type : 'get',
            url : url,
            success : function(data){
                addMainProductFromProductGroup(data[0],pdata,currentC,userqty);
            }
        });
    }
    
}

function addMainProductFromProductGroup(data,mainData,currentC,userqty) {
    var qty = mainData[currentC].qty*userqty;

    var gstName = gstDefault;
   
 
    var result = cal(qty,data.purchaserate,data.productdiscount,gstName);

  
    cumulativeAmount = 0;
    var type = data.productcategory;
    
    if(type == "general"){
        var mainName = data.productname;
    }else if(type == "specific"){
        var mainName = data.subnameofproduct;
    }

    var makename = data.makename;
    if(!makename){
        makename = "-";
    }
    var mainId = data.pid;
    var mainQty = qty;
    var mainHSN = "-";
    if(data.hsnno){
        mainHSN = data.hsnno;
    }

    var mainCatNo = "-";
    if(data.productcode){
        mainCatNo = data.productcode;
    }
    var mainUnitId = data.unitid;
    var mainUnitText = "";
    var mainsalerate = result.rate;
    var mainDis = result.discountper;
    var mainDisAmount = result.discountamount;
    var mainGstid = gstDefaultId;
    var mainGst = gstName+"%"; 
    var mainGSTAmount = result.gstamount;
    var mainAmount =result.amount;
    var mainTotalAmount =result.total;       
 
   

    // selctedResult.push(temp);
    count++;

    //push array
    var temp = {
        count : count,
        productid : data.pid,
        productname : mainName,
        makename : makename,
        hsn : mainHSN,
        catno :mainCatNo,
        unitid : mainUnitId,
        salerate : mainsalerate,
        discount : mainDis,
        gstid : mainGstid,
        gstname : mainGst,
        qty : mainQty,
        accessories : []
    }
    for (let i = 0; i < productList.length; i++) {
           
        if(productList[i].compartmentseqence == currentClickTableId){
            productList[i].products.push(temp);
        }
    }

   

   
    
    units.forEach(element => {
        if(element.productunitid == mainUnitId){
            mainUnitText = element.productunitname;
        }
    });
    
    

   
    var showProductQty = "'"+"#showProductQty"+"'";
    var showProductDisPer = "'"+"#showProductDisPer"+"'";
    var showProductRate =   "'"+"#showProductRate"+"'";
    //PRODUCT
    var record =    '<tr id="tr'+count+'">'+          
                    '<input type="hidden" id="pcont'+count+'" name="pcont'+count+'" value="'+count+'">'+                      
                    '<input type="hidden" id="pid'+count+'" name="pid'+count+'" value="'+mainId+'">'+
                    '<input type="hidden" id="pname'+count+'" name="pname'+count+'" value="'+mainName+'">'+
                    '<input type="hidden" id="pmake'+count+'" name="pmake'+count+'" value="'+makename+'">'+
                    '<input type="hidden" id="ppanelcompartmentid'+count+'" name="ppanelcompartmentid'+count+'" value="'+currentCompartmentId+'">'+
                    '<input type="hidden" id="pcompartmentseq'+count+'" name="pcompartmentseq'+count+'" value="'+currentClickTableId+'">'+
                    '<input type="hidden" id="phsn'+count+'" name="phsn'+count+'" value="'+mainHSN+'">'+
                    '<input type="hidden" id="pcatno'+count+'" name="pcatno'+count+'" value="'+mainCatNo+'">'+
                    '<input type="hidden" id="pqty'+count+'" name="pqty'+count+'" value="'+mainQty+'">'+
                    '<input type="hidden" id="uid'+count+'" name="uid'+count+'" value="'+mainUnitId+'">'+
                    '<input type="hidden" id="unitText'+count+'" name="unitText'+count+'" value="'+mainUnitText+'">'+
                    '<input type="hidden" id="salerate'+count+'" name="salerate'+count+'" value="'+mainsalerate+'">'+
                    '<input type="hidden" id="discount'+count+'" name="discount'+count+'" value="'+mainDis+'">'+
                    '<input type="hidden" id="discountAmount'+count+'" name="discountAmount'+count+'" value="'+mainDisAmount+'">'+
                    '<input type="hidden" id="gstid'+count+'" name="gstid'+count+'" value="'+mainGstid+'">'+
                    '<input type="hidden" id="gstname'+count+'" name="gstname'+count+'" value="'+mainGst+'">'+
                    '<input type="hidden" id="gstamount'+count+'" name="gstamount'+count+'" value="'+mainGSTAmount+'">'+
                    '<input type="hidden" id="amount'+count+'" name="amount'+count+'" value="'+mainTotalAmount+'">'+
                    '<input type="hidden" id="cumulativeAmountId'+count+'" name="cumulativeAmountId'+count+'" value="">'+
                    '<input type="hidden" id="numberofaccessories'+count+'" name="numberofaccessories'+count+'" value="">'+
                    '<td style="width:40px; border: 1px solid black; text-align: center;">';
                    record+='<button type="button" id="addproductnaration'+count+'" onclick="addproductnaration('+count+','+currentClickTableId+')"><i class="fa fa-plus"></i></button>';
                    record+='&#9670;</td>'+
                    '<td style="width:220px; border: 1px solid black;" onclick="replaceproduct('+count+','+mainId+')" >'+mainName+'</td>'+
                    '<td style="width:80px; border: 1px solid black; text-align: center;" >'+mainCatNo+'</td>'+
                    '<td style="width:40px; border: 1px solid black; text-align: center;">'+
                    '<input class="input-fix" type="number" min="0" step="any"  id="showProductQty'+count+'" value="'+mainQty+'" onchange="rowProductCal('+count+','+showProductQty+')" onfocus="getProductRowOldValueForCal('+count+','+showProductQty+')">'+
                    '</td>'+
                    '<td style="width:40px; border: 1px solid black; text-align: center;" >'+makename+'</td>'+
                    '<td style="width:40px; border: 1px solid black; text-align: center;">'+
                    '<input class="input-fix" type="number" min="0" step="any"  id="showProductRate'+count+'" value="'+mainsalerate+'" onchange="rowProductCal('+count+','+showProductRate+')" onfocus="getProductRowOldValueForCal('+count+','+showProductRate+')">'+
                    '</td>'+
                    '<td style="width:40px; border: 1px solid black; text-align: center;">'+
                    '<input class="input-fix" type="number" min="0"  step="any" max="100" id="showProductDisPer'+count+'" value="'+mainDis+'" onchange="rowProductCal('+count+','+showProductDisPer+')" onfocus="getProductRowOldValueForCal('+count+','+showProductDisPer+')">'+
                    '</td>'+
                    '<td style="width:40px; border: 1px solid black; text-align: center;" class="fedprod" id="showProductTotalAmount'+count+'">'+mainTotalAmount+'</td>'+
                    '<td style="width:5px; border: 1px solid black; text-align: center;" onclick="removeProduct('+count+','+mainId+')"><i class="fas fa-times" style="color: red;"></i></td>'+  
                    '</tr>';    

    $('#compartmenttable'+currentClickTableId).append(record);

    $('#numberOfProductInBill').val(count);
 
   
    var totalLength=mainData[currentC].accessoriess.length;
 
   
    if(!totalLength || totalLength == undefined){
        totalLength = 0;
        updateBillTotal();
        allClear();
        addRowFromProductGroup(currentC+1,mainData.length,mainData,userqty);
    }

    cumulativeAmount =Number(parseFloat(mainTotalAmount).toFixed(2)) + Number(parseFloat(cumulativeAmount).toFixed(2));  
    $('#cumulativeAmountId'+count).val(cumulativeAmount);
    var numberofaccessories = 0;
    if(totalLength != 0){
        tempAccArray = mainData[currentC].accessoriess;
        
        for (var i = 0; i < totalLength; i++) {
            numberofaccessories++;
            var tempQtyValue = tempAccArray[i].qty;
            var accessId = tempAccArray[i].aPid;
            setAeccoris(tempQtyValue,accessId,mainData[currentC].mainPid,mainQty );
        }
        addRowFromProductGroup(currentC+1,mainData.length,mainData,userqty);

    }
   
    
   

    $('#numberofaccessories'+count).val(numberofaccessories);
    // resertEnterRow();
    allClear(); 
}


//---------------------------------On Product Click Update---------------------------------
var oldRowValue;

function getProductRowOldValueForCal(pcount,id){
    var currentValue = parseFloat($(id+pcount).val());
    oldRowValue = currentValue;
}

function rowProductCal(pcount,id){
    var currentValue =  $(id+pcount).val();
    var temp;
    if(currentValue == undefined || currentValue == "" || !currentValue){
        if(oldRowValue == undefined || oldRowValue == "" || !oldRowValue){
            temp = 0;
        }else{
            temp = oldRowValue;
        }
    }else{
        temp = currentValue;
    }
    $(id+pcount).val(temp);
    rowProductUpdateCal(pcount);
}

function rowProductUpdateCal(pcount){   
    var q =  parseFloat($('#showProductQty'+pcount).val());
    var r =  parseFloat($('#showProductRate'+pcount).val());
    var d =  parseFloat($('#showProductDisPer'+pcount).val());
    var g = parseFloat($('#gstid'+pcount).val());

    var gstPer = 0;
    gstmaster.forEach(element => {
        if(element.gstmasterid == g){
            gstPer = parseFloat(element.gstname);
        }
    });

    

    var result = cal(q,r,d,gstPer);

    $('#pqty'+pcount).val(result.qty);
    $('#salerate'+pcount).val(result.rate);
    $('#discount'+pcount).val(result.discountper);
    $('#discountAmount'+pcount).val(result.discountamount);
    $('#gstamount'+pcount).val(result.gstamount);
    $('#amount'+pcount).val(result.total);

    $('#showProductQty'+pcount).val(result.qty);
    $('#showProductDisPer'+pcount).val(result.discountper);
    $('#showProductTotalAmount'+pcount).text(result.total);

    var accCount =  parseFloat($('#numberofaccessories'+pcount).val());

    if(accCount > 0){
        var mPid = $('#pid'+pcount).val();
        var mCId = $('#compartmentseq'+pcount).val();

        if(mPid && mPid != undefined && mPid != "" && $('#pid'+pcount).length && mCId && mCId != undefined && mCId != "" && $('#compartmentseq'+pcount).length){
            var total = parseFloat(result.total).toFixed(2);
            for(var i = 1;i <= accessCount;i++){
                var c = $('#aamount'+i).val();
                if(c && c != undefined && c != "" && $('#aamount'+i).length){
                    var aPid = $('#aiproductid'+i).val();
                    var aCid = $('#acompartmentseq'+i).val();
                    if(aPid == mPid && mCId == aCid){
                        total = Number(parseFloat($('#aamount'+i).val()).toFixed(2)) + Number(total);
                    }
                }
            }
            $('#cumulativeAmountId'+pcount).val(parseFloat(total).toFixed(2));
        }
    }else{
        $('#cumulativeAmountId'+pcount).val(parseFloat(result.total).toFixed(2));
    }
    updateBillTotal();
    feedertotal();
    changefeedertotal();
}


//--------------------------------On Product Acc Click Update--------------------------------

var oldAccRowValue;


function getAccRowOldValueForCal(pCount,paccCount,id){
    var currentValue = parseFloat($(id+paccCount).val());
    oldAccRowValue = currentValue;
}

function rowAccCal(pCount,paccCount,id){
    var currentValue =  $(id+paccCount).val();
    var temp;
    if(currentValue == undefined || currentValue == "" || !currentValue){
        if(oldRowValue == undefined || oldRowValue == "" || !oldRowValue){
            temp = 0;
        }else{
            temp = oldRowValue;
        }
    }else{
        temp = currentValue;
    }
    $(id+paccCount).val(temp);
    rowAccUpdateCal(pCount,paccCount);
}

function rowAccUpdateCal(pCount,paccCount){

    var q =  parseFloat($('#accessQty'+paccCount).val());
    var r =  parseFloat($('#accessRate'+paccCount).val());
    var d =  parseFloat($('#accessDisPer'+paccCount).val());
    var g =  parseFloat($('#agstid'+paccCount).val());

    var gstPer = 0;
    gstmaster.forEach(element => {
        if(element.gstmasterid == g){
            gstPer = parseFloat(element.gstname);
        }
    });

    var result = cal(q,r,d,gstPer);

    $('#aqty'+paccCount).val(result.qty);
    $('#asalerate'+paccCount).val(result.rate);
    $('#adiscount'+paccCount).val(result.discountper);
    $('#adiscountAmount'+paccCount).val(result.discountamount);
    $('#agstamount'+paccCount).val(result.gstamount);
    $('#aamount'+paccCount).val(result.total);
    

    $('#accessQty'+paccCount).val(result.qty);
    $('#accessRate'+paccCount).val(result.rate);
    $('#accessDisPer'+paccCount).val(result.discountper);
    $('#agstamount'+paccCount).val(result.gstamount);
    $('#accessTotalAmount'+paccCount).text(result.total);

    var accCount =  parseFloat($('#numberofaccessories'+pCount).val());
   
    if(accCount > 0){
    
        var mPid = $('#pid'+pCount).val();
        var mFid = $('#feederid'+pCount).val();

        var total = parseFloat($('#amount'+pCount).val()).toFixed(2);
 
        for(var i = 1;i <= accessCount;i++){
            var c = $('#aamount'+i).val();
            if(c && c != undefined && c != "" && $('#aamount'+i).length){
                var aPid = $('#aiproductid'+i).val();
                var aFid = $('#afeederid'+i).val();
                if(aPid == mPid && mFid == aFid){
                    total = Number(parseFloat($('#aamount'+i).val()).toFixed(2)) + Number(total);
                }
            }
        }
        $('#cumulativeAmountId'+pCount).val(parseFloat(total).toFixed(2));
    }
    updateBillTotal();
    feedertotal();
    changefeedertotal();

}

//===================================Replcae==============================================================================================

//-----------------------------------replace product-------------------------------------------
var currentReplcaeSeq = "";
var currentReplasePid = "";
var currentReplaseCount = "";
var currentReplaseType = "";  // 1 for specific and 2 for genral
var newReplacePid = "";
var currentReplaceCompartment = "";
// ajax call for replace product
function replaceproduct(pCount,pPid) {

    var pSeq= $('#pcompartmentseq'+pCount).val();
    var pqty = $('#pqty'+pCount).val();
    var url = '/getproductajax/'+pPid; 
    var cId = $('#ppanelcompartmentid'+pCount).val();

    currentReplcaeSeq = pSeq;
    currentReplasePid = pPid;
    currentReplaseCount = pCount;
    currentReplaceCompartment = cId;
    currentReplaseType="";
    newReplacePid = pPid;
    $.ajax({
        type : 'get',
        url : url,
        success : function(data){
            setReplaceProductModel(data[0],pqty,pPid);
        }
    }); 
}

//set replace product moodule
function setReplaceProductModel(data,pqty,pPid) {

    $('#replaceproductqty').val(pqty);

    $('#simlierproductreplace').hide();
    $('#tablesimlierproductreplace tbody').html('');

    if(data.productcategory == "specific"){
        setCurrentSpecificationInModel(data,pPid)
        getAccProductReplace(pPid);
        currentReplaseType = 1;
        
    $('#replaceProductModal').modal('show');  
    }else if(data.productcategory == "general"){
        currentReplaseType = 2;
    }
 
}

//---------------------current replace Product is spacification----------------
function setCurrentSpecificationInModel(data,pPid) {
   

    var selGroup = data.groupname;
    var selCompany =   "-"+data.makename;
    var selProductName =  data.productname;
    var _token = $('input[name="_token"]').val();
    var queryName =  selProductName.replace(selGroup,'');
    queryName = queryName.replace(selCompany,'');
    var groupid = data.groupid;
 
    var url = $('#urlgetSimilerProduct').val();
    $.ajax({
         type : 'POST',
         url : url,
         data : {productName:queryName,groupid:groupid, _token : _token},
         success : function(data){
           
             arraySortForReplace(data,pPid);
         }
     });
}


function  arraySortForReplace(data,productid) {
    var sortData = [];
    data.forEach(element => {
        // refiltering a url
        var temp = {
            id : element.id,
            name : element.subnameofproduct,
            sellrate : element.sellrate,
            makename : element.makename,
            stock : element.stockqty,
            discount : element.productdiscount,
            amount : discountCal(element.purchaserate,element.productdiscount)
        }
        sortData.push(temp);    
    });

    var newArray = sortData.sort(function(a,b) {
        return a.amount -b.amount;
    });

    setSimlierProductReplace(newArray,productid);
}

//---------------------Set Simlier Product in model table ----------------
function setSimlierProductReplace(data,productid){

    $('#simlierproductreplace').show();
    $('#tablesimlierproductreplace tbody').html('');
    var selectProductrate = 0;
    data.forEach(element => {
        if(element.id == productid){
            selectProductrate  = element.amount;
        }
    }); 

    var html ="";
    data.forEach(element => {

        if(element.id == productid){
        html = html +
                    '<tr>'+
                    '<td>'+element.name+'</td>'+
                    '<td>'+element.makename+'</td>'+
                    '<td>'+element.sellrate+'</td>'+
                    '<td>'+element.stock+'</td>'+                            
                    '<td>'+element.discount+'</td>'+
                    '<td>'+element.amount+'</td>'+
                    '<td><input type="radio" onchange="changeSelctedProductReplace('+element.id+')" name="selctedProduct" value="'+element.id+'" checked>'+
                    '</td>'+
                '</tr>';
        }else{
            if(Number(element.amount) > Number(selectProductrate)){
                html = html +
                    '<tr style="color:red !important; ">'+
                        '<td>'+element.name+'</td>'+
                        '<td>'+element.makename+'</td>'+
                        '<td>'+element.sellrate+'</td>'+
                        '<td>'+element.stock+'</td>'+
                        '<td>'+element.discount+'</td>'+
                        '<td>'+element.amount+'</td>'+
                        '<td><input type="radio" onchange="changeSelctedProductReplace('+element.id+')" name="selctedProduct" value="'+element.id+'" >'+
                        '</td>'+
                    '</tr>';
            }else if(Number(element.amount) < Number(selectProductrate)){
             
                html = html +
                    '<tr  style="color:green !important;">'+
                        '<td>'+element.name+'</td>'+
                        '<td>'+element.makename+'</td>'+
                        '<td>'+element.sellrate+'</td>'+
                        '<td>'+element.stock+'</td>'+
                        '<td>'+element.discount+'</td>'+
                        '<td>'+element.amount+'</td>'+
                        '<td><input type="radio" onchange="changeSelctedProductReplace('+element.id+')" name="selctedProduct" value="'+element.id+'" >'+
                        '</td>'+
                    '</tr>';
            }else if(Number(element.amount) == Number(selectProductrate)){
                html = html +
                    '<tr>'+
                        '<td>'+element.name+'</td>'+
                        '<td>'+element.makename+'</td>'+
                        '<td>'+element.sellrate+'</td>'+
                        '<td>'+element.stock+'</td>'+                            
                        '<td>'+element.discount+'</td>'+
                        '<td>'+element.amount+'</td>'+
                        '<td><input type="radio" onchange="changeSelctedProductReplace('+element.id+')" name="selctedProduct" value="'+element.id+'" >'+
                        '</td>'+
                    '</tr>';
            }
        }


        

    });

    $('#tablesimlierproductreplace').append(html);
    
}


//----------------------------Product Repalce model Acc--------------------
function getAccProductReplace(productid){
    var urlaccessories = $('#urlaccessories').val();
    var _token = $('input[name="_token"]').val();

    // clear add product model
    $('#tableListAccessories tbody').html('');

    $('#productaccessoriesreplacediv').hide();
    $('#tableListReplaceAccessories tbody').html('');
   
    if(productid){
        
        $.ajax({
            type : 'POST',
            url : urlaccessories,
            data : {productid:productid, _token : _token},
            success : function(data){
                $('#productaccessoriesreplacediv').show();
                $('#tableListReplaceAccessories tbody').append(data);
            }
        });
    }
}

//----------------------------change similer product --------------------
function changeSelctedProductReplace(id){
    newReplacePid = id;
    getAccProductReplace(id);
}

//---------------------------rplace button click--------------------
function onReplaceClick() {
    addReplaceProduct();
}

function addReplaceProduct() {
    if(currentReplaseType == 1){
        var qty = ($("#replaceproductqty").val()).trim();
        var id = newReplacePid;
        
        if(id == "" || !id){
            alert("Please Select Product");
            return false;   
        }
        if(qty == "" || !qty){
            $('#productqty_error').show();
            return false;   
        }else{
            $('#productqty_error').hide();
        }

        //check a product already list in this particluar compartment table
        for (let i = 0; i < productList.length; i++) {
           
            if(productList[i].compartmentseqence == currentReplcaeSeq){

                for (let k = 0; k < productList[i].products.length; k++) {
                    if(productList[i].products[k].productid == id){
                        alert("This Product Already List in "+ productList[i].compartmentname +" Compartment.");
                        return false;
                    }
                }
            }
        }
        clearOldProduacat();
        $('#replaceProductModal').modal('hide');
     

    }
}

function clearOldProduacat() {
    
    if(currentReplaseType == 1){
        for (let i = 0; i < productList.length; i++) {
            if(productList[i].compartmentseqence == currentReplcaeSeq){
                 var temp = productList[i].products;
                for (let k = 0; k < productList[i].products.length; k++) {
                    if(productList[i].products[k].productid == currentReplasePid){
                        temp.splice(k, 1);
                    }
                }
                productList[i].products = temp;
            }
        }
        

        var numAcc = parseInt($('#numberofaccessories'+currentReplaseCount).val());
 
      
        var qty = ($("#replaceproductqty").val()).trim();

        if(numAcc == 0 ){
            $('#tr'+currentReplaseCount).empty();
            ajaxCallForNewReplaceProduct(newReplacePid,qty);
        }else{
            for(var i = 1 ; i <= accessCount;i++){
                if($('#aiproductid'+i).length){
                    var mainId = parseInt($('#aiproductid'+i).val());
                    var countNo =parseInt($('#aprodctcountno'+i).val());
                    if(mainId == currentReplasePid &&  countNo == currentReplaseCount){
                        $('#atr'+i).remove();
                    }
                }
            }
            $('#tr'+currentReplaseCount).empty();
            ajaxCallForNewReplaceProduct(newReplacePid,qty);
        }

    }else if(currentReplaseType == 2){

    }
}

function ajaxCallForNewReplaceProduct(id,qty) {
    var url = '/getproductajax/'+id; 
    selectData = "";
    selectPId = id;
    $.ajax({
        type : 'get',
        url : url,
        success : function(data){
            updateRow(data[0],qty);
        }
    });
}


function updateRow(data,qty) {

    var gstName = gstDefault;
   
 
    var result = cal(qty,data.purchaserate,data.productdiscount,gstName);

  
    cumulativeAmount = 0;
    var type = data.productcategory;
    
    if(type == "general"){
        var mainName = data.productname;
    }else if(type == "specific"){
        var mainName = data.subnameofproduct;
    }

    var makename = data.makename;
    if(!makename){
        makename = "-";
    }
    var mainId = data.pid;
    var mainQty = qty;
    var mainHSN = "-";
    if(data.hsnno){
        mainHSN = data.hsnno;
    }

    var mainCatNo = "-";
    if(data.productcode){
        mainCatNo = data.productcode;
    }
    var mainUnitId = data.unitid;
    var mainUnitText = "";
    var mainsalerate = result.rate
    var mainDis = result.discountper;
    var mainDisAmount = result.discountamount;
    var mainGstid = gstDefaultId;
    var mainGst = gstName+"%"; 
    var mainGSTAmount = result.gstamount;
    var mainAmount =result.amount;
    var mainTotalAmount =result.total;       
 
   

    // selctedResult.push(temp);
    count++;

    //push array
    var temp = {
        count : currentReplaseCount,
        productid : data.pid,
        productname : mainName,
        makename : makename,
        hsn : mainHSN,
        catno :mainCatNo,
        unitid : mainUnitId,
        salerate : mainsalerate,
        discount : mainDis,
        gstid : mainGstid,
        gstname : mainGst,
        qty : mainQty,
        accessories : []
    }
    for (let i = 0; i < productList.length; i++) {
           
        if(productList[i].compartmentseqence == currentReplcaeSeq){
            productList[i].products.push(temp);
        }
    }


   
    
    units.forEach(element => {
        if(element.productunitid == mainUnitId){
            mainUnitText = element.productunitname;
        }
    });
    
    
    var showProductQty = "'"+"#showProductQty"+"'";
    var showProductDisPer = "'"+"#showProductDisPer"+"'";
    var showProductRate =   "'"+"#showProductRate"+"'";
    //PRODUCT
    var record =    '<input type="hidden" id="pcont'+currentReplaseCount+'" name="pcont'+currentReplaseCount+'" value="'+currentReplaseCount+'">'+
    '<input type="hidden" id="pid'+currentReplaseCount+'" name="pid'+currentReplaseCount+'" value="'+mainId+'">'+
    '<input type="hidden" id="pname'+currentReplaseCount+'" name="pname'+currentReplaseCount+'" value="'+mainName+'">'+
    '<input type="hidden" id="pmake'+currentReplaseCount+'" name="pmake'+currentReplaseCount+'" value="'+makename+'">'+
    '<input type="hidden" id="ppanelcompartmentid'+currentReplaseCount+'" name="ppanelcompartmentid'+currentReplaseCount+'" value="'+currentReplaceCompartment+'">'+
    '<input type="hidden" id="pcompartmentseq'+currentReplaseCount+'" name="pcompartmentseq'+currentReplaseCount+'" value="'+currentReplcaeSeq+'">'+
    '<input type="hidden" id="phsn'+currentReplaseCount+'" name="phsn'+currentReplaseCount+'" value="'+mainHSN+'">'+
    '<input type="hidden" id="pcatno'+currentReplaseCount+'" name="pcatno'+currentReplaseCount+'" value="'+mainCatNo+'">'+
    '<input type="hidden" id="pqty'+currentReplaseCount+'" name="pqty'+currentReplaseCount+'" value="'+qty+'">'+
    '<input type="hidden" id="uid'+currentReplaseCount+'" name="uid'+currentReplaseCount+'" value="'+mainUnitId+'">'+
    '<input type="hidden" id="unitText'+currentReplaseCount+'" name="unitText'+currentReplaseCount+'" value="'+mainUnitText+'">'+
    '<input type="hidden" id="salerate'+currentReplaseCount+'" name="salerate'+currentReplaseCount+'" value="'+mainsalerate+'">'+
    '<input type="hidden" id="discount'+currentReplaseCount+'" name="discount'+currentReplaseCount+'" value="'+mainDis+'">'+
    '<input type="hidden" id="discountAmount'+currentReplaseCount+'" name="discountAmount'+currentReplaseCount+'" value="'+mainDisAmount+'">'+
    '<input type="hidden" id="gstid'+currentReplaseCount+'" name="gstid'+currentReplaseCount+'" value="'+mainGstid+'">'+
    '<input type="hidden" id="gstname'+currentReplaseCount+'" name="gstname'+currentReplaseCount+'" value="'+mainGst+'">'+
    '<input type="hidden" id="gstamount'+currentReplaseCount+'" name="gstamount'+currentReplaseCount+'" value="'+mainGSTAmount+'">'+
    '<input type="hidden" id="amount'+currentReplaseCount+'" name="amount'+currentReplaseCount+'" value="'+mainTotalAmount+'">'+
    '<input type="hidden" id="cumulativeAmountId'+currentReplaseCount+'" name="cumulativeAmountId'+currentReplaseCount+'" value="">'+
    '<input type="hidden" id="numberofaccessories'+currentReplaseCount+'" name="numberofaccessories'+currentReplaseCount+'" value="">'+
    '<td style="width:40px; border: 1px solid black; text-align: center;"> <button type="button" id="addproductnaration'+count+'" onclick="addproductnaration('+currentReplaseCount+','+currentReplcaeSeq+')"><i class="fa fa-plus"></i></button> &#9670;</td>'+
    '<td style="width:220px; border: 1px solid black;" onclick="replaceproduct('+currentReplaseCount+','+mainId+')" >'+mainName+'</td>'+
    '<td style="width:80px; border: 1px solid black; text-align: center;" >'+mainCatNo+'</td>'+
    '<td style="width:40px; border: 1px solid black; text-align: center;">'+
        '<input class="input-fix" type="number" min="0" step="any"  id="showProductQty'+currentReplaseCount+'" value="'+mainQty+'" onchange="rowProductCal('+currentReplaseCount+','+showProductQty+')" onfocus="getProductRowOldValueForCal('+currentReplaseCount+','+showProductQty+')">'+
        '</td>'+
    '<td style="width:40px; border: 1px solid black; text-align: center;" >'+makename+'</td>'+
    '<td style="width:40px; border: 1px solid black; text-align: center;">'+
        '<input class="input-fix" type="number" min="0" step="any"  id="showProductRate'+currentReplaseCount+'" value="'+mainsalerate+'" onchange="rowProductCal('+currentReplaseCount+','+showProductRate+')" onfocus="getProductRowOldValueForCal('+currentReplaseCount+','+showProductRate+')">'+
        '</td>'+
    '<td style="width:40px; border: 1px solid black; text-align: center;">'+
    '<input class="input-fix" type="number" min="0" step="any"  max="100" id="showProductDisPer'+currentReplaseCount+'" value="'+mainDis+'" onchange="rowProductCal('+currentReplaseCount+','+showProductDisPer+')" onfocus="getProductRowOldValueForCal('+currentReplaseCount+','+showProductDisPer+')">'+
    '</td>'+
    '<td style="width:40px; border: 1px solid black; text-align: center;" class="fedprod" id="showProductTotalAmount'+currentReplaseCount+'">'+mainTotalAmount+'</td>'+
    '<td style="width:5px; border: 1px solid black; text-align: center;" onclick="removeProduct('+currentReplaseCount+','+mainId+')"><i class="fas fa-times" style="color: red;"></i></td>';
        
    $('#tr'+currentReplaseCount).html(record);   

    var totalLength=$('#tableListReplaceAccessories').find('input[name="accessoriescheckbox[]"]').length;
   
    if(!totalLength || totalLength == undefined){
        totalLength = 0;
        updateBillTotal();
      
        feedertotal();
        changefeedertotal();
    }


    cumulativeAmount =Number(parseFloat(mainTotalAmount).toFixed(2)) + Number(parseFloat(cumulativeAmount).toFixed(2));  
    $('#cumulativeAmountId'+currentReplaseCount).val(parseFloat(cumulativeAmount).toFixed(2));

    var numberofaccessories = 0;
   
    var tempAccArray = [];
    for (var i = 1; i <= totalLength; i++) {
        
        var tempId = "#accessoriesadd"+i;
        if ($(tempId).prop("checked") == true) {
            numberofaccessories++;
            var tempQty = "#tdqty"+i;
            var tempQtyValue = ($(tempQty).text()).trim();
            var temp = "#accessoriesid"+i;
            var accessId = $(temp).val();
            var temp = {
                tempQtyValue :tempQtyValue,
                accessId : accessId,
                accessCount : accessCount,
                mainId : mainId,
                mainQty : mainQty
            }
            tempAccArray.push(temp);
           
        }
    }


    for(var j = tempAccArray.length-1 ; j >= 0 ; j--){
        setAeccorisForReplace(tempAccArray[j].tempQtyValue,tempAccArray[j].accessId,tempAccArray[j].mainId,tempAccArray[j].mainQty,tempAccArray[j].accessCount);
    }

    $('#numberofaccessories'+currentReplaseCount).val(numberofaccessories);
 
  
}

// ----------------------------Set Aeccoris Ajax Call------------------------------------
function setAeccorisForReplace(qty,accessId,productID,productQty){

    var url = '/getproductajax/'+accessId; 
    $.ajax({
        type : 'get',
        url : url,
        success : function(data){
            setTableForReplaceAeccoris(data[0],qty,productID,productQty)     
        }
    });

}

// ----------------------------Set Replace Aeccoris into table--------------------------------
function setTableForReplaceAeccoris(data,qty,productID,productQty) {
    var totalQty = qty*productQty;

    var gstName = gstDefault;
  

    var result = cal(totalQty,data.purchaserate,data.productdiscount,gstName);


    var mainName = data.productname;
    var mainId = data.pid;
    var makename = data.makename;
    if(!makename){
        makename = "-";
    }
    var mainQty = parseFloat(totalQty);
    var mainHSN = "-";
    if(data.hsnno){
        mainHSN = data.hsnno;
    }
    var mainCatNo = "-";
    if(data.productcode){
        mainCatNo = data.productcode;
    }
    var mainUnitId = data.unitid;
    var mainUnitText = "";
    var mainsalerate = parseFloat(data.sellrate).toFixed(2);
    var mainDis = result.discountper;
    var mainDisAmount = result.discountamount;
    var mainGstid = gstDefaultId;
    var mainGst = gstName+"%"; 
    var mainGSTAmount = result.gstamount;
    var mainAmount = result.amount;
    var mainTotalAmount = result.total;
    

    units.forEach(element => {
        if(element.productunitid == mainUnitId){
            mainUnitText = element.productunitname;
        }
    });
  


    accessCount++;

    var accessQty = "'"+"#accessQty"+"'";
    var accessDisPer = "'"+"#accessDisPer"+"'";
    var accessRate =   "'"+"#accessRate"+"'";
    var record =    '<tr id="atr'+accessCount+'">'+
                    '<input type="hidden" id="aprodctcountno'+accessCount+'" name="aprodctcountno'+accessCount+'" value="'+currentReplaseCount+'">'+
                    '<input type="hidden" id="aiproductid'+accessCount+'" name="aiproductd'+accessCount+'" value="'+productID+'">'+
                    '<input type="hidden" id="aid'+accessCount+'" name="aid'+accessCount+'" value="'+mainId+'">'+
                    '<input type="hidden" id="apanelcompartmentid'+accessCount+'" name="apanelcompartmentid'+accessCount+'" value="'+currentReplaceCompartment+'">'+
                    '<input type="hidden" id="acompartmentseq'+accessCount+'" name="acompartmentseq'+accessCount+'" value="'+currentReplcaeSeq+'">'+
                    '<input type="hidden" id="aname'+accessCount+'" name="apname'+accessCount+'" value="'+mainName+'">'+
                    '<input type="hidden" id="amake'+accessCount+'" name="amake'+accessCount+'" value="'+makename+'">'+
                    '<input type="hidden" id="ahsn'+accessCount+'" name="ahsn'+accessCount+'" value="'+mainHSN+'">'+
                    '<input type="hidden" id="acatno'+accessCount+'" name="acatno'+accessCount+'" value="'+mainCatNo+'">'+
                    '<input type="hidden" id="aqty'+accessCount+'" name="aqty'+accessCount+'" value="'+mainQty+'">'+
                    '<input type="hidden" id="aunitText'+accessCount+'" name="aunitText'+accessCount+'" value="'+mainUnitText+'">'+
                    '<input type="hidden" id="auid'+accessCount+'" name="auid'+accessCount+'" value="'+mainUnitId+'">'+
                    '<input type="hidden" id="asalerate'+accessCount+'" name="asalerate'+accessCount+'" value="'+mainsalerate+'">'+
                    '<input type="hidden" id="adiscount'+accessCount+'" name="adiscount'+accessCount+'" value="'+mainDis+'">'+
                    '<input type="hidden" id="adiscountAmount'+accessCount+'" name="adiscountAmount'+accessCount+'" value="'+mainDisAmount+'">'+
                    '<input type="hidden" id="agstid'+accessCount+'" name="agstid'+accessCount+'" value="'+mainGstid+'">'+
                    '<input type="hidden" id="agstname'+accessCount+'" name="agstname'+accessCount+'" value="'+mainGst+'">'+
                    '<input type="hidden" id="agstamount'+accessCount+'" name="agstamount'+accessCount+'" value="'+mainGSTAmount+'">'+
                    '<input type="hidden" id="aamount'+accessCount+'" name="aamount'+accessCount+'" value="'+mainTotalAmount+'">'+ 
                    '<td style="width:40px; border: 1px solid black; text-align: center;"></td>'+
                    '<td style="border: 1px solid black;"><i style="font-size:12px !important;">'+mainName+'</i></td>'+
                    '<td style="border: 1px solid black; text-align: center;"><i style="font-size:12px !important;">'+mainCatNo+'</i></td>'+
                    '<td style="border: 1px solid black; text-align: center;">'+
                    '<input class="input-fix" style="font-style:italic; font-size:12px !important;" type="number" min="0" step="any"  id="accessQty'+accessCount+'" value="'+mainQty+'" onchange="rowAccCal('+count+','+accessCount+','+accessQty+')" onfocus="getAccRowOldValueForCal('+count+','+accessCount+','+accessQty+')">'+
                    '</td>'+
                    '<td style="border: 1px solid black; text-align: center;"><i style="font-size:12px !important;">'+makename+'</i></td>'+
                    '<td style="border: 1px solid black; text-align: center;">'+
                    '<input class="input-fix" style="font-style:italic; font-size:12px !important;" type="number" min="0" step="any"  id="accessRate'+accessCount+'" value="'+mainsalerate+'" onchange="rowAccCal('+count+','+accessCount+','+accessRate+')" onfocus="getAccRowOldValueForCal('+count+','+accessCount+','+accessRate+')">'+
                    '</td>'+
                    '<td style="border: 1px solid black; text-align: center;">'+
                    '<input class="input-fix" style="font-style:italic; font-size:12px !important;" type="number" min="0" step="any"  id="accessDisPer'+accessCount+'" value="'+mainDis+'" onchange="rowAccCal('+count+','+accessCount+','+accessDisPer+')" onfocus="getAccRowOldValueForCal('+count+','+accessCount+','+accessDisPer+')">'+
                    '</td>'+
                    '<td style="border: 1px solid black; text-align: center;" class="fedacc" ><i style="font-size:12px !important;" id="accessTotalAmount'+accessCount+'">'+mainTotalAmount+'</i></td>'+
                    '<td style="border: 1px solid black; text-align: center;" onclick="removeProductAcc('+accessCount+','+mainId+','+productID+')"><i class="fas fa-times" style="color: red;"></i></td>'+ 
                    '</tr>';
     if($("#ntr" + currentReplaseCount).length > 0 ){
        $('#ntr'+currentReplaseCount).after(record);
    }else{
         $('#tr'+currentReplaseCount).after(record);
    }
   
    $('#numberOfAccessoriesInBill').val(accessCount);
    cumulativeAmount =Number(parseFloat(mainTotalAmount).toFixed(2)) + Number( parseFloat(cumulativeAmount).toFixed(2));

    $('#cumulativeAmountId'+currentReplaseCount).val(parseFloat(cumulativeAmount).toFixed(2));
    updateBillTotal();
    feedertotal();
    changefeedertotal();

                  
}

//===================================End Replcae=========================================================================================


//===================================Compartment Copy====================================================================================
var currentCopyCompartment = 0;
var currentCopyCompartId = 0;

function copyCompartmentModelOpen(pCompartseq,pCompartId) {

    currentCopyCompartment = pCompartseq;
    currentCopyCompartId = pCompartId;
    
    var copyCompartList = [];
    productList.forEach(element => {
        if(element.compartmentseqence != currentCopyCompartment){
            if(element.products.length > 0){
                var temp = {
                    compartmentSeq : element.compartmentseqence,
                    compartmentname : element.compartmentname 
                }
                copyCompartList.push(temp);
            }
        }
    });


    html = "";
    $('#copyCompartmentList').empty();
    if(copyCompartList.length != 0){
        html = '<option value="">Select Compartment</option>';
        copyCompartList.forEach(element => {
            html = html + '<option value="'+element.compartmentSeq+'">'+element.compartmentname+'</option>';
        });
        $('#copyCompartmentList').append(html);
    }else{
        var html = '<option value="">--No Compart Found With Product--</option>';
        $('#copyCompartmentList').append(html);
    }

    

    $('#copyModel').modal('show');
}

function onClickCopyCompartment() {

    var fromCompartmentId = $('#copyCompartmentList').val();

    if(fromCompartmentId == ""){
        alert('Please Select a Compartment!');
        return;
    }
    fromCompartment = [];

    for (let i = 0; i < productList.length; i++) {
        if(fromCompartmentId == productList[i].compartmentseqence){
            fromCompartment = JSON.parse(JSON.stringify(productList[i].products));
        }        
    }   


    // var tempFromCompartment = fromCompartment[0];

    for(var i = 1 ; i <= accessCount;i++){
        var aCount = $('#aprodctcountno'+i).val();
        for (let k = 0; k < fromCompartment.length; k++) {

            if(aCount == fromCompartment[k].count){
                var mainid = $('#aiproductid'+i).val();
                var productName = $('#aname'+i).val();
                var makename = $('#amake'+i).val();
                var hsn = $('#ahsn'+i).val();
                var catno = $('#acatno'+i).val();
                var pId = $('#aid'+i).val();
                var qty = $('#aqty'+i).val();
                var uid = $('#auid'+i).val();
                var salerate = $('#asalerate'+i).val();
                var discount = $('#adiscount'+i).val();
                var gstid =$('#agstid'+i).val();
                var temp = {
                    mainPid : mainid,
                    productid : pId,
                    productName : productName,
                    makename : makename,
                    hsn : hsn,
                    catno : catno,
                    qty : qty,
                    uid : uid,
                    salerate : salerate,
                    discount :discount,
                    gstid : gstid
                }
                fromCompartment[k].accessories.push(temp);
            }
        
        }
        
    }
   

    var validProduct = [];

    for (let i = 0; i < productList.length; i++) {
        if(currentCopyCompartment == productList[i].compartmentseqence){
            validProduct = JSON.parse(JSON.stringify(productList[i].products));
        }
    }


    for (let k = 0; k < validProduct.length; k++) {
        for (let l = 0; l < fromCompartment.length; l++) {
  
            if(validProduct[k].productid == fromCompartment[l].productid){
                alert("Product Duplication Occurs!");
                return;
            }
            
        }
    }

    


    fromCompartment.forEach(element => {
        addCopyProductCal(element);
    });

    
    $('#copyModel').modal('hide');
    
}

//Copy Product add to table TR
function addCopyProductCal(element){ 

    var gstName = "";
    gstmaster.forEach(each => {
        if(each.gstmasterid == element.gstid){
            
            gstName = each.gstname;
           
        }
    });

  var result = cal(element.qty,element.salerate,element.discount,gstName);


  addCopyProductToTable(result,element,gstName)
}

function addCopyProductToTable(result,data,gstName) {
      
    cumulativeAmount = 0;
    
    var mainName = data.productname;


    var makename = data.makename;
    

    var mainId = data.productid;
    var mainQty = data.qty;
    var mainHSN = data.hsn;
    var mainCatNo = data.catno;
    var mainUnitId = data.unitid;
    var mainUnitText = "";
    var mainsalerate = parseFloat(result.rate).toFixed(2);
    var mainDis = result.discountper;
    var mainDisAmount = result.discountamount;
    var mainGstid = data.gstid;
    var mainGst = gstName+"%"; 
    var mainGSTAmount = result.gstamount;
    var mainAmount =result.amount;
    var mainTotalAmount =result.total;    
    var copycount = data.count;
    
    count++;

    //push array
    var temp = {
        count : count,
        productid : data.productid,
        productname : mainName,
        makename : makename,
        hsn : mainHSN,
        catno :mainCatNo,
        unitid : mainUnitId,
        salerate : mainsalerate,
        discount : mainDis,
        gstid : mainGstid,
        gstname : mainGst,
        qty : mainQty,
        accessories : []
    }
    for (let i = 0; i < productList.length; i++) {
           
        if(productList[i].compartmentseqence == currentCopyCompartment){
            productList[i].products.push(temp);
        }
    }

    units.forEach(element => {
        if(element.productunitid == mainUnitId){
            mainUnitText = element.productunitname;
        }
    });

    var showProductQty = "'"+"#showProductQty"+"'";
    var showProductDisPer = "'"+"#showProductDisPer"+"'";
    var showProductRate =   "'"+"#showProductRate"+"'";
    //PRODUCT
    var showProductQty = "'"+"#showProductQty"+"'";
    var showProductDisPer = "'"+"#showProductDisPer"+"'";
    var showProductRate =   "'"+"#showProductRate"+"'";
    //PRODUCT
    var record =    '<tr id="tr'+count+'">'+          
                    '<input type="hidden" id="pcont'+count+'" name="pcont'+count+'" value="'+count+'">'+                      
                    '<input type="hidden" id="pid'+count+'" name="pid'+count+'" value="'+mainId+'">'+
                    '<input type="hidden" id="pname'+count+'" name="pname'+count+'" value="'+mainName+'">'+
                    '<input type="hidden" id="pmake'+count+'" name="pmake'+count+'" value="'+makename+'">'+
                    '<input type="hidden" id="ppanelcompartmentid'+count+'" name="ppanelcompartmentid'+count+'" value="'+currentCopyCompartId+'">'+
                    '<input type="hidden" id="pcompartmentseq'+count+'" name="pcompartmentseq'+count+'" value="'+currentCopyCompartment+'">'+
                    '<input type="hidden" id="phsn'+count+'" name="phsn'+count+'" value="'+mainHSN+'">'+
                    '<input type="hidden" id="pcatno'+count+'" name="pcatno'+count+'" value="'+mainCatNo+'">'+
                    '<input type="hidden" id="pqty'+count+'" name="pqty'+count+'" value="'+mainQty+'">'+
                    '<input type="hidden" id="uid'+count+'" name="uid'+count+'" value="'+mainUnitId+'">'+
                    '<input type="hidden" id="unitText'+count+'" name="unitText'+count+'" value="'+mainUnitText+'">'+
                    '<input type="hidden" id="salerate'+count+'" name="salerate'+count+'" value="'+mainsalerate+'">'+
                    '<input type="hidden" id="discount'+count+'" name="discount'+count+'" value="'+mainDis+'">'+
                    '<input type="hidden" id="discountAmount'+count+'" name="discountAmount'+count+'" value="'+mainDisAmount+'">'+
                    '<input type="hidden" id="gstid'+count+'" name="gstid'+count+'" value="'+mainGstid+'">'+
                    '<input type="hidden" id="gstname'+count+'" name="gstname'+count+'" value="'+mainGst+'">'+
                    '<input type="hidden" id="gstamount'+count+'" name="gstamount'+count+'" value="'+mainGSTAmount+'">'+
                    '<input type="hidden" id="amount'+count+'" name="amount'+count+'" value="'+mainTotalAmount+'">'+
                    '<input type="hidden" id="cumulativeAmountId'+count+'" name="cumulativeAmountId'+count+'" value="">'+
                    '<input type="hidden" id="numberofaccessories'+count+'" name="numberofaccessories'+count+'" value="">'+
                    '<td style="width:40px; border: 1px solid black; text-align: center;"> <button type="button" id="addproductnaration'+count+'" onclick="addproductnaration('+count+','+currentCopyCompartment+')"><i class="fa fa-plus"></i></button> &#9670;</td>'+
                    '<td style="width:220px; border: 1px solid black;" onclick="replaceproduct('+count+','+mainId+')" >'+mainName+'</td>'+
                    '<td style="width:80px; border: 1px solid black; text-align: center;" >'+mainCatNo+'</td>'+
                    '<td style="width:40px; border: 1px solid black; text-align: center;">'+
                    '<input class="input-fix" type="number" min="0" step="any"  id="showProductQty'+count+'" value="'+mainQty+'" onchange="rowProductCal('+count+','+showProductQty+')" onfocus="getProductRowOldValueForCal('+count+','+showProductQty+')">'+
                    '</td>'+
                    '<td style="width:40px; border: 1px solid black; text-align: center;" >'+makename+'</td>'+
                    '<td style="width:40px; border: 1px solid black; text-align: center;">'+
                    '<input class="input-fix" type="number" min="0" step="any"  id="showProductRate'+count+'" value="'+mainsalerate+'" onchange="rowProductCal('+count+','+showProductRate+')" onfocus="getProductRowOldValueForCal('+count+','+showProductRate+')">'+
                    '</td>'+
                    '<td style="width:40px; border: 1px solid black; text-align: center;">'+
                    '<input class="input-fix" type="number" min="0" step="any"  max="100" id="showProductDisPer'+count+'" value="'+mainDis+'" onchange="rowProductCal('+count+','+showProductDisPer+')" onfocus="getProductRowOldValueForCal('+count+','+showProductDisPer+')">'+
                    '</td>'+
                    '<td style="width:40px; border: 1px solid black; text-align: center;" class="fedprod"  id="showProductTotalAmount'+count+'">'+mainTotalAmount+'</td>'+
                    '<td style="width:5px; border: 1px solid black; text-align: center;" onclick="removeProduct('+count+','+mainId+')"><i class="fas fa-times" style="color: red;"></i></td>'+  
                    '</tr>';
                     if($("#ntr" + copycount).length > 0 ){
                        var copydesc=$('textarea[name="bompanelproductdesc'+copycount+'"]').text();
                        record+= '<tr id="ntr'+count+'"><td colspan="8" style="border:1px solid black;"><textarea cols="80" rows="4"name="bompanelproductdesc'+count+'">'+copydesc+'</textarea></td><td style="border: 1px solid black; text-align: center;" onclick="removeProductNarration('+currentCopyCompartment+','+count+')"><i class="fas fa-times" style="color: red;"></i></td></tr>';
                    }

    $('#compartmenttable'+currentCopyCompartment).append(record);

    $('#numberOfProductInBill').val(count);   
    cumulativeAmount =Number(parseFloat(mainTotalAmount).toFixed(2)) + Number(parseFloat(cumulativeAmount).toFixed(2));  
    $('#cumulativeAmountId'+count).val(parseFloat(cumulativeAmount).toFixed(2));

    var totalLength=data.accessories.length;

    if(!totalLength || totalLength == undefined){
        totalLength = 0;
        updateBillTotal();
        allClear();
        feedertotal();
        changefeedertotal();
        
    }

    var numberofaccessories = 0;
    if(totalLength != 0){
        tempAccArray = data.accessories;
        
        for (var i = 0; i < totalLength; i++) {
            numberofaccessories++;
            setCopyAeccorisCal(data.accessories[i]);
        }

    }

    
    $('#numberofaccessories'+count).val(numberofaccessories);

}
    
function setCopyAeccorisCal(data) {

    var gstName = "";
    gstmaster.forEach(each => {
        if(each.gstmasterid == data.gstid){
            gstName = each.gstname;
        }
    });

  var result = cal(data.qty,data.salerate,data.discount,gstName);


  setCopysetCopyFromTable(data,result,gstName);
}


function setCopysetCopyFromTable(data,result,gstName) {
    var mainName = data.productName;
    var mainProductId = data.mainPid;
    var mainId = data.productid;
    var makename = data.makename;
   
    var mainQty = result.qty;
    var mainHSN = data.hsn;

    var mainCatNo = data.catno;
    
    var mainUnitId = data.uid;
    var mainUnitText = "";
    var mainsalerate = parseFloat(result.rate).toFixed(2);
    var mainDis = result.discountper;
    var mainDisAmount = result.discountamount;
    var mainGstid = data.gstid;
    var mainGst = gstName+"%"; 
    var mainGSTAmount = result.gstamount;
    var mainAmount = result.amount;
    var mainTotalAmount = result.total;
    

    units.forEach(element => {
        if(element.productunitid == mainUnitId){
            mainUnitText = element.productunitname;
        }
    });

    accessCount++;

    var accessQty = "'"+"#accessQty"+"'";
    var accessDisPer = "'"+"#accessDisPer"+"'";
    var accessRate =   "'"+"#accessRate"+"'";
    //ACC
    var record =    '<tr id="atr'+accessCount+'">'+
                    '<input type="hidden" id="aprodctcountno'+accessCount+'" name="aprodctcountno'+accessCount+'" value="'+count+'">'+
                    '<input type="hidden" id="aiproductid'+accessCount+'" name="aiproductd'+accessCount+'" value="'+mainProductId+'">'+
                    '<input type="hidden" id="aid'+accessCount+'" name="aid'+accessCount+'" value="'+mainId+'">'+
                    '<input type="hidden" id="apanelcompartmentid'+accessCount+'" name="apanelcompartmentid'+accessCount+'" value="'+currentCopyCompartId+'">'+
                    '<input type="hidden" id="acompartmentseq'+accessCount+'" name="acompartmentseq'+accessCount+'" value="'+currentCopyCompartment+'">'+
                    '<input type="hidden" id="aname'+accessCount+'" name="apname'+accessCount+'" value="'+mainName+'">'+
                    '<input type="hidden" id="amake'+accessCount+'" name="amake'+accessCount+'" value="'+makename+'">'+
                    '<input type="hidden" id="ahsn'+accessCount+'" name="ahsn'+accessCount+'" value="'+mainHSN+'">'+
                    '<input type="hidden" id="acatno'+accessCount+'" name="acatno'+accessCount+'" value="'+mainCatNo+'">'+
                    '<input type="hidden" id="aqty'+accessCount+'" name="aqty'+accessCount+'" value="'+mainQty+'">'+
                    '<input type="hidden" id="aunitText'+accessCount+'" name="aunitText'+accessCount+'" value="'+mainUnitText+'">'+
                    '<input type="hidden" id="auid'+accessCount+'" name="auid'+accessCount+'" value="'+mainUnitId+'">'+
                    '<input type="hidden" id="asalerate'+accessCount+'" name="asalerate'+accessCount+'" value="'+mainsalerate+'">'+
                    '<input type="hidden" id="adiscount'+accessCount+'" name="adiscount'+accessCount+'" value="'+mainDis+'">'+
                    '<input type="hidden" id="adiscountAmount'+accessCount+'" name="adiscountAmount'+accessCount+'" value="'+mainDisAmount+'">'+
                    '<input type="hidden" id="agstid'+accessCount+'" name="agstid'+accessCount+'" value="'+mainGstid+'">'+
                    '<input type="hidden" id="agstname'+accessCount+'" name="agstname'+accessCount+'" value="'+mainGst+'">'+
                    '<input type="hidden" id="agstamount'+accessCount+'" name="agstamount'+accessCount+'" value="'+mainGSTAmount+'">'+
                    '<input type="hidden" id="aamount'+accessCount+'" name="aamount'+accessCount+'" value="'+mainTotalAmount+'">'+ 
                    '<td style="width:40px; border: 1px solid black; text-align: center;"></td>'+
                    '<td style="border: 1px solid black;"><i style="font-size:12px !important;">'+mainName+'</i></td>'+
                    '<td style="border: 1px solid black; text-align: center;"><i style="font-size:12px !important;">'+mainCatNo+'</i></td>'+
                    '<td style="border: 1px solid black; text-align: center;">'+
                    '<input class="input-fix" style="font-style:italic; font-size:12px !important;" type="number" min="0" step="any"  id="accessQty'+accessCount+'" value="'+mainQty+'" onchange="rowAccCal('+count+','+accessCount+','+accessQty+')" onfocus="getAccRowOldValueForCal('+count+','+accessCount+','+accessQty+')">'+
                    '</td>'+
                    '<td style="border: 1px solid black; text-align: center;"><i style="font-size:12px !important;">'+makename+'</i></td>'+
                    '<td style="border: 1px solid black; text-align: center;">'+
                    '<input class="input-fix" style="font-style:italic; font-size:12px !important;" type="number" min="0" step="any"  id="accessRate'+accessCount+'" value="'+mainsalerate+'" onchange="rowAccCal('+count+','+accessCount+','+accessRate+')" onfocus="getAccRowOldValueForCal('+count+','+accessCount+','+accessRate+')">'+
                    '</td>'+
                    '<td style="border: 1px solid black; text-align: center;">'+
                    '<input class="input-fix" style="font-style:italic; font-size:12px !important;" type="number" min="0" step="any"  id="accessDisPer'+accessCount+'" value="'+mainDis+'" onchange="rowAccCal('+count+','+accessCount+','+accessDisPer+')" onfocus="getAccRowOldValueForCal('+count+','+accessCount+','+accessDisPer+')">'+
                    '</td>'+
                    '<td style="border: 1px solid black; text-align: center;" class="fedacc" ><i style="font-size:12px !important;" id="accessTotalAmount'+accessCount+'">'+mainTotalAmount+'</i></td>'+
                    '<td style="border: 1px solid black; text-align: center;" onclick="removeProductAcc('+accessCount+','+mainId+','+mainProductId+')"><i class="fas fa-times" style="color: red;"></i></td>'+ 
                    '</tr>';
    
    $('#compartmenttable'+currentCopyCompartment).append(record);
    
    $('#numberOfAccessoriesInBill').val(accessCount);
    
    cumulativeAmount =Number(parseFloat(mainTotalAmount).toFixed(2)) + Number( parseFloat(cumulativeAmount).toFixed(2));

    $('#cumulativeAmountId'+count).val(parseFloat(cumulativeAmount).toFixed(2));
    updateBillTotal();
    feedertotal();
    changefeedertotal();

}


//===================================End Compartment Copy================================================================================


// --------------------------------------------- Master Calculat   -----------------------
// parameters 
/* 
    1) Qty
    2) Rate
    3) Discount Per(%)
    4) GST Per(%)
    
    return value
       1) rate 
       2) qty 
       3) amount 
       4) discountper 
       5) discountamount 
       6) gstper 
       7) gstamount 
       8) total  
*/
function cal(calqty,calrate,caldisper,calgstper){

    var retRate = 0;
    var retQty = 0;
    var retAmount = 0;
    var retDisPer = 0;
    var retDisAmount = 0;
    var retGstper = 0;
    var retGstAmount = 0;
    var retTotal = 0;


    if(calrate == "" || calrate === NaN || !calrate){
        retRate = 0;
        calrate =0; 
    }
  
    if(calqty == "" || calqty === NaN || !calqty){
        retQty = 0; 
        calqty= 0;
    }

    if(calrate != "" && calrate !== NaN && calrate || calrate >= 0){
        retRate = parseFloat(calrate);
        retQty = parseFloat(calqty);
        retAmount = retRate*retQty;
        var currentDicount = parseFloat(caldisper);
       
        if(currentDicount || currentDicount == 0){
            if(currentDicount > 100){
                alert('Maximum Discount 100%');
                retDisPer = 0;
                retDisAmount = 0;
            }else{
                var discountAmount =  ((currentDicount*(retRate*retQty)/100));
                retDisPer = currentDicount;
                retDisAmount = discountAmount;
            }
        }else{
            if(currentDicount > 100){
                alert('Maximum Discount 100%');
            }
            retDisPer = 0;
            retDisAmount = 0;
          
        }

        var currentDicountAmount =  parseFloat(retDisAmount);
        calgstper=0;
        if(calgstper && calgstper != "" && calgstper != undefined){
            var newRate = (retRate*retQty)-currentDicountAmount;
            retGstper = parseFloat(calgstper);
            retGstAmount = (retGstper*newRate)/100;
            retTotal = retGstAmount+newRate;
        }else{
            retGstAmount = 0;
            retTotal = (retRate*retQty)-currentDicountAmount;
        }
    }

    var result = {
       rate : retRate.toFixed(2),
       qty :retQty.toFixed(2),
       amount : retAmount.toFixed(2),
       discountper : retDisPer.toFixed(2),
       discountamount : retDisAmount.toFixed(2),
       gstper : retGstper.toFixed(2),
       gstamount : retGstAmount.toFixed(2),
       total : retTotal.toFixed(2) 
    }
    return result;
}

//--------------------------------------------- remove product ---------------------------
function  removeProduct(pCount,pId) {

    var numAcc = parseInt($('#numberofaccessories'+pCount).val());

    var currentTable = $('#pcompartmentseq'+pCount).val(); 
 $('#compartmenttable'+currentTable).find('#ntr'+pCount+'').remove();
    $('textarea#addnarrationtoproduct').val('');
    if(numAcc == 0 ){
        $('#tr'+pCount).remove();
    }else{
        for(var i = 1 ; i <= accessCount;i++){
            if($('#aiproductid'+i).length){
                
                var mainId = parseInt($('#aiproductid'+i).val());
                var countNo =parseInt($('#aprodctcountno'+i).val());
                if(mainId == pId &&  countNo == pCount){
                     $('#atr'+i).remove();
                }
            }
        }
        $('#tr'+pCount).remove();
    }

     $('#ntr'+pCount).remove();

    for (let i = 0; i < productList.length; i++) {
        if(productList[i].compartmentseqence == currentTable){
             var temp = productList[i].products;
            for (let k = 0; k < productList[i].products.length; k++) {
                if(productList[i].products[k].productid == pId){
                    temp.splice(k, 1);
                }
            }
            productList[i].products = temp;
        }
    }

   
    updateBillTotal();
    changefeedertotal();

}

function  removeProductAcc(aCount,pId,mainId) {
   
    var pcount =  $('#aprodctcountno'+aCount).val();
    $('#atr'+aCount).remove();
    var currentAcc = Number($('#numberofaccessories'+pcount).val())-1;
    $('#numberofaccessories'+pcount).val(currentAcc);

    if(currentAcc > 0){
        var mPid = $('#pid'+pcount).val();
        var mCId = $('#pcompartmentseq'+pcount).val();
        //console.log(pcount,mPid,mCId);
        if(mPid && mPid != undefined && mPid != "" && $('#pid'+pcount).length && mCId && mCId != undefined && mCId != "" && $('#pcompartmentseq'+pcount).length){
            var total = parseFloat($('#amount'+pcount).val()).toFixed(2);
           // console.log(mPid,mCId);
            for(var i = 1;i <= accessCount;i++){
                var c = $('#aamount'+i).val();
                if(c && c != undefined && c != "" && $('#aamount'+i).length){
                    var aPid = $('#aiproductid'+i).val();
                    var aCid = $('#acompartmentseq'+i).val();
                    if(aPid == mPid && mCId == aCid){
                        total = Number(parseFloat($('#aamount'+i).val()).toFixed(2)) + Number(total);
                    }
                }
            }
            $('#cumulativeAmountId'+pcount).val(parseFloat(total).toFixed(2));
        }
    }else{
        var result = $('#amount'+pcount).val();
        $('#cumulativeAmountId'+pcount).val(parseFloat(result).toFixed(2));
    }
     updateBillTotal();
     feedertotal();
     changefeedertotal();
}

// ---------------------------------------------checkChange on other compartment -------------------
function  changeOther(id) {

    var status = $('#checkOther'+id).prop('checked');
   
    if(!status){
        var tableId = "#compartmenttable"+id;
        $(tableId+' > tbody').empty();
        for (let i = 0; i < productList.length; i++) {
            if(productList[i].compartmentseqence == id){
                productList[i].products = [];
                break;
            }
        }
        updateBillTotal();
    }

}

//----------------------------------------------------update total----------------------------------------
function updateBillTotal(){
    calBillTotal();
    //calculate();
    calBillGSTTotal(); 
} 

function calBillTotal(){
    var numberOfAccss = parseInt($('#numberOfAccessoriesInBill').val());
    var numberOfProduct = parseInt($('#numberOfProductInBill').val());
   
    var dicamount = 0;
    var subamount = parseInt($('#billSubAmount').val());
    var cumulativeAmount = 0;

 

    //for product

    // for (var index = 1; index <= Number(numberOfProduct); index++) {

    //     //Discount
    //     var dictemp = $('#discountAmount'+index).val(); 
    //     if(dictemp && dictemp != undefined && dictemp != "" && $('#discountAmount'+index).length){
    //         dicamount = parseFloat(dictemp)+dicamount;
    //     }
       
    //     //SubTotal
    //     var tempGstamount = $('#gstamount'+index).val();
    //     var tempamount = $('#amount'+index).val();
    //     if(tempamount && tempamount != undefined && tempamount != "" && $('#gstamount'+index).length){
    //         subamount = parseFloat(tempamount)+Number(subamount)-parseFloat(tempGstamount);
    //     }
    //     if(dictemp && dictemp != undefined && dictemp != ""  && $('#discountAmount'+index).length){
    //         subamount = parseFloat(dictemp)+subamount;
    //     }

    //     //Total
    //     var tempMainAmount = $('#showProductTotalAmount'+index).text();
    //     if(tempMainAmount && tempMainAmount != undefined && tempMainAmount != "" && $('#showProductTotalAmount'+index).length){
    //         cumulativeAmount = parseFloat(tempMainAmount)+Number(cumulativeAmount);
    //     }
        

    // }

    // //for Acc
    // for (var index = 1; index <= Number(numberOfAccss); index++) {
    //     //Discount
    //     dictemp = $('#adiscountAmount'+index).val(); 
    //     if(dictemp && dictemp != undefined && dictemp != "" &&  $('#adiscountAmount'+index).length){
    //         dicamount = parseFloat(dictemp)+dicamount;
    //     }

    //     //SubTotal
    //     var tempGstamount = $('#agstamount'+index).val();
    //     var tempamount = $('#aamount'+index).val();
    //     if(tempamount && tempamount != undefined && tempamount != "" &&  $('#aamount'+index).length){
    //         subamount = parseFloat(tempamount)+Number(subamount)-parseFloat(tempGstamount);
    //     }
    //     if(dictemp && dictemp != undefined && dictemp != "" &&  $('#adiscountAmount'+index).length){
    //         subamount = parseFloat(dictemp)+subamount;
    //     }
        
    //      //Total
    //      var tempMainAmount = $('#accessTotalAmount'+index).text();
    //      if(tempMainAmount && tempMainAmount != undefined && tempMainAmount != "" && $('#accessTotalAmount'+index).length){
    //          cumulativeAmount = parseFloat(tempMainAmount)+Number(cumulativeAmount);
    //      }
        
    // }

    if($('#billextradiscount').val() > 0 ){
        var extradiscount =$('#billextradiscount').val();
        dicamount = dicamount + Number(extradiscount);
    }
    $('#ItemDiscount').val(dicamount.toFixed(2));
    //$('#ItemSubAmount').val(subamount.toFixed(2));
    $('#ItemAmount').val(cumulativeAmount.toFixed(2));

  
}

function calBillGSTTotal(){
    // var numberOfAccss = parseInt($('#numberOfAccessoriesInBill').val());
    // var numberOfProduct = parseInt($('#numberOfProductInBill').val());

    // gstArray = [];

    // gstmaster.forEach(element => {
    //     $('#trGST'+element.gstmasterid).hide();
    //     var temp = {
    //         gstID : element.gstmasterid,
    //         total : 0
    //     }
    //     gstArray.push(temp);
    // });
      

    // for (let index = 1; index <= numberOfProduct; index++) {
    //     if($('#gstid'+index).length){
    //         var id = $('#gstid'+index).val();
    //         var gstAmount = $('#gstamount'+index).val();
    //         gstArray.forEach(element => {
    //             if(element.gstID == id){
    //                 var tempTotal = element.total + parseFloat(gstAmount);
    //                 element.total = parseFloat(tempTotal);
    //             }
    //         });
    //     }
       
    // }

    // for (let index = 1; index <= numberOfAccss; index++) {
    //     if($('#agstid'+index).length){
    //         var id = $('#agstid'+index).val();
    //         var gstAmount = $('#agstamount'+index).val();
    //         gstArray.forEach(element => {
    //             if(element.gstID == id){
    //                 var tempTotal = element.total + parseFloat(gstAmount);
    //                 element.total = parseFloat(tempTotal);
    //             }
    //         });
    //     }
       
    // }

    

      //set gst value in input hidden 
    /* gstArray.forEach(element => {
        if(element.total != 0){
            $('#gstAmount'+element.gstID).val(element.total.toFixed(2));
        }else{
            $('#gstAmount'+element.gstID).val(0);
        }
    }); */
    calLaborAmount();
}

// ----------------------------------------Profit and Labor GST Cal-------------------------------------------------------

function calLaborAmount() {
    var laborPer =Number($('#laborpername').val());
    $('#laborper').val(laborPer);
    var billAmount = Number($('#ItemSubAmount').val());
    
 
   
    var temp = (billAmount*laborPer)/100;
    $('#laborPrice').val(parseFloat(temp).toFixed(2));
  
    var laborGst = (temp*gstDefault)/100;  
    $('#laborgstamount').val(parseFloat(laborGst).toFixed(2));

    var temp = Number(temp)+Number(laborGst);
    $('#laborAmount').val(parseFloat(temp).toFixed(2));

    calProfit();
}

function calProfit() {

    var profitPer = Number($('#profitpername').val());
    $('#profitPer').val(profitPer);
    var billAmount = Number($('#ItemSubAmount').val());
    var laborAmount = $('#laborPrice').val();

    var temp = Number(billAmount) + Number(laborAmount);
    temp = Number((temp*profitPer)/100);
    $('#profitPrice').val(parseFloat(temp).toFixed(2));

    var gstAmount =  Number((temp*gstDefault)/100);
    $('#profitgstAmount').val(parseFloat(gstAmount).toFixed(2));

    var temp = Number(gstAmount)+Number(temp);
    $('#profitAmount').val(parseFloat(temp).toFixed(2));

    addlaborAndProfitTex();


}

function addlaborAndProfitTex() {
    var laborgst =  $('#laborgstamount').val();
    var profitgst =  $('#profitgstAmount').val();

    gstArray.forEach(element => {
        if(element.gstID == gstDefaultId){
            var temp = Number(element.total) + Number(profitgst) + Number(laborgst);
            element.total = parseFloat(temp).toFixed(2);
        }
    });


    onTransportChange();
}

//-------------------------------------Transport charge------------------------------------------------

function  onTransportChange() {
    var transportCharge = parseFloat($('#billTransport').val()).toFixed(2);

   
    if(transportCharge == "" || isNaN(transportCharge)){
        $('#billTransport').val('0.00');
        transportCharge = 0;
    }
    
    // var trasGST = (transportCharge*gstDefault)/100;
    var trasGST = 0;
    var trasAmount = Number(transportCharge)+Number(trasGST) ;

    $('#transportAmount').val(trasAmount);
    $('#transportGST').val(trasGST);

    gstArray.forEach(element => {
        if(element.gstID == gstDefaultId){
            var temp = Number(element.total) + Number(trasGST);
            element.total = parseFloat(temp).toFixed(2);
        }
    });
 

    onPackageChange();
}

function  onPackageChange() {
    var packageCharge = parseFloat($('#billpackage').val()).toFixed(2);

   
    if(packageCharge == "" || isNaN(packageCharge)){
        $('#billpackage').val('0.00');
        packageCharge = 0;
    }
    
    // var trasGST = (transportCharge*gstDefault)/100;
    var packGST = 0;
    var packAmount = Number(packageCharge)+Number(packGST) ;

    $('#packageAmount').val(packAmount);
    $('#packageGST').val(packGST);

    gstArray.forEach(element => {
        if(element.gstID == gstDefaultId){
            var temp = Number(element.total) + Number(packGST);
            element.total = parseFloat(temp).toFixed(2);
        }
    });

    roundOff();
}

function roundOff() {
    
    var roundOff = $('#billroundoffamount').val();

    if(roundOff == "" || isNaN(roundOff)){
        $('#billroundoffamount').val('0.00');
        packageCharge = 0;
    }
    setFianlTotal();
}

//------------------------------------- final total----------------------------------------------------

function setFianlTotal() {
  
    $('#billDiscountHidden').val($('#ItemDiscount').val());
    $('#billDiscount').val($('#ItemDiscount').val());
    //console.log('ItemSubAmount'+$('#ItemSubAmount').val());
    var displaysubtotal = Number($('#ItemSubAmount').val());
    var subTotal = Number($('#ItemSubAmount').val());
    $('#billSubAmountHidden').val(parseFloat(subTotal).toFixed(2));
   // $('#billSubAmount').val(parseFloat(displaysubtotal).toFixed(2));

    var gstTotal = $('#defultGst').val();
    var roundOff = $('#billroundoffamount').val();

    gstArray.forEach(element => {

        gstTotal = Number(gstTotal) + Number(element.total);
        if(element.total != 0){
            $('#trGST'+element.gstID).show();
            $('#billGstAmount'+element.gstID).val(parseFloat(element.total).toFixed(2));
            $('#gstAmount'+element.gstID).val(parseFloat(element.total).toFixed(2));
        }else{
            $('#gstAmount'+element.gstID).val(0);
        }
    });

    $('#billGSTAmountHidden').val(parseFloat(gstTotal).toFixed(2));
     // Item + Labor + Profit + Transport + Packaging  + round off
     var FianlTotal = Number($('#billSubAmount').val())-Number($('#ItemDiscount').val());
     var aftergst=0;
    aftergst=Number(parseFloat(FianlTotal)*(Number(gstTotal)/100));
    FianlTotal=FianlTotal+aftergst;
    $('#totalgst').val(aftergst.toFixed(2));
    FianlTotal=FianlTotal+Number($('#transportAmount').val())+Number($('#packageAmount').val());
     // console.log('FianlTotal'+FianlTotal);
     // if(!FianlTotal >= 0){
     //      location.reload();
     // }

    $('#billAmount').val(parseFloat(FianlTotal).toFixed(2));
    $('#billTotalAmount').val(parseFloat(FianlTotal).toFixed(2));
    FianlTotal = Number($('#billAmount').val());
    
    if(roundOff < 0){
   
        FianlTotal=FianlTotal - Math.abs(roundOff);
      
        
    }
     if(roundOff > 0){
      
        FianlTotal=FianlTotal + Math.abs(roundOff);

    }
    // console.log(FianlTotal);
    $('#billAmount').val(parseFloat(FianlTotal).toFixed(2));
    $('#billTotalAmount').val(parseFloat(FianlTotal).toFixed(2));

}

// ---------------------------------------------------------------------------------------
function changeValid(){
    var t = $('#validityBOMId :selected').text();
    $("#validPeriod").val(t);
}

function changeScopeOfwork(){
    var t = $('#scopeofworkId :selected').text();
    $("#scopeofwork").val(t);
}

function changePaymenTterm(){
    
    var t = $('#paymenttermid :selected').text();
    $("#paymentterm").val(t);
}

function changeNarration() {
    var t = $('#bomnarrationid :selected').text();
    $("#bomnarration").val(t);
}

// -----------------------------------------------------------------------------------------
// submit verification
function  onFormSubmit() {  

    var validArray = [];
    productList.forEach(element => {
        var comProductList = element.products;
        comProductList.forEach(element2 => {
            validArray.push(element2);
        });
    });

    if(validArray.length == 0){
        alert("Add some product in bom");
        return false;
    }else{
        return true;
    }

    
}

//------------------------------------------------------------------------------------------

var currentMasterType = 0;
var title = "";

function addMaster(pCurrent) {
    currentMasterType =pCurrent;

    if(currentMasterType == 1){
        title = "Narration"
    }else if(currentMasterType == 2){
        title = "Validity Period"
    }else if(currentMasterType == 3){
        title = "Scope of Work";
    }else if(currentMasterType == 4){
        title = "Payment Terms";
    }else{
        return;
    }
    $('#masterModelTitle').text("Add "+title);
    $('#subTitle').text(title);

    $('#addMasterModel').modal('show');
}

function addToMasterAjax() {
    var url = $('#commonURL').val();
    var _token = $('input[name="_token"]').val();
    var name = $('#newmastername').val();
    if(name){
       
        $.ajax({
            type : 'POST', 
            url : url,
            data : {name:name, type:currentMasterType,_token : _token},
            success : function(data){
               //console.log(data);   
               if(data.state){
                    updateMasterData(data);
                    $('#addMasterModel').modal('hide');
                    $('#newmastername').val('');
               }else{
                   alert(data.message);
               }
            }
        });
    }else{
        alert("Please Enter "+title);
    }
}

function updateMasterData(data) {
    if(currentMasterType == 1){
        var html = '<option value="">--Select Narration Terms--</option>';
                    
        data.data.forEach(element => {
            html = html+'<option value="'+element.bomnarrationid+'">'+element.bomnarrationname+'</option>';
        });

        $('#bomnarrationid').empty();
        $('#bomnarrationid').append(html);
        $('#bomnarrationid').val(data.id).trigger('change');
    }else if(currentMasterType == 2){
        var html = '<option value="">--Select Validity Period Terms--</option>';
                    
        data.data.forEach(element => {
            html = html+'<option value="'+element.validitybomid+'">'+element.validityPeriod +'</option>';
        });

        $('#validityBOMId').empty();
        $('#validityBOMId').append(html);
        $('#validityBOMId').val(data.id).trigger('change');
        
    }else if(currentMasterType == 3){
        var html = '<option value="">--Select Scope Of Work--</option>';
                    
        data.data.forEach(element => {
            html = html+'<option value="'+element.scopeofworkid+'">'+element.scopeofwork +'</option>';
        });

        $('#scopeofworkId').empty();
        $('#scopeofworkId').append(html);
        $('#scopeofworkId').val(data.id).trigger('change');
       
    }else if(currentMasterType == 4){
        var html = '<option value="">--Select  Payment Terms--</option>';
                    
        data.data.forEach(element => {
            html = html+'<option value="'+element.paymenttermid+'">'+element.paymentterm +'</option>';
        });

        $('#paymenttermid').empty();
        $('#paymenttermid').append(html);
        $('#paymenttermid').val(data.id).trigger('change');
    }else{
        return;
    }
    currentMasterType =0;
}

// ---------------------------------------------------------------------------------------------

function changeIinquiryPanel() {
    $('#changePanelModel').modal('show');
}

function applyChangePanel() {
    var id = $('#changePanelId').val();

    inquiryPanelInfo.forEach(element => {
        if(id == element.inquirypanelinfoid){
            $('#description').text('');
            $('#description').text(element.description);
        }
    });
    $('#changePanelModel').modal('hide');
}
function addproductnaration(productid,currentClickTableId){
 
    if($("#ntr" + productid).length == 0) {
        $('#addproductnarationmodal').modal();
        $('#modalproductid').val('');
        $('#modalproductid').val(productid);
        $('#modalcurrentClickTableId').val(currentClickTableId);
      }else{
        alert('Product Description already Exist');
      }

}
function addnarrationtoproduct(){
  var modalproductid  =  $('#modalproductid').val();
  var modalcurrentClickTableId  =  $('#modalcurrentClickTableId').val();
  var addnarrationtoproduct = $('textarea#addnarrationtoproduct').val();
  var appendnarrtion = '<tr id="ntr'+modalproductid+'"><td colspan="8" style="border:1px solid black;"><textarea cols="80" rows="4"name="bompanelproductdesc'+modalproductid+'">'+addnarrationtoproduct+'</textarea></td><td style="border: 1px solid black; text-align: center;" onclick="removeProductNarration('+modalcurrentClickTableId+','+modalproductid+')"><i class="fas fa-times" style="color: red;"></i></td></tr>';
//   var addnarrationtoproduct  =  $('#addnarrationtoproduct').html();

  $('#compartmenttable'+modalcurrentClickTableId).find('#tr'+modalproductid+'').after(appendnarrtion);
  $('#addproductnarationmodal').modal('hide');
  $('textarea#addnarrationtoproduct').val('');
}
function removeProductNarration(modalcurrentClickTableId,modalproductid){
    $('#compartmenttable'+modalcurrentClickTableId).find('#ntr'+modalproductid+'').remove();
    $('textarea#addnarrationtoproduct').val('');
}
function feedertotal(){
 
    var noff = $('#numberOffeeder').val();
    // console.log('numberOfFeeder'+numberOfFeeder);
    var comptarray = [];
    var discountarray =[];
    
   
    for(var index = 0; index < noff ; index++) {
        // $('#feedertotal'+feeders[index].feederid).val(0);
    
            var compartments = feeders[index].compatments;
           // console.log(feeders[0].compatments);
            for (let k = 0; k < compartments.length; k++) {
              
                if(compartments[k].compartmentseqence){
                    compseq = compartments[k].compartmentseqence;
                   
                   
                    var trlength=$('#compartmenttable'+compseq+' tbody tr').length;
                 
                    if(trlength != undefined){
                        if(discountarray.some(ele => ele.feederid === feeders[index].feederid)){
                           
                        } else{
                            discountarray.push({
                                feederid:feeders[index].feederid,
                                discount:0,
 
                            });
                        }
                      
                        comptarray.push({
                            feederid:feeders[index].feederid,
                            compseq:compseq, 
                            trlength:trlength,
                            total:0,
                            discount:0,
                        });
                       
                    }
                  
                }
            }
           
    }
    globaldiscountarray=discountarray;
    calfedeertotal(comptarray);
 
  
}

function calfedeertotal(comptarray){

var numberOfAccss = parseInt($('#numberOfAccessoriesInBill').val());
var numberOfProduct = parseInt($('#numberOfProductInBill').val());
var currentacce=1;
var currentprod=1;
     for(var j=0; j<=comptarray.length;j++){
        var total=0;

        if(comptarray[j]){

            for(var temp=1;temp<=comptarray[j].trlength;temp++){

                // console.log(comptarray[j].compseq);
                $('#feedertotal'+comptarray[j].feederid).val(0);

                total=0;
                   
                        $('#compartmenttable'+comptarray[j].compseq+' tbody').find('.fedacc').each(function(){
                            total+=Number($(this).text());
                        });
                        $('#compartmenttable'+comptarray[j].compseq+' tbody').find('.fedprod').each(function(){
                            total+=Number($(this).text());
                        });
                        
              
            }
            comptarray[j].total =  comptarray[j].total + total;
        //    console.log(comptarray);
            $('#feedertotal'+comptarray[j].feederid).val(0);
        }
      
        
    }
    compfinalarray = comptarray;
   
    //  console.log(discountarray);
    //  console.log(discountarray);
    
    globaldiscountarray.forEach(element => {
        if($('#feederdiscinput'+element.feederid).length){
            var feederdisc= $('#feederdiscinput'+element.feederid).val();
            if(feederdisc > 0 ){
                element.discount = element.discount + feederdisc;
            }
        }
    });
    globaldiscountarray = globaldiscountarray;
        comptarray.forEach(element => {
            if($('#feederdiscinput'+element.feederid).length){
                var feederdisc= $('#feederdiscinput'+element.feederid).val();
                if(feederdisc > 0 ){
                    element.discount = element.discount + feederdisc;
                }
            }
    
        // $('#feedertotal'+element.feederid).val(element.total);
        if($('#feedertotal'+element.feederid).val() > 0 ){
            var temptotal=0;
            temptotal = Number($('#feedertotal'+element.feederid).val());
            temptotal= temptotal + Number(element.total);
            var ftt=$('#feedertotal'+element.feederid).val(); 
             
            temptotal = temptotal;
            if(temptotal < 0 ){

            }else{
                temptotal = temptotal.toFixed(2);
                $('#feedertotal'+element.feederid).val(temptotal);
                // var ftt=$('#feedertotal'+element.feederid).val(); 
                //         ftt=Number(ftt)-element.discount;
                    
                //         $('#feedertotal'+element.feederid).val(ftt);
            }
           
        }else{
            var temptotal=0;
            temptotal = element.total;
            if(temptotal < 0 ){

            }else{
            temptotal = temptotal.toFixed(2);
            $('#feedertotal'+element.feederid).val(temptotal);
  
            }
        }
        compfinalarray = comptarray;

        // $('#compartmenttable'+element.compseq).append( element.total);
    });
   

}

function addfeederdis(feederid){

   if($('#feederdiscinput'+feederid).length){
    alert('Already Exist');
   }else{
    var feederdisc = '&nbsp; <input type="text"  id="feederdiscinput'+feederid+'" Placeholder="Discount" onfocusOut="changefeedertotal()">';
    $('#addfeederdis'+feederid).after(feederdisc);
   }
    
}

function changefeedertotal(){
    feedertotal();
    var subamt=0;
    var fdrtotal=0;
    globaldiscountarray.forEach(element => {
           
        $('#feedertotal'+element.feederid).val();
        var fdrtotalinit=Number($('#feedertotal'+element.feederid).val()); 
        
        fdrtotal=fdrtotalinit-Number(element.discount);
        if(fdrtotal >= 0)
        {
            subamt=subamt+Number(fdrtotal.toFixed(2));
            $('#feedertotal'+element.feederid).val(fdrtotal.toFixed(2));
            // console.log('subamt'+subamt);
            $('#billSubAmount').val(parseFloat(subamt));
            $('#ItemSubAmount').val(parseFloat(subamt));
            
            calLaborAmount();
        }else{
            $('#feederdiscinput'+element.feederid).val('0');
            $('#feedertotal'+element.feederid).val(fdrtotalinit.toFixed(2));
                      
        }
        

        
        
        });
        var subtotal = Number($('#billSubAmount').val());
        $('#ItemSubAmount').val(parseFloat(subtotal).toFixed(2));

        subtotal=subtotal+Number($('#laborAmount').val())+Number($('#profitAmount').val());
      
        $('#billSubAmount').val(parseFloat(subtotal).toFixed(2));
        var labornprofitamt=0;
        var laboramt=Number($('#laborAmount').val());
        var profitamt=Number($('#profitAmount').val());
        labornprofitamt=laboramt+profitamt;
        
        $('#labpro').val(labornprofitamt.toFixed(2));
        setFianlTotal();
    
}
