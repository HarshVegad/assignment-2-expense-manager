var currentType;
var selctedResult = [];
var count = 0;
var accessCount = 0;
var srNo = 0;
var cumulativeAmount;
var productAddType = 0;
 
$( document ).ready(function() {
    
    currentType = 0;
    $(window).keydown(function(event){
        if(event.keyCode == 13) {
          event.preventDefault();
          return false;
        }
      });
    
    $('input[type=radio][name=productType]').change(function() {
        if (this.value == 'BOM') {
            // setAsBom();
            currentType = 0;
        }else if (this.value == 'Product') {
            // setAsProduct();
            currentType = 1;
            $("#bomid").val('').trigger('change');
            $('#bomHSN_error').hide();
            $('#bomName_error').hide();
            clearBomValue();
        }
    });

    $('input[type=radio][name=addproductType]').change(function() {
        if (this.value == 'Specific') {
            $('#addproductTypeSpecific').show();
            $('#productaccessoriesdiv').hide(); 
            $('#tableListAccessories tbody').empty();
            $('#productid').empty();
            productAddType = 0;
        }else if (this.value == 'Genral') {
            $('#addproductTypeSpecific').hide();
            productAddType = 1;
            genralProductAjax();
        }
    });

 
});

function openModel(){
    if(currentType == 0){
        $('#bomModel').modal('show');
    }else if(currentType == 1){
        if(productAddType == 1){
            genralProductAjax();
        }
        $('#productModel').modal('show');
    }
}

function bomChange(){
    var bomid = $('#bomid').val();
    clearBomValue();
    for(var i = 0; i<boms.length ; i++){
      if(bomid == boms[i].bomid){
            setBomValue(i);
            break;
        }
    }
}

function changeCompany(){
    let company = $('#companyid').val();
    $('#productgroupid').val('').trigger('change');
    $('#productid').empty();
    $('#productsubgroupid').empty();

}

function groupChange(){

    let productgroupid = $('#productgroupid').val();
    var url = $('#urlGetProductGroup').val();
    
    var _token = $('input[name="_token"]').val();
    let company = $('#companyid').val();

    $('#productaccessoriesdiv').hide();
    $('#tableListAccessories tbody').empty();


    $('#productsubgroupid').empty();
    $('#productid').empty();
    if(productgroupid){
        $.ajax({
            type : 'POST',
            url : url,
            data : {productgrouptypeid:productgroupid, _token : _token},
            success : function(data){
              
                $('#productsubgroupid').append(data);
            }
        });
    }

}

function subGroupChange(){

    let productsubgroup = $("#productsubgroupid").val();
    let productgroup = $('#productgroupid').val();
    let company = $('#companyid').val();
    var _token = $('input[name="_token"]').val();
    var url = $('#urlGetProduct').val();

    if(company == ""){
        alert("Please Select Make");
        return;
    }

    $('#productaccessoriesdiv').hide(); 
    $('#tableListAccessories tbody').empty();
    $('#productid').empty();
    if(productsubgroup){
        $.ajax({
            type : 'GET',
            url : url,
            data : {groupid:productsubgroup, grouptypeid:productgroup, companyid:company, _token : _token},
            success : function(data){
                $('#productid').append(data);
            }
        });
    }

}

function genralProductAjax(){
    var url = $('#urlGetGenralProduct').val();
    $('#productaccessoriesdiv').hide(); 
    $('#tableListAccessories tbody').empty();
    $('#productid').empty();
    $.ajax({
        type : 'GET',
        url : url,
        success : function(data){
            $('#productid').append(data);
        }
    });

}

function productChange(){

    var productid = $('#productid').val();
    var urlaccessories = $('#urlaccessories').val();
    var _token = $('input[name="_token"]').val();

    $('#productaccessoriesdiv').hide();
    $('#tableListAccessories tbody').html('');

    if(productid){
        $.ajax({
            type : 'POST',
            url : urlaccessories,
            data : {productid:productid, _token : _token},
            success : function(data){
                $('#productaccessoriesdiv').show();
                $('#tableListAccessories tbody').append(data);
            }
        });
    }
    
  
    
}

function productreset(){
   
    $('#productid').empty();
    $('#productsubgroupid').empty();
    $('#productgroupid').val('').trigger('change');
    $('#companyid').val('').trigger('change');

}

function changeqty(pcount){
    var id='#tdqty'+pcount;
    let $this = $(id);
    let qty = $('#tdqty'+pcount).text();
    let $input = $('<input>', {
        type: 'number',
        blur : function(){
            $this.text($(this).val());
            if(($(this).val()).trim() == "" || $(this).val() == null){
               $this.text(qty);
            }
        },
        keyup : function(e){
            if (e.which === 13) $input.blur();
        }
    }).appendTo( $this.empty() ).focus()
}

function clearBomValue(){
    $('#bomName').val("");
    $('#bomHSN').val("");
    $('#bomRate').val("");
    $('#bomQTY').val("");
    $('#bomAmount').val('');
    $("#gstbomid").val('').trigger('change');
    $('#gstBOMAmount').val('');
    $('#discountid').val(0);
    $('#discountAmount').val(0);
    // $('#bomid').val('').trigger('change');   
}

function setBomValue(i){
    var qtyGSTAmount = 1;
    
    if(bomItemMaster[0].bomitemmastername != null){
        $('#bomName').val(bomItemMaster[0].bomitemmastername);
    }
    if(bomItemMaster[0].bomitemmasterhsn != null){
        $('#bomHSN').val(bomItemMaster[0].bomitemmasterhsn);
    }
    if(boms[i].bomamount != null){
        $('#bomRate').val(boms[i].bomamount);
    }
    if(boms[i].qty != null){
        qtyGSTAmount = parseFloat(boms[i].qty);
        $('#bomQTY').val(boms[i].qty);
    }
    if(boms[i].gstmasterid != null){
        $('#gstbomid').val(boms[i].gstmasterid);     
        $('#gstbomid').trigger('change');
    }
    if(boms[i].bomgstamount != null){
        var gstAmount = parseFloat(boms[i].bomgstamount)*qtyGSTAmount;
        $('#gstBOMAmount').val(gstAmount);
    }
    if(boms[i].bomtotalamount != null){
        var totalAmount = parseFloat(boms[i].bomtotalamount)*qtyGSTAmount;
        $('#bomAmount').val(totalAmount);
    }
}

function calGST(){
    var rate = parseFloat($('#bomRate').val());
    var currentGst = $('#gstbomid').val();
    var currentTxt;
    var qty = parseFloat($('#bomQTY').val());
  
    if(rate == "" || rate === NaN || !rate){
        rate = 0; 
        $('#bomRate').val(0);
    }

    if(qty == "" || qty === NaN || !qty){
        qty = 1; 
        $('#bomQTY').val(1);
    }

    if(rate != "" && rate !== NaN && rate || rate >= 0){

        var currentDicount = parseFloat($('#discountid').val());
        if(currentDicount || currentDicount == 0){
            if(currentDicount > 100){
                alert('Maximum Discount 100%');
                $('#discountid').val(0);
                $('#discountAmount').val(0);
            }else{
                var discountAmount =  ((currentDicount*(rate*qty)/100));
                $('#discountAmount').val(discountAmount.toFixed(2));
            }
        }else{
            if(currentDicount > 100){
                alert('Maximum Discount 100%');
            }
            $('#discountid').val(0);
            $('#discountAmount').val(0);
        }

        var currentDicountAmount =  parseFloat($('#discountAmount').val());

        if(currentGst && currentGst != ""){
            gstmaster.forEach(element => {
                if(element.gstmasterid == currentGst){
                    var newRate = (rate*qty)-currentDicountAmount;
                    currentTxt = parseFloat(element.gstname);
                    updateGSTPrice = (currentTxt*newRate)/100;
                    updateTotalAmount = updateGSTPrice+newRate;
                    $('#bomAmount').val(updateTotalAmount.toFixed(2));
                    $('#gstBOMAmount').val(updateGSTPrice.toFixed(2));
                }
            });
        }else{
            updateGSTPrice = 0;
            updateTotalAmount = (rate*qty)-currentDicountAmount;
            $('#bomAmount').val(updateTotalAmount.toFixed(2));
            $('#gstBOMAmount').val(updateGSTPrice.toFixed(2));
        }
    }
   

}

function addBOMInTable(){
    if(bomValid()){
        $('#bomModel').modal('hide');
        addRowAsBom();
        clearBomValue();
    }
}

function addRowAsBom(){
    var productId = $('#bomid').val();
    var productName = ($('#bomName').val()).trim();
    var hsncode = ($('#bomHSN').val()).trim();
    var qty =  ($('#bomQTY').val()).trim();
    var rate = ($('#bomRate').val()).trim();
    var gstAmount = $('#gstBOMAmount').val(); 
    var discountPer = $('#discountid').val();
    var discountAmount = $('#discountAmount').val();
    var gstId =  $("#gstbomid").val()
    var amount = $('#bomAmount').val();
    var narration =$('#bomnarration').val();

    if(!narration){
        narration = "";
    }
    
    var gstText = "";
    gstmaster.forEach(element => {
        if(element.gstmasterid == gstId){
            gstText = element.gstname+" %";
        }
    });

    count++;
    srNo++;



    var showBOMQTYID = "'"+"#showBOMQty"+"'";
    var showBOMDisPer = "'"+"#showBOMDisPer"+"'";
    var showBOMRate =   "'"+"#showBOMRate"+"'";

    var record =    '<tr id="tr'+count+'">'+
                    '<input type="hidden" id="type'+count+'" name="type'+count+'" value="1">'+   
                    '<input type="hidden" id="pcont'+count+'" name="pcont'+count+'" value="'+count+'">'+
                    '<input type="hidden" id="pid'+count+'" name="pid'+count+'" value="'+productId+'">'+
                    '<input type="hidden" id="pname'+count+'" name="pname'+count+'" value="'+productName+'">'+
                    '<input type="hidden" id="phsn'+count+'" name="phsn'+count+'" value="'+hsncode+'">'+
                    '<input type="hidden" id="pqty'+count+'" name="pqty'+count+'" value="'+qty+'">'+
                    '<input type="hidden" id="unitText'+count+'" name="unitText'+count+'" value="'+bomItemMaster[0].saleunit+'">'+
                    '<input type="hidden" id="uid'+count+'" name="uid'+count+'" value="'+bomItemMaster[0].bomitemmasterid +'">'+
                    '<input type="hidden" id="salerate'+count+'" name="salerate'+count+'" value="'+rate+'">'+
                    '<input type="hidden" id="discount'+count+'" name="discount'+count+'" value="'+discountPer+'">'+
                    '<input type="hidden" id="discountAmount'+count+'" name="discountAmount'+count+'" value="'+discountAmount+'">'+
                    '<input type="hidden" id="gstid'+count+'" name="gstid'+count+'" value="'+gstId+'">'+
                    '<input type="hidden" id="gstname'+count+'" name="gstname'+count+'" value="'+gstText+'">'+
                    '<input type="hidden" id="gstamount'+count+'" name="gstamount'+count+'" value="'+gstAmount+'">'+
                    '<input type="hidden" id="amount'+count+'" name="amount'+count+'" value="'+amount+'">'+
                    '<input type="hidden" id="narration'+count+'" name="narration'+count+'" value="'+narration+'">'+
                    '<input type="hidden" id="cumulativeAmountId'+count+'" name="cumulativeAmountId'+count+'" value="'+amount+'">'+
                    '<td id="srNo'+count+'">'+srNo+'</td>'+
                    '<td>'+productName+'</td>'+
                    '<td>'+hsncode+'</td>'+
                    '<td id="showBOMRate'+count+'" onclick="calGSTForBOM('+count+','+showBOMRate+')">'+rate+'</td>'+
                    '<td id="showBOMQty'+count+'" onclick="calGSTForBOM('+count+','+showBOMQTYID+')">'+qty+'</td>'+
                    '<td>'+bomItemMaster[0].saleunit+'</td>'+
                    '<td id="showBOMDisPer'+count+'" onclick="calGSTForBOM('+count+','+showBOMDisPer+')">'+discountPer+'</td>'+
                    '<td id="showBOMDisAmount'+count+'">'+discountAmount+'</td>'+
                    '<td id="showBOMGST'+count+'" >'+gstText+'</td>'+
                    '<td id="showBOMGSTAmount'+count+'">'+gstAmount+'</td>'+
                    '<td id="showBOMTotalAmount'+count+'">'+amount+'</td>'+
                    '<td id="cumulativeAmount'+count+'">'+amount+'</td>'+
                    // '<td onclick="removeProduct(1,'+count+','+productId+')"><i class="fas fa-times" style="color: red;"></i></td>'+  
                    '</tr>';
    if(narration){
        record = record+ 
                    '<tr id="trnarration'+count+'">'+
                    '<td></td>'+
                    '<td style="font-size:12px !important">'+narration+'</td>'+
                    '<td></td>'+
                    '<td></td>'+
                    '<td></td>'+
                    '<td></td>'+
                    '<td></td>'+
                    '<td></td>'+
                    '<td></td>'+
                    '<td></td>'+
                    '</tr>';
    }

    $('#tableList').append(record);
    $('#numberOfProductInBill').val(count);
    updateBillTotal();
}

function bomValid(){
    var bomName = ($('#bomName').val()).trim();
    var hsncode = ($('#bomHSN').val()).trim();
    var bomid = $('#bomid').val();
    if(!bomid || bomid ==  ""){
        alert('Please Select BOM');
        return false;
    }

    if(bomName == "" || !bomName){
        $('#bomName_error').show();
        return false;   
    }else{
        $('#bomName_error').hide();
    }
    if(hsncode == "" || !hsncode){
        $('#bomHSN_error').show();
        return false;
    }else{
        $('#bomHSN_error').hide();
    }

    return true;
}

function addProductInTable(){
    var qty = ($("#productqty").val()).trim();
    var id = $('#productid').val();
    

    for(var i=0; i<selctedResult.length; i++) {
        if (selctedResult[i] == id){
            alert(" product already selected");
            return false;
        }
    }

    if(id == "" || !id){
        alert("Please Select Product");
        return false;   
    }

    if(qty == "" || !qty){
        $('#productqty_error').show();
        return false;   
    }else{
        $('#productqty_error').hide();
    }


    $('#productModel').modal('hide');
    ajaxCall(id,qty);
    
}

function ajaxCall(id,qty){
    selctedResult.push(id);
    var url = '/getproductajax/'+id; 
    $.ajax({
        type : 'get',
        url : url,
        success : function(data){
          

            setTableForProduct(data[0],qty);
           
            
        }
    });
    
}

function setTableForProduct(data,qty){

    cumulativeAmount =0;

    var type = data.productcategory;
    if(type == "general"){
        var mainName = data.productname;
    }else if(type == "specific"){
        var mainName = data.subnameofproduct;
    }

    // var mainName = data.productname;
    var mainId = data.pid;
    var mainQty = qty;
    var mainHSN = "-";
    if(data.hsnno){
        mainHSN = data.hsnno;
    }
    var mainUnitId = data.unitid;
    var mainUnitText = "";
    var mainsalerate = data.sellrate;
    var mainDis = "0";
    var mainDisAmount = "0";
    var mainGstid = data.gstpersentage;
    var mainGst = ""; 
    var mainGSTAmount = "";
    var mainAmount = parseFloat(mainQty)*parseFloat(mainsalerate);
    var narration =$('#productnarration').val();
    
    if(!narration){
        narration = "";
    } 

    gstmaster.forEach(element => {
        if(element.gstmasterid == data.gstpersentage){
            mainGst = element.gstname+"%";
            mainGSTAmount = (mainAmount*element.gstname)/100;
        }
    });
    
    units.forEach(element => {
        if(element.productunitid == mainUnitId){
            mainUnitText = element.productunitname;
        }
    });

    var mainTotalAmount = mainAmount+mainGSTAmount;    
    count++;
    srNo++;

    var showProductQty = "'"+"#showProductQty"+"'";
    var showProductDisPer = "'"+"#showProductDisPer"+"'";
    var showProductRate =   "'"+"#showProductRate"+"'";
    //PRODUCT
    var record =    '<tr id="tr'+count+'">'+
                    '<input type="hidden" id="type'+count+'" name="type'+count+'" value="2">'+           
                    '<input type="hidden" id="pcont'+count+'" name="pcont'+count+'" value="'+count+'">'+                      
                    '<input type="hidden" id="pid'+count+'" name="pid'+count+'" value="'+mainId+'">'+
                    '<input type="hidden" id="pname'+count+'" name="pname'+count+'" value="'+mainName+'">'+
                    '<input type="hidden" id="phsn'+count+'" name="phsn'+count+'" value="'+mainHSN+'">'+
                    '<input type="hidden" id="pqty'+count+'" name="pqty'+count+'" value="'+qty+'">'+
                    '<input type="hidden" id="uid'+count+'" name="uid'+count+'" value="'+mainUnitId+'">'+
                    '<input type="hidden" id="unitText'+count+'" name="unitText'+count+'" value="'+mainUnitText+'">'+
                    '<input type="hidden" id="salerate'+count+'" name="salerate'+count+'" value="'+mainsalerate+'">'+
                    '<input type="hidden" id="discount'+count+'" name="discount'+count+'" value="'+mainDis+'">'+
                    '<input type="hidden" id="discountAmount'+count+'" name="discountAmount'+count+'" value="'+mainDisAmount+'">'+
                    '<input type="hidden" id="gstid'+count+'" name="gstid'+count+'" value="'+mainGstid+'">'+
                    '<input type="hidden" id="gstname'+count+'" name="gstname'+count+'" value="'+mainGst+'">'+
                    '<input type="hidden" id="gstamount'+count+'" name="gstamount'+count+'" value="'+mainGSTAmount+'">'+
                    '<input type="hidden" id="amount'+count+'" name="amount'+count+'" value="'+mainTotalAmount+'">'+
                    '<input type="hidden" id="narration'+count+'" name="narration'+count+'" value="'+narration+'">'+
                    '<input type="hidden" id="cumulativeAmountId'+count+'" name="cumulativeAmountId'+count+'" value="">'+
                    '<input type="hidden" id="numberofaccessories'+count+'" name="numberofaccessories'+count+'" value="">'+
                    '<td id="srNo'+count+'">'+srNo+'</td>'+
                    '<td>'+mainName+'</td>'+
                    '<td>'+mainHSN+'</td>'+
                    '<td id="showProductRate'+count+'" onclick="calGSTForProduct('+count+','+showProductRate+')">'+mainsalerate+'</td>'+
                    '<td id="showProductQty'+count+'" onclick="calGSTForProduct('+count+','+showProductQty+')">'+mainQty+'</td>'+
                    '<td>'+mainUnitText+'</td>'+
                    '<td id="showProductDisPer'+count+'" onclick="calGSTForProduct('+count+','+showProductDisPer+')">'+mainDis+'</td>'+
                    '<td id="showProductDisAmount'+count+'">'+mainDisAmount+'</td>'+
                    '<td id="showProductGST'+count+'" >'+mainGst+'</td>'+
                    '<td id="showProductGSTAmount'+count+'">'+mainGSTAmount+'</td>'+
                    '<td id="showProductTotalAmount'+count+'">'+mainTotalAmount+'</td>'+
                    '<td id="cumulativeAmount'+count+'">'+mainTotalAmount+'</td>'+
                    // '<td onclick="removeProduct(2,'+count+','+mainId+')"><i class="fas fa-times" style="color: red;"></i></td>'+  
                    '</tr>';
        if(narration){
            record = record+ 
                        '<tr id="trnarration'+count+'">'+
                        '<td></td>'+
                        '<td style="font-size:12px !important">'+narration+'</td>'+
                        '<td></td>'+
                        '<td></td>'+
                        '<td></td>'+
                        '<td></td>'+
                        '<td></td>'+
                        '<td></td>'+
                        '<td></td>'+
                        '<td></td>'+
                        '</tr>';
                    }
                

    $('#tableList').append(record);
    $('#numberOfProductInBill').val(count);

    var totalLength=$('#tableListAccessories').find('input[name="accessoriescheckbox[]"]').length;
   
    if(!totalLength || totalLength == undefined){
        totalLength = 0;
        updateBillTotal();
    }

  
  
    cumulativeAmount =Number(parseFloat( mainTotalAmount).toFixed(2)) +Number( parseFloat(cumulativeAmount).toFixed(2));  
    $('#cumulativeAmountId'+count).val(cumulativeAmount);
   
    var numberofaccessories = 0;
    for (var i = 1; i <= totalLength; i++) {

        var tempId = "#accessoriesadd"+i;
        if ($(tempId).prop("checked") == true) {
            numberofaccessories++;
            var tempQty = "#tdqty"+i;
            var tempQtyValue = ($(tempQty).text()).trim();
            var temp = "#accessoriesid"+i;
            var accessId = $(temp).val();
            setAeccoris(tempQtyValue,accessId,mainId,mainQty);
        }
    }
    
    $('#numberofaccessories'+count).val(numberofaccessories);
    productreset(); 

}

function setAeccoris(qty,accessId,productID,productQty){
    
    var url = '/getproductajax/'+accessId; 
    $.ajax({
        type : 'get',
        url : url,
        success : function(data){
            setTableForAeccoris(data[0],qty,productID,productQty)     
        }
    });

}

function setTableForAeccoris(data,qty,productID,productQty){
    
    var totalQty = qty*productQty;

    var mainName = data.productname;
    var mainId = data.pid;
    var mainQty = totalQty;
    var mainHSN = "-";
    if(data.hsnno){
        mainHSN = data.hsnno;
    }
    var mainUnitId = data.unitid;
    var mainUnitText = "";
    var mainsalerate = data.sellrate;
    var mainDis = "0";
    var mainDisAmount = "0";
    var mainGstid = data.gstpersentage;
    var mainGst = ""; 
    var mainGSTAmount = "";
    var mainAmount = parseFloat(mainQty)*parseFloat(mainsalerate);
    gstmaster.forEach(element => {
        if(element.gstmasterid == data.gstpersentage){
            mainGst = element.gstname+"%";
            mainGSTAmount = (mainAmount*element.gstname)/100;
        }
    });

    units.forEach(element => {
        if(element.productunitid == mainUnitId){
            mainUnitText = element.productunitname;
        }
    });

    var mainTotalAmount = mainAmount+mainGSTAmount;
    accessCount++;

    var accessQty = "'"+"#accessQty"+"'";
    var accessDisPer = "'"+"#accessDisPer"+"'";
    var accessRate =   "'"+"#accessRate"+"'";
    //ACC
    var record =    '<tr id="atr'+accessCount+'">'+
                    '<input type="hidden" id="prodctCountNo'+accessCount+'" name="prodctCountNo'+accessCount+'" value="'+count+'">'+
                    '<input type="hidden" id="aiproductid'+accessCount+'" name="aiproductd'+accessCount+'" value="'+productID+'">'+
                    '<input type="hidden" id="aid'+accessCount+'" name="aid'+accessCount+'" value="'+mainId+'">'+
                    '<input type="hidden" id="aname'+accessCount+'" name="apname'+accessCount+'" value="'+mainName+'">'+
                    '<input type="hidden" id="ahsn'+accessCount+'" name="ahsn'+accessCount+'" value="'+mainHSN+'">'+
                    '<input type="hidden" id="+aqty'+accessCount+'" name="aqty'+accessCount+'" value="'+mainQty+'">'+
                    '<input type="hidden" id="+aunitText'+accessCount+'" name="aunitText'+accessCount+'" value="'+mainUnitText+'">'+
                    '<input type="hidden" id="auid'+accessCount+'" name="auid'+accessCount+'" value="'+mainUnitId+'">'+
                    '<input type="hidden" id="asalerate'+accessCount+'" name="asalerate'+accessCount+'" value="'+mainsalerate+'">'+
                    '<input type="hidden" id="adiscount'+accessCount+'" name="adiscount'+accessCount+'" value="'+mainDis+'">'+
                    '<input type="hidden" id="adiscountAmount'+accessCount+'" name="adiscountAmount'+accessCount+'" value="'+mainDisAmount+'">'+
                    '<input type="hidden" id="agstid'+accessCount+'" name="agstid'+accessCount+'" value="'+mainGstid+'">'+
                    '<input type="hidden" id="agstname'+accessCount+'" name="agstname'+accessCount+'" value="'+mainGst+'">'+
                    '<input type="hidden" id="agstamount'+accessCount+'" name="agstamount'+accessCount+'" value="'+mainGSTAmount+'">'+
                    '<input type="hidden" id="aamount'+accessCount+'" name="aamount'+accessCount+'" value="'+mainTotalAmount+'">'+ 
                    '<td></td>'+
                    '<td><i style="font-size:12px !important">'+mainName+'</i></td>'+
                    '<td><i style="font-size:12px !important">'+mainHSN+'</i></td>'+
                    '<td><i style="font-size:12px !important" id="accessRate'+accessCount+'" onclick="calGSTForAccess('+count+','+accessCount+','+accessRate+')">'+mainsalerate+'</i></td>'+
                    '<td><i style="font-size:12px !important" id="accessQty'+accessCount+'" onclick="calGSTForAccess('+count+','+accessCount+','+accessQty+')" >'+mainQty+'</i></td>'+
                    '<td><i style="font-size:12px !important" >'+mainUnitText+'</i></td>'+
                    '<td><i style="font-size:12px !important" id="accessDisPer'+accessCount+'" onclick="calGSTForAccess('+count+','+accessCount+','+accessDisPer+')">'+mainDis+'</i></td>'+
                    '<td><i style="font-size:12px !important" id="accessDisAmount'+accessCount+'">'+mainDisAmount+'</i></td>'+
                    '<td><i style="font-size:12px !important">'+mainGst+'</i></td>'+
                    '<td><i style="font-size:12px !important" id="accessGSTAmount'+accessCount+'">'+mainGSTAmount+'</i></td>'+
                    '<td><i style="font-size:12px !important" id="accessTotalAmount'+accessCount+'">'+mainTotalAmount+'</i></td>'+
                    '</tr>';

    $('#tableList').append(record);
    $('#numberOfAccessoriesInBill').val(accessCount);
    

    cumulativeAmount = Number(parseFloat( mainTotalAmount).toFixed(2)) + Number(parseFloat(cumulativeAmount).toFixed(2));   
    $('#cumulativeAmount'+count).text(cumulativeAmount.toFixed(2));
    $('#cumulativeAmountId'+count).val(cumulativeAmount.toFixed(2));
    updateBillTotal();
}

function calGSTForProduct(count,idString){
    var oldAmount = $('#amount'+count).val();
    var oldCumulativeAmount = $('#cumulativeAmountId'+count).val();
    var id=idString+count;
    let $this = $(id);
    let oldValue = $(id).text();
    let $input = $('<input>', {
        type: 'number',
        blur : function(){
            $this.text($(this).val());
            if(($(this).val()).trim() == "" || $(this).val() == null){
                if(oldValue.trim() == "" || oldValue == null || !oldValue){
                    $(id).text(0);
                }else{
                    $(id).text(oldValue);
                }
            }else{
                tableProductCal(count,oldAmount,oldCumulativeAmount);
                
            }
        },
        keyup : function(e){
            if (e.which === 13) $input.blur();
        }
    }).appendTo( $this.empty() ).focus();
}

function calGSTForBOM(count,idString){

    var id=idString+count;
    let $this = $(id);
    let oldValue = $(id).text();
    let $input = $('<input>', {
        type: 'number',
        blur : function(){
            $this.text($(this).val());
            if(($(this).val()).trim() == "" || $(this).val() == null){   
                if(oldValue.trim() == "" || oldValue == null || !oldValue){
                    $(id).text(0);
                }else{
                    $(id).text(oldValue);
                }
               
            }else{
                tableBOMCal(count);
                
            }
        },
        keyup : function(e){
            if (e.which === 13) $input.blur();
        }
    }).appendTo( $this.empty() ).focus();

    
    

}

function tableBOMCal(count){

    var rate = parseFloat($('#showBOMRate'+count).text());
    var currentGst = parseFloat($('#gstid'+count).val());
    var currentTxt;
    var qty = parseFloat($('#showBOMQty'+count).text());

    if(rate == "" || rate === NaN || !rate){
        rate = 0; 
        $('#showBOMRate'+count).text(0);
        $('#salerate'+count).val(0);
    }
  
    if(qty == "" || qty === NaN || !qty){
        qty = 1; 
        $('#showBOMQty'+count).text(1);
        $('#pqty'+count).val(1);
    }

    if(rate != "" && rate !== NaN && rate || rate >= 0){
        $('#salerate'+count).val(rate);
        $('#pqty'+count).val(qty);
        var currentDicount = parseFloat($('#showBOMDisPer'+count).text());
       
        if(currentDicount || currentDicount == 0){
            if(currentDicount > 100){
                alert('Maximum Discount 100%');
                $('#showBOMDisPer'+count).text(0);
                $('#showBOMDisAmount'+count).text(0);
                $('#discount'+count).val(0);
                $('#discountAmount'+count).val(0);
            }else{
                var discountAmount =  ((currentDicount*(rate*qty)/100));
                $('#showBOMDisAmount'+count).text(discountAmount.toFixed(2));
                $('#discountAmount'+count).val(discountAmount.toFixed(2));
                $('#discount'+count).val(currentDicount);
            }
        }else{
            if(currentDicount > 100){
                alert('Maximum Discount 100%');
            }
            $('#showBOMDisPer'+count).text(0);
            $('#showBOMDisAmount'+count).text(0);
            $('#discount'+count).val(0);
            $('#discountAmount'+count).val(0);
        }

        var currentDicountAmount =  parseFloat($('#showBOMDisAmount'+count).text());

        if(currentGst && currentGst != ""){
            gstmaster.forEach(element => {
                if(element.gstmasterid == currentGst){
                    var newRate = (rate*qty)-currentDicountAmount;
                    currentTxt = parseFloat(element.gstname);
                    updateGSTPrice = (currentTxt*newRate)/100;
                    updateTotalAmount = updateGSTPrice+newRate;
                    $('#showBOMGSTAmount'+count).text(updateGSTPrice.toFixed(2));
                    $('#gstamount'+count).val(updateGSTPrice.toFixed(2));
                    $('#showBOMTotalAmount'+count).text(updateTotalAmount.toFixed(2));
                    $('#amount'+count).val(updateTotalAmount.toFixed(2));
                    $('#cumulativeAmount'+count).text(updateTotalAmount.toFixed(2));
                    $('#cumulativeAmountId'+count).val(updateTotalAmount.toFixed(2));
                    }
                });
            }else{
                updateGSTPrice = 0;
                updateTotalAmount = (rate*qty)-currentDicountAmount;
                $('#showBOMGSTAmount'+count).text(updateGSTPrice.toFixed(2));
                $('#gstamount'+count).val(updateGSTPrice.toFixed(2));
                $('#showBOMTotalAmount'+count).text(updateTotalAmount.toFixed(2));
                $('#amount'+count).val(updateTotalAmount.toFixed(2));
                $('#cumulativeAmount'+count).text(updateTotalAmount.toFixed(2));
                $('#cumulativeAmountId'+count).val(updateTotalAmount.toFixed(2));
        }
    }

    updateBillTotal();
   
}

function tableProductCal(count,oldAmount,oldCumulativeAmount){

    var rate = parseFloat($('#showProductRate'+count).text());
    var currentGst = parseFloat($('#gstid'+count).val());
    var currentTxt;
    var qty = parseFloat($('#showProductQty'+count).text());

    if(rate == "" || rate === NaN || !rate){
        rate = 0; 
        $('#showProductRate'+count).text(0);
        $('#salerate'+count).val(0);
    }
  
    if(qty == "" || qty === NaN || !qty){
        qty = 0; 
        $('#showProductQty'+count).text(0);
        $('#pqty'+count).val(0);
    }

    if(rate != "" && rate !== NaN && rate || rate >= 0){
        $('#salerate'+count).val(rate);
        $('#pqty'+count).val(qty);
        var currentDicount = parseFloat($('#showProductDisPer'+count).text());
       
        if(currentDicount || currentDicount == 0){
            if(currentDicount > 100){
                alert('Maximum Discount 100%');
                $('#showProductDisPer'+count).text(0);
                $('#showProductDisAmount'+count).text(0);
                $('#discount'+count).val(0);
                $('#discountAmount'+count).val(0);
            }else{
                var discountAmount =  ((currentDicount*(rate*qty)/100));
                $('#showProductDisAmount'+count).text(discountAmount.toFixed(2));
                $('#discountAmount'+count).val(discountAmount.toFixed(2));
                $('#discount'+count).val(currentDicount);
            }
        }else{
            if(currentDicount > 100){
                alert('Maximum Discount 100%');
            }
            $('#showProductDisPer'+count).text(0);
            $('#showProductDisAmount'+count).text(0);
            $('#discount'+count).val(0);
            $('#discountAmount'+count).val(0);
        }

        var currentDicountAmount =  parseFloat($('#showProductDisAmount'+count).text());

        if(currentGst && currentGst != ""){
            gstmaster.forEach(element => {
                if(element.gstmasterid == currentGst){
                    var newRate = (rate*qty)-currentDicountAmount;
                    currentTxt = parseFloat(element.gstname);
                    updateGSTPrice = (currentTxt*newRate)/100;
                    updateTotalAmount = updateGSTPrice+newRate;
                    $('#showProductGSTAmount'+count).text(updateGSTPrice.toFixed(2));
                    $('#gstamount'+count).val(updateGSTPrice.toFixed(2));
                    $('#showProductTotalAmount'+count).text(updateTotalAmount.toFixed(2));
                    $('#amount'+count).val(updateTotalAmount.toFixed(2));

                    var tempCumulativeAmount = parseFloat(oldCumulativeAmount)+updateTotalAmount-parseFloat(oldAmount);

                    $('#cumulativeAmount'+count).text(tempCumulativeAmount.toFixed(2));
                    $('#cumulativeAmountId'+count).val(tempCumulativeAmount.toFixed(2));
                    }
                });
            }else{
                updateGSTPrice = 0;
                updateTotalAmount = (rate*qty)-currentDicountAmount;
                $('#showProductGSTAmount'+count).text(updateGSTPrice.toFixed(2));
                $('#gstamount'+count).val(updateGSTPrice.toFixed(2));
                $('#showProductTotalAmount'+count).text(updateTotalAmount.toFixed(2));
                $('#amount'+count).val(updateTotalAmount.toFixed(2));

                var tempCumulativeAmount = parseFloat(oldCumulativeAmount)+updateTotalAmount-parseFloat(oldAmount);

                
                // var tempCumulativeAmount = parseFloat($('#cumulativeAmount'+count).text()); 
                $('#cumulativeAmount'+count).text(tempCumulativeAmount.toFixed(2));
                $('#cumulativeAmountId'+count).val(tempCumulativeAmount.toFixed(2));
        }
    }
    updateBillTotal();
}

function calGSTForAccess(count,accssCount , idString){
    var oldAmount = $('#aamount'+accssCount).val();
    var oldCumulativeAmount = $('#cumulativeAmountId'+count).val();
    var id=idString+accssCount;
    let $this = $(id);
    let oldValue = $(id).text();
    let $input = $('<input>', {
        type: 'number',
        blur : function(){
            $this.text($(this).val());
            if(($(this).val()).trim() == "" || $(this).val() == null){
                
                if(oldValue.trim() == "" || oldValue == null || !oldValue){
                    $(id).text(0);
                }else{
                    $(id).text(oldValue);
                }
            }else{
                tableProductAccessCal(count,accssCount,oldAmount,oldCumulativeAmount);
                
            }
        },
        keyup : function(e){
            if (e.which === 13) $input.blur();
        }
    }).appendTo( $this.empty() ).focus();

}

function tableProductAccessCal(count,accssCount,oldAmount,oldCumulativeAmount){

    var rate = parseFloat($('#accessRate'+accssCount).text());
    var currentGst = $('#agstid'+accssCount).val();
    var currentTxt;
    var qty = parseFloat($('#accessQty'+accssCount).text());

    if(rate == "" || rate === NaN || !rate){
        rate = 0; 
        $('#accessRate'+accssCount).text(0);
        $('#asalerate'+accssCount).val(0);
    }
  
    if(qty == "" || qty === NaN || !qty){
        qty = 1; 
        $('#accessQty'+accssCount).text(1);
        $('#aqty'+accssCount).val(1);
    }

    if(rate != "" && rate !== NaN && rate || rate >= 0){
        $('#asalerate'+accssCount).val(rate);
        $('#aqty'+accssCount).val(qty);
        var currentDicount = parseFloat($('#accessDisPer'+accssCount).text());
       
        if(currentDicount || currentDicount == 0){
            if(currentDicount > 100){
                alert('Maximum Discount 100%');
                $('#accessDisPer'+accssCount).text(0);
                $('#accessDisAmount'+accssCount).text(0);
                $('#adiscount'+accssCount).val(0);
                $('#adiscountAmount'+accssCount).val(0);
            }else{
                var discountAmount =  ((currentDicount*(rate*qty)/100));
                $('#accessDisAmount'+accssCount).text(discountAmount.toFixed(2));
                $('#adiscountAmount'+accssCount).val(discountAmount.toFixed(2));
                $('#adiscount'+accssCount).val(currentDicount);
            }
        }else{
            if(currentDicount > 100){
                alert('Maximum Discount 100%');
            }
            $('#accessDisPer'+accssCount).text(0);
            $('#accessDisAmount'+accssCount).text(0);
            $('#adiscount'+accssCount).val(0);
            $('#adiscount'+accssCount).val(0);
        }

        var currentDicountAmount =  parseFloat($('#accessDisAmount'+accssCount).text());

        if(currentGst && currentGst != ""){
            gstmaster.forEach(element => {
                if(element.gstmasterid == currentGst){
                    var newRate = (rate*qty)-currentDicountAmount;
                    currentTxt = parseFloat(element.gstname);
                    updateGSTPrice = (currentTxt*newRate)/100;
                    updateTotalAmount = updateGSTPrice+newRate;
                    $('#accessGSTAmount'+accssCount).text(updateGSTPrice.toFixed(2));
                    $('#agstamount'+accssCount).val(updateGSTPrice.toFixed(2));
                    $('#accessTotalAmount'+accssCount).text(updateTotalAmount.toFixed(2));
                    $('#aamount'+accssCount).val(updateTotalAmount.toFixed(2));

                    var tempCumulativeAmount = parseFloat(oldCumulativeAmount)+updateTotalAmount-parseFloat(oldAmount);

                    $('#cumulativeAmount'+count).text(tempCumulativeAmount.toFixed(2));
                    $('#cumulativeAmountId'+count).val(tempCumulativeAmount.toFixed(2));
                    }
                });
            }else{
                updateGSTPrice = 0;
                updateTotalAmount = (rate*qty)-currentDicountAmount;
                $('#accessGSTAmount'+accssCount).text(updateGSTPrice.toFixed(2));
                $('#agstamount'+accssCount).val(updateGSTPrice.toFixed(2));
                $('#accessTotalAmount'+accssCount).text(updateTotalAmount.toFixed(2));
                $('#aamount'+accssCount).val(updateTotalAmount.toFixed(2));

                var tempCumulativeAmount = parseFloat(oldCumulativeAmount)+updateTotalAmount-parseFloat(oldAmount);

                // var tempCumulativeAmount = parseFloat($('#cumulativeAmount'+count).text()); 
                $('#cumulativeAmount'+count).text(tempCumulativeAmount.toFixed(2));
                $('#cumulativeAmountId'+count).val(tempCumulativeAmount.toFixed(2));
               
        }
    }
    updateBillTotal();

}

function updateBillTotal(){
    calBillTotal();
    calBillGSTTotal();
}

function calBillTotal(){
    var numberOfAccss = parseInt($('#numberOfAccessoriesInBill').val());
    var numberOfProduct = parseInt($('#numberOfProductInBill').val());
    
    var dicamount = 0;
    var subamount = 0;
    var cumulativeAmount = 0;
    console.log('count '+numberOfProduct);


    for (let index = 1; index < Number(numberOfProduct)+1; index++) {
          console.log('index: '+index);
        var dictemp = $('#discountAmount'+index).val(); 
        if(dictemp && dictemp != undefined && dictemp != "" && $('#discountAmount'+index).length){
            dicamount = parseFloat(dictemp)+dicamount;
        }
        
        var tempGstamount = $('#gstamount'+index).val();
        var tempamount = $('#amount'+index).val();
        if(tempamount && tempamount != undefined && tempamount != "" && $('#gstamount'+index).length){
            subamount = parseFloat(tempamount)+subamount-parseFloat(tempGstamount);
        }
        if(dictemp && dictemp != undefined && dictemp != ""  && $('#discountAmount'+index).length){
            subamount = parseFloat(dictemp)+subamount;
        }
        // console.log('value: ',document.getElementById('cumulativeAmountId2').value);
        // console.log('index: ',document.getElementById('cumulativeAmountId2'));
        // console.log(index,$('#cumulativeAmountId'+index));
        // console.log(index,$('#cumulativeAmountId'+index).val(),"Value");
        var tempcumulativeAmount = $('#cumulativeAmount'+index).text();
        if(tempcumulativeAmount && tempcumulativeAmount != undefined && tempcumulativeAmount != "" && $('#cumulativeAmount'+index).length){
            cumulativeAmount = parseFloat(tempcumulativeAmount)+cumulativeAmount;
        }
        
    }
    for (let index = 1; index <= numberOfAccss; index++) {
        
        var dictemp = $('#adiscountAmount'+index).val(); 
        if(dictemp && dictemp != undefined && dictemp != "" &&  $('#adiscountAmount'+index).length){
            dicamount = parseFloat(dictemp)+dicamount;
        }

        var tempGstamount = $('#agstamount'+index).val();
        var tempamount = $('#aamount'+index).val();
        if(tempamount && tempamount != undefined && tempamount != "" &&  $('#aamount'+index).length){
            subamount = parseFloat(tempamount)+subamount-parseFloat(tempGstamount);
        }
        if(dictemp && dictemp != undefined && dictemp != "" &&  $('#adiscountAmount'+index).length){
            subamount = parseFloat(dictemp)+dicamount;
        }
        
    }
    
    $('#billDiscount').text(dicamount.toFixed(2));
    $('#billDiscountHidden').val(dicamount.toFixed(2));
    
    $('#billSubAmount').text(subamount.toFixed(2));
    $('#billSubAmountHidden').val(subamount.toFixed(2));

    $('#billTotalAmount').text(cumulativeAmount.toFixed(2));
    $('#billAmount').val(cumulativeAmount.toFixed(2));
}

function calBillGSTTotal(){
    var numberOfAccss = parseInt($('#numberOfAccessoriesInBill').val());
    var numberOfProduct = parseInt($('#numberOfProductInBill').val());

    var gstArray = [];

    gstmaster.forEach(element => {
        $('#trGST'+element.gstmasterid).hide();
        var temp = {
            gstID : element.gstmasterid,
            total : 0
        }
        gstArray.push(temp);
    });
      

    for (let index = 1; index <= numberOfProduct; index++) {
        if($('#gstid'+index).length){
            var id = $('#gstid'+index).val();
            var gstAmount = $('#gstamount'+index).val();
            gstArray.forEach(element => {
                if(element.gstID == id){
                    var tempTotal = element.total + parseFloat(gstAmount);
                    element.total = parseFloat(tempTotal);
                }
            });
        }
       
    }

    for (let index = 1; index <= numberOfAccss; index++) {
        if($('#agstid'+index).length){
            var id = $('#agstid'+index).val();
            var gstAmount = $('#agstamount'+index).val();
            gstArray.forEach(element => {
                if(element.gstID == id){
                    var tempTotal = element.total + parseFloat(gstAmount);
                    element.total = parseFloat(tempTotal);
                }
            });
        }
       
    }

    gstArray.forEach(element => {
        if(element.total != 0){
            $('#trGST'+element.gstID).show();
            $('#billGstAmount'+element.gstID).val(element.total.toFixed(2));
            $('#gstAmount'+element.gstID).text(element.total.toFixed(2));
        }else{
            $('#billGstAmount'+element.gstID).val(0);
        }
    });


}

function resetBill(){
    selctedResult = [];
    count = 0;
    accessCount = 0;
    srNo = 0;
    $("#tableList > tbody").empty();  

    gstmaster.forEach(element => {
        $('#trGST'+element.gstmasterid).hide();
        $('#billGstAmount'+element.gstmasterid).val(0);
    });
    
    $('#billDiscount').text(0);
    $('#billDiscountHidden').val(0);
    
    $('#billSubAmount').text(0);
    $('#billSubAmountHidden').val(0);

    $('#billTotalAmount').text(0);
    $('#billAmount').val(0);
}

function removeProduct(type,currentClickCount,productid){
    console.log(type,currentClickCount,productid);

    if(type == 2){
        // var numbernumberofaccessories = $('#numberofaccessories'+currentClickCount).val();

        for (let j = 1; j <= Number(accessCount); j++) {
            var check = $('#aiproductid'+j).val();
            if(Number(check) == productid){
                  $('#atr'+j).remove();
            }
        }

       
    }
    
    $('#tr'+currentClickCount).remove();
    var tempCount = 0;
    for (let i = 1; i <=count  ; i++) {
        console.log($('#srNo'+i));
        if($('#srNo'+i).length){
            tempCount++;
            console.log(tempCount);
            $('#srNo'+i).text(tempCount);
            
        }
    }
    srNo = tempCount;
    calBillTotal();
    calBillGSTTotal();
}

function removeAeccoris(){

}


