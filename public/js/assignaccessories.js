
var selectAccessoriesResult = [];

var accessoriesCount = 0;

var lock = 0;

var totalProduct = 0;
var productArray = [];
 
function unitAjax(id,selectedText) {
    
    var url = $('#urlProductUnit').val();
    var _token = $('input[name="_token"]').val();


    if (id) {
        $.ajax({
            type: 'POST',
            url: url,
            data: {id: id, _token: _token},
            success: function (data) {
                
                $('#productAccessoriesId').val(id);
                $('#productAccessories').val(selectedText);
                $('#accessoiesunit').val(data.productunitname);

            }
        });
    } 
}

function addAccessoriesTotable() {

    if(lock != 0){
        alert("Accessories are locked");
        return;
    }

    var getval = $('#productAccessoriesId').val();
    var productqty = $('#productqtyA').val();
    
    if (!getval) {
        alert("Please Accessories Select Product");
        return;
    } else if (!productqty) {
        alert("Please enter quantity");
        return;
    } else {
        var selectedText = $('#productAccessoriesId :selected').text();
        var unitname = $('#accessoiesunit'+getval).val();


        for (var i = 0; i < selectAccessoriesResult.length; i++) {
            if (selectAccessoriesResult[i].id == getval) {
                alert(selectedText + " accessories product already selected");
                return;
            }
        }

        accessoriesCount++;
        var record = '<tr id="tr' + accessoriesCount + '">'+
                     '<input type="hidden" name="productAccessoriesid' + accessoriesCount + '" value="' + getval + '" >'+
                     '<input type="hidden" name="productAccessoriesqty' + accessoriesCount + '" value="' + productqty + '" >'+
                     '<td>' + accessoriesCount + '</td>'+
                     '<td>' + selectedText + '</td>'+
                     '<td>' + productqty + '</td>'+
                     '<td>' + unitname + '</td>'+
                     '<td onclick="removeaccessories(' + accessoriesCount + ')"><i class="fas fa-times" style="color: red;"></i></td>'+
                     '</tr>';

        $('#tableAccessoriesList').append(record);
        var temp = {
            count : accessoriesCount,
            id : getval,
            name : selectedText,
            qty : productqty,
            unitname : unitname
        }
        selectAccessoriesResult.push(temp);

        $('#productAccessoriesId').val('').trigger('change');
        $("#productqtyA").val('');


        $('#accessoriesLength').val(accessoriesCount);

    }
}

var currentRemoveAcc = 0;

function removeModelResponse() {


    id = currentRemoveAcc;
    var temp=[];
    var tempCount =1;


    for (let i = 0; i < selectAccessoriesResult.length; i++) {

        if(selectAccessoriesResult[i].count != id){
            var j = {
                count : tempCount,
                id : selectAccessoriesResult[i].id,
                name : selectAccessoriesResult[i].name,
                qty : selectAccessoriesResult[i].qty,
                unitname : selectAccessoriesResult[i].unitname
            }
            temp.push(j);
            tempCount++;
        }
    }

    selectAccessoriesResult = temp;
    
    $('#tableAccessoriesList > tbody').empty();

    for (let i = 0; i < selectAccessoriesResult.length; i++) {
        var record = '<tr id="tr' + selectAccessoriesResult[i].count + '">'+
            '<input type="hidden" name="productAccessoriesid' + selectAccessoriesResult[i].count + '" value="' + selectAccessoriesResult[i].id + '" >'+
            '<input type="hidden" name="productAccessoriesqty' + selectAccessoriesResult[i].count + '" value="' + selectAccessoriesResult[i].qty + '" >'+
            '<td>' + selectAccessoriesResult[i].count + '</td>'+
            '<td>' + selectAccessoriesResult[i].name + '</td>'+
            '<td>' + selectAccessoriesResult[i].qty + '</td>'+
            '<td>' + selectAccessoriesResult[i].unitname + '</td>'+
            '<td onclick="removeaccessories(' + selectAccessoriesResult[i].count + ')"><i class="fas fa-times" style="color: red;"></i></td>'+
            '</tr>';

        $('#tableAccessoriesList').append(record);
    }

    $('#length').val(selectAccessoriesResult.length);
    accessoriesCount = selectAccessoriesResult.length;
    
   $('#removeWarringModel').modal('hide');
   currentRemoveAcc =0;
}

function removeaccessories(id) {
   
    if(lock == 1){
        alert("Accessories are locked");
        return;
    }

    
   currentRemoveAcc = id;
   $('#removeWarringModel').modal('show');
}

function resetAccessories() {

    $("#tableAccessoriesList > tbody").empty();
    while (selectAccessoriesResult.length > 0) {
        selectAccessoriesResult.pop();
    }
    accessoriesCount = 0;
    $('#accessoriesLength').val(accessoriesCount);

}

function  lockTable() {

    if(lock == 3){
        alert("This Accessories Under Use");
        return;
    }

    if(lock == 0){
        lock = 1;
        $('#lockTableBtn').text("Unlock");
    }else if(lock == 1){
        lock = 0;
        $('#lockTableBtn').text("Lock");
    }

}


function groupTypeChange() {
    let grouptypeid = $('#groupTypeid').val();
    var url = $('#urlGetGroup').val();
    var _token = $('input[name="_token"]').val();

    $('#groupid').html('');
    if (groupid) {
        $.ajax({
            type: 'POST',
            url: url,
            data: {grouptypeid: grouptypeid, _token: _token},
            success: function (data) {
                $('#groupid').append(data);
            }
        });
    } else {
        alert('please select Group');
    }
}

function findProduct() {

    $("#tableProductList > tbody").empty();
    productArray = [];

    let productsubgroup = $("#groupname").val();
    let productgroup = $('#grouptypename').val();
    let company = $('#makename').val();
    let specific = $('#specificationname').val();
    let subspecificationname = $('#subspecificationname').val();
    let  productid = $('#productid').val();
    let anykeyword=  $('#anykeyword').val();
    var _token = $('input[name="_token"]').val();
    var url = $('#urlGetProduct').val();

    totalProduct = 0;
    // if(!company){
    //     alert("Please Select Company");
    //     return;
    // }

    // if(!productgroup){
    //     alert("Please Select Group Type");
    //     return;
    // }

    // if(!productsubgroup){
    //     alert("Please Select Product");
    //     return;
    // }
    // if(specific){

    // }
  
   
  
        $.ajax({
            type : 'POST',
            url : url,

            data : {groupid:productsubgroup, grouptypeid:productgroup, companyid:company,
                specific:specific,subspecificationname:subspecificationname,productid:productid,
                anykeyword:anykeyword, _token : _token},
            success : function(data){
                totalProduct = data.length;
                productArray = data;
                addProductList(data);

            }
        });
   
}


function addProductList(data) {
   var count = 1;
   if ($('#selectAll').is(":checked"))
   {
        data.forEach(element => {
            var record = '<tr id="tr' + element.id + '">'+
                '<td>' + count + '</td>'+
                '<td>' + element.subnameofproduct + '</td>'+
                '<td><input class="checkSelect" type="checkbox"  id="checkP'+element.id+'" onchange="checkOne('+element.id+')" checked></td>'+  
                '<td id="tdPstatus'+ element.id+'" class="checkStaus">Pending</td>'+ 
                '<td id="tdCount'+ element.id+'">-</td>'+
                '<td><a href="editproduct/'+element.id+'" title="Edit" target="_blank"><i class="fa fa-eye" style="color : #f39c12;"></i></a></td>'+
                '</tr>';
            $('#tableProductList').append(record);
            count++;
        });

   }else{
        data.forEach(element => {
            var record = '<tr id="tr' + element.id + '">'+
                '<td>' + count + '</td>'+
                '<td>' + element.subnameofproduct + '</td>'+
                '<td><input class="checkSelect" type="checkbox" onchange="checkOne('+element.id+')"  id="checkP'+element.id+'"></td>'+  
                '<td id="tdPstatus'+ element.id+'"  class="checkStaus">-</td>'+ 
                '<td id="tdCount'+ element.id+'">-</td>'+
                '<td><a href="editproduct/'+element.id+'" title="Edit" target="_blank"><i class="fa fa-eye" style="color : #f39c12;"></i></a></td>'+
                '</tr>';
            $('#tableProductList').append(record);
            count++;
        });
   }
    
}

function checkAll() {
    if ($('#selectAll').is(":checked"))
    {
        $(".checkSelect").prop("checked",true);
        $('.checkStaus').text("Pending");
    }else{
        
        $(".checkSelect").prop("checked",false);
        $('.checkStaus').text("-");
    }
    
}

function checkOne(id) {
    if ($('#checkP'+id).is(":checked")){
        $('#tdPstatus'+id).text("Pending");
    }else{
        $('#tdPstatus'+id).text("-");
    }
}

var totalCheck = 0;

function onset() {
    if(lock == 0){
        alert("Please Lock Accessories");
        $('html, body').animate({
            scrollTop: $("#lockTableBtn").offset().top
        }, 1000);
        return;
    }
    if(lock == 3){
        alert("On Process");
        return;
    }
    if(selectAccessoriesResult.length == 0){
        alert("Please Enter Accessories");
        $('html, body').animate({
            scrollTop: $("#productAccessoriesId").offset().top
        }, 1000);
        return;
    }
    lock = 3;
    $('#btnSet').text("On Progress");
    getSelectProduct();
}

function getSelectProduct() {
    productArray.forEach(element => {
        if ($('#checkP'+element.id).is(":checked")){
            ajaxCall(element.id);
        }
    });
  
    lock = 0;
    $('#lockTableBtn').text("Lock");
    $('#btnSet').text("SET");
    $('#btnClear').show();
}



function ajaxCall(id) {
   
    var _token = $('input[name="_token"]').val();
    var url = $('#setAccessories').val();

    $.ajax({
        type : 'POST',
        url : url,
        data : {pid:id, accessories:selectAccessoriesResult, _token : _token},
        success : function(data){
            if(data.status){
                totalCheck++;
                $('#totalCheck').text(totalCheck);
                $('#tdPstatus'+id).text("Done");
                $('#tdCount'+id).text(data.conut);
                $('html, body').animate({
                    scrollTop: $('#tdCount'+id).offset().top
                }, 100);
            }
          
        }
    });
}

function clearAll() {
    selectAccessoriesResult = [];
    $('#totalCheck').text(0);
    $("#tableProductList > tbody").empty();
    $("#tableAccessoriesList > tbody").empty();
    productArray = [];
    accessoriesCount = 0;
    totalProduct = 0;
    totalCheck = 0;
    $('#btnClear').hide();
}
