// compartments array from badle
// groupTypes array from badle
var compartmentsOpetion =""; 
var groupTypesOpetion ="";
var count;
var compartmentSubGrpArray = []; 
var mainCompartment = [];

$( document ).ready(function() {
   
    if(compartments.length == 0){
        compartmentsOpetion = '<option value="">--No Compartment Available--</option>';
    }else{
        compartmentsOpetion = '<option value="">Select Compartment</option>';
        compartments.forEach(element => {
            compartmentsOpetion = compartmentsOpetion+'<option value="'+element.compartmentid+'">'+element.compartmentname+'</option>';
        });
    } 
    if(groupTypes.length == 0){
        groupTypesOpetion = '<option value="">--No Group Type Available--</option>';
    }else{
        groupTypesOpetion = '<option value="">Select Group Type</option>';
        groupTypesOpetion = groupTypesOpetion+'<option value="-1">All Group Type</option>';
        groupTypes.forEach(element => {
            groupTypesOpetion = groupTypesOpetion+'<option value="'+element.id+'">'+element.grouptypename+'</option>';
        });
        
    }
  

    onBomPanelChange();
});


function  onBomPanelChange() {
    var id = $('#bompanelmasterid').val();
    count = 0;
    $('#divFeedersList').empty();
    $('#length').val(0);
    if(id){
        ajaxCall(id);
    }
}

function  ajaxCall(id) {
    $('#divMainFeeder').empty();
    var url= $('#getPanelMasterFeeder').val();
    var _token = $('input[name="_token"]').val();
    
    if(id){
       
        $.ajax({
            type : 'POST',
            url : url,
            data : {panelId:id, _token : _token},
            success : function(data){
               
                setFeederView(data);
                
                $('#length').val(data.length);
            }
        });
    }
} 

function  setFeederView(data) {
    compartmentSubGrpArray = [];

    mainCompartment = [];
    data.forEach(element => {
        count++;
        var temp ={
            countNumber : count,
            grouptypeList : []
        }
        compartmentSubGrpArray.push(temp);

        var mainTemp = {
            countNumber: count,
            feedername : element.feedername,
            feederid : element.feederid,
            compartments : []
        }
        mainCompartment.push(mainTemp);

        
        var html =  '<div class="card"  style="border-top: 0px !important">'+
                        '<div class="card-header with-border">'+
                            '<h3 class="card-title">'+element.feedername+'</h3>'+
                            '<div class="card-tools pull-right">'+
                                '<button type="button" class="btn btn-tool" data-card-widget="collapse">'+
								'<i class="fas fa-minus"></i>'+
							    '</button>'+
                            '</div>'+
                        '</div>'+
                        '<div class="card-body">'+
                            '<div class="row">'+
                                '<div class="col-md-6">'+
                                 '<label for="compartmentSelectId'+count+'">Compartment</label>'+
                                    '<div class="input-group">'+
                                        '<select  id="compartmentSelectId'+count+'" class="form-control select2 compartmentList">'+compartmentsOpetion+'</select>'+
                                        '<span class="input-group-addon">'+          
                                        '<button class="btn bg-blue" type="button" onclick="addCompartment('+count+')" title="Add New Compartment"><i class="fa fa-plus"></i></button>'+
                                        '</span>'+
                                    '</div>'+
                                '</div>'+
                                '<div class="col-md-4">'+
                                    '<div class="form-group">'+
                                        '<div class="input group">'+
                                            '<label for="groupTypeSelectId'+count+'">Group Type</label>'+
                                            '<select  id="groupTypeSelectId'+count+'" class="form-control select2">'+groupTypesOpetion+'</select>'+
                                        '</div>'+
                                    '</div>'+
                                '</div>'+
                                '<div class="col-md-1">'+
                                    '<div class="form-group row">'+
                                        '<label>&nbsp;</label>'+
                                        '<div class="form-control btn btn-primary" onclick="onAddGroupTypeInTiTable('+count+')"><i class="fas fa-arrow-right"  aria-hidden="true"></i></div>'+
                                    '</div>'+
                                '</div>'+
                                '<div class="col-md-8">'+
                                    '<div class="form-group">'+
                                        '<table id="groupTypeListTable'+count+'" class="table" style="margin-right: 2%">'+
                                            '<thead>'+
                                                '<th>Sr No.</th>'+
                                                '<th>GroupType</th>'+
                                                '<th>&nbsp;</th>'+
                                            '</thead>'+
                                            '<tbody>'+
                                            '</tbody>'+
                                        '</table>'+
                                    '</div>'+
                                '</div>'+
                                '<div class="col-md-12 row">'+
                                    '<div class="col-md-3">'+
                                        '<div onclick="adonAddCompartmentInToTable('+count+')" class="btn btn-primary">Add Compartment</div>'+
                                    '</div>'+
                                '</div>'+
                                '<div class="col-md-12" style="margin-top: 15px">'+
                                    '<div class="">'+
                                        '<table class="table table-bordered" id="compartmentListTable'+count+'">'+
                                            '<thead>'+
                                                '<th>#</th>'+
                                                '<th>Compartment Name</th>'+
                                                '<th>Group Type</th>'+
                                                '<th>Action</th>'+
                                            '</thead>'+
                                            '<tbody>'+
                                            '</tbody>'+
                                        '</table>'+    
                                    '</div>'+
                                '</div>'+
                            '</div>'+
                        '</div>'+
                    '</div>'+
                    '<input type="hidden" name="sublength'+count+'" id="sublength'+count+'" value="0">';

        $('#divFeedersList').append(html);
        $('.select2').select2();

    });

    console.log(mainCompartment);

}

function onAddGroupTypeInTiTable(pCount) {
    
    var id='#groupTypeSelectId'+pCount;
    var grpTypeId = $(id).val();
    var grpTypeText = $(id+' :selected').text();
    if(!grpTypeId){
        alert('Please Select Group Type');
        return;
    }

    var currentComgrpTypeList = [];

    compartmentSubGrpArray.forEach(element =>{
        if(element.countNumber == pCount){
            currentComgrpTypeList = element.grouptypeList;
        }
    });
  
    var groupSequenceNo = currentComgrpTypeList.length;

    for(var i = 0; i<currentComgrpTypeList.length ;i++){
        if(currentComgrpTypeList[i].grpTypeId == grpTypeId){
            alert('This Group Type Alredy Listed');
            return;
        }
    }

    for(var j = 0 ; j<compartmentSubGrpArray.length;j++){
        if(compartmentSubGrpArray[j].countNumber == pCount){
            groupSequenceNo++;
            var temp = {
                groupSequenceNo : groupSequenceNo,
                grpTypeId : grpTypeId,
                grpTypename : grpTypeText
            }
            compartmentSubGrpArray[j].grouptypeList.push(temp);
            break;
        }

    }


    
    var html =  '<tr id="trComGroupTypeTable'+pCount+''+groupSequenceNo+'">'+
                    '<input type="hidden" id="comGroupTypeid'+pCount+''+groupSequenceNo+'"  value="'+grpTypeId+'">'+
                    '<input type="hidden" id="comGroupTypeSeqence'+pCount+''+groupSequenceNo+'"  value="'+groupSequenceNo+'">'+
                    '<td>'+groupSequenceNo+'</td>'+
                    '<td>'+grpTypeText+'</td>'+
                    '<td onclick="removeGroupType('+pCount+','+groupSequenceNo +')"><i class="fas fa-times" style="color: red;"></i></td>'+
                '</tr>';
    $('#groupTypeListTable'+pCount).append(html);

    $(id).val('').trigger('change');
}

function removeGroupType(pCount,groupSequenceNo) {
   
    var currentComgrpTypeList = [];

    compartmentSubGrpArray.forEach(element =>{
        if(element.countNumber == pCount){
            currentComgrpTypeList = element.grouptypeList;
            return;
        }
    });
    
    var tempAarry = currentComgrpTypeList;
    for(var i = 0; i<currentComgrpTypeList.length ;i++){
        if(currentComgrpTypeList[i].groupSequenceNo == groupSequenceNo){
            tempAarry.splice(i, 1);
        }
    }

    currentComgrpTypeList = tempAarry;

    var tempSeq = 0;
    for(var j = 0 ; j<currentComgrpTypeList.length ; j++){
        tempSeq++;
        currentComgrpTypeList[j].groupSequenceNo = tempSeq;
    }

    var id ="#groupTypeListTable"+pCount;
    $(id+' tbody').empty();
    var tempSeq = 0;
    for(var k = 0; k<currentComgrpTypeList.length;k++){
        tempSeq++;
        var html =  '<tr id="trComGroupTypeTable'+pCount+''+tempSeq+'">'+
            '<input type="hidden" id="comGroupTypeid'+pCount+''+tempSeq+'" value="">'+
            '<input type="hidden" id="comGroupTypeSeqence'+pCount+''+tempSeq+'" value="">'+
            '<td>'+tempSeq+'</td>'+
            '<td>'+currentComgrpTypeList[k].grpTypename+'</td>'+
            '<td onclick="removeGroupType('+pCount+','+tempSeq +')"><i class="fas fa-times" style="color: red;"></i></td>'+
            '</tr>';
        
        $('#groupTypeListTable'+pCount).append(html);
    }

}

function adonAddCompartmentInToTable(pCount) {

    var selectedId = '#compartmentSelectId'+pCount;

    var selectedCompartment = $(selectedId).val();
    var selectedCompartmentText = $(selectedId+' :selected').text();
    var id = "#groupTypeListTable"+pCount;
    var grpTablelength =  $(id+' tbody tr').length;

    if(!selectedCompartment){
        alert("Please Select a Compartment");
        return;
    }
    if(grpTablelength == 0){
        alert("Please Select a Group Type");
        return;
    }

    
    var currentMasterCompartment = [];
    var feederid ="";
    var feedername ="";
    for(var i = 0;i <mainCompartment.length;i++){
       if(mainCompartment[i].countNumber == pCount){
            feederid = mainCompartment[i].feederid;
            feedername =  mainCompartment[i].feedername;
            currentMasterCompartment =mainCompartment[i].compartments;
            break;
        }
    }
    var sequenceNo =currentMasterCompartment.length+1;
    
    for(var i = 0 ; i<currentMasterCompartment.length;i++){
        if(selectedCompartment == currentMasterCompartment[i].compartmentid){
            alert("This Compartment already listed");
            return;
        }
    }
    $('#sublength'+pCount).val(sequenceNo);


    var currentCompartment = "";
    compartmentSubGrpArray.forEach(element =>{
        if(element.countNumber == pCount){
            currentCompartment = element;
        }
    });

    var currentGrpList = currentCompartment.grouptypeList;

    var list = "";
    var ids = [];
    currentGrpList.forEach(element => {
       ids.push(element.grpTypeId);
       list = list + '<li>'+element.grpTypename+'</li>';
   });

   var idsJson = JSON.stringify(ids);
   
 
   for(var i = 0;i <mainCompartment.length;i++){
        if(mainCompartment[i].countNumber == pCount){
            var temp ={
                sequenceNo : pCount+''+sequenceNo,
                compartmentid:selectedCompartment,
                compartmentName :selectedCompartmentText,
                groupstypeids :idsJson,
                grouptypeList :currentGrpList

            }
            mainCompartment[i].compartments.push(temp);
        }
    }



    var html =  '<tr id="trCompartment'+pCount+''+sequenceNo+'">'+
    
        '<input type="hidden" id="compartmentCount'+pCount+''+sequenceNo+'" name="compartmentCount'+pCount+''+sequenceNo+'" value="'+pCount+'">'+
        '<input type="hidden" id="compartmentfeederid'+pCount+''+sequenceNo+'" name="compartmentfeederid'+pCount+''+sequenceNo+'" value="'+feederid+'">'+
        '<input type="hidden" id="compartmentfeedername'+pCount+''+sequenceNo+'" name="compartmentfeedername'+pCount+''+sequenceNo+'" value="'+feedername+'">'+
        '<input type="hidden" id="compartmentid'+pCount+''+sequenceNo+'" name="compartmentid'+pCount+''+sequenceNo+'" value="'+selectedCompartment+'">'+
        '<input type="hidden" id="compartmentName'+pCount+''+sequenceNo+'" name="compartmentName'+pCount+''+sequenceNo+'" value="'+selectedCompartmentText+'">'+
        '<input type="hidden" id="compartmentSeqence'+pCount+''+sequenceNo+'" name="compartmentSeqence'+pCount+''+sequenceNo+'" value="'+pCount+''+sequenceNo+'">'+
        '<input type="hidden" id="compartmentGroupType'+pCount+''+sequenceNo+'" name="compartmentGroupType'+pCount+''+sequenceNo+'" value="'+ids+'">'+
        '<td>'+sequenceNo+'</td>'+
        '<td>'+selectedCompartmentText+'</td>'+
        '<td><ul>'+list+'</ul></td>'+
        '<td onclick="removeCompartment('+pCount+','+pCount+''+sequenceNo+')"><i class="fas fa-times" style="color: red;"></i></td>'+
    '</tr>';

    $('#compartmentListTable'+pCount).append(html);

    var groupTableid ="#groupTypeListTable"+pCount;
    $(groupTableid+' tbody').empty();

    compartmentSubGrpArray.forEach(element =>{
        if(element.countNumber == pCount){
            element.grouptypeList =[];
        }
    });

    $(selectedId).val('').trigger('change');
}

function removeCompartment(pCount,sequenceNo) {
    var currentMasterCompartment = [];
    var feederid ="";
    var feedernmae ="";
    for(var i = 0;i <mainCompartment.length;i++){
       if(mainCompartment[i].countNumber == pCount){
            feederid = mainCompartment[i].feederid;
            feedernmae =  mainCompartment[i].feedername;
            currentMasterCompartment =mainCompartment[i].compartments;
            break;
        }
    }

    
    var tempAarry = currentMasterCompartment;
    for(var i = 0; i<currentMasterCompartment.length ;i++){
        if(currentMasterCompartment[i].sequenceNo == sequenceNo){
            tempAarry.splice(i, 1);
        }
    }
    currentMasterCompartment = tempAarry;

    var tempSeq = 0;
    for(var j = 0 ; j<currentMasterCompartment.length ; j++){
        tempSeq++;
        currentMasterCompartment[j].sequenceNo =  pCount+''+tempSeq;
    }
    var id ='#compartmentListTable'+pCount;
    $(id+' tbody').empty();
    var tempSeq = 0;
    for(var k = 0; k<currentMasterCompartment.length;k++){

            var currentGrpList = currentMasterCompartment[k].grouptypeList;

            var list = "";
            var ids = [];
            currentGrpList.forEach(element => {
                ids.push(element.grpTypeId);
                list = list + '<li>'+element.grpTypename+'</li>';
            });

        tempSeq++;
        var html =  '<tr id="trCompartment'+pCount+''+tempSeq+'">'+
            '<input type="hidden" id="compartmentCount'+pCount+''+tempSeq+'" name="compartmentCount'+pCount+''+tempSeq+'" value="'+pCount+'">'+
            '<input type="hidden" id="compartmentfeederid'+pCount+''+tempSeq+'" name="compartmentfeederid'+pCount+''+tempSeq+'" value="'+feederid+'">'+
            '<input type="hidden" id="compartmentfeedername'+pCount+''+tempSeq+'" name="compartmentfeedername'+pCount+''+tempSeq+'" value="'+feedername+'">'+
            '<input type="hidden" id="compartmentid'+pCount+''+tempSeq+'" name="compartmentid'+pCount+''+tempSeq+'" value="'+currentMasterCompartment[k].compartmentid+'">'+
            '<input type="hidden" id="compartmentName'+pCount+''+tempSeq+'" name="compartmentName'+pCount+''+tempSeq+'" value="'+currentMasterCompartment[k].compartmentName+'">'+
            '<input type="hidden" id="compartmentSeqence'+pCount+''+tempSeq+'" name="compartmentSeqence'+pCount+''+tempSeq+'" value="'+tempSeq+'">'+
            '<input type="hidden" id="compartmentGroupType'+pCount+''+tempSeq+'" name="compartmentGroupType'+pCount+''+tempSeq+'" value="'+ids+'">'+
            '<td>'+tempSeq+'</td>'+
            '<td>'+currentMasterCompartment[k].compartmentName+'</td>'+
            '<td><ul>'+list+'</ul></td>'+
            '<td onclick="removeCompartment('+pCount+','+pCount+''+tempSeq+')"><i class="fas fa-times" style="color: red;"></i></td>'+
        '</tr>';

        $('#compartmentListTable'+pCount).append(html);
        
    }
    $('#sublength'+pCount).val(tempSeq);
}

var currentSelector = 0;

function addCompartment(pCount) {
    $('#addCompartmentModel').modal('show');
    currentSelector = pCount;
}

function addCompartmentName() {
    var name = $('#newcompartmentname').val();

    if(name == ""){
        alert('please enter compartment name');
        return;
    }

    var url= $('#addCompartmentUrl').val();
    var _token = $('input[name="_token"]').val();
   
    if(name){
       
        $.ajax({
            type : 'POST',
            url : url,
            data : {compartmentname:name, _token : _token},
            success : function(data){
                if(data.state){

                    $('.compartmentList').empty();                    
                    var html = '<option value="">Select Compartment</option>';
                    
                    data.data.forEach(element => {
                        html = html+'<option value="'+element.compartmentid+'">'+element.compartmentname+'</option>';
                    });

                    $('.compartmentList').append(html);
                    $('#compartmentSelectId'+currentSelector).val(data.id).trigger('change');
                    currentSelector = 0;
                    $('#addCompartmentModel').modal('hide');
                    $('#newcompartmentname').val('');
                }else{
                    alert(data.name);
                }
            }
        });
    }
}

function onSubmit() {

    var id = $('#bompanelmasterid').val();

    if(!id){
        alert("Please Select Panel");
        return false;
    }

    for(var i=1;i<=count;i++){
        var id = "#compartmentListTable"+i;
        var table =  $(id+' tbody tr').length;
        if(table == 0){
            mainCompartment.forEach(element => {
                if(element.countNumber == i){
                    var tableName = element.feedername;
                    alert('Please add compartment in '+tableName);
                }
            });
            return false;
        }
    }

    return true;
    
}
