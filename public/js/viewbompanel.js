var currentUpdatePanelId = 0;

var status = ""
function  updatePanel(pId) {
    currentUpdatePanelId = pId;
    var name =  $('#panelName'+currentUpdatePanelId).text();    
    $('#oldPanelName').val(name);
    status = ""
    bomPanels.forEach(element => {
        if(element.bompanelmasterid == currentUpdatePanelId){
            if(element.status == "Active"){
                $('#statusBtn').text('Deactive');
                status = "Deactive";
            }else{
                status = 'Active';
                $('#statusBtn').text('Active');
            }
        }
    });
    $('#updatePanelModel').modal('show');
}

function updateAjaxPanel() {
    var url = $('#updateURL').val()+currentUpdatePanelId;
    var id = currentUpdatePanelId;
    var name = $('#oldPanelName').val();
    var _token = $('input[name="_token"]').val();

    if(id != 0){
       
        $.ajax({
            type : 'POST',
            url : url,
            data : {panelmastername:name, _token : _token},
            success : function(data){
               if(data.state){
                   $('#panelName'+id).text(data.data.panelmastername);
                   $('#updatePanelModel').modal('hide');
                    currentUpdatePanelId = 0;
               }else{
                   alert(data.message);
               }
            }
        });
    }
}

function cancelUpdate() {
    $('#updatePanelModel').modal('hide');
    currentUpdatePanelId = 0;
}

function changeStatus() {
    var url1 = $('#updateStatusURL').val();
    var id = currentUpdatePanelId;
    var status = 'Deactive';
    alert(status);
    var _token = $('input[name="_token"]').val();
    if(id != 0){
       
        $.ajax({
            type : 'POST',
            url : url1,
            data : {status:status,id: id,_token : _token},
            success : function(data){
                if(data.state){
                    for (let i = 0; i < bomPanels.length; i++) {
                        if(bomPanels[i].bompanelmasterid == data.id){
                            bomPanels[i] = data.data;
                            alert("Successfully "+status);
                            status = "";
                        }
                    }
                       
                    $('#updatePanelModel').modal('hide');
                }else{
                    alert(data.message);
                }
           
            }
        });
    }
}

var currentUpdatePanelCat = 0;

function openCatModel(pid) {
    
    currentUpdatePanelCat = pid;
    $('#addCatPanelModel').modal('show');
}

function  cancelCreateCat() {
    $('#addCatPanelModel').modal('hide');
}

function checkCatNo(pid) {
    var catNo =  $('#catno').val();
    var url = $('#catURL').val();
    var _token = $('input[name="_token"]').val();
     if(!catNo){
         alert("Please Enter Cat No.");
        return;
     }
    $.ajax({
        type : 'POST',
        url : url,
        data : {catNo:catNo, _token : _token},
        success : function(data){
            if(data.status){
                $('#catno').val('');
                window.location = "/panel/create/"+currentUpdatePanelCat+'/'+catNo;
            }else{
                alert(data.message);
            }
        }
    });

}
