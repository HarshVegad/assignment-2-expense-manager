var currentType = 0;

$( document ).ready(function() {
    $('#formbom').hide();
    currentType = 0;
    $('input[type=radio][name=bomtype]').change(function() {
        if (this.value == 'panel') {
            currentType = 0;
            $('#panelorbomid').val('');
            setFromPanel();
        }else if (this.value == 'bom') {
            currentType = 1;
            $('#panelorbomid').val('');
            setFromBOM();

        }
    });
    if(panelinfo){
        loadsuggestedpanelcatno();         
    }

});

function  setFromPanel() {
    $('#fomPanel').show();
    $('#formbom').hide();
    $('#oldBomParty').val('').trigger('change');
    $('#quotationid').val('').trigger('change');
    $('#oldbomid').val('');
    $('#panelorbomid').val('');
}

function setFromBOM() {
    $('#fomPanel').hide();
    $('#formbom').show();
    $('#bompanelmasterid').val('').trigger('change'); 
    // $('#oldBOMModel').modal('show');
    $('#panelorbomid').val('');
}

function openModel() {
    $('#oldBOMModel').modal('show');
}

function partyChange(){
var partyId = $('#partyid').val();
for(var i = 0; i<partys.length ; i++){
    if(partyId == partys[i].id){
        setGSTType(i);
        break;
    }
}
}
 
function setGSTType(i){

    var gstCode = partys[i].gstcode;
    var gstState = partys[i].statename;
 
    if("24" == gstCode){
        $('#gstType').val('GST').trigger('change');
    }else{
        $('#gstType').val('IGST').trigger('change');
    }
    var placeofsupplay = gstCode+'-'+gstState;
    $('#placeofsupply').val(placeofsupplay);
}

function getBOMPanel() {

    var id = $('#bompanelmasterid').val();
    var url = $('#urlGetPanel').val();
    var _token = $('input[name="_token"]').val();
    $('#bompanelmasteridhidden').val(id);
    $('#bompanelid').empty();
    $.ajax({
        type : 'POST',
        url : '/panel/getPanels',
        data : {bompanelmasterid:id, _token : _token},
        success : function(data){

            var html = "";
             
            if(data.length == 0){
                html = '<option value="">--No Panel Available--</option>';
            }else{
                html = '<option value="">--Select Panel--</option>';
            }

            data.forEach(element => {
                html = html+ '<option value="'+element.bompanelid+'">'+element.panelcatno+'</option>';
            });

            
            $('#bompanelid').append(html);
        }
    });
}

function vaild() {
    if(currentType == 0){
        
        var id = $('#bompanelmasterid').val();
        var pId = $('#bompanelid').val();

        if(id == undefined || id == null || id == ""){
            alert("Please Select PANEL MATER");
            return false;
        }
        
        
        if(pId == undefined || pId == null || pId == ""){
            alert("Please Select PANEL");
            return false;
        }

        return true;
        
        
    }else if(currentType == 1){

        var id = $('#panelorbomid').val();

        if(id == undefined || id == null || id == ""){
            alert("Please Select BOM");
            return false;
        }
        return true;
    }
}

function onPanelChange(){
   var id = $('#bompanelid').val();
   $('#panelorbomid').val(id);
}

var masterPanelId = 0;
var tempList = [];

function oldbomParty() {
    

    var partyid = $('#similartocustomer').val();
    var qutID = $('#similartoquotationno').val();
    var partyName = $('#similartocustomer :selected').text();
    var qutNo = $('#similartoquotationno :selected').text();
    masterPanelId = $('#sugpanelcatno').val();
 
    if(!partyid){
        alert('Please Select Cutomer');
        return;
    }
    if(!qutID){
        alert('Please Select Quotation');
        return;
    }
    if(masterPanelId == 0){
        alert('Please Re-Select Quotation');
        return;
    }



    $('#oldbomid').val(qutNo);
    $('#panelorbomid').val(qutID);
    
    $('#bompanelmasteridhidden').val(masterPanelId);

    $('#oldBOMModel').modal('toggle');

}

function onChangeOldBOMParty() {

    var partyid = $('#oldBomParty').val();

    var _token = $('input[name="_token"]').val();
    var url = $('#urlgetoldBOM').val();
    $('#quotationid').empty();
    tempList = [];
    if(partyid){
        $.ajax({
            type : 'POST',
            url : url,
            data : {partyId:partyid, _token : _token},
            success : function(data){
               
                var html = "";
                    if(data.length == 0){
                        html = '<option value="">--No Qutation Available--</option>';
                    }else{
                        html = '<option value="">Select Qutation No.</option>';
                    }
                    data.forEach(element => {
                        tempList.push(element);
                        html = html + '<option value="'+element.bomid+'">'+element.qutationno+'</option>';
                    });

                    $('#quotationid').append(html);
                
            }
        });
    }
    
}
function loadsuggestedpanelcatno(){

    $('#similartoquotationno').empty();
    $('#similartocustomer').empty();
    $('#listofquotationno').remove();
    var _token = $('input[name="_token"]').val();
    var url = '/getsuggPanelCatno';
    var nameOfpanelid = $('#nameOfpanel').val();
    // var similartocustomer = $('#similartocustomer').val(); 
    $.ajax({
        url:url,
        method:"GET",
        data : {_token:_token,nameOfpanelid:nameOfpanelid},
        success:function(result)
        {
           var response = result.sugpanelcatno;
            if(response){
                
                
                $('#sugpanelcatno').empty();
                
             var suggPanelCatno = '<option value="">--Please Select--</option>';
             response.forEach(element => {
                     suggPanelCatno  +='<option value="'+element.bompanelid+'" >'+element.panelcatno+'</option>';
                   
                });
                $('#sugpanelcatno').append(suggPanelCatno);
                if(panelinfo){
                
                    $('#sugpanelcatno').val(panelinfo.suggestedpanelcatnopanelid).trigger('change');
                   
                     $('#bompanelid').val(panelinfo.suggestedpanelcatnopanelid).trigger('change');

                    loadsimilarcustomer();

                }
                $('#inquiryids').val(result.inquiryids);
                //  ();
                // genrateData();
            }else{
               
            }
        },
        dataType:"json"
    });
   
}


$('#similartoquotationno').on('change',function(){
    var similartoquotationno = $('#similartoquotationno').val();
    var similartoquotationno = $('#similartoquotationno').select2("data");

   $('#listofquotationno').remove();
   var append = '<table  id="listofquotationno" align="center" style="margin-left: 35%;margin-top: 5%;"><tbody>';
   var index=1;

   similartoquotationno.forEach(element => {
    if(element.id){
   var url = 'exportbompdf/'+element.id+'/ap';
    append+="<tr class='margin'><td> "+index+" </td><td><a href='/exportbompdf/"+element.id+"/ap/option' target='_blank'>"+element.text+"</a></td></tr>";
    index++;   
    }
  
});
   append+='</tbody></table>';
   $('#viewsuggestion').after(append);
});
function pageRedirect(urlparameter) {
    window.location.replace('\sdfgd');
    // window.location.href = "exportbompdf/"+urlparameter+"/ap";
  } 
  function loadsimilarcustomer(){
    var _token = $('input[name="_token"]').val();
    var sugpanelcatnos = $('#sugpanelcatno').val();
    var nameOfpanelid = $('#nameOfpanel').val();
    $.ajax({
        url: '/loadsimilarcustomers',
        method:"GET",
        data : {_token:_token,nameOfpanelid:nameOfpanelid,sugpanelcatnos:sugpanelcatnos},
        success:function(result)
        {
           
            if(result){
                $('#similartoquotationno').empty();
                $('#similartocustomer').empty();
                $('#listofquotationno').remove();
             var similarcustomer = '<option value="" >--Please Select--</option>';
                result.forEach(element => {
               
                    similarcustomer  +='<option value="'+element.partyid+'" >'+element.companyname+'</option>';
                   
                });
                // similartocustomer
                // similartoquotationno
                 $('#similartocustomer').append(similarcustomer);

         
                
            }else{
               
            }
        },
        dataType:"json"
    });
} 
function loadsimilartoquotationno(){
    var _token = $('input[name="_token"]').val();
    var sugpanelcatnos = $('#sugpanelcatno').val();
    var nameOfpanelid = $('#nameOfpanel').val();
    var similartocustomer=$('#similartocustomer').val();
    var inquiryids = $('#inquiryids').val();
    $.ajax({
        url: '/loadsimilarfieldsforinquiry',
        method:"GET",
        data : {_token:_token,nameOfpanelid:nameOfpanelid,sugpanelcatnos:sugpanelcatnos,inquiryids:inquiryids,similartocustomer:similartocustomer},
        success:function(result)
        {
           
            if(result){
                 $('#similartoquotationno').empty().trigger("change");
                // $('#similartoquotationno').empty();
             var similarcustomer = '<option value="" >--Please Select--</option>';
                result.forEach(element => {
               
                    similarcustomer  +='<option value="'+element.bomid+'" >'+element.qutationno+'</option>';
                   
                });
                // similartocustomer
                // similartoquotationno
                 $('#similartoquotationno').append(similarcustomer);
                 
                // $('#sugpanelcatno').select2();
                // loadsimilarfields();
                
            }else{
               
            }
        },
        dataType:"json"
    });
}

// $('#nameOfpanel').on('change',function(){
//     var _token = $('input[name="_token"]').val();
//     var url = '/getsuggPanelCatno';
//     var nameOfpanelid = $('#nameOfpanel').val();
  
//     $.ajax({
//         url:url,
//         method:"GET",
//         data : {_token:_token,suggpanelid:nameOfpanelid},
//         success:function(result)
//         {
           
//             if(result){
//                 $('#sugpanelcatno').empty();
//              var suggPanelCatno = '<option value="" >--Please Select--</option>';
//                 result.forEach(element => {
//                      suggPanelCatno  +='<option value="'+element.bompanelid+'" >'+element.panelcatno+'</option>';
                   
//                 });
//                 $('#sugpanelcatno').append(suggPanelCatno);
//                 $('#sugpanelcatno').select2();
//                 loadsimilarfields();
               
//             }else{
               
//             }
//         },
//         dataType:"json"
//     });
   
// });
// function loadsimilarfields(){
//     var _token = $('input[name="_token"]').val();
//     var sugpanelcatnos = $('#sugpanelcatno').val();
//     var nameOfpanelid = $('#nameOfpanel').val();
//     $.ajax({
//         url: '/loadsimilarfieldsforinquiry',
//         method:"GET",
//         data : {_token:_token,nameOfpanelid:nameOfpanelid,sugpanelcatnos:sugpanelcatnos},
//         success:function(result)
//         {
           
//             if(result){
//                 $('#similartocustomer').empty();
//                 $('#similartoquotationno').empty();
//              var similarcustomer = '<option value="">--Please Select--</option>';
//                 result.forEach(element => {
               
//                     similarcustomer  +='<option value="'+element.bomid+'" >'+element.qutationno+'</option>';
                   
//                 });
//                 // similartocustomer
//                 // similartoquotationno
//                  $('#similartoquotationno').append(similarcustomer);
//                  $('#similartoquotationno').select2();
//                 // $('#sugpanelcatno').select2();
//                 // loadsimilarfields();
//                 loadsimilarcustomer();
                
//             }else{
               
//             }
//         },
//         dataType:"json"
//     });
// }
// function loadsimilarcustomer(){
//     var _token = $('input[name="_token"]').val();
//     var sugpanelcatnos = $('#sugpanelcatno').val();
//     var nameOfpanelid = $('#nameOfpanel').val();
//     $.ajax({
//         url: '/loadsimilarcustomers',
//         method:"GET",
//         data : {_token:_token,nameOfpanelid:nameOfpanelid,sugpanelcatnos:sugpanelcatnos},
//         success:function(result)
//         {
           
//             if(result){
//                 $('#similartocustomer').empty();
              
//              var similarcustomer = '<option value="" >--Please Select--</option>';
//                 result.forEach(element => {
               
//                     similarcustomer  +='<option value="'+element.partyid+'" >'+element.companyname+'</option>';
                   
//                 });
//                 // similartocustomer
//                 // similartoquotationno
//                  $('#similartocustomer').append(similarcustomer);
//                  $('#similartocustomer').select2();
//                 // loadsimilarfields();
                
//             }else{
               
//             }
//         },
//         dataType:"json"
//     });
// }
function onQuotation() {

    var qutID = $('#quotationid').val();

    // console.log(tempList);
    tempList.forEach(element => {
        if(element.bomid == qutID){
            masterPanelId = element.bompanelid;
        }
    });
    
}