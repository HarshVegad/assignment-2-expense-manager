var selctedResult = [];
var count;

$(document).ready(function() {
   count = $('#TandCLength').val();
   console.log(count);

   $("#tableTandCList tr:gt(0)").each(function () {
        var this_row = $(this);   
        var val = this_row.find('td:eq(0)').children('input:eq(0)').val();
        selctedResult.push(val);
    });

});

function onAddTandC(){
    // alert("demo");
    var url = $("#urlTandC").val();
    var name = $("#sbtermsname").val();
    var isdefault = "Yes"; 
    var _token = $('input[name="_token"]').val();
    if ($('#isdefault').is(':checked')) {
        isdefault = "Yes";
    } else { 
        isdefault = "No"; 
    } 
    var url = $('#urlTandC').val();

    $.ajax({
        url:url,
        method:"POST",
        data:{_token: _token,sbtermsname:name,isdefault:isdefault},
        success:function(result)
        {
                $("#sbtermsname").val("");
                $('#addTandC').modal('toggle');
                updateOption(result);
        },
        error:function(result){
            alert("The T&C has already been added.");
        },

         dataType:"json"
    });

}

function partyChange(){
  var partyId = $('#partyid').val();
  clearValue();
  for(var i = 0; i<partys.length ; i++){
      if(partyId == partys[i].id){
        setValue(i);
        setGSTType(i);
        break;
      }
  }
}

function setValue(i){

    if(partys[i].mainaddress1 != null){
        $('#buyeraddress1').val(partys[i].mainaddress1);
    }
    if(partys[i].mainaddress2 != null){
        $('#buyeraddress2').val(partys[i].mainaddress2);
    }
    if(partys[i].mainaddress3 != null){
        $('#buyeraddress3').val(partys[i].mainaddress3);
    }

    if(partys[i].shipaddress1 != null){
        $('#shipaddress1').val(partys[i].shipaddress1);
    }
    if(partys[i].shipaddress2 != null){
        $('#shipaddress2').val(partys[i].shipaddress2);
    }
    if(partys[i].shipaddress3 != null){
        $('#shipaddress3').val(partys[i].shipaddress3);
    }

    if(partys[i].gstno != null){
        $('#partygstno').val(partys[i].gstno);
    }
    
    
}

function setGSTType(i){

    var gstCode = partys[i].gstcode;
    var gstState = partys[i].statename;

    if("24" == gstCode){
        $('#gstType').val('GST').trigger('change');
    }else{
        $('#gstType').val('IGST').trigger('change');
    }
    var placeofsupplay = gstCode+'-'+gstState;
    $('#placeofsupply').val(placeofsupplay);
}

function  clearValue(){
    $('#buyeraddress1').val("");
    $('#buyeraddress2').val("");
    $('#buyeraddress3').val("");
    $('#shipaddress1').val("");
    $('#shipaddress2').val("");
    $('#shipaddress3').val("");
    $('#partygstno').val("");
    $('#gstType').val('').trigger('change');
    $('#placeofsupply').val('');
}

function bankchange(){
        
    var banksId = $('#bankid').val();
    clearBankValue();
    for(var i = 0; i<banks.length ; i++){
      if(banksId == banks[i].bankid){
        setBankValue(i);
        break;
        }
    }
}

function setBankValue(i){
    if(banks[i].bankname != null){
        $('#bankname').val(banks[i].bankname);
    }
    if(banks[i].accountnumber != null){
        $('#bankaccount').val(banks[i].accountnumber);
    }
    if(banks[i].ifsccode != null){
        $('#ifsccode').val(banks[i].ifsccode);
    }
}

function clearBankValue(){
    $('#bankname').val('');
    $('#bankaccount').val('');
    $('#ifsccode').val('');
}

function updateOption(result){
    $('#TandC').append("<option value="+result.sbtermsid+" selected>"+result.sbtermsname+"</option>"); 
}

function addTandCTotable(){
    var getval =  $('#TandC').val();
    var selectedText =$('#TandC :selected').text();

    if(!getval){
        alert("Please Select Product Type");
        return;
    }else{
        for(var i=0; i<selctedResult.length; i++) {
            if (selctedResult[i] == getval){
                alert(selectedText+" T&C already listed");
                return;
            }
        }
    }

    count++;
    $('#tableTandCList').append('<tr><td><input type="hidden" name="TandCid'+count+'" value="'+getval+'" >'+count+'<\/td><td>'+selectedText+'<\/td></tr>');
    selctedResult.push(getval);
    $("#TandC").val('').trigger('change');


    $('#TandCLength').val(count);

}


function resetTandCTable(){
    $("#tableTandCList > tbody").empty();
    while(selctedResult.length > 0) {
        selctedResult.pop();
    }
    count = 0;
    $('#TandCLength').val(count);
    console.log(selctedResult);
}
