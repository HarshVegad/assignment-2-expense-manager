var currentCompany;
var currentYear;

$( document ).ready(function() {
    $.ajax({
        url:"/companyListAjax",
        method:"GET",
        success:function(result)
        {
          appendData(result);
        },
         dataType:"json"
    });


    $.ajax({
        url:"/yearListAjax",
        method:"GET",
        success:function(result)
        {
            appendDataOnYear(result);
        },
         dataType:"json"
    });

   
    currentYear = $("#sessionYearId").val();
    currentCompany = $("#sessionCompanyId").val();
    
    
    setDefalt();
    
});


function appendData(result){
    $('#selectCompany').find('option').remove();
    $('#selectCompany').append("<option value=''>Select Company</option>");
    if(result.length != 0){
        
        for(var i = 0 ; i<result.length;i++){
            if(currentCompany == result[i].bankcompanyid){
                $('#selectCompany').append("<option value='"+result[i].bankcompanyid+"' selected> "+result[i].bankcompanyname+" </option>"); 
            }else{
                $('#selectCompany').append("<option value='"+result[i].bankcompanyid+"' > "+result[i].bankcompanyname+" </option>"); 
            }
        }
    }else{
        $('#selectCompany').append("<option value=''>No Company Found</option>");
    }
  
}

function setDefalt(){
    

    if(currentCompany == 0 || currentYear == 0){
        alert('Please Select Commpany and current year');   
        $("#myModal").modal();
    }

}


function appendDataOnYear(result){

    $('#selectYear').find('option').remove()
    $('#selectYear').append("<option value=''>Select Year</option>");
    if(result.length != 0){
        for(var i = 0 ; i<result.length;i++){
            if(currentYear == result[i].yearid){
                $('#selectYear').append("<option value="+result[i].yearid+" selected> "+result[i].yearname+"</option>");
            }else{
                $('#selectYear').append("<option value="+result[i].yearid+"> "+result[i].yearname+"</option>");
            }
                
        }
    }else{
        $('#selectYear').append("<option value=''>No Year Found</option>");
    }
    
}




function setSession(){
    var companyId = $('#selectCompany').val();
    var yearId =$('#selectYear').val();
    
    var _token = $('input[name="_token"]').val();
    $.ajax({
        url:"/setBankSession",
        method:"POST",
        data: {companyId:companyId,yearId:yearId, _token:_token},
        success:function(result)
        {
           $("#sessionYearId").val(result.yearId);
           $("#sessionCompanyId").val(result.companyId);
           
            
            if(result.companyId != companyId){
                alert('Cannnot set a defult company,Please try agin');
                return;
            }
            if(result.yearId != yearId){
                alert('Cannnot set a defult Year,Please try agin');
                return;
            }
            location.reload(true);

        },
        dataType:"json"
    });
}