var listFeeder = [];
var lengthFeeder = 0;
var feederRecord = [];

$( document ).ready(function() {
    $(window).keydown(function(event){
        if(event.keyCode == 13) {
            openSaveWarring();
          event.preventDefault();
          return false;
        }
      });
});
var nooffeeder = 0;

function addFeeder() {

    var currentFeeder =  $('#feederid').val();
  
    var maintemp;
    

    if(currentFeeder){
    
        for (let i = 0; i < feeders.length; i++) {
           if(feeders[i].feederid == currentFeeder){
                if(listFeeder.length == 0){
                    var temp = {
                        feederid : currentFeeder,
                        feedername : feeders[i].feedername,
                        feedercount : 1
                    }
                    listFeeder.push(temp);
                    maintemp = temp;
               
             
                    
                }else{
                    var count = 1;
                    for (let k = 0; k < listFeeder.length; k++) {
                        if(listFeeder[k].feederid == currentFeeder){
                            count = Number(listFeeder[k].feedercount)+1;
                            listFeeder[k].feedercount = count;
                            maintemp = listFeeder[k];
                            break;
                        }
                    }
                    if(count == 1){
                        var temp = {
                            feederid : currentFeeder,
                            feedername : feeders[i].feedername,
                            feedercount : count
                        }
                        maintemp = temp;
                       
                        listFeeder.push(temp);
                       
                    }
                }
           } 
           
           
        }
        nooffeeder++; 
    }else{
        alert("Please Select Feeder");
        return;
    }

    addFieldFeeder(maintemp);
   
    if(currentFeeder == 1 || currentFeeder == 3){
        
        createincomerdiv(nooffeeder);
    }
    if(currentFeeder == 2){
        
        createmeteringdiv(nooffeeder);
    }

}
function createmeteringdiv(nooffeeder){

    var meteringdiv = '<div class="col-sm-12 lastfeeder" style="padding-left:10%"><br>'+
    '<div class="form-group createincomerdiv" id="ratingdiv">'+
    // '<input type="checkbox" id="box-2" id="analog'+nooffeeder+'" value="Analog">'+
    // '<label for="box-2">Analog</label>'
  
        '<div class="col-sm-2">'+
                '<label><input type="radio" name="metering" class="custom-radio"  value="Analog Metering"  id="analog'+nooffeeder+'" onchange="genratemeteringdata('+nooffeeder+')"> Analog Metering</label>'+
        '</div>'+
        '<div class="col-sm-2">'+
            '<label><input type="radio" name="metering" value="Digital Metering" class="custom-radio"  onchange="genratemeteringdata('+nooffeeder+')" id="digital'+nooffeeder+'" > Digital Metering</label>'+
        '</div>'+
        '<label for="rating" class="col-sm-1 control-label" id="ratinglabel">Accessories</label>'+
        '<div class="col-sm-3">'+
        '<select class="form-control updateSelect2 select2" data-placeholder="--Select Accessories--" multiple onchange="genratemeteringdata('+nooffeeder+')" id="accessories'+nooffeeder+'">'+
            '<option value="Amp Selector Switch">Amp Selector Switch</option>'+
            '<option value="Volt Selector Switch">Volt Selector Switch</option>'+
            '<option value="A/m Selector Switch">A/m Selector Switch</option>'+
            '<option value="TNC With 2 No + 2 NC SPR RTN 25A">TNC With 2 No + 2 NC SPR RTN 25A</option>'+
            '<option value="R-Y-B Indication Lamp">R-Y-B Indication Lamp</option>'+
            '<option value="R-G-O Indication Lamp">R-G-O Indication Lamp</option>'+
            '<option value="Control Fuse">Control Fuse</option>'+
            '<option value="C.T">C.T</option>'+
            '<option value="Other">Other</option>'+
        '</select></div>'+
        '<div class="col-sm-4">'+
        '<label for="rating" class="col-sm-2 control-label" id="ratinglabel">Phase</label>'+
        '<div class="col-sm-6">'+
            '<select class="form-control" onchange="genratemeteringdata('+nooffeeder+')" id="phase'+nooffeeder+'" >'+
                '<option selected disabled>--Select--</option>'+
                '<option value="1-Phase">1-Phase</option>'+
                '<option value="3-Phase">3-Phase</option>'+
            '</select>'+
        '</div>'+
    '</div>'+
        '<br></br><br>'+
    '<div class="form-group" style="padding-left:1%">'+
    '<label for="aprovalmake" class="col-sm-1 control-label" id="aprovalmake">Approval Make</label>'+
    '<div class="col-sm-2">'+
        '<input type="text" class="form-control" onfocusout="genratemeteringdata('+nooffeeder+')" placeholder="Approval Make" id="aprovalmake'+nooffeeder+'" name="value" >'+
    '</div>'+
    '<label for="aprovalmake" class="col-sm-1 control-label" id="other">Other</label>'+
    '<div class="col-sm-5">'+
    
        '<input type="text" class="form-control" onfocusout="genratemeteringdata('+nooffeeder+')" placeholder="Other" id="other'+nooffeeder+'" name="value" >'+
    '</div>'+
    //  '<label for="rating" class="col-sm-1 control-label" id="ratinglabel">Analog</label>'+
    '<div class="col-sm-2" style="display:none;">'+
    '<select class="form-control select2" multiple onchange="genratemeteringdata('+nooffeeder+')" >'+
    '<option selected disabled>--Select--</option>'+
        '<option value="0 - 20 Amp Analog Ampere Meter">0 - 20 Amp Analog Ampere Meter</option>'+
        '<option value="0 - 30 Amp Analog Ampere Meter">0 - 30 Amp Analog Ampere Meter</option>'+
        '<option value="0 - 60 Amp Analog Ampere Meter">0 - 60 Amp Analog Ampere Meter</option>'+
        '<option value="0 - 100 Amp Analog Ampere Meter">0 - 100 Amp Analog Ampere Meter</option>'+
        '<option value="0 - 150 Amp Analog Ampere Meter">0 - 150 Amp Analog Ampere Meter</option>'+
        '<option value="0 - 200  Amp Analog Ampere Meter">0 - 200  Amp Analog Ampere Meter</option>'+
        '<option value="0 - 300 Amp Analog Ampere Meter">0 - 300 Amp Analog Ampere Meter</option>'+

        '<option value="0 - 20 Amp Analog Volt Meter">0 - 20 Amp Analog Volt Meter</option>'+
        '<option value="0 - 30 Amp Analog Volt Meter">0 - 30 Amp Analog Volt Meter</option>'+
        '<option value="0 - 60 Amp Analog Volt Meter">0 - 60 Amp Analog Volt Meter</option>'+
        '<option value="0 - 100 Amp Analog Volt Meter">0 - 100 Amp Analog Volt Meter</option>'+
        '<option value="0 - 200 Amp Analog Volt Meter">0 - 200 Amp Analog Volt Meter</option>'+
        '<option value="0 - 300 Amp Analog Volt Meter">0 - 300 Amp Analog Volt Meter</option>'+
        '<option value="0 - 400 Amp Analog Volt Meter">0 - 400 Amp Analog Volt Meter</option>'+
        '<option value="0 - 500 Amp Analog Volt Meter">0 - 500 Amp Analog Volt Meter</option>'+
        '<option value="0 - 600 Amp Analog Volt Meter">0 - 600 Amp Analog Volt Meter</option>'+
        '<option value="0 - 700 Amp Analog Volt Meter">0 - 700 Amp Analog Volt Meter</option>'+
        '<option value="Other">Other</option>'+
    '</select></div>'+
  
    '<label for="rating" class="col-sm-1 control-label hide" id="ratinglabel">Digital</label>'+
    '<div class="col-sm-2" style="display:none;">'+
    '<select class="form-control select2"  onchange="genratemeteringdata('+nooffeeder+')" id="1digital'+nooffeeder+'">'+
    '<option selected disabled>--Select--</option>'+
        '<option value="Digital Amp Meter - 1 Phase">Digital Amp Meter - 1 Phase</option>'+
        '<option value="Digital Volt Meter - 1 Phase">Digital Volt Meter - 1 Phase</option>'+
        '<option value="Digital Multifuction Meter - 1 Phase">Digital Multifuction Meter - 1 Phase</option>'+
        '<option value="Digital KWH Meter - 1 Phase">Digital KWH Meter - 1 Phase</option>'+
        '<option value="Digital Energy Meter - 1 Phase">Digital Energy Meter - 1 Phase</option>'+
        '<option value="Digital Frequency Meter - 1 Phase">Digital Frequency Meter - 1 Phase</option>'+
        '<option value="Digital VAF Meter - 1 Phase">Digital VAF Meter - 1 Phase</option>'+
        '<option value="Digital PF Meter - 1 Phase">Digital PF Meter - 1 Phase</option>'+
        '<option value="Digital Amp Meter - 3 Phase">Digital Amp Meter - 3 Phase</option>'+
        '<option value="Digital Volt Meter - 3 Phase">Digital Volt Meter - 3 Phase</option>'+
        '<option value="Digital Multifuction Meter - 3 Phase">Digital Multifuction Meter - 3 Phase</option>'+        
        '<option value="Digital KWH Meter - 3 Phase">Digital KWH Meter - 3 Phase</option>'+        
        '<option value="Digital Energy Meter - 3 Phase">Digital Energy Meter - 3 Phase</option>'+        
        '<option value="Digital Frequency Meter - 3 Phase">Digital Frequency Meter - 3 Phase</option>'+        
        '<option value="Digital VAF Meter - 3 Phase">Digital VAF Meter - 3 Phase</option>'+        
        '<option value="Digital PF Meter - 3 Phase">Digital PF Meter - 3 Phase</option>'+     
        '<option value="Other">Other</option>'+   
    '</select></div>'+
   '</div>'+
    '</div><hr>';
    '';
    // $('.select2').select2();
    
    $('#mainFeederDiv'+nooffeeder).after(meteringdiv);
    $('.updateSelect2').select2();
}

function createincomerdiv(nooffeeder){

    var incomerdiv = '<div class="col-sm-12 lastfeeder"  style="padding-left:10%"><br><div class="form-group createincomerdiv" id="ratingdiv">'+
    '<label for="rating" class="col-sm-1 control-label" id="ratinglabel">Rating</label>'+
    '<div class="col-sm-2">'+
        '<input type="text" class="form-control" onfocusout="genrateincomerdata('+nooffeeder+')" placeholder="Enter Rating (AMP)" id="rating'+nooffeeder+'" name="value" >'+
    '</div>'+
    '<label for="pole" class="col-sm-1 control-label" id="polelabel">POLE</label>'+
    '<div class="col-sm-2">'+
    '<select class="form-control" onchange="genrateincomerdata('+nooffeeder+')" id="pole'+nooffeeder+'">'+
    '<option selected disabled>--Select--</option>'+
        '<option value="SP">SP</option>'+
        '<option value="SP">SP</option>'+
        '<option value="SPN">SPN</option>'+
        '<option value="DP">DP</option>'+
        '<option value="TP">TP</option>'+
        '<option value="TPN">TPN</option>'+
        '<option value="FP">FP</option>'+
        '<option value="Other">Other</option>'+
    '</select></div>'+
    
      '<label for="breaking capacity" class="col-sm-2 control-label" id="ratinglabel">Breaking Capacity (KA)</label>'+
    '<div class="col-sm-2">'+
        '<input type="text" class="form-control"  onfocusout="genrateincomerdata('+nooffeeder+')" placeholder="Enter Breaking Capacity (KA)" id="breakingcapacity'+nooffeeder+'" name="value" >'+
    '</div>'+
    '</div>'+
    '<div class="form-group   id="type">'+
    '<label for="rating" class="col-sm-1 control-label" id="typelabel">Type</label>'+
    '<div class="col-sm-2">'+
    '<select class="form-control"  onchange="genrateincomerdata('+nooffeeder+')" id="type'+nooffeeder+'">'+
    '<option selected disabled>--Select--</option>'+
        '<option value="TMD">TMD</option>'+
        '<option value="TMF">TMF</option>'+
        '<option value="MICRO">MICRO</option>'+
        '<option value="Other">Other</option>'+
    '</select></div>'+
    ''+
    '<label for="pole" class="col-sm-1 control-label" id="polelabel">Product</label>'+
    '<div class="col-sm-2">'+
    '<select class="form-control"  onchange="genrateincomerdata('+nooffeeder+')" id="product'+nooffeeder+'">'+
        '<option selected disabled>--Select--</option>'+
        '<option value="MCB">MCB</option>'+
        '<option value="MCCB">MCCB</option>'+
        '<option value="ACB">ACB</option>'+
        '<option value="CHANGE">CHANGE</option>'+
        '<option value="OVER">OVER</option>'+
        '<option value="Other">Other</option>'+
    '</select></div>'+
    
      '<label for="approvalmake" class="col-sm-2 control-label" id="ratinglabel">Approval Make</label>'+
    '<div class="col-sm-2">'+
        '<input type="text" class="form-control" id="apmake'+nooffeeder+'" onfocusout="genrateincomerdata('+nooffeeder+')" placeholder="Enter Approval Make" id="approvalmake" name="value" >'+
    '</div>'+
    '</div><hr></div><br>';
    console.log('nooffeeder'+nooffeeder);
    $('.lastfeeder:last').append(incomerdiv);
  
}
function genratemeteringdata(nooffeeder){
    

    $('#value'+nooffeeder).val('');
    // let analog = $('#analog'+nooffeeder).val();
    // let digital = $('#digital'+nooffeeder).val();
    let anadigital=$("input[name='metering']:checked").val();
    let accessories = $('#accessories'+nooffeeder).val();
    let aprovalmake = $('#aprovalmake'+nooffeeder).val();
    let other = $('#other'+nooffeeder).val();
    
    let phase= $('#phase'+nooffeeder).val();
    var metering="";
    // if(analog){
    //     metering = metering + " "+ analog+"";
    // }
    // if(digital){
    //     metering = metering + " "+ digital+ "";
    // }
    
    if(anadigital){
        metering = metering + " "+ anadigital+ "";
    }
    if(phase){
        metering = metering + " "+ phase+ "";
    }
    if(accessories){
        metering = metering + " "+ accessories+ "";
    }
    if(aprovalmake){
        metering = metering + " "+ aprovalmake+ "";
    }
    if(other){
        metering = metering + " "+ other+ "";
    }

    $('#value'+nooffeeder).val(metering);
    
    genrateData();
}
function genrateincomerdata(nooffeeder){
    let rating = $('#rating'+nooffeeder).val();
    let pole = $('#pole'+nooffeeder).val();
    let breakingcapacity =  $('#breakingcapacity'+nooffeeder).val();
    let type = $('#type'+nooffeeder).val();
    let product = $('#product'+nooffeeder).val();
    let apmake = $('#apmake'+nooffeeder).val();
    var incomer="";
    if(rating){
        incomer = incomer + " "+ rating+"A";
    }
    if(pole){
        incomer = incomer + " "+ pole+ "";
    }
    if(breakingcapacity){
        incomer = incomer + " "+ breakingcapacity+ "KA";
    }
    if(type){
        incomer = incomer + " "+ type+ "";
    }
      if(product){
        incomer = incomer + " "+ product+ "";
    }
    if(apmake){
        incomer = incomer + " "+ apmake+ "";
    }

    $('#value'+nooffeeder).val(incomer);
    
    genrateData();
}
function addFieldFeeder(value) {

    

    lengthFeeder++;
    var temp = {
        feederid : value.feederid,
        feedername : value.feedername,
        feedercount : value.feedercount,
        mainCount : lengthFeeder
    }

    feederRecord.push(temp);

    var html = '<div class="form-group mainFeederDiv lastfeeder" id="mainFeederDiv'+lengthFeeder+'"><br>'+
                    '<label for="role" class="col-sm-3 control-label" id="feederLabel'+lengthFeeder+'">'+value.feedername+' '+ value.feedercount+'</label>'+
                    '<div class="col-sm-5">'+
                        '<input type="text" class="form-control"  onfocusout="genrateData()" placeholder="Enter '+value.feedername+'" id="value'+lengthFeeder+'" name="value'+lengthFeeder+'" >'+
                    '</div>'+
                    '<a class="col-sm-1" onclick="removeFeeder('+lengthFeeder+')"><i class="fas fa-times" style="color: red;"></i></a>'+
                    '<input type="hidden"  name="lablename'+lengthFeeder+'" id="lablename'+lengthFeeder+'" value="'+value.feedername+' '+ value.feedercount+'">'+
                    '<input type="hidden" name="seq'+lengthFeeder+'" value="'+lengthFeeder+'">'+
                '</div>';

                if ( $(".mainFeederDiv").length > 0 ) {
                    $('.lastfeeder:last').after(html);}
                    else  {
                        $('#track').before(html); }
    //  $('#track').after(html);
    $('#feederlength').val(lengthFeeder);
    $('#feederid').val('').trigger('change');
}


function genrateData() {

    var TypeofPanel = ($('#nameOfpanel').val()) ? $('#nameOfpanel :selected').text() : "Nil";
    var PanelStructure =($('#panelstructure').val()) ? $('#panelstructure :selected').text() : "Nil"; 
    var Opening = ($('#opening').val()) ? $('#opening :selected').text() : "Nil";   
    var PanelMaterialGuage = ($('#panelguage').val()) ? $('#panelguage :selected').text() : "Nil";    
    var AngleIronBaseFramenSize = ($('#angleironframe').val()) ? $('#angleironframe :selected').text() : "Nil";
    var PaintShade = ($('#paintandshade').val()) ? $('#paintandshade :selected').text() : "Nil";     
    var Busbar = ($('#busbar').val()) ? $('#busbar :selected').text() : "Nil";    
    var BusbarRating = ($('#busbarrating').val()) ? $('#busbarrating :selected').text() : "Nil"; 
    var SwitchgearMake =($('#switchgearmake').val()) ? $('#switchgearmake').val() : "Nil";      
    var MeteringMake = ($('#meteringmake').val()) ? $('#meteringmake').val() : "Nil";    
    var WireMake = ($('#wiremake').val()) ? $('#wiremake :selected').text() : "Nil";     $('#wiremake :selected').text();
    var PanelDimention = ($('#paneldimention').val()) ? $('#paneldimention :selected').text() : "Nil";    
    var Other = ($('#other').val()) ? $('#other :selected').text() : "Nil";     
    // var liftinghock = $('#liftinghock').val();
    // var busbarsupporter = $('#busbarsupporter').val();
    // var hilamsheet = $('#hilamsheet').val();
    // var nutboltandwasher = $('#nutboltandwasher').val();
    // var hardwareothers = $('#hardwareothers').val();

    // var panellabel = $('#panellabel').val();
    // var stickers = $('#stickers').val();
    // var cabletie = $('#cabletie').val();
    // var spiralpiraltype = $('#spiralpiraltype').val();
    // var wiretap = $('#wiretap').val();
    // var rivets = $('#rivets').val();
    // var accessoriesothers = $('#accessoriesothers').val();

    // var wiresize = $('#wiresize').val();
    // var lug = $("#lug").val();
    // var connectors = $('#connectors').val();
    // var wirecablesother = $('#wirecablesother').val();

    var result = "";
    var halfpara ="";

    
   

    // for (let i = 1; i <= lengthFeeder; i++) {
    //     var lable = $('#lablename'+i).val();
    //     var key = $('#value'+i).val();
    //     if(key){
    //         console.log(key);
    //         halfpara = halfpara +  lable + ":" + " "+key+"\n" ;
    //     }
    // }
   
  console.log($('#switchgearmake').val());
    if(TypeofPanel){
        result = result + "\nType of Panel  : " +TypeofPanel;
    }
    if(PanelStructure){
        result = result + "\nPanel Structure : " +PanelStructure;
    }
    if(Opening){
        result = result + "\nOpening : " +Opening;
    }
    if(PanelMaterialGuage){
        result = result + "\nPanel & MaterialGuage : " +PanelMaterialGuage;
    }
    if(AngleIronBaseFramenSize){
        result = result + "\nAngle Iron / Base Frame & Size : " +AngleIronBaseFramenSize;
    }
    if(PaintShade){
        result = result + "\nPaint & Shade : " +PaintShade;
    }
    if(Busbar){
        result = result + "\nBusbar : " +Busbar;
    }
    if(BusbarRating){
        result = result + "\nBusbarRating : " +BusbarRating;
    }
    if(SwitchgearMake){
        result = result + "\nSwitchgear Make : " +SwitchgearMake;
    }
    if(MeteringMake){
        result = result + "\nMetering Make : " +MeteringMake;
    }
    if(WireMake){
        result = result + "\nWire Make : " +WireMake;
    }
    if(PanelDimention){
        result = result + "\nPanel Dimention :" +PanelDimention;
    }
   if(Other){
        result = result + "\nOther : " +Other;
    }
    var sugpanelcatno =($('#sugpanelcatno').val()) ? $('#sugpanelcatno :selected').text() : "Nil"; 
     var similartocustomer=($('#similartocustomer').val()) ? $('#similartocustomer :selected').text() : "Nil"; 
     var similartoquotationno=($('#similartoquotationno').val()) ? $('#similartoquotationno :selected').text() : "Nil"; 
    result = result + "\nSuggested Panelcatno : " +sugpanelcatno+" , ";
     result = result + "\nSimilar to Customer : " +similartocustomer;
      result = result + "\nSimilar to Quotationno : " +similartoquotationno;
    $('#panelDescription').html(result); 
    // $('#panelhalfdescription').text(result);
//     var finaldesc=$('#panelDescription').text();
//     finaldesc += ' Suggested Panel Cat No ' + $('#sugpanelcatno').text();
//    $('#panelDescription').text(result);
   
}
function loadsuggestedpanelcatno(){
    $('#similartocustomer').empty();
    $('#similartoquotationno').empty();
    $('#listofquotationno').remove();
    var _token = $('input[name="_token"]').val();
    var url = '/getsuggPanelCatno';
    var nameOfpanelid = $('#nameOfpanel').val();
    // var similartocustomer = $('#similartocustomer').val(); 
    $.ajax({
        url:url,
        method:"GET",
        data : {_token:_token,nameOfpanelid:nameOfpanelid},
        success:function(result)
        {
           var response = result.sugpanelcatno;
            if(response){
                
                
                $('#sugpanelcatno').empty();
                
             var suggPanelCatno = '<option value="" >--Please Select--</option>';
             response.forEach(element => {
                     suggPanelCatno  +='<option value="'+element.bompanelid+'" >'+element.panelcatno+'</option>';
                   
                });
                $('#sugpanelcatno').append(suggPanelCatno);
            
                $('#inquiryids').val(result.inquiryids);
                //  ();
                // genrateData();
            }else{
               
            }
        },
        dataType:"json"
    });
   
}

$('#similartoquotationno').on('change',function(){
    var similartoquotationno = $('#similartoquotationno').val();
    var similartoquotationno = $('#similartoquotationno').select2("data");

   $('#listofquotationno').remove();
   var append = '<table  id="listofquotationno" align="center"><tbody>';
   var index=1;
   
   similartoquotationno.forEach(element => {
    if(element.id){
    var url = 'exportbompdf/'+element.id+'/ap/option';
    append+="<tr class='margin'><td>"+index+"</td><td><a href='/exportbompdf/"+element.id+"/ap/option' target='_blank'>"+element.text+"</a></td></tr>";
    index++; 
    }
     
});
   append+='</tbody></table>';
   $('#viewsuggestion').after(append);
});
function pageRedirect(urlparameter) {
    window.location.replace('\sdfgd');
    // window.location.href = "exportbompdf/"+urlparameter+"/ap";
  } 
  function loadsimilarcustomer(){
    $('#similartocustomer').empty();
    var _token = $('input[name="_token"]').val();
    var sugpanelcatnos = $('#sugpanelcatno').val();
    var nameOfpanelid = $('#nameOfpanel').val();
    $.ajax({
        url: '/loadsimilarcustomers',
        method:"GET",
        data : {_token:_token,nameOfpanelid:nameOfpanelid,sugpanelcatnos:sugpanelcatnos},
        success:function(result)
        {
           
            if(result){
                $('#similartoquotationno').empty();
                $('#similartocustomer').empty();
                $('#listofquotationno').remove();
             var similarcustomer = '<option value="" >--Please Select--</option>';
                result.forEach(element => {
               
                    similarcustomer  +='<option value="'+element.partyid+'" >'+element.companyname+'</option>';
                   
                });
                // similartocustomer
                // similartoquotationno
                 $('#similartocustomer').append(similarcustomer);

         
                
            }else{
               
            }
        },
        dataType:"json"
    });
    genrateData();
} 
function loadsimilartoquotationno(){
    var _token = $('input[name="_token"]').val();
    var sugpanelcatnos = $('#sugpanelcatno').val();
    var nameOfpanelid = $('#nameOfpanel').val();
    var similartocustomer=$('#similartocustomer').val();
    var inquiryids = $('#inquiryids').val();
    $.ajax({
        url: '/loadsimilarfieldsforinquiry',
        method:"GET",
        data : {_token:_token,nameOfpanelid:nameOfpanelid,sugpanelcatnos:sugpanelcatnos,inquiryids:inquiryids,similartocustomer:similartocustomer},
        success:function(result)
        {
           
            if(result){
                 $('#similartoquotationno').empty().trigger("change");
                // $('#similartoquotationno').empty();
             var similarcustomer = '<option value="" >--Please Select--</option>';
                result.forEach(element => {
               
                    similarcustomer  +='<option value="'+element.bomid+'" >'+element.qutationno+'</option>';
                   
                });
                // similartocustomer
                // similartoquotationno
                 $('#similartoquotationno').append(similarcustomer);
                 $('#similartoquotationno').select2().trigger("change");;
                // $('#sugpanelcatno').select2();
                // loadsimilarfields();
                
            }else{
               
            }
        },
        dataType:"json"
    });
    genrateData();
}

function removeFeeder(id) { 


    var removeCount = id;
    var removeFeederCount = 0;
    var removeFeederid = 0;
    var removeFeederName = "";

    for (let i = 0; i < feederRecord.length; i++) {
        if(feederRecord[i].mainCount == removeCount){
            removeFeederName = feederRecord[i].feedername;
            removeFeederid = feederRecord[i].feederid;
            removeFeederCount = feederRecord[i].feedercount;
            feederRecord.splice(i, 1);
            break;
        }
    }


    for (let j = 0; j < listFeeder.length; j++) {
        if(listFeeder[j].feederid == removeFeederid){
            var temp = Number(listFeeder[j].feedercount) - 1;
            listFeeder[j].feedercount = temp;
            break;
        }
    }

    for (let k = 0; k < feederRecord.length; k++) {
        if(feederRecord[k].feederid == removeFeederid){
            var tempCount = feederRecord[k].mainCount;
            if(tempCount > removeCount){
                var c = Number(feederRecord[k].feedercount) - 1;
                feederRecord[k].feedercount = c;
                $('#feederLabel'+tempCount).text(removeFeederName+" "+c);
                $('#lablename'+tempCount).val(removeFeederName+" "+c);
            }
        }
        
    }

    
    $('#mainFeederDiv'+removeCount).remove();
    genrateData();
}

   $(function () {
     //Initialize Select2 Elements
     $('.select2').select2()
   
     //Datemask dd/mm/yyyy
     $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
     //Datemask2 mm/dd/yyyy
     $('#datemask2').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' })
     //Money Euro
     $('[data-mask]').inputmask()
   
     //Date range picker
     $('#reservation').daterangepicker()
     //Date range picker with time picker
     $('#reservationtime').daterangepicker({ timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A' })
     //Date range as a button
     $('#daterange-btn').daterangepicker(
       {
         ranges   : {
           'Today'       : [moment(), moment()],
           'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
           'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
           'Last 30 Days': [moment().subtract(29, 'days'), moment()],
           'This Month'  : [moment().startOf('month'), moment().endOf('month')],
           'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
         },
         startDate: moment().subtract(29, 'days'),
         endDate  : moment()
       },
       function (start, end) {
         $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
       }
     )
   
     //Date picker
     $('#datepicker').datepicker({
       autoclose: true
     })
   
     //iCheck for checkbox and radio inputs
     $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
       checkboxClass: 'icheckbox_minimal-blue',
       radioClass   : 'iradio_minimal-blue'
     })
     //Red color scheme for iCheck
     $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
       checkboxClass: 'icheckbox_minimal-red',
       radioClass   : 'iradio_minimal-red'
     })
     //Flat red color scheme for iCheck
     $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
       checkboxClass: 'icheckbox_flat-green',
       radioClass   : 'iradio_flat-green'
     })
   
     //Colorpicker
     $('.my-colorpicker1').colorpicker()
     //color picker with addon
     $('.my-colorpicker2').colorpicker()
   
     //Timepicker
     $('.timepicker').timepicker({
       showInputs: false
     });
    });
    function  openModel() {
        $('#addMasterModel').modal('show');
    }
    
    function addpanelandmaterialguageAjax() {
        var panelandmaterialguage = $('#panelandmaterialguage').val();
        var url = '/inquiry/addpanelandmaterialguage';
        var _token = $('input[name="_token"]').val();
    
        if(panelandmaterialguage){    
            $.ajax({
                type : 'POST',
                url : url,
                data : {panelandmaterialguage:panelandmaterialguage,_token : _token},
                success : function(data){
                
                   if(data.state){
                    updatePanelAndMaterialData(data);
                        $('#addMasterModel').modal('hide');
                        $('#panelandmaterialguage').val('');
                   }else{
                       alert(data.message);
                   }
                }
            });
        }else{
            alert("Please Enter Data");
        }
    }
    function updatePanelAndMaterialData(data) {

        var html = '<option value="">--Select Terms--</option>';
                    
        data.data.forEach(element => {
            html = html+'<option value="'+element.panelandmaterialguage+'">'+element.panelandmaterialguage+'</option>';
        });
     
        $('#panelguage').empty();
        $('#panelguage').append(html);
        $('#panelguage').val(data.id).trigger('change');
    }
    function updateSuggestedData(data,label) {
        $('#'+l+'').empty();
        var html = '<option value="">--Select Terms--</option>';
        var l=  label;   
        var array = JSON.parse(JSON.stringify(data.data));
        console.log();
        for (let index = 0; index < array.length; index++) {
            if(array[index][''+l+''] != null){

                html = html+'<option value="'+array[index][''+l+'']+'">'+array[index][''+l+'']+'</option>';
            }
            
        }
       
       console.log(l);
        $('#'+l+'').empty();
        $('#'+l+'').append(html);
        $('#'+l+'').val(data.id).trigger('change');
        $('#'+l+'').select2();
    }
    function convetuppercase(strval){
        var str = strval;
        str = str.toLowerCase().replace(/\b[a-z]/g, function(letter) {
            return letter.toUpperCase();
        });
        return str;
    }
    
    function  openmodelforallsuggestion(label) {
        if(label != "" && label != undefined)
        {
            $('#suggestionlabel').val(label);
            var labelCase = convetuppercase(label);
            $('#labeldisplay').html(labelCase);

            $('#addsuggestionModel').modal('show');
            $('#suggetion').val('');
        }
       
    }
    function addsuggestiontotable(){
        var suggetion = $('#suggetion').val();
        var label = $('#suggestionlabel').val();
        var url = '/inquiry/addsuggstion';
        var _token = $('input[name="_token"]').val();
    
        if(panelandmaterialguage){    
            $.ajax({
                type : 'POST',
                url : url,
                data : {suggetion:suggetion,label:label,_token : _token},
                success : function(data){
                
                   if(data.state){
                    updateSuggestedData(data,label);
                        $('#addsuggestionModel').modal('hide');
                        $('#suggestionlabel').val('');
                        $('#labeldisplay').html('');
                        $('#suggetion').val('');
                   }else{
                       alert(data.message);
                   }
                }
            });
        }else{
            alert("Please Enter Data");
        }
    }