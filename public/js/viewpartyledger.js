var partaccountid;


$(document).ready(function() {
    $("#noRecordFound").hide();
});

function changeParty(){
    partaccountid = $('#partyaccountid').val();
    $('#currentBalance').text("");
    setBalance();
    $("#mainTable > tbody").empty(); 
}

function setBalance(){
    partyaccounts.forEach(element => {
        if(element.partyaccountid == partaccountid){
            var type ="" 
            if(element.balancetype == "DR"){
                type = "Dr";
            }else if(element.balancetype == "CR"){
                type = "Cr";
            }
            $('#currentBalance').text(element.balance+" "+type);
        }
    });
}

function ajaxCall(){
    var startDate = $('#start_date').val();
    var endDate = $('#end_date').val();
    console.log(partaccountid);
    if(partaccountid == undefined || partaccountid == ""){
        alert('Please Select Party');
    }else{
        if(startDate && startDate != undefined && startDate != ""){
            if(endDate && endDate != undefined && endDate != ""){
               callAjax();
            }else{
                alert('select end date');
            }
        }else{
            alert('select start date');
        }
    }
   
}

function callAjax(){
    var url = $("#url").val(); 
    var startDate = $('#start_date').val();
    var endDate = $('#end_date').val();
    var _token = $('input[name="_token"]').val();

    $.ajax({
        url:url,
        method:"POST",
        data : {_token:_token,startDate:startDate,endDate:endDate,partaccountid:partaccountid},
        success:function(result)
        {
            if(result.length == 0){
                $('#noRecordFound').show();
            }else{
                $('#noRecordFound').hide();
                setTable(result);
            }
            
        },
        dataType:"json"
    });

}

function setTable(result){

    $("#mainTable > tbody").empty();  
    var debitAmount = 0;
    var creditAmount = 0; 

    result.forEach(element => {
        var description ="";
        if( element.type == "Openning Balance"){
            description = "Openning Balance";
        }else if(element.paymenttype == "Cheque"){
            description = "Cheque \nCheque No-"+element.chequenumber+"\n"+element.bankname+" "+element.accountnumber;   
        }else if(element.paymenttype == "Bank"){
            description = element.bankname+" "+element.accountnumber+"\n UTR No.-"+element.utrnumber+"-"+element.utrtype;
        }else if(element.paymenttype == "Cash"){
            description = element.description;
        }

        if(element.amounttype == "CR"){
            $('#mainTable').append('<tr>'+
                        '<td>'+formatDate(element.transactiondate)+'</td>'+
                        '<td>'+description+'</td>'+
                        '<td>'+element.type+'</td>'+
                        '<td>'+element.partytransactionid+'</td>'+
                        '<td></td>'+
                        '<td>'+element.amount+'</td></tr>'); 
            creditAmount = creditAmount +parseFloat(element.amount);
        }else if(element.amounttype == "DR"){
            $('#mainTable').append('<tr>'+
                        '<td>'+formatDate(element.transactiondate)+'</td>'+
                        '<td>'+description+'</td>'+
                        '<td>'+element.type+'</td>'+
                        '<td>'+element.partytransactionid+'</td>'+
                        '<td>'+element.amount+'</td>'+
                        '<td></td></tr>'); 
            debitAmount = debitAmount +parseFloat(element.amount);
        }
    });

    
    $('#mainTable').append('<tr style="border-top:2px solid black">'+
    '<th></th>'+
    '<th></th>'+
    '<th></th>'+
    '<th></th>'+
    '<th>'+debitAmount.toFixed(2)+'</th>'+
    '<th>'+creditAmount.toFixed(2)+'</th></tr>'); 
    if(creditAmount < debitAmount){
        var temp = debitAmount-creditAmount;
        $('#mainTable').append('<tr style=" color: red;">'+
        '<th></th>'+
        '<th></th>'+
        '<th></th>'+
        '<th>Dr Closing Balance</th>'+
        '<th></th>'+
        '<th>'+temp.toFixed(2)+'</th></tr>');
        $('#mainTable').append('<tr style="border-bottom:2px solid black; border-top:1px solid black">'+
        '<th></th>'+
        '<th></th>'+
        '<th></th>'+
        '<th></th>'+
        '<th></th>'+
        '<th>'+debitAmount.toFixed(2)+'</th></tr>'); 
    }else if(creditAmount > debitAmount){
        var temp = creditAmount-debitAmount;
        $('#mainTable').append('<tr style=" color: green;">'+
        '<th></th>'+
        '<th></th>'+
        '<th></th>'+
        '<th>Cr Closing Balance</th>'+
        '<th>'+temp.toFixed(2)+'</th>'+
        '<th></th></tr>');
        $('#mainTable').append('<tr style="border-bottom:2px solid black; border-top:1px solid black">'+
        '<th></th>'+
        '<th></th>'+
        '<th></th>'+
        '<th></th>'+
        '<th>'+creditAmount.toFixed(2)+'</th>'+
        '<th></th></tr>'); 
    }
    
}

function formatDate(date) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + (d.getDate()+1),
        year = d.getFullYear();
    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;
    return [day, month, year].join('/');
}

