var count = 0;
var selctedResult = [];

var count = 0;
var accessoriesCount = 0;
var selctedResult = [];
var selectAccessoriesResult = [];
var radioValue;
var accessoriesRadioValue;
var type = 0;


$(document).ready(function() {
    type = 2;
    radioValue = $('form input[type=radio][name=type]:checked').val();
   
    accessoriesRadioValue = $('form input[type=radio][name=accessoriesType]:checked').val();
   
    $('#warranty').change(function(){
        let warranty = $(this).val();
        if(warranty == 'yes'){
            $('#wd').show();
        }else{
            $('#wd').hide();
        }
    });



    $('#addaccessories').click(function(){
        $('#accessoriesSpecific').show();
    });
});

function groupTypeChange(){
    let grouptypeid = $('#grouptypeid').val();
    var url = $('#urlGetGroup').val();
    var _token = $('input[name="_token"]').val();

    $('#groupid').html('');
    $('#specification').html('');
    if(groupid){
        $.ajax({
            type : 'POST',
            url : url,  
            data : {grouptypeid:grouptypeid, _token : _token},
            success : function(data){
                $('#groupid').append(data);
            }
        });
    }else{
        alert('please select Group');
    }
}

function companyChange(){
    // resetProductName();
}

function changeGrp(){
    $('#specification').empty();
    var grpid = $('#groupid').val();
    var url = $('#urlGetGroupTeplate').val();
    var _token = $('input[name="_token"]').val();
    console.log(grpid,url);
    $.ajax({
        url:url,
        method:"POST",
        data:{groupid:grpid , _token:_token},
        success:function(result)
        {
          // 
          var html='';
          var oldspecificationid='';
          var count = result.length;
          $.each(result,function(i,specification){
              // alert(i);
              if(oldspecificationid!=specification.specificationid)
              {
                  html+= '<div class="form-group">'+
                  '<label for="groupid" class="col-sm-4 control-label">'+specification.specificationname+
                  '</label><div class="col-sm-8"><select name="subspecificationid'+i+
                  '" class="form-control select2" id="subspecificationid'+i+'"'+
                  'onchange="setname('+count+')">'+
                  '<option value="">--Select Subspecification--</option>';
                  $.each(result,function(j,subspecification)
                  {
                      if(subspecification.subspecificationname!="")
                      {
                          if(specification.specificationid==subspecification.specificationid)
                          {
                              html+='<option value="'+subspecification.subspecificationname+'">'+subspecification.subspecificationname+'</option>';
                          }
                      }
                  });

                  html +='</select></div></div>';	
                  oldspecificationid = specification.specificationid;
              }		
          });
          $('#specification').append(html);
      },
      dataType:'json'
  });
}

var tempproductname='';
function setname(i)
{
    $('#productname').val('');
    var pname='';
    if(i!='narration')
    {
        for(var j=0;j<=i;j++)
        {
            // alert(j);
            if(typeof $('#subspecificationid'+j).val()!=="undefined")
            {
                pname+=' '+$('#subspecificationid'+j).val(); 
            }   
        }   
        tempproductname=pname;
        $("#mainproductname").val(tempproductname+' '+$('#groupid :selected').text()+'-'+ $('#companyid :selected').text());
        $('#productname').val(tempproductname+' '+$('#groupid :selected').text()+'-'+ $('#companyid :selected').text())
    }
    else{
        // alert();
        if($('#narration').val()=="")
        {
            
        $("#mainproductname").val(tempproductname+' '+$('#groupid :selected').text()+'-'+ $('#companyid :selected').text());
            $('#productname').val(tempproductname+' '+$('#groupid :selected').text()+'-'+ $('#companyid :selected').text())
        }
        else{
            $('#productname').val(tempproductname+' '+$('#narration').val()+' '+$('#groupid :selected').text()+'-'+ $('#companyid :selected').text())
        }
    }
}


function setproducttype(){
var radioValue = $("input[name='productcategory']:checked").val();
    if(radioValue=="general"){   
        $('#specific').hide();
        $('#productname').val('');
        $('#specification').empty();
        $('#producttype').val('');
        $('#productname').attr('readonly',false);
        $('#companyid').attr('disabled',true);
        $('#groupid').attr('disabled',true);
        $('#mainproductname').val('');
        type = 1;
    }
    if(radioValue=="specific")
    {
        $('#specific').show();
        $('#productname').val('');
        $('#productname').attr('readonly',true);
        $('#companyid').attr('disabled',false);
        $('#groupid').attr('disabled',false);
        type = 2;
    }
}

function addunitsubmit()
	{
        var _token=$("input[name=_token]").val();
        var url = $('#urlAjaxAddUnit').val();


		if($('#unitname').val()=="")
		{
			return false;
		}

		$('#unitmodalcontent').hide();

		$('#unitmodalprogress').show();

		$('#addunit').prop('disabled', true);

		$.ajax({type : 'POST', url: url, data: {unitname : $('#unitname').val(),_token:_token}, success: function(result){

			$('#unitmodalprogress').hide();

			$('#unitmodalcontent').show();

			$('#addunitbtn').prop('disabled', false);

			if(result!="")
			{
            // alert(result);
			/// alert($('#unitname').val());
			$('#unitid').append('<option selected="selected" value="'+result+'">'+$('#unitname').val()+'</option>');
			$('#punitid').append('<option value="'+result+'">'+$('#unitname').val()+'</option>');

			$('#addunit').modal('hide');    
			$('#unitname').val(''); 
			$('#unitid').selectpicker('refresh');
			$('#unitid').focus();

		}
		else
		{
			$('#unitmodalalert').show();

		}
	}});

		return false;
}

function checkValid(){
    if( type == 2){
        console.log($('#grouptypeid').val());
        console.log($('#companyid').val());
        console.log($('#companyid').val());
        if(!$('#grouptypeid').val()){
            alert('Please Select Group Type');
            return false;
        }
        if(!$('#companyid').val()){
            alert('Please Select Make');
            return false;
        }
        if(!$('#companyid').val()){
            alert('Please Select Make');
            return false;
        }

        return true;
    }
}

// ----------------------------------------------
function addTotable(){

    var getval =  $('#specificationid').val();
    var selectedText =$('#specificationid :selected').text();
    
    if(!getval){
        alert("Please Select Specification");
        return;
    }else{

        for(var i=0; i<selctedResult.length; i++) {
            if (selctedResult[i] == getval){
                alert(selectedText+" specification already selected");
                return;
            }
        }
        
        count++;
        $('#tableList').append('<tr><td>'+count+'<\/td><td>'+selectedText+'<input type="hidden" name="specificationid'+count+'" value="'+getval+'" ><input type="hidden" name="sequenceno'+count+'" value="'+count+'"><\/td></tr>');
        selctedResult.push(getval);
        $("#specificationid").val('').trigger('change');

        $('#length').val(count);
        
    }

    
}

function vaild(){
    var el = $("#btnSubmit");
    el.prop('disabled', true);
    setTimeout(function(){el.prop('disabled', false); }, 3000);
    if(count == 0){
        alert('Please Select Specification');
        return false;
    }else{
        return true;
    }
}

function reset(){
    $("#tableList > tbody").empty();
    while(selctedResult.length > 0) {
        selctedResult.pop();
    }
    count = 0;
    $('#length').val(count);
    
    $("#specificationid").val('').trigger('change');
}