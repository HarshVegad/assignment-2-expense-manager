var currentType;
var selctedResult = [];
var count = 0;
var accessCount = 0;
var srNo = 0;
var cumulativeAmount;

$( document ).ready(function() {

    $('#selectByGroup').hide();    
    currentType = 0;
    $(window).keydown(function(event){
        if(event.keyCode == 13) {
          event.preventDefault();
          return false;
        }
      });
    
    $('input[type=radio][name=addproductType]').change(function() {
        if (this.value == 'Specific') {
            currentType = 0;
            setSpecific();
        }else if (this.value == 'Genral') {
            currentType = 1;
            setGenral();
        }else if (this.value == 'ProductGroup') {
            currentType = 2;
            setProductGroup();
        }
    }); 
});

function allClear(){  
    if(currentType == 0){
        $("#radioSpecific").prop("checked", true);
        setSpecific();
    }else if(currentType == 1){
        $("#radioGenral").prop("checked", true);
        setGenral();
    }else if(currentType == 2){
        $("#radioProductGroup").prop("checked", true);
        setProductGroup();
    }
    
    $('#tableListgroupProductAccessories tbody').empty();
    $('#productqty').val('');
    $('#productnarration').text('');
    $('#selectFeeder').val('').trigger('change');

    
    $('#simlierproduct').hide();
    $('#tablesimlierproduct tbody').empty();
}

function setSpecific(){

    $('#selectProduct').show();
    $('#specificProduct').show();
    $('#selectQty').show();
    $('#productlabnarration').show();
    $('#productnarration').show();
    $('#selectByGroup').hide();
    
    $('#tableListgroupProductAccessories tbody').empty();
    
    $('#grouptypeid').val('').trigger('change');
    $('#productid').empty();
    $('#groupid').empty();
   
    $('#productaccessoriesdiv').hide();
    $('#tableListAccessories tbody').html('');

    $('#simlierproduct').hide();
    $('#tablesimlierproduct tbody').html('');
    $('#productnarration').text('');

}

function setGenral(){

    $('#selectProduct').show();
    $('#selectQty').show();
    $('#selectByGroup').hide();
    $('#productlabnarration').show();
    $('#productnarration').show();
    $('#specificProduct').hide();
    
    $('#tableListgroupProductAccessories tbody').empty();

    $('#grouptypeid').val('').trigger('change');
    $('#productid').empty();
    $('#groupid').empty();
    genralProductAjax();
   
    $('#productaccessoriesdiv').hide();
    $('#tableListAccessories tbody').html('');

    $('#simlierproduct').hide();
    $('#tablesimlierproduct tbody').html('');
    $('#productnarration').text('');

}

function setProductGroup(){

    $('#selectProduct').hide();
    $('#specificProduct').hide();
    $('#selectQty').hide();
    $('#productnarration').hide();
    $('#selectByGroup').show();
    $('#productlabnarration').hide();

    $('#grouptypeid').val('').trigger('change');
    $('#productid').empty();
    $('#groupid').empty();

    $('#productaccessoriesdiv').hide();
    $('#tableListAccessories tbody').html('');

    $('#simlierproduct').hide();
    $('#tablesimlierproduct tbody').html('');
    $('#productnarration').text('');

}

function changeCompany(){
    let company = $('#companyid').val();
    $('#grouptypeid').val('').trigger('change');
    $('#productid').empty();
    $('#groupid').empty();
    $('#simlierproduct').hide();
    $('#tablesimlierproduct tbody').empty();

    
    $('#simlierproduct').hide();
    $('#tablesimlierproduct tbody').empty();

    
    $('#productaccessoriesdiv').hide(); 
    $('#tableListAccessories tbody').empty();
   
}

function groupTypeChange(){
    let grouptypeid = $('#grouptypeid').val();
    var url = $('#urlGetProductGroup').val();
    var _token = $('input[name="_token"]').val();
    let company = $('#companyid').val();

    $('#groupid').empty();
    $('#productid').empty();
    
    $('#simlierproduct').hide();
    $('#tablesimlierproduct tbody').empty();

    
    $('#productaccessoriesdiv').hide(); 
    $('#tableListAccessories tbody').empty();
    
    if(grouptypeid){
        $.ajax({
            type : 'POST',
            url : url,
            data : {productgrouptypeid:grouptypeid, _token : _token},
            success : function(data){
                $('#groupid').append(data);
            }
        });
    }
}

function groupChange(){
    let productsubgroup = $("#groupid").val();
    let productgroup = $('#grouptypeid').val();
    let company = $('#companyid').val();
    var _token = $('input[name="_token"]').val();
    var url = $('#urlGetProduct').val();

    $('#simlierproduct').hide();
    $('#tablesimlierproduct tbody').empty();

    if(company == ""){
        alert("Please Select Make");
        return;
    }

    $('#productaccessoriesdiv').hide(); 
    $('#tableListAccessories tbody').empty();
    $('#productid').empty();
    

    if(productsubgroup){
        $.ajax({
            type : 'GET',
            url : url,
            data : {groupid:productsubgroup, grouptypeid:productgroup, companyid:company, _token : _token},
            success : function(data){
                $('#productid').append(data);
            }
        });
    }
}

var newSelectedProductId;

function productChange(){
    newSelectedProductId = "";
    var productid = $('#productid').val();
    newSelectedProductId = productid;
    if(currentType == 0){
        getSimlierProduct(productid);
        getAccProduct(productid);
    }else if(currentType == 1){
        getAccProduct(productid);
    }

}

function getSimlierProduct(productid){
   var selGroup = $("#groupid :selected").text();
   var selCompany =   "-"+$('#companyid :selected').text();
   var selProductName =  $('#productid :selected').text();
   var _token = $('input[name="_token"]').val();
   var queryName =  selProductName.replace(selGroup,'');
   queryName = queryName.replace(selCompany,'');

   $('#simlierproduct').hide();
   $('#tablesimlierproduct tbody').html('');

   var url = $('#urlgetSimilerProduct').val();
   $.ajax({
        type : 'POST',
        url : url,
        data : {productName:queryName, _token : _token},
        success : function(data){
          
            arraySort(data,productid,queryName);
        }
    });
   
}

function arraySort(data,productid,queryName) {
    
    var sortData = [];
    data.forEach(element => {
        // refiltering a url
        var selGroup = $("#groupid :selected").text();
        var selCompany =  "-"+element.makename;
        var newName =  element.subnameofproduct.replace(selGroup,'');
        var newName =  newName.replace(selCompany,'');

        if(queryName == newName){
            var temp = {
                id : element.id,
                name : element.subnameofproduct,
                sellrate : element.sellrate,
                makename : element.makename,
                stock : element.stockqty,
                discount : element.productdiscount,
                amount : discountCal(element.sellrate,element.productdiscount)
            }
            sortData.push(temp);
        }
        
    });

    var newArray = sortData.sort(function(a,b) {
        return a.amount -b.amount;
    });

    setSimlierProduct(newArray,productid);
}


function setSimlierProduct(data,productid){

    $('#simlierproduct').show();
    $('#tablesimlierproduct tbody').html('');
    var selectProductrate = 0;
    data.forEach(element => {
        if(element.id == productid){
            selectProductrate  = element.amount;
        }
    }); 

    var html ="";
    data.forEach(element => {

        if(element.id == productid){
        html = html +
                    '<tr>'+
                    '<td>'+element.name+'</td>'+
                    '<td>'+element.makename+'</td>'+
                    '<td>'+element.sellrate+'</td>'+
                    '<td>'+element.stock+'</td>'+                            
                    '<td>'+element.discount+'</td>'+
                    '<td>'+element.amount+'</td>'+
                    '<td><input type="radio" onchange="changeSelctedProduct('+element.id+')" name="selctedProduct" value="'+element.id+'" checked>'+
                    '</td>'+
                '</tr>';
        }else{
            if(Number(element.amount) > Number(selectProductrate)){
                html = html +
                    '<tr style="color:red !important; ">'+
                        '<td>'+element.name+'</td>'+
                        '<td>'+element.makename+'</td>'+
                        '<td>'+element.sellrate+'</td>'+
                        '<td>'+element.stock+'</td>'+
                        '<td>'+element.discount+'</td>'+
                        '<td>'+element.amount+'</td>'+
                        '<td><input type="radio" onchange="changeSelctedProduct('+element.id+')" name="selctedProduct" value="'+element.id+'" >'+
                        '</td>'+
                    '</tr>';
            }else if(Number(element.amount) < Number(selectProductrate)){
             
                html = html +
                    '<tr  style="color:green !important;">'+
                        '<td>'+element.name+'</td>'+
                        '<td>'+element.makename+'</td>'+
                        '<td>'+element.sellrate+'</td>'+
                        '<td>'+element.stock+'</td>'+
                        '<td>'+element.discount+'</td>'+
                        '<td>'+element.amount+'</td>'+
                        '<td><input type="radio" onchange="changeSelctedProduct('+element.id+')" name="selctedProduct" value="'+element.id+'" >'+
                        '</td>'+
                    '</tr>';
            }else if(Number(element.amount) == Number(selectProductrate)){
                html = html +
                    '<tr>'+
                        '<td>'+element.name+'</td>'+
                        '<td>'+element.makename+'</td>'+
                        '<td>'+element.sellrate+'</td>'+
                        '<td>'+element.stock+'</td>'+                            
                        '<td>'+element.discount+'</td>'+
                        '<td>'+element.amount+'</td>'+
                        '<td><input type="radio" onchange="changeSelctedProduct('+element.id+')" name="selctedProduct" value="'+element.id+'" >'+
                        '</td>'+
                    '</tr>';
            }
        }


        

    });

    $('#tablesimlierproduct').append(html);
}

function  discountCal(rate,discount) {
   var temp = (parseFloat(rate)*parseFloat(discount))/100;    
   var result=  Number(parseFloat(rate)) - Number(parseFloat(temp));
    return parseFloat(result).toFixed(2);
}

                                                                  
function getAccProduct(productid){
    var urlaccessories = $('#urlaccessories').val();
    var _token = $('input[name="_token"]').val();

    $('#productaccessoriesdiv').hide();
    $('#tableListAccessories tbody').html('');
   
    if(productid){
        
        $.ajax({
            type : 'POST',
            url : urlaccessories,
            data : {productid:productid, _token : _token},
            success : function(data){
                $('#productaccessoriesdiv').show();
                $('#tableListAccessories tbody').append(data);
            }
        });
    }
}

function genralProductAjax(){
    var url = $('#urlGetGenralProduct').val();
    $('#productaccessoriesdiv').hide(); 
    $('#tableListAccessories tbody').empty();
    $('#productid').empty();
    $.ajax({
        type : 'GET',
        url : url,
        success : function(data){
            $('#productid').append(data);
        }
    });

}

function changeqty(pcount){
    var id='#tdqty'+pcount;
    let $this = $(id);
    let qty = $('#tdqty'+pcount).text();
    let $input = $('<input>', {
        type: 'number',
        blur : function(){
            $this.text($(this).val());
            if(($(this).val()).trim() == "" || $(this).val() == null){
               $this.text(qty);
            }
        },
        keyup : function(e){
            if (e.which === 13) $input.blur();
        }
    }).appendTo( $this.empty() ).focus()
}

function addProductInTable(){

    if(currentType == 0 || currentType == 1){

        var qty = ($("#productqty").val()).trim();
        var id = newSelectedProductId;
        var feeder = $('#selectFeeder').val();
    
        if(id == "" || !id){
            alert("Please Select Product");
            return false;   
        }
        if(qty == "" || !qty){
            $('#productqty_error').show();
            return false;   
        }else{
            $('#productqty_error').hide();
        }
       
        if(!feeder || feeder == undefined || feeder == ""){
            alert("Select Feeder")
            return false;
        }

        $('#selectModal').modal('hide');
        ajaxCall(id,qty,feeder);
       
    }else if(currentType == 2){
       var id = $('#productgroupid').val();
       var feeder = $('#selectFeeder').val();

       if(id == "" || !id){
        alert("Please Select Product Group");
        return false;   
        }
        if(!feeder || feeder == undefined || feeder == ""){
            alert("Select Feeder")
            return false;
        }

        appProductGroupIntoList(feeder);
        $('#selectModal').modal('hide');
    }
}

var selectData;
var selectPId;
var selectQty;
var selectSaleRate;
var selctgstPer;
var selctgstAmount;
var selctdecPer;
var selctdecAmount;
var selctedTotal;

function ajaxCall(id,qty,feederid){
   
    var url = '/getproductajax/'+id; 
    selectData = "";
    selectPId = id;
    $.ajax({
        type : 'get',
        url : url,
        success : function(data){
            addRowProduct(data[0],qty,feederid);
        }
    });
    
}

function addRowProduct(data, qty ,fId){ 

  
    cumulativeAmount = 0;
    var type = data.productcategory;
    
    if(type == "general"){
        var mainName = data.productname;
    }else if(type == "specific"){
        var mainName = data.subnameofproduct;
    }

    var makename = data.makename;
    if(!makename){
        makename = "-";
    }
    var feederId = fId;
    var feederName = "";
    var mainId = data.pid;
    var mainQty = qty;
    var mainHSN = "-";
    if(data.hsnno){
        mainHSN = data.hsnno;
    }
    var mainUnitId = data.unitid;
    var mainUnitText = "";
    var mainsalerate = parseFloat(data.sellrate).toFixed(2);
    var mainDis = "0";
    var mainDisAmount = "0";
    var mainGstid = data.gstpersentage;
    var mainGst = ""; 
    var mainGSTAmount ="";
    var mainAmount =parseFloat(mainQty)*parseFloat(mainsalerate);
    var narration =$('#productnarration').val();

      
    var temp = {
        pid : data.pid,
        feederid : feederId
    }

    count++;
    srNo++;
    
    if(!narration){
        narration = "";
    }

    gstmaster.forEach(element => {
        if(element.gstmasterid == data.gstpersentage){
            mainGst = element.gstname+"%";
            mainGSTAmount = (mainAmount*element.gstname)/100;
        }
    });
    
    units.forEach(element => {
        if(element.productunitid == mainUnitId){
            mainUnitText = element.productunitname;
        }
    });
    
    feeders.forEach(element =>{
        if(element.feederid == feederId){
            feederName = element.feedername;
        }
    })

    var mainTotalAmount = Number(parseFloat(mainAmount).toFixed(2))+Number(parseFloat(mainGSTAmount).toFixed(2));     

    var showProductQty = "'"+"#showProductQty"+"'";
    var showProductDisPer = "'"+"#showProductDisPer"+"'";
    var showProductRate =   "'"+"#showProductRate"+"'";
    //PRODUCT
    var record =    '<tr id="tr'+count+'">'+          
                    '<input type="hidden" id="pcont'+count+'" name="pcont'+count+'" value="'+count+'">'+                      
                    '<input type="hidden" id="pid'+count+'" name="pid'+count+'" value="'+mainId+'">'+
                    '<input type="hidden" id="pname'+count+'" name="pname'+count+'" value="'+mainName+'">'+
                    '<input type="hidden" id="pmake'+count+'" name="pmake'+count+'" value="'+makename+'">'+
                    '<input type="hidden" id="feederid'+count+'" name="feederid'+count+'" value="'+feederId+'">'+
                    '<input type="hidden" id="feedername'+count+'" name="feedername'+count+'" value="'+feederName+'">'+
                    '<input type="hidden" id="phsn'+count+'" name="phsn'+count+'" value="'+mainHSN+'">'+
                    '<input type="hidden" id="pqty'+count+'" name="pqty'+count+'" value="'+qty+'">'+
                    '<input type="hidden" id="uid'+count+'" name="uid'+count+'" value="'+mainUnitId+'">'+
                    '<input type="hidden" id="unitText'+count+'" name="unitText'+count+'" value="'+mainUnitText+'">'+
                    '<input type="hidden" id="salerate'+count+'" name="salerate'+count+'" value="'+mainsalerate+'">'+
                    '<input type="hidden" id="discount'+count+'" name="discount'+count+'" value="'+mainDis+'">'+
                    '<input type="hidden" id="discountAmount'+count+'" name="discountAmount'+count+'" value="'+mainDisAmount+'">'+
                    '<input type="hidden" id="gstid'+count+'" name="gstid'+count+'" value="'+mainGstid+'">'+
                    '<input type="hidden" id="gstname'+count+'" name="gstname'+count+'" value="'+mainGst+'">'+
                    '<input type="hidden" id="gstamount'+count+'" name="gstamount'+count+'" value="'+mainGSTAmount+'">'+
                    '<input type="hidden" id="amount'+count+'" name="amount'+count+'" value="'+mainTotalAmount+'">'+
                    '<input type="hidden" id="narration'+count+'" name="narration'+count+'" value="'+narration+'">'+
                    '<input type="hidden" id="cumulativeAmountId'+count+'" name="cumulativeAmountId'+count+'" value="">'+
                    '<input type="hidden" id="numberofaccessories'+count+'" name="numberofaccessories'+count+'" value="">'+
                    '<td style="width:40px; border: 1px solid black; text-align: center;" id="srNo'+count+'">'+srNo+'</td>'+
                    '<td style="width:40px; border: 1px solid black;"  >'+mainName+'</td>'+
                    '<td style="width:40px; border: 1px solid black; text-align: center;" >'+feederName+'</td>'+
                    '<td style="width:40px; border: 1px solid black; text-align: center;" >'+makename+'</td>'+
                    '<td style="width:40px; border: 1px solid black; text-align: center;">'+
                    '<input class="input-fix" type="number" min="0" id="showProductQty'+count+'" value="'+mainQty+'" onchange="rowProductCal('+count+','+showProductQty+')" onfocus="getProductRowOldValueForCal('+count+','+showProductQty+')">'+
                    '</td>'+
                    '<td style="width:40px; border: 1px solid black; text-align: center;">'+
                    '<input class="input-fix" type="number" min="0" id="showProductRate'+count+'" value="'+mainsalerate+'" onchange="rowProductCal('+count+','+showProductRate+')" onfocus="getProductRowOldValueForCal('+count+','+showProductRate+')">'+
                    '</td>'+
                    '<td style="width:40px; border: 1px solid black; text-align: center;">'+
                    '<input class="input-fix" type="number" min="0" max="100" id="showProductDisPer'+count+'" value="'+mainDis+'" onchange="rowProductCal('+count+','+showProductDisPer+')" onfocus="getProductRowOldValueForCal('+count+','+showProductDisPer+')">'+
                    '</td>'+
                    '<td style="width:40px; border: 1px solid black; text-align: center;" id="showProductTotalAmount'+count+'">'+mainTotalAmount+'</td>'+
                    '<td style="width:40px; border: 1px solid black; text-align: center;" onclick="removeProduct('+count+','+mainId+')"><i class="fas fa-times" style="color: red;"></i></td>'+  
                    '</tr>';
        if(narration){
            record = record+ 
                        '<tr id="trnarration'+count+'">'+
                        '<td  style="width:40px; border: 1px solid black; text-align: center;"></td>'+
                        '<td style="width:40px; border: 1px solid black;  font-size:12px !important">'+narration+'</td>'+
                        '<td  style="width:40px; border: 1px solid black; text-align: center;"></td>'+
                        '<td  style="width:40px; border: 1px solid black; text-align: center;"></td>'+
                        '<td  style="width:40px; border: 1px solid black; text-align: center;"></td>'+
                        '<td  style="width:40px; border: 1px solid black; text-align: center;"></td>'+
                        '<td  style="width:40px; border: 1px solid black; text-align: center;"></td>'+
                        '<td  style="width:40px; border: 1px solid black; text-align: center;"></td>'+
                        '<td  style="width:40px; border: 1px solid black; text-align: center;"></td>'+
                        '</tr>';
                    }
                

    $('#tableList').append(record);

    $('#numberOfProductInBill').val(count);
 
   
    var totalLength=$('#tableListAccessories').find('input[name="accessoriescheckbox[]"]').length;
   
    if(!totalLength || totalLength == undefined){
        totalLength = 0;
        updateBillTotal();
        allClear();
    }

    cumulativeAmount =Number(parseFloat(mainTotalAmount).toFixed(2)) + Number(parseFloat(cumulativeAmount).toFixed(2));  
    $('#cumulativeAmountId'+count).val(cumulativeAmount);

   
    var numberofaccessories = 0;
    for (var i = 1; i <= totalLength; i++) {

        var tempId = "#accessoriesadd"+i;
        if ($(tempId).prop("checked") == true) {
            numberofaccessories++;
            var tempQty = "#tdqty"+i;
            var tempQtyValue = ($(tempQty).text()).trim();
            var temp = "#accessoriesid"+i;
            var accessId = $(temp).val();
            setAeccoris(tempQtyValue,accessId,mainId,mainQty,feederId);
        }
    }
    
   

    $('#numberofaccessories'+count).val(numberofaccessories);
    // resertEnterRow();
    allClear();
}

function setAeccoris(qty,accessId,productID,productQty,feederId){

    var url = '/getproductajax/'+accessId; 
    $.ajax({
        type : 'get',
        url : url,
        success : function(data){
            setTableForAeccoris(data[0],qty,productID,productQty,feederId)     
        }
    });

}

function setTableForAeccoris(data,qty,productID,productQty,feederId){
    
    var totalQty = qty*productQty;

    var mainName = data.productname;
    var mainId = data.pid;
    var makename = data.makename;
    if(!makename){
        makename = "-";
    }
    var feederName = "";
    var mainQty = parseFloat(totalQty);
    var mainHSN = "-";
    if(data.hsnno){
        mainHSN = data.hsnno;
    }
    var mainUnitId = data.unitid;
    var mainUnitText = "";
    var mainsalerate = parseFloat(data.sellrate);
    var mainDis = "0";
    var mainDisAmount = "0";
    var mainGstid = data.gstpersentage;
    var mainGst = ""; 
    var mainGSTAmount = "";
    var mainAmount = parseFloat(mainQty)*parseFloat(mainsalerate);
    gstmaster.forEach(element => {
        if(element.gstmasterid == data.gstpersentage){
            mainGst = element.gstname+"%";
            mainGSTAmount = (mainAmount*element.gstname)/100;
        }
    });

    units.forEach(element => {
        if(element.productunitid == mainUnitId){
            mainUnitText = element.productunitname;
        } 
    });
    feeders.forEach(element =>{
        if(element.feederid == feederId){
            feederName = element.feedername;
        }
    })

    var mainTotalAmount = mainAmount+mainGSTAmount;
    accessCount++;

    var accessQty = "'"+"#accessQty"+"'";
    var accessDisPer = "'"+"#accessDisPer"+"'";
    var accessRate =   "'"+"#accessRate"+"'";
    //ACC
    var record =    '<tr id="atr'+accessCount+'">'+
                    '<input type="hidden" id="aprodctcountno'+accessCount+'" name="aprodctcountno'+accessCount+'" value="'+count+'">'+
                    '<input type="hidden" id="aiproductid'+accessCount+'" name="aiproductd'+accessCount+'" value="'+productID+'">'+
                    '<input type="hidden" id="aid'+accessCount+'" name="aid'+accessCount+'" value="'+mainId+'">'+
                    '<input type="hidden" id="afeederid'+accessCount+'" name="afeederid'+accessCount+'" value="'+feederId+'">'+
                    '<input type="hidden" id="afeedername'+accessCount+'" name="afeedername'+accessCount+'" value="'+feederName+'">'+
                    '<input type="hidden" id="aname'+accessCount+'" name="apname'+accessCount+'" value="'+mainName+'">'+
                    '<input type="hidden" id="ahsn'+accessCount+'" name="ahsn'+accessCount+'" value="'+mainHSN+'">'+
                    '<input type="hidden" id="aqty'+accessCount+'" name="aqty'+accessCount+'" value="'+mainQty+'">'+
                    '<input type="hidden" id="aunitText'+accessCount+'" name="aunitText'+accessCount+'" value="'+mainUnitText+'">'+
                    '<input type="hidden" id="auid'+accessCount+'" name="auid'+accessCount+'" value="'+mainUnitId+'">'+
                    '<input type="hidden" id="asalerate'+accessCount+'" name="asalerate'+accessCount+'" value="'+mainsalerate+'">'+
                    '<input type="hidden" id="adiscount'+accessCount+'" name="adiscount'+accessCount+'" value="'+mainDis+'">'+
                    '<input type="hidden" id="adiscountAmount'+accessCount+'" name="adiscountAmount'+accessCount+'" value="'+mainDisAmount+'">'+
                    '<input type="hidden" id="agstid'+accessCount+'" name="agstid'+accessCount+'" value="'+mainGstid+'">'+
                    '<input type="hidden" id="agstname'+accessCount+'" name="agstname'+accessCount+'" value="'+mainGst+'">'+
                    '<input type="hidden" id="agstamount'+accessCount+'" name="agstamount'+accessCount+'" value="'+mainGSTAmount+'">'+
                    '<input type="hidden" id="aamount'+accessCount+'" name="aamount'+accessCount+'" value="'+mainTotalAmount+'">'+ 
                    '<td></td>'+
                    '<td style="border: 1px solid black;"><i style="font-size:12px !important;">'+mainName+'</i></td>'+
                    '<td style="border: 1px solid black; text-align: center;"><i style="font-size:12px !important;">'+feederName+'</i></td>'+
                    '<td style="border: 1px solid black; text-align: center;"><i style="font-size:12px !important;">'+makename+'</i></td>'+
                    '<td style="border: 1px solid black; text-align: center;">'+
                    '<input class="input-fix" style="font-style:italic; font-size:12px !important;" type="number" min="0" id="accessQty'+accessCount+'" value="'+mainQty+'" onchange="rowAccCal('+count+','+accessCount+','+accessQty+')" onfocus="getAccRowOldValueForCal('+count+','+accessCount+','+accessQty+')">'+
                    '</td>'+
                    '<td style="border: 1px solid black; text-align: center;">'+
                    '<input class="input-fix" style="font-style:italic; font-size:12px !important;" type="number" min="0" id="accessRate'+accessCount+'" value="'+mainsalerate+'" onchange="rowAccCal('+count+','+accessCount+','+accessRate+')" onfocus="getAccRowOldValueForCal('+count+','+accessCount+','+accessRate+')">'+
                    '</td>'+
                    '<td style="border: 1px solid black; text-align: center;">'+
                    '<input class="input-fix" style="font-style:italic; font-size:12px !important;" type="number" min="0" id="accessDisPer'+accessCount+'" value="'+mainDis+'" onchange="rowAccCal('+count+','+accessCount+','+accessDisPer+')" onfocus="getAccRowOldValueForCal('+count+','+accessCount+','+accessDisPer+')">'+
                    '</td>'+
                    '<td style="border: 1px solid black; text-align: center;"><i style="font-size:12px !important;" id="accessTotalAmount'+accessCount+'">'+mainTotalAmount+'</i></td>'+
                    '<td style="border: 1px solid black; text-align: center;" onclick="removeProductAcc('+accessCount+','+mainId+','+productID+')"><i class="fas fa-times" style="color: red;"></i></td>'+ 
                    '</tr>';
    $('#tableList').append(record);
    $('#numberOfAccessoriesInBill').val(accessCount);
    cumulativeAmount =Number(parseFloat( mainTotalAmount).toFixed(2)) + Number( parseFloat(cumulativeAmount).toFixed(2));

    $('#cumulativeAmountId'+count).val(parseFloat(cumulativeAmount).toFixed(2));
    updateBillTotal();
}

function appProductGroupIntoList(feeder){
    var length =  $('#productgrouplength').val();
    if(length == undefined || length == "" || !length){
        return;
    }

    var pArray = [];
    for (let i = 1; i <= length; i++) {
        var ptype = parseInt($('#accessoriesid'+i).val());
        var pname = $('#productgrpname'+i).val();
        var pid = parseInt($('#productgrp'+i).val());
        var qty = parseFloat($('#productgroupqty'+i).val());

        if(qty < 0 ){
            alert('Please Enter Minmum 1 Qty '+pname);
            return;
        }

        if(ptype == 0){
            var temp = {
                mainPid : pid,
                mainPname : pname,
                feederId : feeder,
                qty : qty,
                accessoriess : []
            }
            pArray.push(temp);
        }
    }



    for (let k = 0; k < pArray.length; k++) {
      
        for (let i = 1; i <= length; i++) {
            var ptype = parseInt($('#accessoriesid'+i).val());
            var pname = $('#productgrpname'+i).val();
            var pid = parseInt($('#productgrp'+i).val());
            var qty = parseFloat($('#productgroupqty'+i).val());
            var mainpid = pArray[k].mainPid;
            if(ptype != 0){
                if(mainpid == ptype){
                    var temp = {
                        masterPid : mainpid,
                        aPid : pid,
                        feederId : feeder,
                        aPname : pname,
                        qty : qty
                    }   
                    pArray[k].accessoriess.push(temp);
                }
            }
        }
        
    }

    var arrayLength = pArray.length;

    

    addRowFromProductGroup(0,arrayLength,pArray);
}

function addRowFromProductGroup(currentC,arrayLength,pdata){ 

    if(currentC < arrayLength){
        var url = '/getproductajax/'+pdata[currentC].mainPid; 
        selectData = "";
        selectPId = pdata[currentC].mainPid;
        $.ajax({
            type : 'get',
            url : url,
            success : function(data){
                addMainProductFromProductGroup(data[0],pdata,currentC);
            }
        });
    }
    
}

function addMainProductFromProductGroup(data,mainData,currentC) {
   

    cumulativeAmount = 0;
    var type = data.productcategory;
    
    if(type == "general"){
        var mainName = data.productname;
    }else if(type == "specific"){
        var mainName = data.subnameofproduct;
    }

    var makename = data.makename;
    if(!makename){
        makename = "-";
    }
    var feederId = mainData[currentC].feederId;
    var feederName = "";
    var mainId = data.pid;
    var mainQty = mainData[currentC].qty;
    var mainHSN = "-";
    if(data.hsnno){
        mainHSN = data.hsnno;
    }
    var mainUnitId = data.unitid;
    var mainUnitText = "";
    var mainsalerate = parseFloat(data.sellrate).toFixed(2);
    var mainDis = "0";
    var mainDisAmount = "0";
    var mainGstid = data.gstpersentage;
    var mainGst = ""; 
    var mainGSTAmount ="";
    var mainAmount =parseFloat(mainQty)*parseFloat(mainsalerate);
    var narration =$('#productnarration').val();

      
    

    count++;
    srNo++;
    
    if(!narration){
        narration = "";
    }

    gstmaster.forEach(element => {
        if(element.gstmasterid == data.gstpersentage){
            mainGst = element.gstname+"%";
            mainGSTAmount = (mainAmount*element.gstname)/100;
        }
    });
    
    units.forEach(element => {
        if(element.productunitid == mainUnitId){
            mainUnitText = element.productunitname;
        }
    });
    
    feeders.forEach(element =>{
        if(element.feederid == feederId){
            feederName = element.feedername;
        }
    })

    var mainTotalAmount = Number(parseFloat(mainAmount).toFixed(2))+Number(parseFloat(mainGSTAmount).toFixed(2));     

    var showProductQty = "'"+"#showProductQty"+"'";
    var showProductDisPer = "'"+"#showProductDisPer"+"'";
    var showProductRate =   "'"+"#showProductRate"+"'";
    //PRODUCT
    var record =    '<tr id="tr'+count+'">'+          
                    '<input type="hidden" id="pcont'+count+'" name="pcont'+count+'" value="'+count+'">'+                      
                    '<input type="hidden" id="pid'+count+'" name="pid'+count+'" value="'+mainId+'">'+
                    '<input type="hidden" id="pname'+count+'" name="pname'+count+'" value="'+mainName+'">'+
                    '<input type="hidden" id="pmake'+count+'" name="pmake'+count+'" value="'+makename+'">'+
                    '<input type="hidden" id="feederid'+count+'" name="feederid'+count+'" value="'+feederId+'">'+
                    '<input type="hidden" id="feedername'+count+'" name="feedername'+count+'" value="'+feederName+'">'+
                    '<input type="hidden" id="phsn'+count+'" name="phsn'+count+'" value="'+mainHSN+'">'+
                    '<input type="hidden" id="pqty'+count+'" name="pqty'+count+'" value="'+mainQty+'">'+
                    '<input type="hidden" id="uid'+count+'" name="uid'+count+'" value="'+mainUnitId+'">'+
                    '<input type="hidden" id="unitText'+count+'" name="unitText'+count+'" value="'+mainUnitText+'">'+
                    '<input type="hidden" id="salerate'+count+'" name="salerate'+count+'" value="'+mainsalerate+'">'+
                    '<input type="hidden" id="discount'+count+'" name="discount'+count+'" value="'+mainDis+'">'+
                    '<input type="hidden" id="discountAmount'+count+'" name="discountAmount'+count+'" value="'+mainDisAmount+'">'+
                    '<input type="hidden" id="gstid'+count+'" name="gstid'+count+'" value="'+mainGstid+'">'+
                    '<input type="hidden" id="gstname'+count+'" name="gstname'+count+'" value="'+mainGst+'">'+
                    '<input type="hidden" id="gstamount'+count+'" name="gstamount'+count+'" value="'+mainGSTAmount+'">'+
                    '<input type="hidden" id="amount'+count+'" name="amount'+count+'" value="'+mainTotalAmount+'">'+
                    '<input type="hidden" id="narration'+count+'" name="narration'+count+'" value="'+narration+'">'+
                    '<input type="hidden" id="cumulativeAmountId'+count+'" name="cumulativeAmountId'+count+'" value="">'+
                    '<input type="hidden" id="numberofaccessories'+count+'" name="numberofaccessories'+count+'" value="">'+
                    '<td style="width:40px; border: 1px solid black; text-align: center;" id="srNo'+count+'">'+srNo+'</td>'+
                    '<td style="width:40px; border: 1px solid black;"  >'+mainName+'</td>'+
                    '<td style="width:40px; border: 1px solid black; text-align: center;" >'+feederName+'</td>'+
                    '<td style="width:40px; border: 1px solid black; text-align: center;" >'+makename+'</td>'+
                    '<td style="width:40px; border: 1px solid black; text-align: center;">'+
                    '<input class="input-fix" type="number" min="0" id="showProductQty'+count+'" value="'+mainQty+'" onchange="rowProductCal('+count+','+showProductQty+')" onfocus="getProductRowOldValueForCal('+count+','+showProductQty+')">'+
                    '</td>'+
                    '<td style="width:40px; border: 1px solid black; text-align: center;">'+
                    '<input class="input-fix" type="number" min="0" id="showProductRate'+count+'" value="'+mainsalerate+'" onchange="rowProductCal('+count+','+showProductRate+')" onfocus="getProductRowOldValueForCal('+count+','+showProductRate+')">'+
                    '</td>'+
                    '<td style="width:40px; border: 1px solid black; text-align: center;">'+
                    '<input class="input-fix" type="number" min="0" max="100" id="showProductDisPer'+count+'" value="'+mainDis+'" onchange="rowProductCal('+count+','+showProductDisPer+')" onfocus="getProductRowOldValueForCal('+count+','+showProductDisPer+')">'+
                    '</td>'+
                    '<td style="width:40px; border: 1px solid black; text-align: center;" id="showProductTotalAmount'+count+'">'+mainTotalAmount+'</td>'+
                    '<td style="width:40px; border: 1px solid black; text-align: center;" id="showProductTotalAmount'+count+'" onclick="removeProduct('+count+','+mainId+')"><i class="fas fa-times" style="color: red;"></i></td>'+  
                    '</tr>';
        if(narration){
            record = record+ 
                        '<tr id="trnarration'+count+'">'+
                        '<td  style="width:40px; border: 1px solid black; text-align: center;"></td>'+
                        '<td style="width:40px; border: 1px solid black;  font-size:12px !important">'+narration+'</td>'+
                        '<td  style="width:40px; border: 1px solid black; text-align: center;"></td>'+
                        '<td  style="width:40px; border: 1px solid black; text-align: center;"></td>'+
                        '<td  style="width:40px; border: 1px solid black; text-align: center;"></td>'+
                        '<td  style="width:40px; border: 1px solid black; text-align: center;"></td>'+
                        '<td  style="width:40px; border: 1px solid black; text-align: center;"></td>'+
                        '<td  style="width:40px; border: 1px solid black; text-align: center;"></td>'+
                        '<td  style="width:40px; border: 1px solid black; text-align: center;"></td>'+
                        '</tr>';
                    }
                

    $('#tableList').append(record);

    $('#numberOfProductInBill').val(count);
 
   
    var totalLength=mainData[currentC].accessoriess.length;
 
   
    if(!totalLength || totalLength == undefined){
        totalLength = 0;
        updateBillTotal();
        allClear();
        addRowFromProductGroup(currentC+1,mainData.length,mainData);
    }

    cumulativeAmount =Number(parseFloat(mainTotalAmount).toFixed(2)) + Number(parseFloat(cumulativeAmount).toFixed(2));  
    $('#cumulativeAmountId'+count).val(cumulativeAmount);
    var numberofaccessories = 0;
    if(totalLength != 0){
        tempAccArray = mainData[currentC].accessoriess;
        
        for (var i = 0; i < totalLength; i++) {
            numberofaccessories++;
            var tempQtyValue = tempAccArray[i].qty;
            var accessId = tempAccArray[i].aPid;
            setAeccoris(tempQtyValue,accessId,mainData[currentC].mainPid,mainData[currentC].qty,tempAccArray[i].feederId );
        }
        addRowFromProductGroup(currentC+1,mainData.length,mainData);

    }
   
    
   

    $('#numberofaccessories'+count).val(numberofaccessories);
    // resertEnterRow();
    allClear(); 
}


function resertEnterRow(){
    $('#selectrowmake').val("");
    $('#selectrowqty').val("");
    $('#selectFeeder').val('').trigger('change');
    $('#selectrowrate').val("");
    $('#selectrowdisper').val("");
    $('#selectrowamount').val("");
    $('#selectproductname').text("Select Product");
    
    selectData = "";
    selectPId = "";
    selectQty = "";
    selctgstPer = "";
    selctgstAmount = "";
    selctdecPer = "";
    selctdecAmoun= "";
    selectSaleRate = "";
    selctedTotal = "";
}

var oldRowValue;

function getProductRowOldValueForCal(pcount,id){
    var currentValue = parseFloat($(id+pcount).val());
    oldRowValue = currentValue;
}

function rowProductCal(pcount,id){
    var currentValue =  $(id+pcount).val();
    var temp;
    if(currentValue == undefined || currentValue == "" || !currentValue){
        if(oldRowValue == undefined || oldRowValue == "" || !oldRowValue){
            temp = 0;
        }else{
            temp = oldRowValue;
        }
    }else{
        temp = currentValue;
    }
    $(id+pcount).val(temp);
    rowProductUpdateCal(pcount);
}

function rowProductUpdateCal(pcount){   
    var q =  parseFloat($('#showProductQty'+pcount).val());
    var r =  parseFloat($('#showProductRate'+pcount).val());
    var d =  parseFloat($('#showProductDisPer'+pcount).val());
    var g = parseFloat($('#gstid'+pcount).val());

    var gstPer = 0;
    gstmaster.forEach(element => {
        if(element.gstmasterid == g){
            gstPer = parseFloat(element.gstname);
        }
    });

    

    var result = cal(q,r,d,gstPer);

    $('#pqty'+pcount).val(result.qty);
    $('#salerate'+pcount).val(result.rate);
    $('#discount'+pcount).val(result.discountper);
    $('#discountAmount'+pcount).val(result.discountamount);
    $('#gstamount'+pcount).val(result.gstamount);
    $('#amount'+pcount).val(result.total);

    $('#showProductQty'+pcount).val(result.qty);
    $('#showProductDisPer'+pcount).val(result.discountper);
    $('#showProductTotalAmount'+pcount).text(result.total);

    var accCount =  parseFloat($('#numberofaccessories'+pcount).val());

    if(accCount > 0){
        var mPid = $('#pid'+pcount).val();
        var mFid = $('#feederid'+pcount).val();

        if(mPid && mPid != undefined && mPid != "" && $('#pid'+pcount).length && mFid && mFid != undefined && mFid != "" && $('#feederid'+pcount).length){
            var total = parseFloat(result.total).toFixed(2);
            for(var i = 1;i <= accessCount;i++){
                var c = $('#aamount'+i).val();
                if(c && c != undefined && c != "" && $('#aamount'+i).length){
                    var aPid = $('#aiproductid'+i).val();
                    var aFid = $('#afeederid'+i).val();
                    if(aPid == mPid && mFid == aFid){
                        total = Number(parseFloat($('#aamount'+i).val()).toFixed(2)) + Number(total);
                    }
                }
            }
            $('#cumulativeAmountId'+pcount).val(total);
        }
    }else{
        $('#cumulativeAmountId'+pcount).val(result.total);
    }
    updateBillTotal();
}


var oldAccRowValue;


function getAccRowOldValueForCal(pCount,paccCount,id){
    var currentValue = parseFloat($(id+paccCount).val());
    oldAccRowValue = currentValue;
}

function rowAccCal(pCount,paccCount,id){
    var currentValue =  $(id+paccCount).val();
    var temp;
    if(currentValue == undefined || currentValue == "" || !currentValue){
        if(oldRowValue == undefined || oldRowValue == "" || !oldRowValue){
            temp = 0;
        }else{
            temp = oldRowValue;
        }
    }else{
        temp = currentValue;
    }
    $(id+paccCount).val(temp);
    rowAccUpdateCal(pCount,paccCount);
}

function rowAccUpdateCal(pCount,paccCount){

    var q =  parseFloat($('#accessQty'+paccCount).val());
    var r =  parseFloat($('#accessRate'+paccCount).val());
    var d =  parseFloat($('#accessDisPer'+paccCount).val());
    var g =  parseFloat($('#agstid'+paccCount).val());

    var gstPer = 0;
    gstmaster.forEach(element => {
        if(element.gstmasterid == g){
            gstPer = parseFloat(element.gstname);
        }
    });

    var result = cal(q,r,d,gstPer);

    $('#aqty'+paccCount).val(result.qty);
    $('#asalerate'+paccCount).val(result.rate);
    $('#adiscount'+paccCount).val(result.discountper);
    $('#adiscountAmount'+paccCount).val(result.discountamount);
    $('#agstamount'+paccCount).val(result.gstamount);
    $('#aamount'+paccCount).val(result.total);
    

    $('#accessQty'+paccCount).val(result.qty);
    $('#accessRate'+paccCount).val(result.rate);
    $('#accessDisPer'+paccCount).val(result.discountper);
    $('#agstamount'+paccCount).val(result.gstamount);
    $('#accessTotalAmount'+paccCount).text(result.total);

    var accCount =  parseFloat($('#numberofaccessories'+pCount).val());
   
    if(accCount > 0){
    
        var mPid = $('#pid'+pCount).val();
        var mFid = $('#feederid'+pCount).val();

        var total = parseFloat($('#amount'+pCount).val()).toFixed(2);
 
        for(var i = 1;i <= accessCount;i++){
            var c = $('#aamount'+i).val();
            if(c && c != undefined && c != "" && $('#aamount'+i).length){
                var aPid = $('#aiproductid'+i).val();
                var aFid = $('#afeederid'+i).val();
                if(aPid == mPid && mFid == aFid){
                    total = Number(parseFloat($('#aamount'+i).val()).toFixed(2)) + Number(total);
                }
            }
        }
        $('#cumulativeAmountId'+pCount).val(parseFloat(total).toFixed(2));
    }
    updateBillTotal();

}


function cal(calqty,calrate,caldisper,calgstper){

    var retRate = 0;
    var retQty = 0;
    var retAmount = 0;
    var retDisPer = 0;
    var retDisAmount = 0;
    var retGstper = 0;
    var retGstAmount = 0;
    var retTotal = 0;


    if(calrate == "" || calrate === NaN || !calrate){
        retRate = 0;
        calrate =0; 
    }
  
    if(calqty == "" || calqty === NaN || !calqty){
        retQty = 0; 
        calqty= 0;
    }

    if(calrate != "" && calrate !== NaN && calrate || calrate >= 0){
        retRate = parseFloat(calrate);
        retQty = parseFloat(calqty);
        retAmount = retRate*retQty;
        var currentDicount = parseFloat(caldisper);
       
        if(currentDicount || currentDicount == 0){
            if(currentDicount > 100){
                alert('Maximum Discount 100%');
                retDisPer = 0;
                retDisAmount = 0;
            }else{
                var discountAmount =  ((currentDicount*(retRate*retQty)/100));
                retDisPer = currentDicount;
                retDisAmount = discountAmount;
            }
        }else{
            if(currentDicount > 100){
                alert('Maximum Discount 100%');
            }
            retDisPer = 0;
            retDisAmount = 0;
          
        }

        var currentDicountAmount =  parseFloat(retDisAmount);

        if(calgstper && calgstper != "" && calgstper != undefined){
            var newRate = (retRate*retQty)-currentDicountAmount;
            retGstper = parseFloat(calgstper);
            retGstAmount = (retGstper*newRate)/100;
            retTotal = retGstAmount+newRate;
        }else{
            retGstAmount = 0;
            retTotal = (retRate*retQty)-currentDicountAmount;
        }
    }

    var result = {
       rate : retRate.toFixed(2),
       qty :retQty.toFixed(2),
       amount : retAmount.toFixed(2),
       discountper : retDisPer.toFixed(2),
       discountamount : retDisAmount.toFixed(2),
       gstper : retGstper.toFixed(2),
       gstamount : retGstAmount.toFixed(2),
       total : retTotal.toFixed(2) 
    }
    return result;
}


function  removeProduct(pCount,pId) {

    var numAcc = parseInt($('#numberofaccessories1'+pCount).val());

    if(numAcc == 0 ){
        $('#tr'+pCount).remove();
    }else{
        for(var i = 1 ; i <= accessCount;i++){
            if($('#aiproductid'+i).length){
                var mainId = parseInt($('#aiproductid'+i).val());
                var countNo =parseInt($('#aprodctcountno'+i).val());
                if(mainId == pId &&  countNo == pCount){
                    $('#atr'+i).remove();
                }
            }
        }
        $('#tr'+pCount).remove();
    }

    $('#trnarration'+pCount).remove();

    var temCount = 0;
    for(var i = 1; i  <= count; i++){     
        if($('#srNo'+i).length){
            temCount++;
            $('#srNo'+i).text(temCount);
        }
    }
    srNo = temCount;
    updateBillTotal();
}

function  removeProductAcc(aCount,pId,mainId) {
     $('#atr'+aCount).remove();
     updateBillTotal();
}
 

function updateBillTotal(){
    calBillTotal();
    calBillGSTTotal();
}

function calBillGSTTotal(){
    var numberOfAccss = parseInt($('#numberOfAccessoriesInBill').val());
    var numberOfProduct = parseInt($('#numberOfProductInBill').val());

    var gstArray = [];

    gstmaster.forEach(element => {
        $('#trGST'+element.gstmasterid).hide();
        var temp = {
            gstID : element.gstmasterid,
            total : 0
        }
        gstArray.push(temp);
    });
      

    for (let index = 1; index <= numberOfProduct; index++) {
        if($('#gstid'+index).length){
            var id = $('#gstid'+index).val();
            var gstAmount = $('#gstamount'+index).val();
            gstArray.forEach(element => {
                if(element.gstID == id){
                    var tempTotal = element.total + parseFloat(gstAmount);
                    element.total = parseFloat(tempTotal);
                }
            });
        }
       
    }

    for (let index = 1; index <= numberOfAccss; index++) {
        if($('#agstid'+index).length){
            var id = $('#agstid'+index).val();
            var gstAmount = $('#agstamount'+index).val();
            gstArray.forEach(element => {
                if(element.gstID == id){
                    var tempTotal = element.total + parseFloat(gstAmount);
                    element.total = parseFloat(tempTotal);
                }
            });
        }
       
    }

    gstArray.forEach(element => {
        if(element.total != 0){
            $('#trGST'+element.gstID).show();
            $('#billGstAmount'+element.gstID).val(element.total.toFixed(2));
            $('#gstAmount'+element.gstID).val(element.total.toFixed(2));
        }else{
            $('#billGstAmount'+element.gstID).val(0);
        }
    });


}

function calBillTotal(){
    var numberOfAccss = parseInt($('#numberOfAccessoriesInBill').val());
    var numberOfProduct = parseInt($('#numberOfProductInBill').val());
    
    var dicamount = 0;
    var subamount = 0;
    var cumulativeAmount = 0;

    for (var index = 1; index <= Number(numberOfProduct); index++) {

        //Discount
        var dictemp = $('#discountAmount'+index).val(); 
        if(dictemp && dictemp != undefined && dictemp != "" && $('#discountAmount'+index).length){
            dicamount = parseFloat(dictemp)+dicamount;
        }

        //SubTotal
        var tempGstamount = $('#gstamount'+index).val();
        var tempamount = $('#amount'+index).val();
        if(tempamount && tempamount != undefined && tempamount != "" && $('#gstamount'+index).length){
            subamount = parseFloat(tempamount)+Number(subamount)-parseFloat(tempGstamount);
        }
        if(dictemp && dictemp != undefined && dictemp != ""  && $('#discountAmount'+index).length){
            subamount = parseFloat(dictemp)+subamount;
        }

        //Total
        var tempMainAmount = $('#showProductTotalAmount'+index).text();
        if(tempMainAmount && tempMainAmount != undefined && tempMainAmount != "" && $('#showProductTotalAmount'+index).length){
            cumulativeAmount = parseFloat(tempMainAmount)+Number(cumulativeAmount);
        }

    }

    for (var index = 1; index <= Number(numberOfAccss); index++) {
        //Discount
        dictemp = $('#adiscountAmount'+index).val(); 
        if(dictemp && dictemp != undefined && dictemp != "" &&  $('#adiscountAmount'+index).length){
            dicamount = parseFloat(dictemp)+dicamount;
        }

        //SubTotal
        var tempGstamount = $('#agstamount'+index).val();
        var tempamount = $('#aamount'+index).val();
        if(tempamount && tempamount != undefined && tempamount != "" &&  $('#aamount'+index).length){
            subamount = parseFloat(tempamount)+Number(subamount)-parseFloat(tempGstamount);
        }
        if(dictemp && dictemp != undefined && dictemp != "" &&  $('#adiscountAmount'+index).length){
            subamount = parseFloat(dictemp)+subamount;
        }
        
         //Total
         var tempMainAmount = $('#accessTotalAmount'+index).text();
         if(tempMainAmount && tempMainAmount != undefined && tempMainAmount != "" && $('#accessTotalAmount'+index).length){
             cumulativeAmount = parseFloat(tempMainAmount)+Number(cumulativeAmount);
         }
        
    }

    
  
    
    $('#billDiscount').val(dicamount.toFixed(2));
    $('#billDiscountHidden').val(dicamount.toFixed(2));
    
    $('#billSubAmount').val(subamount.toFixed(2));
    $('#billSubAmountHidden').val(subamount.toFixed(2));

    $('#billTotalAmount').val(cumulativeAmount.toFixed(2));
    $('#billAmount').val(cumulativeAmount.toFixed(2));
}

function changeValid(){
    var t = $('#validityBOMId :selected').text();
    $("#validPeriod").val(t);
}

function changeScopeOfwork(){
    var t = $('#scopeofworkId :selected').text();
    $("#scopeofwork").val(t);
}

function changePaymenTterm(){
    
    var t = $('#paymenttermid :selected').text();
    $("#paymentterm").val(t);
}


function productGroupChange(){

    var productgrpid = $('#productgroupid').val();
    var producturl = '/getproductgrp/'+productgrpid;

    $('#tableListgroupProductAccessories tbody').empty();

    if(productgrpid){

        $.ajax({

            type : 'get',
            url : producturl, 
            success : function(productgrp){

                $('#productgroupaccessoriesdiv').show();

                $('#tableListgroupProductAccessories tbody').append(productgrp);
            }
        });
    }

}


function onFormSubmit() {
    var table =  $('#tableList tbody tr').length;
    if(table > 0){
        return true
    }
    alert('Please add Feeder!');
    return false;
}