var currentType = 0;

$( document ).ready(function() {

    $('#formbom').show();
    currentType = 0;
    $('input[type=radio][name=bomtype]').change(function() {
        if (this.value == 'panel') {
            currentType = 0;
            $('#panelorbomid').val('');
            setFromPanel();
        }else if (this.value == 'bom') {
            currentType = 1;
            
            $('#panelorbomid').val('');
            setFromBOM();

        }
    });


});

function  setFromPanel() {
    $('#fomPanel').show();
    $('#formbom').hide();
    $('#oldBomParty').val('').trigger('change');
    $('#quotationid').val('').trigger('change');
    $('#oldbomid').val('');
    $('#panelorbomid').val('');
}

function setFromBOM() {
    $('#fomPanel').hide();
    $('#formbom').show();
    $('#bompanelmasterid').val('').trigger('change'); 
    $('#oldBOMModel').modal('show');
    $('#panelorbomid').val('');
}

function openModel() {
    $('#oldBOMModel').modal('show');
}

function partyChange(){
var partyId = $('#partyid').val();
for(var i = 0; i<partys.length ; i++){
    if(partyId == partys[i].id){
        setGSTType(i);
        break;
    }
}
}
 
function setGSTType(i){

    var gstCode = partys[i].gstcode;
    var gstState = partys[i].statename;
 
    if("24" == gstCode){
        $('#gstType').val('GST').trigger('change');
    }else{
        $('#gstType').val('IGST').trigger('change');
    }
    var placeofsupplay = gstCode+'-'+gstState;
    $('#placeofsupply').val(placeofsupplay);
}

function getBOMPanel() {
    var id = $('#bompanelmasterid').val();
    var url = $('#urlGetPanel').val();
    var _token = $('input[name="_token"]').val();
    $('#bompanelmasteridhidden').val(id);
    $('#bompanelid').empty();
    $.ajax({
        type : 'POST',
        url : url,
        data : {bompanelmasterid:id, _token : _token},
        success : function(data){

            var html = "";
             
            if(data.length == 0){
                html = '<option value="">--No Panel Available--</option>';
            }else{
                html = '<option value="">--Select Panel--</option>';
            }

            data.forEach(element => {
                html = html+ '<option value="'+element.bompanelid+'">'+element.panelcatno+'</option>';
            });

            
            $('#bompanelid').append(html);
        }
    });
}

function vaild() {
    if(currentType == 0){
        
        var id = $('#bompanelmasteridhidden').val();
        var pId = $('#panelorbomid').val();

        if(id == undefined || id == null || id == ""){
            location.reload(true);
        }
        
        
        if(pId == undefined || pId == null || pId == ""){
            location.reload(true);
        }

        return true;
        
        
    }else if(currentType == 1){

        var id = $('#panelorbomid').val();

        if(id == undefined || id == null || id == ""){
            alert("Please Select BOM");
            return false;
        }
        return true;
    }
}

function onPanelChange(){
   var id = $('#bompanelid').val();
   $('#panelorbomid').val(id);
}

var masterPanelId = 0;
var tempList = [];

function oldbomParty() {
    var partyid = $('#oldBomParty').val();
    var qutID = $('#quotationid').val();
    var partyName = $('#oldBomParty :selected').text();
    var qutNo = $('#quotationid :selected').text();
 
    if(!partyid){
        alert('Please Select Party');
        return;
    }
    if(!qutID){
        alert('Please Select Qutation');
        return;
    }
    if(masterPanelId ==0){
        alert('Please Re-Select Qutation');
        return;
    }



    $('#oldbomid').val(qutNo);
    $('#panelorbomid').val(qutID);
    
    $('#bompanelmasteridhidden').val(masterPanelId);

    $('#oldBOMModel').modal('toggle');

}

function onChangeOldBOMParty() {
    var partyid = $('#oldBomParty').val();
    var _token = $('input[name="_token"]').val();
    var url = $('#urlgetoldBOM').val();
    $('#quotationid').empty();
    tempList = [];
    if(partyid){
        $.ajax({
            type : 'POST',
            url : url,
            data : {partyId:partyid, _token : _token},
            success : function(data){
               
                var html = "";
                    if(data.length == 0){
                        html = '<option value="">--No Qutation Available--</option>';
                    }else{
                        html = '<option value="">Select Qutation No.</option>';
                    }
                    data.forEach(element => {
                        tempList.push(element);
                        html = html + '<option value="'+element.bomid+'">'+element.qutationno+'</option>';
                    });

                    $('#quotationid').append(html);
                
            }
        });
    }
    
}


function onQuotation() {

    var qutID = $('#quotationid').val();

    // console.log(tempList);
    tempList.forEach(element => {
        if(element.bomid == qutID){
            masterPanelId = element.bompanelid;
        }
    });
    
}