var count = 0;
var accessoriesCount = 0;
var selctedResult = [];
var selectAccessoriesResult = [];
var radioValue;
var accessoriesRadioValue;
var type = 0;
var onLoad = 0;

var fTime = 0;

var sepCount = [];

$(document).ready(function() {
    $(window).keydown(function(event){
        if(event.keyCode == 13) {
            openSaveWarring();  
          event.preventDefault();
          return false;
        }
      });
    
    temp = $('form input[type=radio][name=productcategory]:checked').val();
    if(temp == "specific"){
        type = 2;
        $('#hsnno').attr('required',true);
        $('#hsnlabel').show();
        $('#accessoriesSpecific').show();
    }else{
        type = 1;
        $('#hsnlabel').hide();
        $('#hsnno').attr('required',false);
        $('#accessoriesSpecific').hide();
    } 

    radioValue = $('form input[type=radio][name=type]:checked').val();
   
    accessoriesRadioValue = $('form input[type=radio][name=accessoriesType]:checked').val();
   
    $('#warranty').change(function(){
        let warranty = $(this).val();
        if(warranty == 'yes'){
            $('#wd').show();
        }else{
            $('#wd').hide();
        }
    });

    if( type == 2){
        changeGrp();    
    }
   


    $('#addaccessories').click(function(){
        $('#accessoriesSpecific').show();
    });


});


function groupTypeChange(){
    if(onLoad!=0)
    {
        $('#productname').val('');
        $('#narration').val('');
    }
    let grouptypeid = $('#grouptypeid').val();
    var url = $('#urlGetGroup').val();
    var _token = $('input[name="_token"]').val();

    $('#groupid').html('');
    $('#specification').html('');
    if(groupid){
        $.ajax({
            type : 'POST',
            url : url,  
            data : {grouptypeid:grouptypeid, _token : _token},
            success : function(data){
                $('#groupid').append(data);
            }
        });
    }else{
        alert('please select Group');
    }
}

function companyChange(){
    if(onLoad!=0)
    {
        $('#productname').val('');
        $('#narration').val('');
    }
    // resetProductName();
}

function changeGrp(){
    if(onLoad!=0)
    {
        $('#productname').val('');
        $('#narration').val('');
    }
    var tempCompanyId = $('#companyid').val();
    if(!tempCompanyId){
        alert('Please Select a Make');
        return;
    }
    $('#specification').empty();
    var grpid = $('#groupid').val();
    var url = $('#urlGetGroupTeplate').val();
    var _token = $('input[name="_token"]').val();
    $.ajax({
        url:url,
        method:"POST",
        data:{groupid:grpid , _token:_token},
        success:function(result)
        {
          // 
          var html='';
          var oldspecificationid='';
          var count = result.length;
          
            
          $.each(result,function(i,specification){
           
              if(oldspecificationid!=specification.specificationid)
              {
                  sepCount.push(i);
                  html+= '<div class="form-group">'+
                  '<label for="groupid" class="col-sm-4 control-label">'+specification.specificationname+
                  '</label><div class="col-sm-12"><select name="subspecificationid'+i+
                  '" class="form-control updateSelect2 select2" id="subspecificationid'+i+'"'+
                  'onchange="setname('+count+')">'+
                  '<option value="">--Select Subspecification--</option>';
                  $.each(result,function(j,subspecification)
                  {
                      if(subspecification.subspecificationname!="")
                      {
                          if(specification.specificationid==subspecification.specificationid)
                          {
                              html+='<option value="'+subspecification.subspecificationname+'">'+subspecification.subspecificationname+'</option>';
                          }
                      }
                  });

                  html +='</select></div></div>';	
                  oldspecificationid = specification.specificationid;
              }		
          });
           $('#specification').append(html);
          $('.updateSelect2').select2();
          
          if(fTime == 0){
            setDefultSpecificatin();
          }
          fTime++;
        
        
     
      },
      dataType:'json'
  });

  if(onLoad != 0){
    var urlInfo = $('#urlGetGroupInfo').val();

    $.ajax({
        url:urlInfo,
        method:"POST",
        data:{groupid:grpid , _token:_token},
        success:function(result)
        {
            if(result.grouphsn){
                $('#hsnno').val(result.grouphsn);
            }else{
                $('#hsnno').val("");
            }
        },
        dataType:'json'
    });
  }

  
  onLoad = 1;
}

var tempproductname='';
function setname(i)
{
    $('#productname').val('');
    var pname='';
    if(i!='narration')
    {
        for(var j=0;j<=i;j++)
        {
            // alert(j);
            if(typeof $('#subspecificationid'+j).val()!="undefined")
            {
                pname+=' '+$('#subspecificationid'+j).val(); 
            }   
        }   
        tempproductname=pname;
        
        $("#mainproductname").val(tempproductname+' '+$('#groupid :selected').text()+'-'+ $('#companyid :selected').text());
        $('#productname').val(tempproductname+' '+($('#narration').val()).trim() +' '+$('#groupid :selected').text()+'-'+ $('#companyid :selected').text())
    }
    else{
        if(($('#narration').val()).trim() =="")
        {
            
            $("#mainproductname").val(tempproductname+' '+$('#groupid :selected').text()+'-'+ $('#companyid :selected').text());
            $('#productname').val(tempproductname+' '+$('#groupid :selected').text()+'-'+ $('#companyid :selected').text())
        }
        else{
            $("#mainproductname").val(tempproductname+' '+$('#groupid :selected').text()+'-'+ $('#companyid :selected').text());
            $('#productname').val(tempproductname+' '+($('#narration').val()).trim() +' '+$('#groupid :selected').text()+'-'+ $('#companyid :selected').text())
        }
    }

    
}

function setDefultSpecificatin() {
    var name = $('#mainproductname').val();
    var makeName = "-"+$('#companyid :selected').text();
    var grpName = $('#groupid :selected').text();
    var narration =($('#narration').val()).trim();
 
    var queryName =  name.replace(grpName,'');
    queryName = queryName.replace(makeName,'');
   
    var sepList = queryName.split(' ');
    for (let i = 0; i < sepCount.length; i++) {
        $('#subspecificationid'+sepCount[i]).val(sepList[i]).trigger('change');
        
    }
    $('#productname').val(queryName+narration+" "+grpName+makeName);
}


function setproducttype(){
var radioValue = $("input[name='productcategory']:checked").val();
    if(radioValue=="general"){   
        $('#specific').hide();
        $('#productname').val('');
        $('#specification').empty();
        $('#producttype').val('');
        $('#productname').attr('readonly',false);
        $('#companyid').attr('disabled',true);
        $('#groupid').attr('disabled',true);
        $('#mainproductname').val('');

        $('#hsnlabel').hide();
        $('#hsnno').attr('required',false);

        type = 1;
    }
    if(radioValue=="specific")
    {
        $('#specific').show();
        $('#productname').val('');
        $('#productname').attr('readonly',true);
        $('#companyid').attr('disabled',false);
        $('#groupid').attr('disabled',false);

        $('#hsnno').attr('required',true);
        $('#hsnlabel').show();

        type = 2;
    }
}

function addunitsubmit()
	{
        var _token=$("input[name=_token]").val();
        var url = $('#urlAjaxAddUnit').val();


		if($('#unitname').val()=="")
		{
			return false;
		}

		$('#unitmodalcontent').hide();

		$('#unitmodalprogress').show();

		$('#addunit').prop('disabled', true);

		$.ajax({type : 'POST', url: url, data: {unitname : $('#unitname').val(),_token:_token}, success: function(result){

			$('#unitmodalprogress').hide();

			$('#unitmodalcontent').show();

			$('#addunitbtn').prop('disabled', false);

			if(result!="")
			{
            // alert(result);
			/// alert($('#unitname').val());
			$('#unitid').append('<option selected="selected" value="'+result+'">'+$('#unitname').val()+'</option>');
			$('#punitid').append('<option value="'+result+'">'+$('#unitname').val()+'</option>');

			$('#addunit').modal('hide');    
			$('#unitname').val(''); 
			$('#unitid').selectpicker('refresh');
			$('#unitid').focus();

		}
		else
		{
			$('#unitmodalalert').show();

		}
	}});

		return false;
}

function openSaveWarring() {
    $('#saveWarringModel').modal('show');
}

function closeModel() {
    $('#saveWarringModel').modal('hide');
}

function checkValid(){
    if( type == 2){
        if(!$('#grouptypeid').val()){
            alert('Please Select Group Type');
            return false;
        }
        if(!$('#companyid').val()){
            alert('Please Select Make');
            return false;
        }
        if(!$('#companyid').val()){
            alert('Please Select Make');
            return false;
        }
        if (!$('#groupid').val()) {
            alert('Please Select Group');
            return false;
        }
        var pname = $('#productname').val();
        if(pname == "" || pname == undefined){
            alert('Please Product');
            return false;
        }
        var el = $("#btnSubmit");
        el.prop('disabled', true);
        setTimeout(function(){el.prop('disabled', false); }, 3000);
        return true;
    }
}


// -------------------------------------------------------
function setFeederType()
{
    radioValue = $("input[name='type']:checked").val();
    
    if(radioValue=="0"){   
        $('#specific').hide();
        reset();    
        
    }
    if(radioValue=="1")
    {
        $('#specific').show();
    }
}

function setFeederAccessoriesType(){
    accessoriesRadioValue = $("input[name='accessoriesType']:checked").val();

    if(accessoriesRadioValue=="0"){   
        $('#accessoriesSpecific').hide();
        resetAccessories();
        
    }
    if(accessoriesRadioValue=="1")
    {
        $('#accessoriesSpecific').show();
    }
}

function addTotable(){

    var getval =  $('#producttypeid').val();
    var productqty =  $('#productqty').val();
    var selectedText =$('#producttypeid :selected').text();
    
    if(!getval){
        alert("Please Select Product Type");
        return;
    }else if(!productqty){
        alert("Please enter quantity");
        return;
    }else{

        for(var i=0; i<selctedResult.length; i++) {
            if (selctedResult[i] == getval){
                alert(selectedText+" product type already selected");
                return;
            }
        }
        
        count++;
        
       $('#tableAccessoriesList').append('<tr id="tr'+accessoriesCount+'"><input type="hidden" name="productAccessoriesid'+accessoriesCount+'" value="'+getval+'" ><input type="hidden" name="productAccessoriesqty'+accessoriesCount+'" value="'+productqty+'" ><td>'+accessoriesCount+'<\/td><td>'+selectedText+'<\/td><td>'+productqty+'<\/td><td><a onclick="removeaccessories('+accessoriesCount+')" title="Remove"><i class="fas fa-times" style="color:red;"></i></a></td></tr>');
        selctedResult.push(getval);
        $("#producttypeid").val('');
        $("#productqty").val('');

        $('#length').val(count);
        
    }

    
}

function removeaccessories(id){


    $('#tr'+id).remove();
 
   

}

function reset(){
    
    $("#tableList > tbody").empty();
    while(selctedResult.length > 0) {
        selctedResult.pop();
    }
    count = 0;
    $('#length').val(count);
    
}

function vaild(){
    
    if(radioValue == 1){
        if(count == 0){
            alert('Please Select Specification');
            return false;
        }
    }
    if(accessoriesRadioValue == 1){
        if(accessoriesCount == 0){
            alert('Please Accessories Product');
            return false;
        }else{
            return true;
        }
    }else{
        return true;
    }
   
}


function addAccessoriesTotable() {

    var getval = $('#productAccessoriesId').val();
    var productqty = $('#productqty').val();
    var selectedText = $('#productAccessories').val();
    var unitname = $('#accessoiesunit').val();

    if (!getval) {
        alert("Please Accessories Select Product");
        return;
    } else if (!productqty) {
        alert("Please enter quantity");
        return;
    } else {

        for (var i = 0; i < selectAccessoriesResult.length; i++) {
            if (selectAccessoriesResult[i].id == getval) {
                alert(selectedText + " accessories product already selected");
                return;
            }
        }

        accessoriesCount++;
        var record = '<tr id="tr' + accessoriesCount + '">'+
                     '<input type="hidden" name="productAccessoriesid' + accessoriesCount + '" value="' + getval + '" >'+
                     '<input type="hidden" name="productAccessoriesqty' + accessoriesCount + '" value="' + productqty + '" >'+
                     '<td>' + accessoriesCount + '</td>'+
                     '<td>' + selectedText + '</td>'+
                     '<td>' + productqty + '</td>'+
                     '<td>' + unitname + '</td>'+
                     '<td onclick="removeaccessories(' + accessoriesCount + ')"><i class="fas fa-times" style="color: red;"></i></td>'+
                     '</tr>';

        $('#tableAccessoriesList').append(record);
        var temp = {
            count : accessoriesCount,
            id : getval,
            name : selectedText,
            qty : productqty,
            unitname : unitname
        }
        selectAccessoriesResult.push(temp);

        $('#productAccessoriesId').val('');
        $('#productAccessories').val('');
        $('#accessoiesunit').val('');
        $("#productqty").val('');

        clearAccGenModel();
        clearAccSepModel();

        $('#accessoriesLength').val(accessoriesCount);

    }
}


function removeaccessories(id) {
    var temp=[];
    var tempCount =1;

    for (let i = 0; i < selectAccessoriesResult.length; i++) {

        if(selectAccessoriesResult[i].count != id){
            var j = {
                count : tempCount,
                id : selectAccessoriesResult[i].id,
                name : selectAccessoriesResult[i].name,
                qty : selectAccessoriesResult[i].qty,
                unitname : selectAccessoriesResult[i].unitname
            }
            temp.push(j);
            tempCount++;
        }
    }

    selectAccessoriesResult = temp;
    
    $('#tableAccessoriesList > tbody').empty();

    for (let i = 0; i < selectAccessoriesResult.length; i++) {
        var record = '<tr id="tr' + selectAccessoriesResult[i].count + '">'+
            '<input type="hidden" name="productAccessoriesid' + selectAccessoriesResult[i].count + '" value="' + selectAccessoriesResult[i].id + '" >'+
            '<input type="hidden" name="productAccessoriesqty' + selectAccessoriesResult[i].count + '" value="' + selectAccessoriesResult[i].qty + '" >'+
            '<td>' + selectAccessoriesResult[i].count + '</td>'+
            '<td>' + selectAccessoriesResult[i].name + '</td>'+
            '<td>' + selectAccessoriesResult[i].qty + '</td>'+
            '<td>' + selectAccessoriesResult[i].unitname + '</td>'+
            '<td onclick="removeaccessories(' + selectAccessoriesResult[i].count + ')"><i class="fas fa-times" style="color: red;"></i></td>'+
            '</tr>';

        $('#tableAccessoriesList').append(record);
    }

    $('#length').val(selectAccessoriesResult.length);
    accessoriesCount = selectAccessoriesResult.length;
}

function resetAccessories(){
    
    $("#tableAccessoriesList > tbody").empty();
    while(selectAccessoriesResult.length > 0) {
        selectAccessoriesResult.pop();
    }
    accessoriesCount = 0;
    $('#accessoriesLength').val(accessoriesCount);
    
}

$(document).ready(function() {
    radioValue = $('form input[type=radio][name=type]:checked').val();
    $("#tableList tr:gt(0)").each(function () {
        var this_row = $(this);
        count++;
        // var productId = $.trim(this_row.find('td:eq(1)').html());//td:eq(0) means first td of this row
        
        var val = this_row.find('td:eq(0)').children('input:eq(0)').val();
        selctedResult.push(val);
        // var Quantity = $.trim(this_row.find('td:eq(2)').html())
    });
    $('#length').val(count);

    accessoriesRadioValue = $('form input[type=radio][name=accessoriesType]:checked').val();

    var a =  $("#tableAccessoriesList tr:gt(0)");

    var j = 1;
    for (let i = 0; i < a.length; i++) {
        
        var temp = {
            count : j,
            id : $('#productAccessoriesid'+j).val(),
            name :  $('#accProductName'+j).text(),
            qty : $('#productAccessoriesqty'+j).val(),
            unitname : $('#accUnitName'+j).text()
        }
        selectAccessoriesResult.push(temp);
        j++; 
        accessoriesCount++;
    }

  
    $('#accessoriesLength').val(accessoriesCount);
 
});

function onsaleDiscountChange() {
    var d =  $('#discount').val();
    var p = $('#prate').val();
    if(d == ""){
        $('#discount').val('0');
        $('#srate').val(p);
        return;
    }
    if(p == "" ){
        $('#srate').val("");
        return;
    }
    if(d == 0){
        
        $('#srate').val(p);
        return;
    }
    if(d > 100){
        alert('Max Discount 100%');
        
        $('#discount').val('0');
        return;
    }
    if(p == "" || !p || p == undefined){
        alert("Please Enter Listing Price");
        $('#srate').val('');
        $('#discount').val('');
    }
    var temp = (Number(parseFloat(p))-Number(parseFloat((p*d)/100))).toFixed(2);
    console.log('temp'+temp);
    $('#srate').val(temp);
}
 $('.twodigit').on('input', function () {
        this.value = this.value.match(/^\d+\.?\d{0,2}/);
    });

//--------------------------for access-----------------------------------------
var currentAcctype = 0;
$( document ).ready(function() {

    $('#divSep').hide();
    currentAcctype = 0;
    $('input[type=radio][name=productTypeAcc]').change(function() {
        if (this.value == 0) {
            currentAcctype = 0;
            clearAccSepModel();
            $('#divSep').hide();
            $('#divGenral').show();
        }else if (this.value == 1) {
            $('#divGenral').hide();
            clearAccGenModel();
            $('#divSep').show();
            currentAcctype =  1;
        }
    });

});

function openModel(){
    $('#generalmodal').modal('show');
} 


function changeCompanyAcc () {
  $('#productgroupAccid').val('').trigger('change');
}

function groupChangeAcc() {
    let grouptypeid = $('#productgroupAccid').val();
    var url = $('#urlGetGroup').val();
    var _token = $('input[name="_token"]').val();

    $('#productsubgroupAccid').html('');
    if (groupid) {
        $.ajax({
            type: 'POST',
            url: url,
            data: {grouptypeid: grouptypeid, _token: _token},
            success: function (data) {
                $('#productsubgroupAccid').append(data);
            }
        });
    } else {
        alert('please select Group');
    }
}

function subGroupChangeAcc() {
    let productsubgroup = $("#productsubgroupAccid").val();
    let productgroup = $('#productgroupAccid').val();
    let company = $('#companyidAcc').val();
    var _token = $('input[name="_token"]').val();
    var url = $('#urlGetProduct').val();

   

    $('#productidAcc').empty();
    if(productsubgroup && company && productgroup){
        $.ajax({
            type : 'POST',
            url : url,
            data : {groupid:productsubgroup, grouptypeid:productgroup, companyid:company, _token : _token},
            success : function(data){
                $('#productidAcc').append(data);
            }
        });
    }
}

function addProductInInput() {


    if(currentAcctype == 0){ //Genral

        if(!$('#generalproduct').val()){
            alert("Please Select Product");
            return ;
        }
        var getval = $('#generalproduct').val();
        var selectedText = $('#generalproduct :selected').text();
       
        for (var i = 0; i < selectAccessoriesResult.length; i++) {
            if (selectAccessoriesResult[i].id == getval) {
                alert(selectedText + " accessories product already selected");
                return;
            }
        }


        unitAjax(getval,selectedText);

        $('#productAccessoriesId').val(getval);
        $('#productAccessories').val(selectedText);

        $('#generalmodal').modal('hide');

    }else if(currentAcctype == 1){
        
        if(!$('#productidAcc').val()){
            alert("Please Select Product");
            return ;
        }

        var getval = $('#productidAcc').val();
        var selectedText = $('#productidAcc :selected').text();

        for (var i = 0; i < selectAccessoriesResult.length; i++) {
            if (selectAccessoriesResult[i].id == getval) {
                alert(selectedText + " accessories product already selected");
                return;
            }
        }

        unitAjax(getval,selectedText);
  
        $('#generalmodal').modal('hide');

    }
}
 
function unitAjax(id,selectedText) {
    
    var url = $('#urlProductUnit').val();
    var _token = $('input[name="_token"]').val();


    if (id) {
        $.ajax({
            type: 'POST',
            url: url,
            data: {id: id, _token: _token},
            success: function (data) {
                
                $('#productAccessoriesId').val(id);
                $('#productAccessories').val(selectedText);
                $('#accessoiesunit').val(data.productunitname);

            }
        });
    } 
}

function clearAccGenModel() {
    $('#generalproduct').val('').trigger('change');
}

function clearAccSepModel() {
    $('#productgroupAccid').val('').trigger('change');
    $('#productidAcc').empty();
}