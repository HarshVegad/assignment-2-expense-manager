
$( document ).ready(function() {

    lists.forEach(element => {
        cal(element);
    });

});


function  cal(element) {
    var ListingPrice = parseFloat(element.price).toFixed(2);
    var pDiscount = parseFloat(element.per).toFixed(2);
    var temp = (Number(ListingPrice)*Number(pDiscount))/100;
    var sellprice = parseFloat(temp).toFixed(2);
   
    console.log(sellprice,pDiscount);

    var url = $('#url').val();
    var _token = $('input[name="_token"]').val();

        
        $.ajax({
            type : 'POST',
            url : url,
            data : {productid:element.id, ListingPrice:ListingPrice ,purchaseDiscount : pDiscount,sellPrice :sellprice,sellDiscount : 0.00,_token : _token},
            success : function(data){
                console.log(data);
               $('#pUpdate'+element.id).text("Done");
            }
        });
    }
