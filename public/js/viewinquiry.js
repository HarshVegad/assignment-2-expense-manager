
var currentRevId = 0;

$( document ).ready(function() {
    
});
function  reviseBOM() {
    var idi =$('#iId').val();

    window.location.replace("/bom/createBomInquiryRevise/r/"+idi+"/"+currentRevId);
}
 
function openReviseBOMModel(id) {
    $('#reviseModel').modal('show');
    currentRevId = id
}

function passid(bomid, option) {
    $('#bomid').val(bomid);
 
    $('#option').val(option);
    if (option == 'view') {
        $('#Label').text('View Quotation');
    } else {
        $('#Label').text('Export Bom PDF');
    }
    if (option == 'quotation') {
        $('#Label').text('View Quotation');
    } else if(option == 'bom'){
        $('#Label').text('Export Bom PDF');
    }
}

function exportpdf(type) {
    let bomid = $('#bomid').val();
    let option = $('#option').val();
    
    if (option == 'view') {
        $('<a href="/viewbompdf/'+ bomid + '/' + type + '/' + option +'" target="_blank">External Link</a>')[0].click();
        // $("<a href='/viewbompdf/'"+ bomid + "'/'" + type + "'target='_blank'></a>")[0].click();  
       // window.location = "/viewbompdf/" + bomid + '/' + type;
    }
    if (option == 'export' || option == 'bom' || option == 'quotation') {
        //alert(option);
        // window.location = "/exportbompdf/" + bomid + '/' + type;
        $('<a href="/exportbompdf/'+ bomid + '/' + type + '/' + option +'" target="_blank">External Link</a>')[0].click();
        // $("<a href='/exportbompdf/' + bomid + '"/"' + type + "'target='_blank'></a>")[0].click();  
    }
}
function lockbom(bomid){
    var bomid=bomid;
    var _token = $('input[name="_token"]').val();
    $.ajax({
        type : 'POST',
        url : '/lockbom',
        data : {bomid:bomid, _token : _token},
        success : function(data){
           if(data == 'true'){
               alert('BOM Locked SuccessFully');
               location.reload();
           }
        }
    });
}
function unlockbom(bomid){
    var bomid=bomid;
    var _token = $('input[name="_token"]').val();
    $.ajax({
        type : 'POST',
        url : '/unlockbom',
        data : {bomid:bomid, _token : _token},
        success : function(data){
           if(data == 'true'){
               alert('BOM UnLocked SuccessFully');
              location.reload();
           }
        }
    });
}
function allowedit(){
    $('#savedetail').removeClass('d-none');
    $('#allowedit').addClass('d-none');
    $('#companyname').attr('readonly',false);
    $('#contactperson').attr('readonly',false);
    $('#mobile1').attr('readonly',false);
    $('#email1').attr('readonly',false);
    $('#billingaddress1').attr('readonly',false);
    $('#billingaddress2').attr('readonly',false);
    $('#billingaddress3').attr('readonly',false);
    $('#shippingaddress1').attr('readonly',false);
    $('#shippingaddress2').attr('readonly',false);
    $('#shippingaddress3').attr('readonly',false);
    $('#statename').attr('disabled',false);
    $('#cityname').attr('disabled',false);
    $('#division').attr('readonly',false);
    $('#place').attr('readonly',false);
}
function savedetail(){
    $('#allowedit').removeClass('d-none');
    $('#savedetail').addClass('d-none');
    $('#companyname').attr('readonly',true);
    $('#contactperson').attr('readonly',true);
    $('#mobile1').attr('readonly',true);
    $('#email1').attr('readonly',true);
    $('#billingaddress1').attr('readonly',true);
    $('#billingaddress2').attr('readonly',true);
    $('#billingaddress3').attr('readonly',true);
    $('#shippingaddress1').attr('readonly',true);
    $('#shippingaddress2').attr('readonly',true);
    $('#shippingaddress3').attr('readonly',true);
    $('#statename').attr('disabled',true);
    $('#cityname').attr('disabled',true);
    $('#division').attr('readonly',true); 
    $('#place').attr('readonly',true);

    var inquiryid = $('#iId').val();
    var _token = $('input[name="_token"]').val();
    var companyname = $('#companyname').val()
    var contactperson =  $('#contactperson').val();
    var mobile1= $('#mobile1').val();
    var email1= $('#email1').val();
    var billingaddress1=  $('#billingaddress1').val();
    var billingaddress2=  $('#billingaddress2').val();
    var billingaddress3=  $('#billingaddress3').val();
    var shippingaddress1= $('#shippingaddress1').val();
    var shippingaddress2=  $('#shippingaddress2').val();
    var shippingaddress3=  $('#shippingaddress3').val();
    var statename=  $('#statename').val();
    var cityname=   $('#cityname').val();
    var division=   $('#division').val();
    var place=  $('#place').val();
   
    $('#quotationid').empty();
    tempList = [];
    if(inquiryid){
        $.ajax({
            type : 'POST',
            url : '/editinquirydetail',
            data : {inquiryid:inquiryid,
                companyname:companyname,
                contactperson:contactperson,
                mobile1:mobile1,email1:email1,
                billingaddress1:billingaddress1,
                billingaddress2:billingaddress2,
                billingaddress3:billingaddress3,
                shippingaddress1:shippingaddress1,
                shippingaddress2:shippingaddress2,
                shippingaddress3:shippingaddress3,
                statename:statename,
                cityname:cityname,
                division:division,
                place:place, _token : _token},
            success : function(data){
               if(data == 'true'){
                   alert('Data Saved SuccessFully');
               }
            }
        });
    }
    

}
function setCity(){
    var _token = $('input[name="_token"]').val();
    var id=$('#statename').val();
    

    $.ajax({
        url:'/getCities',
        method:"POST",
        data : {_token:_token,stateid:id},
        success:function(result)
        {
            $('#cityname').empty();
            var html = "";
            if(result){
                
                var html = '<option value="">--Select City--</option>';		
                result.forEach(element => {
                    html = html + '<option value="'+element.cityid+'">'+element.cityname+'</option>';
                });
            }else{
                html = '<option value="">--No city Available--</option>';
            }
            $('#cityname').append(html);

            // if(cityId != 0){
            //     $('#cityname').val(cityId).trigger('change');
            // }
            
        },
        dataType:"json"
    });
}

