<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Incomes;

class IncomeCategory extends Model
{
    protected $table = 'income_categories';
    protected $primaryKey = 'id';
    protected $fillable = ['name'];

    public function incomes()
    {
        return $this->hasMany(Incomes::class);
    }

}

