<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Session;
use Carbon\Carbon;


class Incomes extends Model
{
    protected $table = 'incomes';
    protected $primaryKey = 'id';
 	protected $fillable = [
       'entry_date','amount','status', 'remark', 'created_by_id','income_category_id','account_id'];

     

    /**
     * Set to null if empty
     * @param $input
     */
    public function setIncomeCategoryIdAttribute($input)
    {
        $this->attributes['income_category_id'] = $input ? $input : null;
    }

   
   

    /**
     * Set to null if empty
     * @param $input
     */
    public function setCreatedByIdAttribute($input)
    {
        $this->attributes['created_by_id'] = $input ? $input : null;
    }
    
    public function income_category()
    {
        return $this->belongsTo(IncomeCategory::class, 'income_category_id');
    }
    
    public function created_by()
    {
        return $this->belongsTo(User::class, 'created_by_id');
    }    
    
   
}

