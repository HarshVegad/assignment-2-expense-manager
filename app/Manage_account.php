<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use App\ExpenseCategory;
use App\IncomeCategory;
use App\Bankmaster;
use App\User;


class Manage_account extends Model
{
    protected $table = 'manage_account';
    protected $primaryKey = 'id';
    protected $fillable = ['entry_date', 'amount', 'remark','status','running_amount','created_by_id','income_category_id','expense_category_id', 'account_id','paymenttype'];

  

    /**
     * Set to null if empty
     * @param $input
     */
    public function setExpenseCategoryIdAttribute($input)
    {
        $this->attributes['expense_category_id'] = $input ? $input : null;
    }

      public function setIncomeCategoryIdAttribute($input)
    {
        $this->attributes['income_category_id'] = $input ? $input : null;
    }


  
    /**
     * Set to null if empty
     * @param $input
     */
    public function setCreatedByIdAttribute($input)
    {
        $this->attributes['created_by_id'] = $input ? $input : null;
    }
    
    public function expense_category()
    {
        return $this->belongsTo(ExpenseCategory::class, 'expense_category_id');
    }

     public function income_category()
    {
        return $this->belongsTo(IncomeCategory::class, 'income_category_id');
    }

     public function bankmaster()
    {
        return $this->belongsTo(Bankmaster::class, 'account_id');
    }
    
    
    
    public function created_by()
    {
        return $this->belongsTo(User::class, 'created_by_id');
    }
}

