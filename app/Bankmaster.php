<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bankmaster extends Model
{
      protected $table = 'bankmaster';
      protected $primaryKey = 'bankid';
   protected $fillable = [
   	'accountno','accountname','ifsccode','bankname','branchname','amount','branchcode','status',
     ];
 }
