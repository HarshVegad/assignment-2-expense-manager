<?php
namespace App;

use Maatwebsite\Excel\Concerns\FromArray;

class Exports implements FromArray
{
    
    public function __construct(array $exports)
    {
      
        $this->exports = $exports;
    }
    public function array(): array
    {
        return $this->exports;
    }
}