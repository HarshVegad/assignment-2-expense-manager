<?php

namespace App\Exports;


use Illuminate\Http\Request;
use DB;
use Helper;
use Session;
use Illuminate\Validation\Rule;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Contracts\View\View;

class UsersExport implements FromView, WithHeadings, ShouldAutoSize
{
    /**
     * @return \Illuminate\Support\Collection
     */


    private  $Manage_account;
    public function __construct($Manage_account)
    {
        $this->Manage_account = $Manage_account;
    }
    public function view(): View
    {
        return view('manage', [
            'Manage_account' => $this->Manage_account
        ]);
    }



    public function headings(): array
    {
        return ['Date', 'PaymentType', 'Account', 'Amount', 'Running Amount', 'Category', 'Remark'];
    }
}
