<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Bankmaster;
use DB;
use Helper;
use Session;
use Illuminate\Validation\Rule;

class BankController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $Bankmaster = Bankmaster::all();

        return view('admin.Bankmaster.viewbank', compact('Bankmaster'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.Bankmaster.addbank');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());

        /*****************************Validation*****************************************/

        $request->validate([

            'accountNo' => 'nullable|min:3|max:255|required',
            'accountName' => 'nullable|min:3|max:255|required',
            'IFSCcode' => 'nullable|min:3|max:255|required',
            'BankName' => 'nullable|min:3|max:255|required',
            'BranchName' => 'nullable|min:3|max:255|required',
            'BranchCode' => 'nullable|min:3|max:255|required',

        ]);
        /*****************************End Validation*****************************************/


        /*************************try code**********************************************/

        DB::beginTransaction();
        try {

            $bankmaster = bankmaster::create([


                'accountno' => $request['accountNo'],
                'accountname' => $request['accountName'],
                'ifsccode' => $request['IFSCcode'],
                'bankname' => $request['BankName'],
                'branchname' => $request['BranchName'],
                'branchcode' => $request['BranchCode'],
            ]);


            DB::commit();
            $success = true;

            /***********************End **try code**********************************************/
        } catch (\Exception $e) {
            /*************cache code**************************/
            $success = false;
            DB::rollback();
        }
        /*************if try code fails**************************/
        if ($success == false) {
            return redirect('dashboard');
        }

        Session::flash('message_display', 'Bank Details is added successfully');
        Session::flash('alert-type', 'success');

        return redirect()->to('bank');
    }



    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $bankmaster = Bankmaster::findOrFail($id);
        return view('admin.Bankmaster.editbank', compact('bankmaster'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        /*****************************Validation*****************************************/

        $result  = request()->validate([
            'accountNo' => 'nullable|min:3|max:255|required',
            'accountName' => 'nullable|min:3|max:255|required',
            'IFSCcode' => 'nullable|min:3|max:255',
            'BankName' => 'nullable|min:3|max:255|required',
            'BranchName' => 'nullable|min:3|max:255',
            'BranchCode' => 'nullable|min:3|max:255',
        ]);

        /*****************************End Validation*****************************************/

        /*************************try code**********************************************/

        DB::beginTransaction();
        try {
            $bankmaster = Bankmaster::findOrFail($id);

            $bankmaster->accountno = $request->accountNo;
            $bankmaster->accountname = $request->accountName;
            $bankmaster->ifsccode = $request->IFSCcode;
            $bankmaster->bankname = $request->BankName;
            $bankmaster->branchname = $request->BranchName;
            $bankmaster->branchcode = $request->BranchCode;
            $bankmaster->save();

            DB::commit();
            $success = true;

            /***********************End **try code**********************************************/
        } catch (\Exception $e) {
            /*************cache code**************************/
            $success = false;
            DB::rollback();
        }
        /*************if try code fails**************************/
        if ($success == false) {
            return redirect('dashboard');
        }

        Session::flash('message_display', 'Bank Details is updated successfully');
        Session::flash('alert-type', 'success');

        return redirect()->to('bank');
    }

    /**
     * Deactive the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */


    public function deactive($id)
    {

        $bankmaster = Bankmaster::findOrFail($id);

        /*************************try code**********************************************/

        DB::beginTransaction();
        try {

            if (!empty($bankmaster)) {
                $bankmaster->status = "Deactive";
                $bankmaster->save();
            }
            DB::commit();
            $success = true;

            /***********************End **try code**********************************************/
        } catch (\Exception $e) {
            /*************cache code**************************/
            $success = false;
            DB::rollback();
        }
        /*************if try code fails**************************/
        if ($success == false) {
            return redirect('dashboard');
        }

        Session::flash('message_display', 'Bank Details is deactive successfully');
        Session::flash('alert-type', 'error');

        return redirect()->to('bank');
    }

    /**
     * Active the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function active($id)
    {

        $bankmaster = Bankmaster::findOrFail($id);

        /*************************try code**********************************************/

        DB::beginTransaction();
        try {

            if (!empty($bankmaster)) {
                $bankmaster->status = "Active";
                $bankmaster->save();
            }
            DB::commit();
            $success = true;

            /***********************End **try code**********************************************/
        } catch (\Exception $e) {
            /*************cache code**************************/
            $success = false;
            DB::rollback();
        }
        /*************if try code fails**************************/
        if ($success == false) {
            return redirect('dashboard');
        }
        Session::flash('message_display', 'Bank Details is active successfully');
        Session::flash('alert-type', 'success');
        return redirect()->to('bank');
    }
}
