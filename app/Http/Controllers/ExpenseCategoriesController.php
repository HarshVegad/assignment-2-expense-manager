<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ExpenseCategory;
use DB;
use Helper;
use Session;
use Illuminate\Validation\Rule;

class ExpenseCategoriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $ExpenseCategory = ExpenseCategory::all();

        return view('admin.ExpenseCategory.viewexpensecategory', compact('ExpenseCategory'));
    }
    public function getFirstNameAttribute($value)
    {
        return ucfirst($value);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.ExpenseCategory.addexpensecategory');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());

        /*****************************Validation*****************************************/

        $request->validate([
            'name' =>  ['required', 'max:100', Rule::unique('expense_categories')],
        ]);
        /*****************************End Validation*****************************************/


        /*************************try code**********************************************/

        DB::beginTransaction();
        try {

            $expense_category = ExpenseCategory::create($request->all());


            DB::commit();
            $success = true;

            /***********************End **try code**********************************************/
        } catch (\Exception $e) {
            /*************cache code**************************/
            $success = false;
            DB::rollback();
        }
        /*************if try code fails**************************/
        if ($success == false) {
            return redirect('dashboard');
        }

        Session::flash('message_display', 'Expense Category is added successfully');
        Session::flash('alert-type', 'success');

        return redirect()->to('expense_categories');
    }



    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $expense_categories = ExpenseCategory::findOrFail($id);
        return view('admin.ExpenseCategory.editexpensecategory', compact('expense_categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        /*****************************Validation*****************************************/

        $result  = request()->validate([
            'name' => ['required', 'max:100', Rule::unique('expense_categories')->ignore($id, 'id')],
        ]);

        /*****************************End Validation*****************************************/

        /*************************try code**********************************************/

        DB::beginTransaction();
        try {
            $expense_category = ExpenseCategory::findOrFail($id);
            $expense_category->update($request->all());



            DB::commit();
            $success = true;

            /***********************End **try code**********************************************/
        } catch (\Exception $e) {
            /*************cache code**************************/
            $success = false;
            DB::rollback();
        }
        /*************if try code fails**************************/
        if ($success == false) {
            return redirect('dashboard');
        }

        Session::flash('message_display', 'Expense Category is updated successfully');
        Session::flash('alert-type', 'success');

        return redirect()->to('expense_categories');
    }

    /**
     * Deactive the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */


    public function deactive($id)
    {

        $expense_categories = ExpenseCategory::findOrFail($id);

        /*************************try code**********************************************/

        DB::beginTransaction();
        try {

            if (!empty($expense_categories)) {
                $expense_categories->status = "Deactive";
                $expense_categories->save();
            }
            DB::commit();
            $success = true;

            /***********************End **try code**********************************************/
        } catch (\Exception $e) {
            /*************cache code**************************/
            $success = false;
            DB::rollback();
        }
        /*************if try code fails**************************/
        if ($success == false) {
            return redirect('dashboard');
        }

        Session::flash('message_display', 'Expense Category is deactive successfully');
        Session::flash('alert-type', 'error');

        return redirect()->to('expense_categories');
    }

    /**
     * Active the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function active($id)
    {

        $expense_categories = ExpenseCategory::findOrFail($id);

        /*************************try code**********************************************/

        DB::beginTransaction();
        try {

            if (!empty($expense_categories)) {
                $expense_categories->status = "Active";
                $expense_categories->save();
            }
            DB::commit();
            $success = true;

            /***********************End **try code**********************************************/
        } catch (\Exception $e) {
            /*************cache code**************************/
            $success = false;
            DB::rollback();
        }
        /*************if try code fails**************************/
        if ($success == false) {
            return redirect('dashboard');
        }
        Session::flash('message_display', 'Expense Category is active successfully');
        Session::flash('alert-type', 'success');
        return redirect()->to('expense_categories');
    }
}
