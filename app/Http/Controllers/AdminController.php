<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Year;
use Session;

class AdminController extends Controller
{
    public function dashboard(Request $request)
    {



        return view('dashboard');
    }

    public function adminlogin()
    {

        return view('admin_login');
    }

    public function check(Request $request)
    {

        if ($request->username == 'admin' && $request->password == 'admin') {

            return redirect()->to('dashboard');
        } else {

            return back()->with('message', 'Username or Password is invalid');
        }
    }

    public function loginprocess(Request $request)
    {

        $email = $request->username;
        $password = $request->password;

        $user = User::where('email', $email)->first();
        if (!empty($user)) {
            if ($user->status != 'Active') {
                return back()->with('message', 'User is not active');
            } else {

                $user_id = $user->id;
                $user_email = $user->email;
                $role = $user->role;
                $firstname = $user->firstname;
                $lastname = $user->lastname;
                $mobile = $user->mobile;
                $user_password = $user->password;

                $Yearid = Year::where('status', 'active')->where('currentyears', 1)->get()->all();

                if ($email == $user_email && $password == $user_password) {
                    session()->put('user_id', $user_id);
                    session()->put('logged_email', $user_email);
                    session()->put('logged_role', $role);
                    session()->put('logged_firstname', $firstname);
                    session()->put('logged_lastname', $lastname);
                    session()->put('logged_mobile', $mobile);
                    session(['yearId' => $Yearid[0]->yearid]);

                    return redirect()->to('dashboard');
                } else {

                    return back()->with('message', 'Email or password is invalid');
                }
            }
        } else {
            return back()->with('message', 'Email not found');
        }
    }

    public function logout()
    {

        Session::flush();

        return redirect()->to('/');
    }
}
