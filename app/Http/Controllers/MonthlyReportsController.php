<?php

namespace App\Http\Controllers;

use DB;
use Illuminate\Http\Request;
use Session;
use App\Incomes;
use App\Expenses;

class MonthlyReportsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $method = $request->method();

        $year = $request->year;
        $month = $request->month;
        $actionbyid = Session::get('user_id');

        $incomes = Incomes::where('created_by_id', $actionbyid)->where('status', 'Active')->select(DB::raw('SUM(amount) as amount'))->get();

        $monthlyincome = Incomes::where('created_by_id', $actionbyid)->where('status', 'Active')->select(DB::raw('SUM(amount) as amount'))->get();

        $expenses = Expenses::where('created_by_id', $actionbyid)->where('status', 'Active')->select(DB::raw('SUM(amount) as amount'))->get();

        $monthlyexpense = Expenses::where('created_by_id', $actionbyid)->where('status', 'Active')->select(DB::raw('SUM(amount) as amount'))->get();

        $monthlyincome['amount'] = 0;
        $monthlyexpense['amount'] = 0;

        if ($request->isMethod('post')) {
            $startdate = '01-' . $month . '-' . $year;
            $startdate = date('Y-m-d', strtotime($startdate));
            $lastdate = '31-' . $month . '-' . $year;
            $lastdate = date('Y-m-d', strtotime($lastdate));

            $monthlyincome = Incomes::where('created_by_id', $actionbyid)->where('status', 'Active')
                ->Where('entry_date', '>=', $startdate)
                ->Where('entry_date', '<=', $lastdate)
                ->select(DB::raw('SUM(amount) as amount'))->get();

            $monthlyexpense = Expenses::where('created_by_id', $actionbyid)->where('status', 'Active')
                ->Where('entry_date', '>=', $startdate)
                ->Where('entry_date', '<=', $lastdate)
                ->select(DB::raw('SUM(amount) as amount'))->get();

            return view('admin.MonthlyReports.monthlyreports', compact('incomes', 'expenses', 'monthlyexpense', 'monthlyincome', 'month', 'year'));
        }

        return view('admin.MonthlyReports.monthlyreports', compact('incomes', 'expenses', 'monthlyexpense', 'monthlyincome', 'month', 'year'));
    }
}
