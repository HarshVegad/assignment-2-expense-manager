<?php

namespace App\Http\Controllers;

use App\User;
use Dotenv\Exception\ValidationException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class LoginController extends Controller
{
    public function login(Request $request)
    {
        $request->validate([
            'email' => ['required', 'email'],
            'password' => ['required']
        ]);
        $user = User::where('email',$request->email)->first();
        if(!$user || !Hash::check($request->password,$user->password)){
                        return response(['Message'=>'Invalid Login Credential']);

        }

    //   $accessToken=  $user->createToken('Auth Toker')->accessToken;
        return  $user->createToken('Auth Toker')->accessToken;
    //    return response([
    //     'user' => Auth::user(),
    //     'accesstoken' =>  $accessToken
    // ]);
        
    }
    public function logout(Request $request)
    {
       $request->user()->token()->delete();
        
    }
}
