<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ExpenseCategory;
use DB;
use App\Expenses;
use App\Bankmaster;
use App\Debitdetails;
use Helper;
use Session;
use App\Manage_account;
use Illuminate\Validation\Rule;

class ExpensesController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    $actionbyid = Session::get('user_id');

    $Expenses = Expenses::with(['expense_category' => function ($query) {
      $query->select('id', 'name');
    }])->where('created_by_id', $actionbyid)->orderBy('expenses.entry_date', 'desc')->get();
    //   dd($Expenses->toArray());


    return view('admin.Expenses.viewexpenses', compact('Expenses'));
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
    $ExpenseCategory = ExpenseCategory::all();
    $bankmaster = Bankmaster::all();


    return view('admin.Expenses.addexpenses', compact('ExpenseCategory', 'bankmaster'));
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    // dd($request->all());

    /*****************************Validation*****************************************/

    $request->validate([
      'expense_category_id' =>  ['required'],
      'entry_date' =>  ['required'],
      'amount' =>  ['required'],
      'account_id' =>  ['required'],


    ]);
    /*****************************End Validation*****************************************/


    /*************************try code**********************************************/

    DB::beginTransaction();
    try {
      $actionbyid = Session::get('user_id');
      $expenses = Expenses::create($request->all() + ['created_by_id' => $actionbyid]);

          /*************************running_amount code**********************************************/

      $creditedetails = Manage_account::where('paymenttype', 'Credit')->where('account_id', $request->account_id)->where('created_by_id', $actionbyid)->select(DB::raw('SUM(amount) as amount'))->get();

      $debitdetails = Manage_account::where('paymenttype', 'Debit')->where('account_id', $request->account_id)->where('created_by_id', $actionbyid)->select(DB::raw('SUM(amount) as amount'))->get();


      $totalcredit = $creditedetails[0]->amount;

      $totaldebit = $debitdetails[0]->amount;

      $running_amount =   $totalcredit - $totaldebit;

      $running_amount =   $running_amount - $request->amount;

                /************************* End running_amount code**********************************************/

      $Manage_account = Manage_account::create($request->all() + ['created_by_id' => $actionbyid] + ['paymenttype' => 'Debit'] + ['running_amount' => $running_amount]);

      DB::commit();
      $success = true;

      /***********************End **try code**********************************************/
    } catch (\Exception $e) {
      /*************cache code**************************/
      $success = false;
      DB::rollback();
    }
    /*************if try code fails**************************/
    if ($success == false) {
      return redirect('dashboard');
    }

    Session::flash('message_display', 'Expense is added successfully');
    Session::flash('alert-type', 'success');

    return redirect()->to('expenses');
  }



  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit($id)
  {

    $ExpenseCategory = ExpenseCategory::all();
    $expenses = Expenses::findOrFail($id);
    $bankmaster = Bankmaster::all();

    return view('admin.expenses.editexpenses', compact('expenses', 'ExpenseCategory', 'bankmaster'));
  }


  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
    /*****************************Validation*****************************************/

    $request->validate([
      'expense_category_id' =>  ['required'],
      'entry_date' =>  ['required'],
      'amount' =>  ['required'],
      'account_id' =>  ['required'],




    ]);

    /*****************************End Validation*****************************************/

    /*************************try code**********************************************/

    DB::beginTransaction();
    try {
      $actionbyid = Session::get('user_id');
      $expenses = Expenses::findOrFail($id);
      $expenses->update($request->all() + ['created_by_id' => $actionbyid]);

      // $incomes = Manage_account::findOrFail($id);
      // $incomes->update($request->all() + ['created_by_id' => $actionbyid] + ['paymenttype' => 'Debit']);



      DB::commit();
      $success = true;

      /***********************End **try code**********************************************/
    } catch (\Exception $e) {
      /*************cache code**************************/
      $success = false;
      DB::rollback();
    }
    /*************if try code fails**************************/
    if ($success == false) {
      return redirect('dashboard');
    }

    Session::flash('message_display', 'Expense is updated successfully');
    Session::flash('alert-type', 'success');

    return redirect()->to('expenses');
  }

  /**
   * Deactive the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */


  public function deactive($id)
  {

    $expenses = Expenses::findOrFail($id);

    /*************************try code**********************************************/

    DB::beginTransaction();
    try {

      if (!empty($expenses)) {
        $expenses->status = "Deactive";
        $expenses->save();
      }
      DB::commit();
      $success = true;

      /***********************End **try code**********************************************/
    } catch (\Exception $e) {
      /*************cache code**************************/
      $success = false;
      DB::rollback();
    }
    /*************if try code fails**************************/
    if ($success == false) {
      return redirect('dashboard');
    }

    Session::flash('message_display', 'Expense is deactive successfully');
    Session::flash('alert-type', 'error');

    return redirect()->to('expenses');
  }

  /**
   * Active the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */

  public function active($id)
  {

    $expenses = Expenses::findOrFail($id);

    /*************************try code**********************************************/

    DB::beginTransaction();
    try {

      if (!empty($expenses)) {
        $expenses->status = "Active";
        $expenses->save();
      }
      DB::commit();
      $success = true;

      /***********************End **try code**********************************************/
    } catch (\Exception $e) {
      /*************cache code**************************/
      $success = false;
      DB::rollback();
    }
    /*************if try code fails**************************/
    if ($success == false) {
      return redirect('dashboard');
    }
    Session::flash('message_display', 'Expense is active successfully');
    Session::flash('alert-type', 'success');
    return redirect()->to('expenses');
  }
}
