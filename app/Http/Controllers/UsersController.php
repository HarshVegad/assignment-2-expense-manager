<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Role;
use App\Location;
use Illuminate\Validation\Rule;
use Helper;
use Session;
use Illuminate\Support\Facades\Mail;
use App\Mail\WelcomeMail;
use App\Jobs\SendEmailJob;

class UsersController extends Controller
{
    public function index(Request $request)
    {
        $users = User::leftjoin('location', 'users.location', 'location.id')->leftjoin('roles', 'users.role', 'roles.roleid')->select('users.*', 'location.locationname', 'roles.employeerole as emprole')->get()->all();

        return view('admin.users', compact('users'));
    }
    public function adduser(Request $request)
    {
        $users = User::get()->all();

        if ($request->isMethod('post')) {
            // dd($request);
            $request->validate([
                'email' => 'required|max:255|unique:users,email',
                'firstname' => 'required|max:255',
                'lastname' => 'required|max:255',
                'mobile' => 'required|unique:users,mobile',
                'password' => ['required', 'string', 'min:8'],
                'location' => 'required'
            ]);
            $user =   User::create([
                'firstname' => $request['firstname'],
                'lastname' => $request['lastname'],
                'mobile' => $request['mobile'],
                'email' => $request['email'],
                'role' => $request['role'],
                'password' => $request['password'],
                'location' => $request['location'],
                //'signature'=>strtolower($request['signature']),
                //'resetcode'=>strtolower($request['resetcode']),
                'status' => 'Active',

            ]);

        //  Mail::to($request['email'])->send(new WelcomeMail());

           $details=[
                "email"=>$request['email'],
                "title"=>'Welcome Mail',
                "body"=>'Welcome to Expense Managing Team Hope you are doing well! We are glad that you took the decision to join  us',
            ];
            dispatch(new SendEmailJob($details));

            Session::flash('message_display', 'User is added successfully');
            Session::flash('alert-type', 'success');

            return redirect('users');
        }
        $roles = Role::where('status', 'Active')->get()->all();
        $location = Location::where('status', 'Active')->get()->all();

        return view('admin.adduser', compact('users', 'location', 'roles'));
    }
    public function edituser($id, Request $request)
    {
        $user = User::findOrFail($id);
        $location = Location::where('status', 'Active')->get()->all();
        if ($request->isMethod('post')) {

            $request->validate([
                'email' => ['required', 'max:255', Rule::unique('users')->ignore($id, 'id')],
                'firstname' => 'required|max:255',
                'lastname' => 'required|max:255',
                'mobile' =>  ['required', Rule::unique('users')->ignore($id, 'id')],
                'password' => 'required|max:255',
                'location' => 'required'

            ]);


            $user->firstname = $request->firstname;
            $user->lastname = $request->lastname;
            $user->email = $request->email;
            $user->role = $request->role;
            $user->mobile = $request->mobile;
            $user->location = $request->location;
            $user->password = !empty($request->password) ? $request->password : $user->password;
            $user->save();

            Session::flash('message_display', 'User is updated successfully');
            Session::flash('alert-type', 'success');

            return redirect('users');
        }

        $roles = Role::where('status', 'Active')->get()->all();
        return view('admin.edituser', compact('user', 'location', 'roles'));
    }
    public function deactiveuser($id)
    {
        $user = User::findOrFail($id);
        $user->status = 'Deactive';
        $user->save();

        Session::flash('message_display', 'User is deactiveted successfully');
        Session::flash('alert-type', 'error');
        return redirect('users');
    }

    public function activeuser($id)
    {
        $user = User::findOrFail($id);
        $user->status = 'Active';
        $user->save();

        Session::flash('message_display', 'User is activeted successfully');
        Session::flash('alert-type', 'success');

        return redirect('users');
    }
}
