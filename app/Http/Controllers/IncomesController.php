<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ExpenseCategory;
use App\Incomes;
use App\IncomeCategory;
use App\Bankmaster;
use App\creditedetails;
use DB;
use App\Manage_account;
use Helper;
use Session;
use Illuminate\Validation\Rule;

class IncomesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $actionbyid = Session::get('user_id');

        $Incomes = Incomes::with(['income_category' => function ($query) {
            $query->select('id', 'name');
        }])->where('created_by_id', $actionbyid)->orderBy('incomes.entry_date', 'desc')->get();
        //  dd($Incomes);


        return view('admin.Incomes.viewincomes', compact('Incomes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $IncomeCategory = IncomeCategory::all();
        $bankmaster = Bankmaster::all();

        return view('admin.Incomes.addincomes', compact('IncomeCategory', 'bankmaster'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());

        /*****************************Validation*****************************************/

        $request->validate([
            'income_category_id' =>  ['required'],
            'entry_date' =>  ['required'],
            'amount' =>  ['required'],
            'account_id' =>  ['required'],


        ]);
        /*****************************End Validation*****************************************/


        /*************************try code**********************************************/

        DB::beginTransaction();
        try {
            $actionbyid = Session::get('user_id');

            $incomes = Incomes::create($request->all() + ['created_by_id' => $actionbyid]);

            /*************************running_amount code**********************************************/

            $creditedetails = Manage_account::where('paymenttype', 'Credit')->where('account_id', $request->account_id)->where('created_by_id', $actionbyid)->select(DB::raw('SUM(amount) as amount'))->get();

            $debitdetails = Manage_account::where('paymenttype', 'Debit')->where('account_id', $request->account_id)->where('created_by_id', $actionbyid)->select(DB::raw('SUM(amount) as amount'))->get();

            $totalcredit = $creditedetails[0]->amount;

            $totaldebit = $debitdetails[0]->amount;

            $running_amount =   $totalcredit - $totaldebit;
            $running_amount =   $running_amount + $request->amount;

            /*************************End running_amount code**********************************************/

            $Manage_account = Manage_account::create($request->all() + ['created_by_id' => $actionbyid] + ['paymenttype' => 'Credit'] + ['running_amount' => $running_amount]);

            DB::commit();
            $success = true;

            /***********************End **try code**********************************************/
        } catch (\Exception $e) {
            /*************cache code**************************/
            $success = false;
            DB::rollback();
        }
        /*************if try code fails**************************/
        if ($success == false) {
            return redirect('dashboard');
        }

        Session::flash('message_display', 'Income is added successfully');
        Session::flash('alert-type', 'success');

        return redirect()->to('incomes');
    }



    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $bankmaster = Bankmaster::all();
        $IncomeCategory = IncomeCategory::all();
        $incomes = Incomes::findOrFail($id);
        return view('admin.Incomes.editincomes', compact('incomes', 'IncomeCategory', 'bankmaster'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        /*****************************Validation*****************************************/

        $request->validate([

            'income_category_id' =>  ['required'],
            'entry_date' =>  ['required'],
            'amount' =>  ['required'],
            'account_id' =>  ['required'],


        ]);

        /*****************************End Validation*****************************************/

        /*************************try code**********************************************/

        DB::beginTransaction();
        try {

            $actionbyid = Session::get('user_id');
            $incomes = Incomes::findOrFail($id);
            $incomes->update($request->all() + ['created_by_id' => $actionbyid]);


            // $incomes = Manage_account::findOrFail($id);
            // $incomes->update($request->all() + ['created_by_id' => $actionbyid] + ['paymenttype' => 'Credit']);

            DB::commit();
            $success = true;

            /***********************End **try code**********************************************/
        } catch (\Exception $e) {
            /*************cache code**************************/
            $success = false;
            DB::rollback();
        }
        /*************if try code fails**************************/
        if ($success == false) {
            return redirect('dashboard');
        }

        Session::flash('message_display', 'Income is updated successfully');
        Session::flash('alert-type', 'success');

        return redirect()->to('incomes');
    }

    /**
     * Deactive the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */


    public function deactive($id)
    {

        $incomes = Incomes::findOrFail($id);

        /*************************try code**********************************************/

        DB::beginTransaction();
        try {

            if (!empty($incomes)) {
                $incomes->status = "Deactive";
                $incomes->save();
            }
            DB::commit();
            $success = true;

            /***********************End **try code**********************************************/
        } catch (\Exception $e) {
            /*************cache code**************************/
            $success = false;
            DB::rollback();
        }
        /*************if try code fails**************************/
        if ($success == false) {
            return redirect('dashboard');
        }

        Session::flash('message_display', 'Income is deactive successfully');
        Session::flash('alert-type', 'error');

        return redirect()->to('incomes');
    }

    /**
     * Active the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function active($id)
    {

        $incomes = Incomes::findOrFail($id);

        /*************************try code**********************************************/

        DB::beginTransaction();
        try {

            if (!empty($incomes)) {
                $incomes->status = "Active";
                $incomes->save();
            }
            DB::commit();
            $success = true;

            /***********************End **try code**********************************************/
        } catch (\Exception $e) {
            /*************cache code**************************/
            $success = false;
            DB::rollback();
        }
        /*************if try code fails**************************/
        if ($success == false) {
            return redirect('dashboard');
        }
        Session::flash('message_display', 'Income is active successfully');
        Session::flash('alert-type', 'success');
        return redirect()->to('incomes');
    }
}
