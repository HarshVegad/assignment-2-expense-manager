<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Role;
use Session;
use Illuminate\Validation\Rule;


class RoleController extends Controller
{
    public function role(Request $request)
    {

        if ($request->isMethod('POST')) {

            $request->validate([

                'employeerole' => 'required|max:255|unique:roles,employeerole'

            ]);

            $role = new Role();
            $role->employeerole = $request->employeerole;
            $role->description = !empty($request->description) ? $request->description : '';
            $role->status = 'Active';
            $role->save();

            Session::flash('message_display', 'Role is added successfully');
            Session::flash('alert-type', 'success');

            return redirect()->route('viewrole');
        }


        return view('role.addrole');
    }



    public function viewrole()
    {

        $roles = Role::all();


        return view('role.viewroles')->with(compact('roles'));
    }


    public function editrole($id, Request $request)
    {

        $role = Role::findOrFail($id);

        if ($request->isMethod('POST')) {

            $request->validate([

                'employeerole' => ['required', 'max:255', Rule::unique('roles')->ignore($id, 'roleid')]

            ]);

            $role->employeerole = $request->employeerole;
            $role->description = !empty($request->description) ? $request->description : '';
            $role->save();

            Session::flash('message_display', 'Role is updated successfully');
            Session::flash('alert-type', 'success');

            return redirect()->route('viewrole');
        }

        return view('role.editrole')->with(compact('role'));
    }


    public function activerole($id)
    {

        $role = Role::findOrFail($id);

        if (!empty($role)) {
            $role->status = "Active";
            $role->save();
        }

        Session::flash('message_display', 'Role is active successfully');
        Session::flash('alert-type', 'success');

        return redirect()->to('viewrole');
    }

    public function deactiverole($id)
    {

        $role = Role::findOrFail($id);

        if (!empty($role)) {
            $role->status = "Deactive";
            $role->save();
        }

        Session::flash('message_display', 'Role is deactive successfully');
        Session::flash('alert-type', 'error');

        return redirect()->to('viewrole');
    }
}
