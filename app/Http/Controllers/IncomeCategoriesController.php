<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\IncomeCategory;
use DB;
use Helper;
use Session;
use Illuminate\Validation\Rule;

class IncomeCategoriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $IncomeCategory = IncomeCategory::all();

        return view('admin.IncomeCategory.viewIncomecategory', compact('IncomeCategory'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.IncomeCategory.addIncomecategory');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());

        /*****************************Validation*****************************************/

        //$storeData =
        $request->validate([
            'name' =>  ['required', 'max:100', Rule::unique('income_categories')],
        ]);
        /*****************************End Validation*****************************************/


        /*************************try code**********************************************/

        DB::beginTransaction();
        try {
            $income_categories = new IncomeCategory();
            $income_categories->create($request->all());
            //   $student = IncomeCategory::create($storeData);


            DB::commit();
            $success = true;

            /***********************End **try code**********************************************/
        } catch (\Exception $e) {
            /*************cache code**************************/
            $success = false;
            DB::rollback();
        }
        /*************if try code fails**************************/
        if ($success == false) {
            return redirect('dashboard');
        }

        Session::flash('message_display', 'Income Category is added successfully');
        Session::flash('alert-type', 'success');

        return redirect()->to('income_categories');
    }



    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $income_categories = IncomeCategory::findOrFail($id);
        return view('admin.IncomeCategory.editIncomecategory', compact('income_categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        /*****************************Validation*****************************************/

        // $validatedData  =
        request()->validate([
            'name' => ['required', 'max:100', Rule::unique('income_categories')->ignore($id, 'id')],
        ]);

        /*****************************End Validation*****************************************/

        /*************************try code**********************************************/

        DB::beginTransaction();
        try {
            $income_category = IncomeCategory::findOrFail($id);
            $income_category->update($request->all());

            //   IncomeCategory::whereId($id)->update($validatedData);

            DB::commit();
            $success = true;

            /***********************End **try code**********************************************/
        } catch (\Exception $e) {
            /*************cache code**************************/
            $success = false;
            DB::rollback();
        }
        /*************if try code fails**************************/
        if ($success == false) {
            return redirect('dashboard');
        }

        Session::flash('message_display', 'Income Category is updated successfully');
        Session::flash('alert-type', 'success');

        return redirect()->to('income_categories');
    }

    /**
     * Deactive the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */


    public function deactive($id)
    {

        $income_categories = IncomeCategory::findOrFail($id);

        /*************************try code**********************************************/

        DB::beginTransaction();
        try {

            if (!empty($income_categories)) {
                $income_categories->status = "Deactive";
                $income_categories->save();
            }
            DB::commit();
            $success = true;

            /***********************End **try code**********************************************/
        } catch (\Exception $e) {
            /*************cache code**************************/
            $success = false;
            DB::rollback();
        }
        /*************if try code fails**************************/
        if ($success == false) {
            return redirect('dashboard');
        }

        Session::flash('message_display', 'Income Category is deactive successfully');
        Session::flash('alert-type', 'error');

        return redirect()->to('income_categories');
    }

    /**
     * Active the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function active($id)
    {

        $income_categories = IncomeCategory::findOrFail($id);

        /*************************try code**********************************************/

        DB::beginTransaction();
        try {

            if (!empty($income_categories)) {
                $income_categories->status = "Active";
                $income_categories->save();
            }
            DB::commit();
            $success = true;

            /***********************End **try code**********************************************/
        } catch (\Exception $e) {
            /*************cache code**************************/
            $success = false;
            DB::rollback();
        }
        /*************if try code fails**************************/
        if ($success == false) {
            return redirect('dashboard');
        }
        Session::flash('message_display', 'Income Category is active successfully');
        Session::flash('alert-type', 'success');
        return redirect()->to('income_categories');
    }
}
