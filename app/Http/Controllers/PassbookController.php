<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Expenses;
use App\ExpenseCategory;
use App\Bankmaster;
use App\Debitdetails;
use App\Creditedetails;
use App\Incomes;
use App\IncomeCategory;
use Helper;
use App\Manage_account;
use Session;
use Illuminate\Validation\Rule;
use Maatwebsite\Excel\Concerns\FromCollection;
use App\Exports\UsersExport;
use Maatwebsite\Excel\Facades\Excel;
//use Excel;

class PassbookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $method = $request->method();


        $bankmaster = Bankmaster::get()->all();
        $year = '';
        $oldamountofbank = '';
        $month = '';
        $actionbyid = Session::get('user_id');

        DB::enableQueryLog();


        $Manage_account = Manage_account::with([
            'expense_category' => function ($query) {
                $query->select('id', 'name');
            }, 'income_category' => function ($query) {
                $query->select('id', 'name');
            }, 'bankmaster' => function ($query) {
                $query->select('bankid', 'bankname');
            }
        ])->where('created_by_id', $actionbyid);



        if ($request->isMethod('post')) {

            $year = $request->year;
            $month = $request->month;
            $bankmaster2 = $request->get('bankmaster');

            $startdate = $year . '-' . $month . '-' . '01';

            $lastdate = $year . '-' . $month . '-' . '31';



            if ($month != "") {
                $Manage_account->whereBetween('manage_account.entry_date', [$startdate, $lastdate]);
            }

            if ($bankmaster2 != "") {
                $Manage_account->where('manage_account.account_id', $bankmaster2);
            }



            $creditede = Manage_account::where('account_id', $bankmaster2)->Where('entry_date', '>=', $startdate)->Where('entry_date', '<=', $lastdate)->where('paymenttype', 'Credit')->select(DB::raw('SUM(amount) as amount'))->get();
            $debitde =  Manage_account::where('account_id', $bankmaster2)->Where('entry_date', '>=', $startdate)->Where('entry_date', '<=', $lastdate)->where('paymenttype', 'Debit')->select(DB::raw('SUM(amount) as amount'))->get();

            $totalcredit = $creditede[0]->amount;

            $totaldebit = $debitde[0]->amount;

            $oldamountofbank =   $totalcredit - $totaldebit;
        }


        $Manage_account =   $Manage_account->orderBy('manage_account.entry_date', 'desc')->get();

        if ($request->excel == 1) {
            ob_end_clean();
            ob_start();
            return    Excel::download(new UsersExport($Manage_account), 'passbook.xlsx');
        }


        return view('admin.Passbook.viewpassbook', compact('bankmaster', 'Manage_account', 'oldamountofbank', 'month', 'year'));
    }

    public function expensereport(Request $request)
    {
        ob_end_clean();
        ob_start();

        return Excel::download(new UsersExport, 'users.xlsx');
    }
}
