<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Debitdetails extends Model
{
    protected $table = 'debitdetails';
    protected $primaryKey = 'id';

    protected $guarded = [];
}
