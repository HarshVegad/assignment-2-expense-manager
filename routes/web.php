
<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('email-test', function () {

    $details['email'] = 'harshv@zignuts.com';

    dispatch(new App\Jobs\SendEmailJob($details));

    dd('done');
});

/*********************************Dashboard*****************************************/

Route::any('dashboard', 'AdminController@dashboard');

/********************************Authentication Routes****************************************/
Route::any('/', 'AdminController@adminlogin');
Route::any('check', 'AdminController@check');
Route::any('loginprocess', 'AdminController@loginprocess')->name('loginprocess');
Route::group(['middleware' => 'Islogin'], function () {
    Route::any('logout', 'AdminController@logout')->name('logout');
    /************************************************************************************************



           /*********************************Admin Role Routes*****************************************/

    Route::any('/role', 'RoleController@role')->name('role');
    Route::any('/viewrole', 'RoleController@viewrole')->name('viewrole');
    Route::any('/editrole/{id}', 'RoleController@editrole')->name('editrole');
    Route::any('/deactiverole/{id}', 'RoleController@deactiverole')->name('deactiverole');
    Route::any('/activerole/{id}', 'RoleController@activerole')->name('activerole');
    /**********************************End**Admin Role Routes********************************************/

    /*********************************Admin users Routes*****************************************/

    Route::any('users', 'UsersController@index');
    Route::any('adduser', 'UsersController@adduser');
    Route::any('edituser/{id}', 'UsersController@edituser');
    Route::any('deactiveuser/{id}', 'UsersController@deactiveuser');
    Route::any('activeuser/{id}', 'UsersController@activeuser');
    /**********************************End**Admin users Routes********************************************



        /*********************************expense_categories*****************************************/

    Route::get('/expense_categories', 'ExpenseCategoriesController@index')->name('expense_categories');
    Route::post('/expense_categories', 'ExpenseCategoriesController@store')->name('storeexpense_categories');
    Route::get('/expense_categories/create', 'ExpenseCategoriesController@create')->name('createexpense_categories');
    Route::get('/expense_categories/{id}/edit', 'ExpenseCategoriesController@edit')->name('editexpense_categories');
    Route::patch('/expense_categories/{id}', 'ExpenseCategoriesController@update')->name('updateexpense_categories');
    Route::get('/expense_categories/deactive/{id}', 'ExpenseCategoriesController@deactive')->name('deactiveexpense_categories');
    Route::get('/expense_categories/active/{id}', 'ExpenseCategoriesController@active')->name('activeexpense_categories');
    /**********************************End**expense_categories********************************************/

    /*********************************income_categories*****************************************/

    Route::get('/income_categories', 'IncomeCategoriesController@index')->name('income_categories');
    Route::post('/income_categories', 'IncomeCategoriesController@store')->name('storeincome_categories');
    Route::get('/income_categories/create', 'IncomeCategoriesController@create')->name('createincome_categories');
    Route::get('/income_categories/{id}/edit', 'IncomeCategoriesController@edit')->name('editincome_categories');
    Route::patch('/income_categories/{id}', 'IncomeCategoriesController@update')->name('updateincome_categories');
    Route::get('/income_categories/deactive/{id}', 'IncomeCategoriesController@deactive')->name('deactiveincome_categories');
    Route::get('/income_categories/active/{id}', 'IncomeCategoriesController@active')->name('activeincome_categories');
    /**********************************End**income_categories********************************************/

    /*********************************Generate Income****************************************/

    Route::get('/incomes', 'IncomesController@index')->name('incomes');
    Route::post('/incomes', 'IncomesController@store')->name('storeincomes');
    Route::get('/incomes/create', 'IncomesController@create')->name('createincomes');
    Route::get('/incomes/{id}/edit', 'IncomesController@edit')->name('editincomes');
    Route::patch('/incomes/{id}', 'IncomesController@update')->name('updateincomes');
    Route::get('/incomes/deactive/{id}', 'IncomesController@deactive')->name('deactiveincomes');
    Route::get('/incomes/active/{id}', 'IncomesController@active')->name('activeincomes');
    /**********************************End**Generate Income********************************************/

    /*********************************Generate Expense****************************************/

    Route::get('/expenses', 'ExpensesController@index')->name('expenses');
    Route::post('/expenses', 'ExpensesController@store')->name('storeexpenses');
    Route::get('/expenses/create', 'ExpensesController@create')->name('createexpenses');
    Route::get('/expenses/{id}/edit', 'ExpensesController@edit')->name('editexpenses');
    Route::patch('/expenses/{id}', 'ExpensesController@update')->name('updateexpenses');
    Route::get('/expenses/deactive/{id}', 'ExpensesController@deactive')->name('deactiveexpenses');
    Route::get('/expenses/active/{id}', 'ExpensesController@active')->name('activeexpenses');
    /**********************************End**Generate Expense********************************************/

    /*********************************Bank****************************************/

    Route::get('/bank', 'BankController@index')->name('bank');
    Route::post('/bank', 'BankController@store')->name('storebank');
    Route::get('/bank/create', 'BankController@create')->name('createbank');
    Route::get('/bank/{id}/edit', 'BankController@edit')->name('editbank');
    Route::patch('/bank/{id}', 'BankController@update')->name('updatebank');
    Route::get('/bank/deactive/{id}', 'BankController@deactive')->name('deactivebank');
    Route::get('/bank/active/{id}', 'BankController@active')->name('activebank');
    /**********************************End**Bank********************************************/


    /*********************************Monthly Report****************************************/

    Route::any('/monthlyreport', 'MonthlyReportsController@index')->name('monthlyreport');

    /**********************************End**Monthly Report********************************************/


     /*********************************Passbook Report****************************************/

    Route::any('/passbook', 'PassbookController@index')->name('passbook');
    Route::any('/passbooksearch', 'PassbookController@index')->name('passbooksearch');
    Route::get('/expensereport', 'PassbookController@expensereport')->name('expensereport');

    /**********************************End**Passbook Report********************************************/
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
