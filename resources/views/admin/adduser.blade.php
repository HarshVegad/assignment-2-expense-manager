@extends('layout.admin_design')

@section('content')
<style type="text/css">
	.help-block{
		color:red;
	}
	.select2{
   		width: 100% !important;
    }
	.select2-container--default .select2-selection--single{
		border-radius: 0px !important;
		max-height: 100% !important;
		height: 36px !important;
		border-color: #d2d6de !important;
		max-width: 100%;
		min-width: 100% !important;
	}
   

</style> 
<div class="content-wrapper">
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="{{url('dashboard')}}">Home</a></li>
            <li class="breadcrumb-item active">User</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>
  <section class="content">
    <div class="row">
      <div class="col-12">
        @if ($message = Session::get('message'))
          <div class="alert alert-success alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button> 
            <strong>{{ $message }}</strong>
          </div>
        @endif 
        <div class="col-md-12">
            <!-- jquery validation -->
            <div class="card card-secondary">
              <div class="card-header">
                <h3 class="card-title">Add User</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <div class="card-body">
				<form action="{{url('adduser')}}" role="form" method="post" class="form-horizontal">
					{{csrf_field()}}
					<div class="form-group">
						<label for="email" class="col-sm-4 control-label">Email<span style="color: red;">*</span></label>
						<div class="col-sm-8">
							<input type="email" class="form-control" value="{{ old('email') }}" placeholder="Email" id="email" name="email" required>
							@if($errors->has('email'))
								<span class="help-block">
								<strong>{{ $errors->first('email') }}</strong>
								</span> @endif

						</div>
					</div>
					<div class="form-group">
						<label for="role" class="col-sm-4 control-label">Role<span style="color: red;">*</span></label>
						<div class="col-sm-8">
							<select class="form-control select2" name="role" id="role" required="">
								@if(!empty($roles))
									<option value="">--Please Select Role--</option>
									@foreach($roles as $role)
										<option value="{{ $role->roleid }}" @if($role->roleid == old('role')) selected="" @endif>{{ $role->employeerole }}</option>
									@endforeach
								@else
									<option value="">--No Roles Available--</option>
								@endif
							
							</select>
						</div>
					</div>
					<div class="form-group">
						<label for="firstname" class="col-sm-4 control-label">First Name<span style="color: red;">*</span></label>
						<div class="col-sm-8">
							<input type="text" class="form-control"  value="{{ old('firstname') }}" placeholder="First Name" id="firstname" name="firstname" required>

								@if($errors->has('firstname'))
								<span class="help-block">
								<strong>{{ $errors->first('firstname') }}</strong>
								</span> @endif
						</div>
					</div>
					<div class="form-group">
						<label for="lastname" class="col-sm-4 control-label">Last Name<span style="color: red;">*</span></label>
						<div class="col-sm-8">
							<input type="text" class="form-control" value="{{ old('lastname') }}"  placeholder="Last Name" id="lastname" name="lastname" required>
								@if($errors->has('lastname'))
								<span class="help-block">
								<strong>{{ $errors->first('lastname') }}</strong>
								</span> @endif
						</div>
					</div>
					<div class="form-group">
						<label for="mobile" class="col-sm-4 control-label">Mobile<span style="color: red;">*</span></label>
						<div class="col-sm-8">
							<input type="text" class="form-control"  value="{{ old('mobile') }}" placeholder="Mobile" pattern="^([0-9]){10}$" maxlength="10" id="mobile" name="mobile">
							@if($errors->has('mobile'))
								<span class="help-block">
								<strong>{{ $errors->first('mobile') }}</strong>
								</span> @endif
						</div>
					</div>

					<div class="form-group">
						<label for="mobile" class="col-sm-4 control-label">Location<span style="color: red;">*</span></label>
						<div class="col-sm-8">
							<select class="form-control select2" name="location" data-live-search="true" required="">
								@if(!empty($location))
									<option value="">--Select Location--</option>
									@foreach($location as $loc)
										<option value="{{ $loc->id }}" @if(old('location') == $loc->id) selected="" @endif>{{ ucfirst($loc->locationname) }}</option>
									@endforeach
								@else
									<option value="">--No location Found--</option>
								@endif
								
							</select>
							@if($errors->has('location'))
								<span class="help-block">
								<strong>{{ $errors->first('location') }}</strong>
								</span> @endif
						</div>
					</div>

					<div class="form-group">
						<label for="mobile" class="col-sm-4 control-label">Password<span style="color: red;">*</span></label>
						<div class="col-sm-8">
							<input type="password" class="form-control"  value="{{ old('password') }}" placeholder="Password"  id="password" name="password" maxlength="60" required="">
							@if($errors->has('password'))
								<span class="help-block">
								<strong>{{ $errors->first('password') }}</strong>
								</span> @endif
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-offset-6 col-sm-6">
							<button name="submit" type="submit" class="btn btn-primary"> Save User</button>
								<a href="{{ url('users') }}" type="button" class="btn btn-danger">Cancel</a>
						</div>
					</div>
				</form>
              </div>    
            </div>
        </div>
      </div>
    </div>
  </section>
</div>

     


@endsection