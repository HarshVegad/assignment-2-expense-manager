@extends('layout.admin_design')
@section('content')

<style type="text/css">
  .help-block {
    color: red;
  }
</style>
<div class="content-wrapper">
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6"></div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item">
              <a href="{{url('dashboard')}}">Home</a>
            </li>
            <li class="breadcrumb-item active">Expense</li>
          </ol>
        </div>
      </div>
    </div>

    <!-- /.container-fluid -->

  </section>

  <!-- Content Header (Page header) -->

  <section class="content">
    <div class="row">
      <div class="col-12">

        <!-- Error Display -->

        @if ($message = Session::get('message'))

        <div class="alert alert-success alert-block">
          <button type="button" class="close" data-dismiss="alert">×</button>
          <strong>{{ $message }}</strong>
        </div>
        @endif

        <!-- End Error Display -->

        <div class="col-md-12">
          <div class="card card-secondary">
            <div class="card-header">
              <h3 class="card-title">Add Expense</h3>
            </div>

            <!-- /.card-header -->

            <!-- form start -->

            <div class="card-body">
              <form action="{{route('storeexpenses')}}" role="form" method="post" class="form-horizontal">
                {{csrf_field()}}


                <div class="form-group">
                  <label for="name" class="col-sm-4 control-label">Expense Category
                    <span style="color: red;">*</span>
                  </label>
                  <div class="col-sm-4">
                    <select name="expense_category_id" class="form-control select2 " style="width: 100%;" data-select2-id="1" tabindex="-1" aria-hidden="true">
                      @foreach ($ExpenseCategory as $category)
                      <option value="{{ $category->id }}">{{ $category->name }}</option>
                      @endforeach
                    </select>
                    @if($errors->has('name'))

                    <span class="help-block">
                      <strong>{{ $errors->first('name') }}</strong>
                    </span> @endif

                  </div>
                </div>


                <div class="form-group">
                  <label for="name" class="col-sm-4 control-label">Entry date
                    <span style="color: red;">*</span>
                  </label>
                  <div class="col-sm-4">
                    <input type="date" class="form-control" name="entry_date" id="entry_date" placeholder="
                       <?php echo date("d-m-Y"); ?>" value="{{date('Y-m-d')}}">

                    @if($errors->has('entry_date'))

                    <span class="help-block">
                      <strong>{{ $errors->first('entry_date') }}</strong>
                    </span> @endif

                  </div>
                </div>



                <div class="form-group">
                  <label for="name" class="col-sm-4 control-label">Select Account
                    <span style="color: red;">*</span>
                  </label>
                  <div class="col-sm-4">
                    <select name="account_id" class="form-control select2 " style="width: 100%;" data-select2-id="1" tabindex="-1" aria-hidden="true">
                      @foreach ($bankmaster as $category)
                      <option value="{{ $category->bankid }}">{{ $category->bankname }}</option>
                      @endforeach
                    </select>
                    @if($errors->has('bankname'))

                    <span class="help-block">
                      <strong>{{ $errors->first('bankname') }}</strong>
                    </span> @endif

                  </div>
                </div>

                <div class="form-group">
                  <label for="name" class="col-sm-4 control-label">Amount
                    <span style="color: red;">*</span>
                  </label>
                  <div class="col-sm-4">
                    <input type="text" class="form-control" value="{{ old('amount') }}" placeholder="Amount" id="amount" name="amount" required>
                    @if($errors->has('amount'))

                    <span class="help-block">
                      <strong>{{ $errors->first('amount') }}</strong>
                    </span> @endif

                  </div>
                </div>


                <div class="form-group">
                  <label for="name" class="col-sm-4 control-label">Remarks
                  </label>
                  <div class="col-sm-4">

                    <textarea name="remark" class="form-control" placeholder="Remarks"></textarea>
                    @if($errors->has('remark'))
                    <span class="help-block">
                      <strong>{{ $errors->first('remark') }}</strong>
                    </span>
                    @endif
                  </div>
                </div>

                <div class="form-group">
                  <div class="col-sm-offset-6 col-sm-6">
                    <button name="submit" type="submit" class="btn btn-primary"> Save </button>
                    <a href="{{ route('expenses')}}" class="btn btn-warning">Back</a>
                  </div>
                </div>

                <!-- /.card-body -->

              </form>
            </div>

            <!-- /.card -->

          </div>

          <!-- End form  -->

        </div>
      </div>
    </div>
  </section>
</div>




@endsection