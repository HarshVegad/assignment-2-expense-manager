@extends('layout.admin_design')

@section('content')


<div class="content-wrapper">
  <section class="content-header">

    <!-- /start - container-fluid -->

    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6"></div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item">
              <a href="{{url('dashboard')}}">Home</a>
            </li>
            <li class="breadcrumb-item active">Expense Category</li>
          </ol>
        </div>
      </div>
    </div>

    <!-- /End - container-fluid -->

  </section>
  <section class="content">
    <div class="row">
      <div class="col-12">
        <div class="col-md-12">

          <!-- Error Display -->

          @if ($message = Session::get('message'))

          <div class="alert alert-success alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <strong>{{ $message }}</strong>
          </div>
          @endif


          <!-- End Error Display -->

          <div class="card card-secondary">
            <div class="card-header">
              <h3 class="card-title">Edit Expense Category</h3>
            </div>

            <!-- Start - Get Value From Databse  -->

            @php
            $name = !empty($expense_categories->name) ? $expense_categories->name : old('name');

            @endphp


            <!--  / End - Get Value From Databse -->

            <!-- form start -->

            <div class="card-body">
              <form data-toggle="validator" action="{{ route('updateexpense_categories',$expense_categories->id) }}" role="form" method="POST" class="form-horizontal">
                {{ csrf_field() }}
                {{ method_field('PATCH')}}


                <div class="form-group">
                  <label for="unitname" class="col-sm-4 control-label">Expense Category Name

                    <span style="color: red;">*</span>
                  </label>
                  <div class="col-sm-8">
                    <input type="text" class="form-control" value="{{ $name }}" placeholder="Enter Expense Category Name" id="name" name="name" required maxlength="255">
                    @if($errors->has('name'))


                    <span class="help-block">
                      <strong>{{ $errors->first('name') }}</strong>
                    </span>
                    @endif


                  </div>
                </div>
                <div class="form-group">
                  <div class="col-sm-offset-4 col-sm-8">
                    <button name="submit" type="submit" class="btn btn-primary">
                      <span class="glyphicon glyphicon-ok" aria-hidden="true"></span> Update

                    </button>
                    <a href="{{ route('expense_categories')}}" class="btn btn-warning">Back</a>
                  </div>
                </div>
              </form>
            </div>
          </div>

          <!-- End form  -->

        </div>
      </div>
  </section>
</div>




@endsection