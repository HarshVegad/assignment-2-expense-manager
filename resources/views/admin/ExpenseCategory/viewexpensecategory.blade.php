@extends('layout.admin_design')

@section('content')
<style type="text/css">
  .red {
    color: red;
    text-decoration-color: red;
  }

  .select2 {
    width: 100% !important;

  }

  .select2-container--default .select2-selection--single {
    border-radius: 2px !important;
    max-height: 100% !important;
    border-color: #d2d6de !important;
    height: 32px;
    max-width: 100%;
    min-width: 100% !important;
  }
</style>
<div class="content-wrapper">
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">

        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="{{url('dashboard')}}">Home</a></li>
            <li class="breadcrumb-item active">Expense Category</li>
          </ol>
        </div>
      </div>
    </div>

    <!-- /.container-fluid -->

  </section>
  <section class="content">
    <div class="row">
      <div class="col-12">

        <!-- Error Display -->

        @if ($message = Session::get('message'))
        <div class="alert alert-success alert-block">
          <button type="button" class="close" data-dismiss="alert">×</button>
          <strong>{{ $message }}</strong>
        </div>
        @endif
        <!-- End Error Display -->

        <div class="card">
          <div class="card-header">
            <h3 class="card-title">All Expense Category</h3>
            <div class="card-tools">
              <a href="{{route('createexpense_categories')}}" class="btn btn-default">Add New</a>
            </div>
          </div>
          <div class="card-body">
            <table id="example1" class="table table-bordered table-striped table-hover">
              <thead>
                <tr>

                  <th>Expense Category</th>
                  <th>Status</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                @foreach($ExpenseCategory as $expensecategory)
                <tr {{ $expensecategory->status == 'Deactive' ? "class=red" : ""}}>
                  <td>{{$expensecategory->name }}</td>
                  <td>{{ucfirst($expensecategory->status)}}</td>

                  <td>
                    <a href="{{ route('editexpense_categories', $expensecategory->id) }}" title="Edit"><i class="fa fa-edit" style="color : #f39c12;"></i></a>
                    @if($expensecategory->status == 'Active')
                    <a href="{{ route('deactiveexpense_categories', $expensecategory->id) }}" title="Deactive"><i class="fas fa-times" style="color: red;"></i></a></td>
                  @else
                  <a href="{{ route('activeexpense_categories', $expensecategory->id) }}" title="Active"><i class="fa fa-check" style="color: green;"></i></a></td>
                  @endif
                </tr>
                @endforeach
              </tbody>
              <tfoot>
                <tr>

                  <th>Expense Category</th>
                  <th>Status</th>
                  <th>Action</th>
                </tr>
              </tfoot>
            </table>
          </div>

          <!-- /.card-body -->

        </div>

        <!-- /.card -->

      </div>

      <!-- /.col -->

    </div>

    <!-- /.row -->

  </section>
</div>

<script type="text/javascript">
  $(function() {
    //Initialize Select2 Elements
    $('.select2').select2()

    //Datemask dd/mm/yyyy
    $('#datemask').inputmask('dd/mm/yyyy', {
      'placeholder': 'dd/mm/yyyy'
    })
    //Datemask2 mm/dd/yyyy
    $('#datemask2').inputmask('mm/dd/yyyy', {
      'placeholder': 'mm/dd/yyyy'
    })
    //Money Euro
    $('[data-mask]').inputmask()

    //Date range picker
    $('#reservation').daterangepicker()
    //Date range picker with time picker
    $('#reservationtime').daterangepicker({
      timePicker: true,
      timePickerIncrement: 30,
      format: 'MM/DD/YYYY h:mm A'
    })
    //Date range as a button
    $('#daterange-btn').daterangepicker({
        ranges: {
          'Today': [moment(), moment()],
          'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
          'Last 7 Days': [moment().subtract(6, 'days'), moment()],
          'Last 30 Days': [moment().subtract(29, 'days'), moment()],
          'This Month': [moment().startOf('month'), moment().endOf('month')],
          'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        startDate: moment().subtract(29, 'days'),
        endDate: moment()
      },
      function(start, end) {
        $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
      }
    )

    //Date picker
    $('#datepicker').datepicker({
      autoclose: true
    })

    //iCheck for checkbox and radio inputs
    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
      checkboxClass: 'icheckbox_minimal-blue',
      radioClass: 'iradio_minimal-blue'
    })
    //Red color scheme for iCheck
    $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
      checkboxClass: 'icheckbox_minimal-red',
      radioClass: 'iradio_minimal-red'
    })
    //Flat red color scheme for iCheck
    $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
      checkboxClass: 'icheckbox_flat-green',
      radioClass: 'iradio_flat-green'
    })

    //Colorpicker
    $('.my-colorpicker1').colorpicker()
    //color picker with addon
    $('.my-colorpicker2').colorpicker()

    //Timepicker
    $('.timepicker').timepicker({
      showInputs: false
    })
  })
</script>
<script type="text/javascript">
  $("#mode").select2({
    placeholder: "Select a Mode"
  });
  $(document).ready(function() {
    $('#example1').DataTable();
  });
</script>
@endsection