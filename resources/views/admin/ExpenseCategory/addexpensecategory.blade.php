@extends('layout.admin_design')
@section('content')

<style type="text/css">
  .help-block {
    color: red;
  }
</style>
<div class="content-wrapper">
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6"></div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item">
              <a href="{{url('dashboard')}}">Home</a>
            </li>
            <li class="breadcrumb-item active">Expense Category</li>
          </ol>
        </div>
      </div>
    </div>

    <!-- /.container-fluid -->

  </section>

  <!-- Content Header (Page header) -->

  <section class="content">
    <div class="row">
      <div class="col-12">

        <!-- Error Display -->

        @if ($message = Session::get('message'))

        <div class="alert alert-success alert-block">
          <button type="button" class="close" data-dismiss="alert">×</button>
          <strong>{{ $message }}</strong>
        </div>
        @endif

        <!-- End Error Display -->

        <div class="col-md-12">
          <div class="card card-secondary">
            <div class="card-header">
              <h3 class="card-title">Add Expense Category</h3>
            </div>

            <!-- /.card-header -->

            <!-- form start -->

            <div class="card-body">
              <form action="{{route('storeexpense_categories')}}" role="form" method="post" class="form-horizontal">
                {{csrf_field()}}


                <div class="form-group">
                  <label for="name" class="col-sm-4 control-label">Expense Category Name
                    <span style="color: red;">*</span>
                  </label>
                  <div class="col-sm-8">
                    <input type="text" class="form-control" value="{{ old('name') }}" placeholder="Expense Category Name" id="name" name="name" required>
                    @if($errors->has('name'))

                    <span class="help-block">
                      <strong>{{ $errors->first('name') }}</strong>
                    </span> @endif

                  </div>
                </div>
                <div class="form-group">
                  <div class="col-sm-offset-6 col-sm-6">
                    <button name="submit" type="submit" class="btn btn-primary"> Save </button>
                    <a href="{{ route('expense_categories')}}" class="btn btn-warning">Back</a>
                  </div>
                </div>

                <!-- /.card-body -->

              </form>
            </div>

            <!-- /.card -->

          </div>

          <!-- End form  -->

        </div>
      </div>
    </div>
  </section>
</div>




@endsection