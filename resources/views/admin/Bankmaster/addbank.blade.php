@extends('layout.admin_design')
@section('content')

<style type="text/css">
  .help-block {
    color: red;
  }
</style>
<div class="content-wrapper">
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6"></div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item">
              <a href="{{url('dashboard')}}">Home</a>
            </li>
            <li class="breadcrumb-item active">Bank Details</li>
          </ol>
        </div>
      </div>
    </div>

    <!-- /.container-fluid -->
  </section>

  <!-- Content Header (Page header) -->

  <section class="content">
    <div class="row">
      <div class="col-12">

        <!-- Error Display -->

        @if ($message = Session::get('message'))

        <div class="alert alert-success alert-block">
          <button type="button" class="close" data-dismiss="alert">×</button>
          <strong>{{ $message }}</strong>
        </div>
        @endif

        <!-- End Error Display -->

        <div class="col-md-12">
          <div class="card card-secondary">
            <div class="card-header">
              <h3 class="card-title">Add Bank Details</h3>
            </div>

            <!-- /.card-header -->

            <!-- form start -->

            <div class="card-body">
              <form action="{{route('storebank')}}" role="form" method="post" class="form-horizontal">
                {{csrf_field()}}

                <div class="form-group">
                  <label>Account No</label>
                  <input type="text" name="accountNo" value="{{ old('accountNo') }}" class="form-control number " autocomplete="off" placeholder="account no" class="span11" maxlength="16" />
                  @if($errors->has('accountNo'))

                  <span class="help-block">
                    <strong>{{ $errors->first('accountNo') }}</strong>
                  </span>
                  @endif

                </div>
                <div class="form-group">
                  <label>Account Name</label>
                  <input type="text" name="accountName" value="{{ old('accountName') }}" class="form-control  " autocomplete="off" placeholder="account name" class="span11" maxlength="16" />
                  @if($errors->has('accountName'))

                  <span class="help-block">
                    <strong>{{ $errors->first('accountName') }}</strong>
                  </span>
                  @endif

                </div>
                <div class="form-group">
                  <label>Bank Name</label>
                  <input type="text" name="BankName" value="{{ old('BankName') }}" class="form-control " autocomplete="off" placeholder="bank name" class="span11" maxlength="16" />
                  @if($errors->has('BankName'))

                  <span class="help-block">
                    <strong>{{ $errors->first('BankName') }}</strong>
                  </span>
                  @endif

                </div>
                <div class="form-group">
                  <label>Branch Name</label>
                  <input type="text" name="BranchName" value="{{ old('BranchName') }}" class="form-control" autocomplete="off" placeholder="branch name" maxlength="20" class="span11" />
                  @if($errors->has('BranchName'))

                  <span class="help-block">
                    <strong>{{ $errors->first('BranchName') }}</strong>
                  </span>
                  @endif

                </div>
                <div class="form-group">
                  <label>IFSC Code</label>
                  <input type="text" name="IFSCcode" value="{{ old('IFSCcode') }}" class="form-control" autocomplete="off" placeholder="IFSC Code" class="span11" maxlength="25" />
                  @if($errors->has('IFSCcode'))

                  <span class="help-block">
                    <strong>{{ $errors->first('IFSCcode') }}</strong>
                  </span>
                  @endif

                </div>

                <div class="form-group">
                  <label>Branch Code</label>
                  <input type="text" name="BranchCode" value="{{ old('BranchCode') }}" class="form-control number" autocomplete="off" maxlength="20" placeholder="branch code" class="span11" />
                  @if($errors->has('BranchCode'))

                  <span class="help-block">
                    <strong>{{ $errors->first('BranchCode') }}</strong>
                  </span>
                  @endif

                </div>
                <div class="form-group">
                  <div class="col-sm-offset-6 col-sm-6">
                    <button name="submit" type="submit" class="btn btn-primary"> Save </button>
                    <a href="{{ route('bank')}}" class="btn btn-warning">Back</a>
                  </div>
                </div>

                <!-- /.card-body -->

              </form>

            </div>

            <!-- /.card -->

          </div>

          <!-- End form  -->

        </div>
      </div>
    </div>
  </section>
</div>




@endsection