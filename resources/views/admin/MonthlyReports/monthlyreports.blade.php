@extends('layout.admin_design')
@section('content')

<style type="text/css">
  .help-block {
    color: red;
  }
</style>
<div class="content-wrapper">
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6"></div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item">
              <a href="{{url('dashboard')}}">Home</a>
            </li>
            <li class="breadcrumb-item active">Monthly Report</li>
          </ol>
        </div>
      </div>
    </div>
    <!-- /.container-fluid -->
  </section>
  <!-- Content Header (Page header) -->
  <section class="content">
    <div class="row">
      <div class="col-12">
        <!-- Error Display -->

        @if ($message = Session::get('message'))

        <div class="alert alert-success alert-block">
          <button type="button" class="close" data-dismiss="alert">×</button>
          <strong>{{ $message }}</strong>
        </div>
        @endif

        <!-- End Error Display -->
        <div class="col-md-12">
          <div class="card card-secondary">
            <div class="card-header">
              <h3 class="card-title">Monthly Report</h3>
            </div>
            <!-- /.card-header -->
            <!-- form start -->
            <div class="card-body">
              <form role="form" action="{{ route('monthlyreport') }}" method="post"> @csrf

                <div class="form-group row">
                  <div class="col-md-3">

                    <select class="form-control span11 select2" title="Select Year" data-live-search="true" data-selected-text-format="count" data-actions-box="true" data-header="Select Year" required="" id="year" name="year" data-sear value="{{ $year }}">
                      <option value="">Select year</option>
                      @for($i = 2019; $i<=2030; $i++) <option value="{{ $i }}" @if($i==$year) selected="" @endif>{{ $i }}</option>
                        @endfor
                    </select>
                  </div>
                  <div class="col-md-3">


                    <select class="form-control span11 select2" title="Select Month" dadta-live-search="true" data-selected-text-format="count" data-actions-box="true" data-header="Select Month" required="" name="month" id="month" placeholder="Month">
                      <option value="">Select Month</option>
                      <option value='01' @if($month=='01' ) selected="" @endif>Janaury</option>
                      <option value='02' @if($month=='02' ) selected="" @endif>February</option>
                      <option value='03' @if($month=='03' ) selected="" @endif>March</option>
                      <option value='04' @if($month=='04' ) selected="" @endif>April</option>
                      <option value='05' @if($month=='05' ) selected="" @endif>May</option>
                      <option value='06' @if($month=='06' ) selected="" @endif>June</option>
                      <option value='07' @if($month=='07' ) selected="" @endif>July</option>
                      <option value='08' @if($month=='08' ) selected="" @endif>August</option>
                      <option value='09' @if($month=='09' ) selected="" @endif>September</option>
                      <option value='10' @if($month=='10' ) selected="" @endif>October</option>
                      <option value='11' @if($month=='11' ) selected="" @endif>November</option>
                      <option value='12' @if($month=='12' ) selected="" @endif>December</option>
                    </select>





                    @if($errors->has('year'))

                    <span class="help-block">
                      <strong>{{ $errors->first('year') }}</strong>
                    </span>
                    @endif

                  </div>
                  <div class="col-md-3">

                    <button type="submit" class="btn btn-primary bg-green">Select Month</button>
                  </div>

                </div>

                <div class="form-group row">
                  <div class="col-md-3 ">
                    <label for="productid" class="control-label">Total Report :-
                    </label>
                  </div>
                </div>


                <div class="form-group row">


                  <div class="col-md-3">


                    <label>Total Income</label>

                    <input type="text" class="form-control" readonly="" value="{{$incomes[0]->amount}}" name="totalamount">

                  </div>
                  <div class="col-md-3">

                    <label>Total Expense</label>

                    <input type="text" class="form-control" readonly="" value="{{$expenses[0]->amount}}" name="totalamount">

                  </div>

                  @if(($incomes[0]->amount) > ($expenses[0]->amount))
                  <div class="col-md-3">

                    @php

                    $totalincome= $incomes[0]->amount;

                    $totalexpense = $expenses[0]->amount;

                    $totalprofit = $totalincome-$totalexpense;


                    @endphp

                    <label>Total Profit</label>

                    <input type="text" class="form-control" readonly="" value="{{ $totalprofit }}" name="totalamount">

                  </div>@endif

                  @if(($incomes[0]->amount) < ($expenses[0]->amount))
                    <div class="col-md-3">

                      @php

                      $totalincome= $incomes[0]->amount;

                      $totalexpense = $expenses[0]->amount;

                      $totalprofit = $totalexpense-$totalincome;


                      @endphp

                      <label>Total Lose</label>

                      <input type="text" class="form-control" readonly="" value="{{ $totalprofit }}" name="totalamount">

                    </div>@endif
                </div>

                <div class="form-group row">
                  <div class="col-md-3 ">
                    <label for="productid" class="control-label">Monthly Report :-
                    </label>
                  </div>
                </div>


                <div class="form-group row">
                  <div class="col-md-3">

                    <label>Monthly Income</label>

                    <input type="text" class="form-control" readonly="" value="{{$monthlyincome[0]->amount == null ? '0' : $monthlyincome[0]->amount}}" name="totalamount">

                  </div>
                  <div class="col-md-3">

                    <label>Monthly Expense</label>

                    <input type="text" class="form-control" readonly="" value="{{$monthlyexpense[0]->amount == null ? '0' : $monthlyexpense[0]->amount}}" name="totalamount">

                  </div>

                  @if(($monthlyincome[0]->amount) > ($monthlyexpense[0]->amount))
                  <div class="col-md-3">

                    @php

                    $totalincome= $monthlyincome[0]->amount;

                    $totalexpense = $monthlyexpense[0]->amount;

                    $totalprofit = $totalincome-$totalexpense;


                    @endphp

                    <label>Monthly Profit</label>

                    <input type="text" class="form-control" readonly="" value="{{ $totalprofit }}" name="totalamount">

                  </div>@endif

                  @if(($monthlyincome[0]->amount) < ($monthlyexpense[0]->amount))
                    <div class="col-md-3">

                      @php

                      $totalincome= $monthlyincome[0]->amount;

                      $totalexpense = $monthlyexpense[0]->amount;

                      $totalprofit = $totalexpense-$totalincome;


                      @endphp

                      <label>Monthly Lose</label>

                      <input type="text" class="form-control" readonly="" value="{{ $totalprofit }}" name="totalamount">

                    </div>@endif
                </div>


              </form>
            </div>
            <!-- /.card -->
          </div>
          <!-- End form  -->
        </div>
      </div>
    </div>
  </section>
</div>


<script type="text/javascript">
  $(function() {

    var d = new Date(),

      y = d.getFullYear();

    $('#year option[value="' + y + '"]').prop('selected', true);


    $('.select2').select2()

    //Datemask dd/mm/yyyy

  })
</script>
@endsection