@extends('layout.admin_design')

@section('content')


<div class="content-wrapper">
  <section class="content-header">

    <!-- /start - container-fluid -->

    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6"></div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item">
              <a href="{{url('dashboard')}}">Home</a>
            </li>
            <li class="breadcrumb-item active">Income</li>
          </ol>
        </div>
      </div>
    </div>

    <!-- /End - container-fluid -->

  </section>
  <section class="content">
    <div class="row">
      <div class="col-12">
        <div class="col-md-12">

          <!-- Error Display -->

          @if ($message = Session::get('message'))

          <div class="alert alert-success alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <strong>{{ $message }}</strong>
          </div>
          @endif


          <!-- End Error Display -->

          <!-- Start - Get Value From Databse  -->

          @php
          $amount = !empty($incomes->amount) ? $incomes->amount : old('amount');
          $entry_date = !empty($incomes->entry_date) ? $incomes->entry_date : old('entry_date');
          $remark = !empty($incomes->remark) ? $incomes->remark : old('remark');

          @endphp


          <!--  / End - Get Value From Databse -->

          <div class="card card-secondary">
            <div class="card-header">
              <h3 class="card-title">Edit Income</h3>
            </div>

            <!-- form start -->

            <div class="card-body">
              <form data-toggle="validator" action="{{ route('updateincomes',$incomes->id) }}" role="form" method="POST" class="form-horizontal">
                {{ csrf_field() }}
                {{ method_field('PATCH')}}


                <div class="form-group">
                  <label for="name" class="col-sm-4 control-label">Income Category
                    <span style="color: red;">*</span>
                  </label>
                  <div class="col-sm-4">
                    <select name="income_category_id" class="form-control select2 " style="width: 100%;" data-select2-id="1" tabindex="-1" aria-hidden="true">
                      @foreach ($IncomeCategory as $IncomeCategory)
                      <option value="{{ $IncomeCategory->id }}" @if ($IncomeCategory->id == $incomes->income_category_id) selected @endif>{{ $IncomeCategory->name }}</option>
                      @endforeach
                    </select>
                    @if($errors->has('name'))

                    <span class="help-block">
                      <strong>{{ $errors->first('name') }}</strong>
                    </span> @endif

                  </div>
                </div>


                <div class="form-group">
                  <label for="name" class="col-sm-4 control-label">Entry date
                    <span style="color: red;">*</span>
                  </label>
                  <div class="col-sm-4">
                    <input type="date" class="form-control" value="{{ $entry_date }}" name="entry_date" id="entry_date" placeholder="Date" readonly="">

                    @if($errors->has('entry_date'))

                    <span class="help-block">
                      <strong>{{ $errors->first('entry_date') }}</strong>
                    </span> @endif

                  </div>
                </div>


                <div class="form-group">
                  <label for="name" class="col-sm-4 control-label">Select Account
                    <span style="color: red;">*</span>
                  </label>
                  <div class="col-sm-4">
                    <select name="account_id" class="form-control select2 " style="width: 100%;" data-select2-id="1" tabindex="-1" aria-hidden="true">
                      @foreach ($bankmaster as $IncomeCategory)
                      <option value="{{ $IncomeCategory->bankid }}" @if ($IncomeCategory->bankid == $incomes->account_id) selected @endif>{{ $IncomeCategory->bankname }}</option>
                      @endforeach
                    </select>
                    @if($errors->has('bankname'))

                    <span class="help-block">
                      <strong>{{ $errors->first('bankname') }}</strong>
                    </span> @endif

                  </div>
                </div>



                <div class="form-group">
                  <label for="name" class="col-sm-4 control-label">Amount
                    <span style="color: red;">*</span>
                  </label>
                  <div class="col-sm-4">
                    <input type="text" class="form-control" value="{{ $amount }}" placeholder="Amount" id="amount" name="amount" readonly="">
                    @if($errors->has('amount'))

                    <span class="help-block">
                      <strong>{{ $errors->first('amount') }}</strong>
                    </span> @endif

                  </div>
                </div>


                <div class="form-group">
                  <label for="name" class="col-sm-4 control-label">Remarks
                    <span style="color: red;">*</span>
                  </label>
                  <div class="col-sm-4">
                    <textarea name="remark" class="form-control" placeholder="Remarks">{{$remark}}</textarea>
                    @if($errors->has('remark'))
                    <span class="help-block">
                      <strong>{{ $errors->first('remark') }}</strong>
                    </span>
                    @endif

                  </div>
                </div>



                <div class="form-group">
                  <div class="col-sm-offset-4 col-sm-8">
                    <button name="submit" type="submit" class="btn btn-primary">
                      <span class="glyphicon glyphicon-ok" aria-hidden="true"></span> Update

                    </button>
                    <a href="{{ route('incomes')}}" class="btn btn-warning">Back</a>
                  </div>
                </div>
              </form>
            </div>
          </div>

          <!-- End form  -->

        </div>
      </div>
  </section>
</div>




@endsection