@extends('layout.admin_design')
@section('content')

<style type="text/css">
    .select2-selection {
        padding-bottom: 25px !important;
    }

    .select2-selection {
        max-width: 446px !important;
    }

    .select2-container .select2-container--default .select2-container--above {
        max-width: 300px;
    }

    .select2 {
        width: 100% !important;
    }

    .select2-container--default .select2-selection--single {
        border-radius: 0px !important;
        max-height: 100% !important;
        height: 36px !important;
        border-color: #d2d6de !important;
        max-width: 100%;
        min-width: 100% !important;
    }
</style>
<div class="content-wrapper">
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6"></div>
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item">
                            <a href="{{ url('dashboard') }}">Home</a>
                        </li>
                        <li class="breadcrumb-item active">User</li>
                    </ol>
                </div>
            </div>
        </div>
        <!-- /.container-fluid -->
    </section>
    <section class="content">
        <div class="row">
            <div class="col-12">

                @if ($message = Session::get('error'))

                <div class="alert alert-danger alert-block">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong>{{ $message }}</strong>
                </div>
                @endif

                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Filters</h3>
                        <div class="card-tools"></div>
                    </div>
                    <div class="card-body">
                        <form action="{{ url('passbooksearch') }}" method="post" id="gstform">
                            {{ csrf_field() }}

                            <div class="row">

                                <div class="col-sm-3">
                                    <label>Select Year</label>
                                    <select class="form-control span11 select2" title="Select Year" data-live-search="true" data-selected-text-format="count" data-actions-box="true" data-header="Select Year" required="" id="year" name="year" data-sear value="{{ $year }}">
                                        <option value="">Select year</option>
                                        @for ($i = 2019; $i <= 2030; $i++) <option value="{{ $i }}" @if ($i==$year) selected="" @endif>{{ $i }}</option>
                                            @endfor

                                    </select>
                                </div>
                                <div class="col-sm-3">
                                    <label>Select Month</label>
                                    <select class="form-control span11 select2" title="Select Month" dadta-live-search="true" data-selected-text-format="count" data-actions-box="true" data-header="Select Month" required="" name="month" id="month" placeholder="Month">
                                        <option value="">Select Month</option>
                                        <option value='01' @if ($month=='01' ) selected="" @endif>Janaury</option>
                                        <option value='02' @if ($month=='02' ) selected="" @endif>February</option>
                                        <option value='03' @if ($month=='03' ) selected="" @endif>March</option>
                                        <option value='04' @if ($month=='04' ) selected="" @endif>April</option>
                                        <option value='05' @if ($month=='05' ) selected="" @endif>May</option>
                                        <option value='06' @if ($month=='06' ) selected="" @endif>June</option>
                                        <option value='07' @if ($month=='07' ) selected="" @endif>July</option>
                                        <option value='08' @if ($month=='08' ) selected="" @endif>August</option>
                                        <option value='09' @if ($month=='09' ) selected="" @endif>September</option>
                                        <option value='10' @if ($month=='10' ) selected="" @endif>October</option>
                                        <option value='11' @if ($month=='11' ) selected="" @endif>November</option>
                                        <option value='12' @if ($month=='12' ) selected="" @endif>December</option>
                                    </select>
                                </div>
                                <div class="col-sm-2">
                                    <label>Select Account</label>

                                    <select name="bankmaster" class="form-control select2 " style="width: 100%;" aria-hidden="true" required="">
                                        <option value="">Select Bank</option>

                                        @foreach ($bankmaster as $category)



                                        <option value="{{ $category->bankid }}">{{ $category->bankname }}</option>


                                        @endforeach

                                    </select>
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-sm-2">
                                    <button type="submit" id="submitbutton" name="search" class="btn btn-success">
                                        <i class="fas fa-search"></i> Search
                                    </button>
                                    <a href="{{ url('passbook') }}" class="btn btn-danger">Clear</a>
                                </div>
                            </div>
                            @foreach ($Manage_account as $g)


                            <input type="hidden" name="expensepayment[]" value="{{ $g }}">
                            @endforeach

                    </div>
                </div>
                <div class="card">
                    <div class="card-header">
                        <input type="hidden" name="excel" value="0" id="excel">
                        <button type="button" class="btn btn-warning fa fa-file-excel-o" id="checkpwd" style="float: right; margin-right: 15px;">
                            Excel</button>
                        <!--  <a href="{{ url('expensereport') }}" style="float: right; margin-right: 15px;" class="btn btn-warning fa fa-file-excel-o">Excel</a> -->

                        <h3 class="card-title">All Bank Entry</h3>
                        @if (!empty($oldamountofbank))


                        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; <label> Account Balance</label>&nbsp; &nbsp;
                        &nbsp;

                        <input style="font-weight:bold" type="text" value=" {{ $oldamountofbank }}" readonly="">

                        @endif


                    </div>
                    </form>
                    {{ csrf_field() }}
                    <div class="card-body">
                        <div class="table-responsive">
                            <table id="expensepayment" class="table table-bordered table-striped table-hover">
                                <thead>
                                    <th>Date</th>
                                    <th>PaymentType</th>
                                    <th>Account</th>
                                    <th>Amount</th>
                                    <th>Running Amount </th>
                                    <th>Created By</th>
                                    <th>Category</th>
                                    <th>Remark</th>
                                </thead>
                                <tbody>
                                    @if (!empty($Manage_account))

                                    @foreach ($Manage_account as $key => $Manage_account_data)



                                    <?php if ($Manage_account_data->paymenttype == 'Credit') {
                                        $color = '#00FF00';
                                    } else {
                                        $color = '#FF0000';
                                    } ?>
                                    <tr>
                                        <td>
                                            {{ date('d-m-Y', strtotime($Manage_account_data->entry_date)) }}
                                        </td>
                                        <td>
                                            {{ $Manage_account_data->paymenttype }}
                                        </td>
                                        <td>{{ $Manage_account_data->bankmaster['bankname'] }}
                                        </td>
                                        <td style="background-color:{{ $color }}">{{ $Manage_account_data->amount }}
                                        </td>
                                        <td style="font-weight:bold">
                                            {{ $Manage_account_data->running_amount }}
                                        </td>
                                        <td>
                                            {{ Session::get('logged_firstname') }}
                                            {{ Session::get('logged_lastname') }}
                                        </td>

                                        @if (empty($Manage_account_data->expense_category['name']))


                                        <td>
                                            {{ $Manage_account_data->income_category['name'] }}
                                        </td>
                                        @else

                                        <td>
                                            {{ $Manage_account_data->expense_category['name'] }}
                                        </td>

                                        @endif

                                        <td>{{ $Manage_account_data->remark }}
                                        </td>
                                    </tr>
                                    @endforeach

                                    @else

                                    <tr>
                                        <td colspan="8">No Data Found</td>
                                    </tr>
                                    @endif

                                </tbody>
                            </table>
                            <!--  -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
</div>
</div>
</div>
</section>
</div>
<script type="text/javascript">
    $(document).ready(function() {
        $('#expensepayment').DataTable({
            paginate: true,
            "order": [
                [0, "desc"]
            ]
        });
    });
</script>
<script type="text/javascript">
    $('#checkpwd').on('click', function() {


        $('#excel').val(1);

        $('#gstform').submit();


    });
</script>
<script type="text/javascript">
    $(function() {

        var d = new Date(),

            y = d.getFullYear();

        $('#year option[value="' + y + '"]').prop('selected', true);


        $('.select2').select2()

        //Datemask dd/mm/yyyy

    })
</script>

<script type="text/javascript">
    $('#submitbutton').click(function(e) {
        e.preventDefault();
        $('#excel').val(0);
        $('#gstform').submit();

    });
</script>

@endsection