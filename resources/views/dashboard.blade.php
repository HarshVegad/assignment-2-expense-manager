@extends('layout.admin_design')

@section('content')
<div class="content-wrapper">
    <section class="content-header">
     	 <h1 style="text-decoration: none;"></h1>
    </section>

</div>
<div class="modal fade" id="selectview" tabindex="-1" role="dialog" aria-labelledby="titleymodalLabel"
aria-hidden="true">
'
</div>
<script type="text/javascript">
  function passid(bomid, option) {
      $('#bomid').val(bomid);
      $('#option').val(option);
      // if (option == 'view') {
      //     $('#Label').text('View Bom');
      // } else {
      //     $('#Label').text('Export Bom PDF');
      // }
  
      if (option == 'quotation') {
        $('#Label').text('View Quotation');
    } else if(option == 'bom'){
        $('#Label').text('Export Bom PDF');
    }
  }
  
  function exportpdf(type) {
      let bomid = $('#bomid').val();
      let option = $('#option').val();
      if (option == 'view') {
       // window.open("/viewbompdf/" + bomid + '/' + type, '_blank');
          //  window.location = "/viewbompdf/" + bomid + '/' + type;
           $('<a href="/viewbompdf/'+ bomid + '/' + type + '/' + option +'" target="_blank">External Link</a>')[0].click();

      }
      if (option ==  'export' || option == 'bom' || option == 'quotation') {
       // window.open("/viewbompdf/" + bomid + '/' + type, '_blank');
          //  window.location = "/exportbompdf/" + bomid + '/' + type;
          
           $('<a href="/exportbompdf/'+ bomid + '/' + type + '/' + option +'" target="_blank">External Link</a>')[0].click();
      }
  }
  
</script>
       @endsection
