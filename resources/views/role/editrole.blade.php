@extends('layout.admin_design')

@section('content')
<style type="text/css">
	.help-block{
		color:red;
	}
</style>
<div class="content-wrapper">
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="{{url('dashboard')}}">Home</a></li>
            <li class="breadcrumb-item active">Role</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>
  <section class="content">
    <div class="row">
      <div class="col-12">
        @if ($message = Session::get('message'))
          <div class="alert alert-success alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button> 
            <strong>{{ $message }}</strong>
          </div>
        @endif 
        <div class="col-md-12">
            <!-- jquery validation -->
            <div class="card card-secondary">
              <div class="card-header">
                <h3 class="card-title">Edit Role</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <div class="card-body">
				<form action="{{ route('editrole', $role->roleid) }}" role="form" method="post" class="form-horizontal">
					{{csrf_field()}}
					<div class="form-group">
						<label for="email" class="col-sm-4 control-label">Role<span style="color: red;">*</span></label>
						<div class="col-sm-8">
							<input type="text" class="form-control" value="{{ $role->employeerole }}" placeholder="Enter Role" id="employeerole" name="employeerole" required maxlength="255">
							@if($errors->has('employeerole'))
							<span class="help-block">
								<strong>{{ $errors->first('employeerole') }}</strong>
							</span> @endif

						</div>
					</div>

					<div class="form-group">
						<label for="email" class="col-sm-4 control-label">Description</label>
						<div class="col-sm-8">
							<textarea class="form-control" value="" placeholder="Enter description" id="description" name="description"  maxlength="255">{{ $role->description }}</textarea>
							@if($errors->has('description'))
							<span class="help-block">
								<strong>{{ $errors->first('description') }}</strong>
							</span> @endif

						</div>
					</div>

					<div class="form-group">
						<div class="col-sm-offset-6 col-sm-6">
							<button name="submit" type="submit" class="btn btn-primary">Update Role</button>
							<a href="{{ url('viewrole') }}" type="button" class="btn btn-danger">Cancel</a>
						</div>
					</div>
				</form>
			  </div>    
			</div>
		</div>
	  </div>
	</div>
  </section>
</div>




@endsection