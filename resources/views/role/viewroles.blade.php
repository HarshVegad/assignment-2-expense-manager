@extends('layout.admin_design')

@section('content')
<style type="text/css">
	.content-wrapper{
		padding-right: 15px !important;
		padding-left: 15px !important;
	}
td{
	max-width: 10%;
}.red{
	color: red;
	text-decoration-color: red;
}
.select2{
	width: 100% !important;
	
}
.select2-container--default .select2-selection--single{
	border-radius: 2px !important;
	max-height: 100% !important;
	    border-color: #d2d6de !important;
	        height: 32px;
	        max-width: 100%;
	        min-width: 100% !important;
}
</style>
<div class="content-wrapper">
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="{{url('dashboard')}}">Home</a></li>
            <li class="breadcrumb-item active">Role</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>
  <section class="content">
    <div class="row">
      <div class="col-12">
        @if ($message = Session::get('message'))
          <div class="alert alert-success alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button> 
            <strong>{{ $message }}</strong>
          </div>
        @endif 
        <div class="card">
          <div class="card-header">
            <h3 class="card-title">All Roles</h3>
            <div class="card-tools">
             <a href="{{ url('role') }}" class="btn btn-default">Add New</a>
            </div>
          </div>
          <div class="card-body">
            <table id="example1" class="table table-bordered table-striped table-hover">
              <thead>
                <tr>
                  	<th>Role</th>
                    <th>Status</th>
                    <th>Action</th>
                </tr>
              </thead>
              <tbody>
                  @if(!empty($roles))
                      @foreach($roles as $role)
                      <tr>
                      <td>{{ $role->employeerole }}</td>
                      <td>

                        @if($role->status == 'Active')
                        <span style="color: green">Active</span>
                        @else
                        <span style="color: red">Deactive</span>
                        @endif

                      </td>
                      @if(Session()->get('logged_role') == '1')
                        <td>  <a href="{{ url('editrole/'.$role->roleid) }}"class="edit" title="Edit"><i class="fa fa-edit"></i></a>
                        @if($role->status == 'Active')
                          <a href="{{ url('deactiverole/'.$role->roleid) }}"class="delete" title="Deactive"><i class="fas fa-times"></i></a></td>
                        @else
                          <a href="{{ url('activerole/'.$role->roleid) }}"class="" title="Active" style="color: green;"><i class="fa fa-check"></i></a></td>
                        @endif  
                        </td>
                      @else 
                      <td>{{'-'}}</td>
                      @endif                
                      </tr>
                    @endforeach
                   @else
                   <tr>
                     <td colspan="3">No Roles Found</td>
                   </tr>
                   @endif
              </tbody>
              <tfoot>
              <tr>
               	<th>Role</th>
                <th>Status</th>
                <th>Action</th>
              </tr>
              </tfoot>
            </table>
          </div>
            <!-- /.box-header -->
           
             <!-- /.table-responsive -->
        </div>
      </div>
    </div>
  </section>
</div>
@endsection


