<table>
    <thead>
    <tr>
        <th align="center">Date</th>
        <th align="center">PaymentType</th>
        <th align="center">Account</th>
        <th align="center">Amount</th>
        <th align="center">Running Amount </th>
        <th align="center">Category</th>
        <th align="center">Remark</th>
    </tr>
    </thead>
    <tbody>
         @if (!empty($Manage_account))

     @foreach ($Manage_account as $key => $Manage_account_data)
                                    <tr>
                                        <td>
                                            {{ date('d-m-Y', strtotime($Manage_account_data->entry_date)) }}
                                        </td>
                                        <td>
                                            {{ $Manage_account_data->paymenttype }}
                                        </td>
                                        <td>{{ $Manage_account_data->bankmaster['bankname'] }}
                                        </td>
                                        <td>{{ $Manage_account_data->amount }}
                                        </td>
                                        <td>
                                            {{ $Manage_account_data->running_amount }}
                                        </td>

                                        @if (empty($Manage_account_data->expense_category['name']))


                                        <td>
                                            {{ $Manage_account_data->income_category['name'] }}
                                        </td>
                                        @else

                                        <td>
                                            {{ $Manage_account_data->expense_category['name'] }}
                                        </td>

                                        @endif

                                        <td>{{ $Manage_account_data->remark }}
                                        </td>
                                    </tr>
                                    @endforeach
                                                                        @endif

    </tbody>
</table>