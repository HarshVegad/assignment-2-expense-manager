<table>
    <thead>
    <tr>
        <th align="center">Date</th>
        <th align="center">PaymentType</th>
        <th align="center">Account</th>
        <th align="center">Amount</th>
        <th align="center">Running Amount </th>
        <th align="center">Created</th>
        <th align="center">Category</th>
        <th align="center">Remark</th>
    </tr>
    </thead>
    <tbody>
     @foreach ($alldata as $key => $alldata_data)
                                    <tr>
                                        <td>
                                            {{ date('d-m-Y', strtotime($alldata_data->entry_date)) }}
                                        </td>
                                        <td>
                                            {{ $alldata_data->paymenttype }}
                                        </td>
                                        <td>{{ $alldata_data->bankmaster['bankname'] }}
                                        </td>
                                        <td>{{ $alldata_data->amount }}
                                        </td>
                                        <td>
                                            {{ $alldata_data->running_amount }}
                                        </td>
                                        <td>
                                            {{ Session::get('logged_firstname') }}
                                            {{ Session::get('logged_lastname') }}
                                        </td>

                                        @if (empty($alldata_data->expense_category['name']))


                                        <td>
                                            {{ $alldata_data->income_category['name'] }}
                                        </td>
                                        @else

                                        <td>
                                            {{ $alldata_data->expense_category['name'] }}
                                        </td>

                                        @endif

                                        <td>{{ $alldata_data->remark }}
                                        </td>
                                    </tr>
                                    @endforeach
    </tbody>
</table>