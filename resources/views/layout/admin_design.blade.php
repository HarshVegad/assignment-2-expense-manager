<!DOCTYPE html>
<html>
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <title> Expense Manager</title>
      <!-- Tell the browser to be responsive to screen width -->
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <link rel="icon" href="{{asset('/images/img/favicon.png')}}">
      <!-- Font Awesome -->
      <link rel="stylesheet" href="{{ asset('/plugins/fontawesome-free/css/all.min.css')}}">
      <link rel="stylesheet" type="text/css" href="{{ asset('css/toastr.css') }}">
      <!-- Ionicons -->
      <!-- overlayScrollbars -->
      <link rel="stylesheet" href="{{ asset('/plugins/overlayScrollbars/css/OverlayScrollbars.min.css')}}">
      <!-- Theme style -->
      <link rel="stylesheet" href="{{ asset('/dist/css/adminlte.min.css')}}">
      <!-- Google Font: Source Sans Pro -->
      <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
      <link rel="stylesheet" href="{{ asset('plugins/select2/css/select2.min.css')}}">
      <link rel="stylesheet" href="{{ asset('plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css')}}">
      <link rel="stylesheet" href="https://cdn.datatables.net/1.10.21/css/dataTables.bootstrap4.min.css">
      <script src="{{ asset('/js/jquery-1.11.0.min.js')}}"></script>
      <link rel="stylesheet" href="../../plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">
      <style>
         table {
         line-height: 1;
         }
         .content-header {
         padding: 5px .5rem;
         }
         .btn-primary {
         color: white !important;
         }
         .btn-outline-success:hover {
         color: white !important;
         }
         .btn-outline-success {
         color: #28a745 !important;
         }
         .btn-success {
         color: white !important;
         }
         .btn-warning {
         color: white !important;
         }
         .btn-danger {
         color: white !important;
         }
         .brand-link:hover {
         color: white !important;
         }
         .btn-default:hover {
         background-color: #343a40 !important;
         color: white!important;
         }
         .btn-default {
         color: #343a40 !important;
         }
         .bg-gradient-secondary .btn-tool:hover,
         .bg-secondary .btn-tool:hover,
         .btn-tool,
         .card-secondary:not(.card-outline) .btn-tool:hover {
         color: #adb5bd !important;
         }
         .select2-selection {
         padding-bottom: 30px !important;
         }
         .fa-edit {
         font-weight: 900;
         padding-right: 16px;
         }
         strong {
         color: red;
         }
         .fa-edit {
         color: #f39c12;
         padding-right: 10px;
         }
         .fa-eye {
         padding-right: 10px;
         }
         .fa-print {
         padding-right: 10px;
         }
         .fa-undo {
         padding-right: 10px;
         }
         .fa-lock {
         padding-right: 10px;
         }
         .fa-times {
         color: red;
         padding-right: 10px;
         }
      </style>
   </head>
   <body class="hold-transition sidebar-mini layout-fixed">
      <!-- Site wrapper -->
      <div class="wrapper">
      <!-- Navbar -->
      <nav class="main-header navbar navbar-expand navbar-white navbar-light">
         <!-- Left navbar links -->
         <ul class="navbar-nav">
            <li class="nav-item">
               <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
            </li>
            <li class="nav-item d-none d-sm-inline-block">
               <a href="{{url('dashboard')}}" class="nav-link">Dashboard</a>
            </li>
         </ul>
         <!-- SEARCH FORM -->
         <form class="form-inline ml-3">
            <div class="input-group input-group-sm">
               <input class="form-control form-control-navbar" type="search" placeholder="Search" aria-label="Search">
               <div class="input-group-append">
                  <button class="btn btn-navbar" type="submit">
                  <i class="fas fa-search"></i>
                  </button>
               </div>
            </div>
         </form>
         <!-- Right navbar links -->
         <ul class="navbar-nav ml-auto">
            <!-- Messages Dropdown Menu -->
            <li class="nav-item dropdown d-none">
               <a class="nav-link" data-toggle="dropdown" href="#">
               <i class="far fa-comments"></i>
               <span class="badge badge-danger navbar-badge">3</span>
               </a>
               <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
                  <a href="#" class="dropdown-item">
                     <!-- Message Start -->
                     <div class="media">
                        <img src="../../dist/img/user1-128x128.jpg" alt="User Avatar" class="img-size-50 mr-3 img-circle">
                        <div class="media-body">
                           <h3 class="dropdown-item-title">
                              Brad Diesel
                              <span class="float-right text-sm text-danger"><i class="fas fa-star"></i></span>
                           </h3>
                           <p class="text-sm">Call me whenever you can...</p>
                           <p class="text-sm text-muted"><i class="far fa-clock mr-1"></i> 4 Hours Ago</p>
                        </div>
                     </div>
                     <!-- Message End -->
                  </a>
                  <div class="dropdown-divider"></div>
                  <a href="#" class="dropdown-item">
                     <!-- Message Start -->
                     <div class="media">
                        <img src="../../dist/img/user8-128x128.jpg" alt="User Avatar" class="img-size-50 img-circle mr-3">
                        <div class="media-body">
                           <h3 class="dropdown-item-title">
                              John Pierce
                              <span class="float-right text-sm text-muted"><i class="fas fa-star"></i></span>
                           </h3>
                           <p class="text-sm">I got your message bro</p>
                           <p class="text-sm text-muted"><i class="far fa-clock mr-1"></i> 4 Hours Ago</p>
                        </div>
                     </div>
                     <!-- Message End -->
                  </a>
                  <div class="dropdown-divider"></div>
                  <a href="#" class="dropdown-item">
                     <!-- Message Start -->
                     <div class="media">
                        <img src="../../dist/img/user3-128x128.jpg" alt="User Avatar" class="img-size-50 img-circle mr-3">
                        <div class="media-body">
                           <h3 class="dropdown-item-title">
                              Nora Silvester
                              <span class="float-right text-sm text-warning"><i class="fas fa-star"></i></span>
                           </h3>
                           <p class="text-sm">The subject goes here</p>
                           <p class="text-sm text-muted"><i class="far fa-clock mr-1"></i> 4 Hours Ago</p>
                        </div>
                     </div>
                     <!-- Message End -->
                  </a>
                  <div class="dropdown-divider"></div>
                  <a href="#" class="dropdown-item dropdown-footer">See All Messages</a>
               </div>
            </li>
            <!-- Notifications Dropdown Menu -->
            <li class="nav-item dropdown d-none">
               <a class="nav-link" data-toggle="dropdown" href="#">
               <i class="far fa-bell"></i>
               <span class="badge badge-warning navbar-badge">15</span>
               </a>
               <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
                  <span class="dropdown-item dropdown-header">15 Notifications</span>
                  <div class="dropdown-divider"></div>
                  <a href="#" class="dropdown-item">
                  <i class="fas fa-envelope mr-2"></i> 4 new messages
                  <span class="float-right text-muted text-sm">3 mins</span>
                  </a>
                  <div class="dropdown-divider"></div>
                  <a href="#" class="dropdown-item">
                  <i class="fas fa-users mr-2"></i> 8 friend requests
                  <span class="float-right text-muted text-sm">12 hours</span>
                  </a>
                  <div class="dropdown-divider"></div>
                  <a href="#" class="dropdown-item">
                  <i class="fas fa-file mr-2"></i> 3 new reports
                  <span class="float-right text-muted text-sm">2 days</span>
                  </a>
                  <div class="dropdown-divider"></div>
                  <a href="#" class="dropdown-item dropdown-footer">See All Notifications</a>
               </div>
            </li>
            <li class="nav-item dropdown ">
               <a class="nav-link"  href="{{ url('logout') }}">
               <i class="fas fa-sign-out-alt"></i>
               </a>
            </li>
            <li class="nav-item">
               <a class="nav-link" for="Signout"data-widget="control-sidebar" data-slide="true" href="#" role="button">
               <i class="fas fa-th-large"></i>
               </a>
            </li>
         </ul>
      </nav>
      <!-- /.navbar -->
      <!-- Main Sidebar Container -->
      <aside class="main-sidebar sidebar-dark-primary elevation-4">
         <!-- Brand Logo -->
         <a  class="brand-link">
            <center>         <span class=" font-weight-light"> Expense Manager </span></center>
         </a>
         <!-- Sidebar -->
         <div class="sidebar">
            <!-- Sidebar user (optional) -->
            <div class="user-panel mt-3 pb-3 mb-3 d-flex">
               <div class="image">
                  @if(Session()->get('logged_role') == '1')
                  <img src="{{asset('/dist/img/user2-160x160.jpg')}}" class="img-circle elevation-2" alt="User Image">
                  @else
                  <img src="{{asset('/dist/img/user2-160x160.jpg')}}" class="img-circle elevation-2" alt="User Image">
                  @endif
                  {{-- <img src="../../dist/img/user2-160x160.jpg" class="img-circle elevation-2" alt="User Image"> --}}
               </div>
               <div class="info">
                  <a  class="d-block">
                  {{Session::get('logged_firstname')}} {{Session::get('logged_lastname')}}</a>
               </div>
            </div>
            <!-- Sidebar Menu -->
            <nav class="mt-2">
               <ul class="nav nav-pills  nav-child-indent nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                  <li class="nav-item">
                     <a href="{{url('dashboard')}}" class="nav-link">
                        <i class="nav-icon fas fa-tachometer-alt"></i>
                        <p>
                           Dashboard
                        </p>
                     </a>
                  </li>
                  {{-- Admin --}}
                  @if(Session()->get('logged_role') == '1')
                  <li class="nav-item has-treeview">
                     <a href="#" class="nav-link">
                        <i class="nav-icon fas fa-user-shield"></i>
                        <p>
                           Admin
                           <i class="right fas fa-angle-left"></i>
                        </p>
                     </a>
                     <ul class="nav nav-treeview">
                        <li class="nav-item has-treeview">
                           <a href="#" class="nav-link">
                              <i class="fas fa-user-cog nav-icon"></i>
                              <p>Roles & Users<i class="right fas fa-angle-left"></i></p>
                           </a>
                           <ul class="nav nav-treeview">
                              <li class="nav-item">
                                 <a href="{{url('viewrole')}}" class="nav-link">
                                    <i class="fas fa-user nav-icon"></i>
                                    <p>Roles</p>
                                 </a>
                              </li>
                              <li class="nav-item">
                                 <a href="{{url('users')}}" class="nav-link">
                                    <i class="fas fa-users nav-icon"></i>
                                    <p>Users</p>
                                 </a>
                              </li>
                           </ul>
                        </li>
                     </ul>
                  </li>
                  <li class="nav-item has-treeview">
                           <a href="{{url('bank')}}" class="nav-link">
                              <i class="nav-icon fas fa-users"></i>
                              <p>Account Details</p>
                           </a>
                        </li>
                    <li class="nav-item has-treeview">
                     <a href="#" class="nav-link">
                        <i class="nav-icon fas fa-user-shield"></i>
                        <p>
                           Income Managment
                           <i class="right fas fa-angle-left"></i>
                        </p>
                     </a>
                     <ul class="nav nav-treeview">
                      
                        <li class="nav-item has-treeview">
                           <a href="{{url('income_categories')}}" class="nav-link">
                              <i class="fas fa-list nav-icon"></i>
                              <p>Income Categories</p>
                           </a>
                        </li>
                         <li class="nav-item has-treeview">
                           <a href="{{url('incomes')}}" class="nav-link">
                              <i class=" fas fa-arrow-circle-right nav-icon"></i>
                              <p>Income</p>
                           </a>
                        </li>
                        
                     </ul>
                  </li>
                  <li class="nav-item has-treeview">
                     <a href="#" class="nav-link">
                        <i class="nav-icon fas fa-user-shield"></i>
                        <p>
                           Expense Managment
                           <i class="right fas fa-angle-left"></i>
                        </p>
                     </a>
                     <ul class="nav nav-treeview">
                      
                        <li class="nav-item has-treeview">
                           <a href="{{url('expense_categories')}}" class="nav-link">
                              <i class="fas fa-list nav-icon"></i>
                              <p>Expense Categories</p>
                           </a>
                        </li>
                         <li class="nav-item has-treeview">
                           <a href="{{url('expenses')}}" class="nav-link">
                              <i class=" fas fa-arrow-circle-left nav-icon"></i>
                              <p>Expenses</p>
                           </a>
                        </li>
               
                     </ul>
                  </li>

                    <li class="nav-item has-treeview">
                     <a href="#" class="nav-link">
                        <i class="nav-icon fas fa-user-shield"></i>
                        <p>
                           Monthly Report
                           <i class="right fas fa-angle-left"></i>
                        </p>
                     </a>
                     <ul class="nav nav-treeview">
                      
                        <li class="nav-item has-treeview">
                             <a href="{{url('monthlyreport')}}" class="nav-link">
                              <i class="nav-icon fas fa-users"></i>
                              <p>Total</p>
                           </a>
                        </li>
                         <li class="nav-item has-treeview">
                           <a href="{{url('passbook')}}" class="nav-link">
                              <i class=" fas fa-arrow-circle-right nav-icon"></i>
                              <p> Passbook</p>
                           </a>
                        </li>
                        
                     </ul>
                  </li>
                   
                  @endif
               </ul>
            </nav>
            <!-- /.sidebar-menu -->
         </div>
         <!-- /.sidebar -->
      </aside>
      <!-- Content Wrapper. Contains page content -->
      @yield('content')
      <!-- /.content-wrapper -->
      <footer class="main-footer">
         <div class="float-right d-none d-sm-block">
         </div>
      </footer>
      <!-- Control Sidebar -->
      <aside class="control-sidebar control-sidebar-dark">
         <!-- Control sidebar content goes here -->
      </aside>
      <!-- /.control-sidebar -->
      <!-- ./wrapper -->
      <!-- jQuery -->
      {{-- <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script> --}}
      <script src="{{asset('plugins/jquery/jquery.min.js')}}"></script>
      <!-- Bootstrap 4 -->
      <script src="{{asset('plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
      <script src="{{asset('plugins/select2/js/select2.full.min.js')}}"></script>
      <!-- overlayScrollbars -->
      <script src="{{asset('plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js')}}"></script>
      <!-- AdminLTE App -->
      <script src="{{asset('dist/js/adminlte.min.js')}}"></script>
      <script src="{{ asset('js/toastr.js') }}"></script>
      <!-- AdminLTE for demo purposes -->
      <script src="{{asset('dist/js/demo.js')}}"></script>
      <script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
      <script src="https://cdn.datatables.net/1.10.21/js/dataTables.bootstrap4.min.js"></script>
      <script>
         $(document).ready(function() {
           $('#example1').DataTable({
              paginate: true,
           });
         } );
         $(document).ready(function($){
         var navs = $('nav > ul.nav');
         
         // Add class .active to current link
         navs.find('a').each(function(){
         
         var cUrl = String(window.location).split('?')[0];
         if (cUrl.substr(cUrl.length - 1) === '#') {
           cUrl = cUrl.slice(0,-1);
         }
           if(!$(this)[0].href.includes("#") && $(this)[0].href!=''){
           }
           
         if ($($(this))[0].href===cUrl && $($(this))[0].href !='#') {
           $(this).addClass('active');
         // $(this).parent().addClass('menu-open open');
           $(this).parents('ul').add(this).each(function(){
              
           $(this).parent().addClass('menu-open open');
           });
         }
         });
         });
         $(function () {
         //Initialize Select2 Elements
         $('.select2').select2({
             theme: 'bootstrap4'
         })
         
         //Initialize Select2 Elements
         $('.select2bs4').select2({
           theme: 'bootstrap4'
         })
         
         //Datemask dd/mm/yyyy
         $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
         //Datemask2 mm/dd/yyyy
         $('#datemask2').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' })
         //Money Euro
         $('[data-mask]').inputmask()
         
         //Date range picker
         $('#reservation').daterangepicker()
         //Date range picker with time picker
         $('#reservationtime').daterangepicker({
           timePicker: true,
           timePickerIncrement: 30,
           locale: {
             format: 'MM/DD/YYYY hh:mm A'
           }
         })
         //Date range as a button
         $('#daterange-btn').daterangepicker(
           {
             ranges   : {
               'Today'       : [moment(), moment()],
               'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
               'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
               'Last 30 Days': [moment().subtract(29, 'days'), moment()],
               'This Month'  : [moment().startOf('month'), moment().endOf('month')],
               'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
             },
             startDate: moment().subtract(29, 'days'),
             endDate  : moment()
           },
           function (start, end) {
             $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
           }
         )
         
         //Timepicker
         $('#timepicker').datetimepicker({
           format: 'LT'
         })
         
         //Bootstrap Duallistbox
         $('.duallistbox').bootstrapDualListbox()
         
         //Colorpicker
         $('.my-colorpicker1').colorpicker()
         //color picker with addon
         $('.my-colorpicker2').colorpicker()
         
         $('.my-colorpicker2').on('colorpickerChange', function(event) {
           $('.my-colorpicker2 .fa-square').css('color', event.color.toString());
         });
         
         $("input[data-bootstrap-switch]").each(function(){
           $(this).bootstrapSwitch('state', $(this).prop('checked'));
         });
         
         })
      </script>
      <script type="text/javascript">
         @if(Session::has('message_display'))
           var type = "{{ Session::get('alert-type', 'info') }}";
           switch(type){
               case 'info':
                   toastr.info("{{ Session::get('message_display') }}");
                   break;
         
               case 'warning':
                   toastr.warning("{{ Session::get('message_display') }}");
                   break;
         
               case 'success':
                   toastr.success("{{ Session::get('message_display') }}");
                   break;
         
               case 'error':
                   toastr.error("{{ Session::get('message_display') }}");
                   break;
           }
         @endif
         @php
               
         Session::forget('message');
         
         
         
         @endphp 
      </script>
   </body>
</html>